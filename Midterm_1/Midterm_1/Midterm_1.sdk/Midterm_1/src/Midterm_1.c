#include "xparameters.h"
#include "xgpio.h"
#include "unistd.h" //To import a microsecond sleep

 

/*====================================================
 Mideterm 1

 Written byte Jeremy Blaire
 2/16/2019

 Some code stoled from Lab 2
======================================================*/
int main (void) 
{

    XGpio dip, push, led;
	int psb_check, dip_check, led_check;
	
    xil_printf("-- Start of the Program --\r\n");
 
    XGpio_Initialize(&dip, XPAR_SWITCHES_DEVICE_ID);
	XGpio_SetDataDirection(&dip, 1, 0xffffffff);
	
	XGpio_Initialize(&push, XPAR_BUTTONS_DEVICE_ID);
	XGpio_SetDataDirection(&push, 1, 0xffffffff);
	
	//DIO instance, Channel, Direction Mask. Set only LEDs in channel to outputs
	XGpio_Initialize(&led, XPAR_LEDS_DEVICE_ID);
	XGpio_SetDataDirection(&led, 1, 0xFFFFFFF0);


	while (1)
	{
		psb_check = XGpio_DiscreteRead(&push, 1);
		dip_check = XGpio_DiscreteRead(&dip, 1);

		if(led_check != (dip_check & psb_check))
		{
			xil_printf("DIP Switch Status %x\nPush Buttons Status %x\n", dip_check, psb_check);
		}
	  
		//Both switch and button must be true to light the LED
		led_check = dip_check & psb_check;

		//Write to LED
		XGpio_DiscreteWrite(&led, 1, led_check);

		//Microsecond Sleep
		usleep(200);
	}
 
}
 
