set_property IOSTANDARD LVCMOS33 [get_ports {switches_tri_i[3]}]
set_property IOSTANDARD LVCMOS33 [get_ports {switches_tri_i[2]}]
set_property IOSTANDARD LVCMOS33 [get_ports {switches_tri_i[1]}]
set_property IOSTANDARD LVCMOS33 [get_ports {switches_tri_i[0]}]
