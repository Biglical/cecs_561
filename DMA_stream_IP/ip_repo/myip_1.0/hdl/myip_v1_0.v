
`timescale 1 ns / 1 ps

	module myip_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface DMA_S_AXI
		parameter integer C_DMA_S_AXI_TDATA_WIDTH	= 32
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface DMA_S_AXI
		input wire  dma_s_axi_aclk,
		input wire  dma_s_axi_aresetn,
		output wire  dma_s_axi_tready,
		input wire [C_DMA_S_AXI_TDATA_WIDTH-1 : 0] dma_s_axi_tdata,
		input wire [(C_DMA_S_AXI_TDATA_WIDTH/8)-1 : 0] dma_s_axi_tstrb,
		input wire  dma_s_axi_tlast,
		input wire  dma_s_axi_tvalid
	);
// Instantiation of Axi Bus Interface DMA_S_AXI
	myip_v1_0_DMA_S_AXI # ( 
		.C_S_AXIS_TDATA_WIDTH(C_DMA_S_AXI_TDATA_WIDTH)
	) myip_v1_0_DMA_S_AXI_inst (
		.S_AXIS_ACLK(dma_s_axi_aclk),
		.S_AXIS_ARESETN(dma_s_axi_aresetn),
		.S_AXIS_TREADY(dma_s_axi_tready),
		.S_AXIS_TDATA(dma_s_axi_tdata),
		.S_AXIS_TSTRB(dma_s_axi_tstrb),
		.S_AXIS_TLAST(dma_s_axi_tlast),
		.S_AXIS_TVALID(dma_s_axi_tvalid)
	);

	// Add user logic here

	// User logic ends

	endmodule
