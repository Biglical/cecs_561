// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Sun Apr  7 08:01:54 2019
// Host        : DESKTOP-MC6SQ8S running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ System_mlhdlc_sfir_fixpt_ip_0_0_sim_netlist.v
// Design      : System_mlhdlc_sfir_fixpt_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "System_mlhdlc_sfir_fixpt_ip_0_0,mlhdlc_sfir_fixpt_ipcore,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "package_project" *) 
(* X_CORE_INFO = "mlhdlc_sfir_fixpt_ipcore,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (IPCORE_CLK,
    IPCORE_RESETN,
    AXI4_Stream_Master_TREADY,
    AXI4_Stream_Slave_TDATA,
    AXI4_Stream_Slave_TVALID,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARESETN,
    AXI4_Lite_AWADDR,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WDATA,
    AXI4_Lite_WSTRB,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_ARADDR,
    AXI4_Lite_ARVALID,
    AXI4_Lite_RREADY,
    AXI4_Stream_Master_TDATA,
    AXI4_Stream_Master_TVALID,
    AXI4_Stream_Master_TLAST,
    AXI4_Stream_Slave_TREADY,
    delayed_xout,
    AXI4_Lite_AWREADY,
    AXI4_Lite_WREADY,
    AXI4_Lite_BRESP,
    AXI4_Lite_BVALID,
    AXI4_Lite_ARREADY,
    AXI4_Lite_RDATA,
    AXI4_Lite_RRESP,
    AXI4_Lite_RVALID);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, ASSOCIATED_BUSIF AXI4_Stream_Master:AXI4_Stream_Slave, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input IPCORE_CLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input IPCORE_RESETN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TREADY" *) input AXI4_Stream_Master_TREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TDATA" *) input [15:0]AXI4_Stream_Slave_TDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TVALID" *) input AXI4_Stream_Slave_TVALID;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input AXI4_Lite_ACLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input AXI4_Lite_ARESETN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR" *) input [15:0]AXI4_Lite_AWADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID" *) input AXI4_Lite_AWVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA" *) input [31:0]AXI4_Lite_WDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB" *) input [3:0]AXI4_Lite_WSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID" *) input AXI4_Lite_WVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY" *) input AXI4_Lite_BREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR" *) input [15:0]AXI4_Lite_ARADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID" *) input AXI4_Lite_ARVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY" *) input AXI4_Lite_RREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TDATA" *) output [15:0]AXI4_Stream_Master_TDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TVALID" *) output AXI4_Stream_Master_TVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TLAST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI4_Stream_Master, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) output AXI4_Stream_Master_TLAST;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI4_Stream_Slave, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) output AXI4_Stream_Slave_TREADY;
  output [13:0]delayed_xout;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY" *) output AXI4_Lite_AWREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY" *) output AXI4_Lite_WREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP" *) output [1:0]AXI4_Lite_BRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID" *) output AXI4_Lite_BVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY" *) output AXI4_Lite_ARREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA" *) output [31:0]AXI4_Lite_RDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP" *) output [1:0]AXI4_Lite_RRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) output AXI4_Lite_RVALID;

  wire \<const0> ;
  wire AXI4_Lite_ACLK;
  wire [15:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [15:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire AXI4_Lite_BVALID;
  wire [29:29]\^AXI4_Lite_RDATA ;
  wire AXI4_Lite_RREADY;
  wire AXI4_Lite_RVALID;
  wire [31:0]AXI4_Lite_WDATA;
  wire AXI4_Lite_WREADY;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [15:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire AXI4_Stream_Master_TVALID;
  wire [15:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire IPCORE_CLK;
  wire IPCORE_RESETN;
  wire [13:0]delayed_xout;

  assign AXI4_Lite_BRESP[1] = \<const0> ;
  assign AXI4_Lite_BRESP[0] = \<const0> ;
  assign AXI4_Lite_RDATA[31] = \<const0> ;
  assign AXI4_Lite_RDATA[30] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[29] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[28] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[27] = \<const0> ;
  assign AXI4_Lite_RDATA[26] = \<const0> ;
  assign AXI4_Lite_RDATA[25] = \<const0> ;
  assign AXI4_Lite_RDATA[24] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[23] = \<const0> ;
  assign AXI4_Lite_RDATA[22] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[21] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[20] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[19] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[18] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[17] = \<const0> ;
  assign AXI4_Lite_RDATA[16] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[15] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[14] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[13] = \<const0> ;
  assign AXI4_Lite_RDATA[12] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[11] = \<const0> ;
  assign AXI4_Lite_RDATA[10] = \<const0> ;
  assign AXI4_Lite_RDATA[9] = \<const0> ;
  assign AXI4_Lite_RDATA[8] = \<const0> ;
  assign AXI4_Lite_RDATA[7] = \<const0> ;
  assign AXI4_Lite_RDATA[6] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[5] = \<const0> ;
  assign AXI4_Lite_RDATA[4] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[3] = \<const0> ;
  assign AXI4_Lite_RDATA[2] = \<const0> ;
  assign AXI4_Lite_RDATA[1] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[0] = \<const0> ;
  assign AXI4_Lite_RRESP[1] = \<const0> ;
  assign AXI4_Lite_RRESP[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore inst
       (.AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR[15:2]),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR[15:2]),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_BVALID(AXI4_Lite_BVALID),
        .AXI4_Lite_RDATA(\^AXI4_Lite_RDATA ),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_RVALID(AXI4_Lite_RVALID),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WREADY(AXI4_Lite_WREADY),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),
        .AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .IPCORE_CLK(IPCORE_CLK),
        .IPCORE_RESETN(IPCORE_RESETN),
        .delayed_xout(delayed_xout),
        .out_valid_reg(AXI4_Stream_Master_TVALID));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt
   (delayed_xout,
    p26y_out_add_temp,
    p23m4_mul_temp_0,
    p22m3_mul_temp_0,
    p21m2_mul_temp_0,
    p20m1_mul_temp_0,
    E,
    D,
    IPCORE_CLK,
    AR);
  output [13:0]delayed_xout;
  output [13:0]p26y_out_add_temp;
  input [13:0]p23m4_mul_temp_0;
  input [13:0]p22m3_mul_temp_0;
  input [13:0]p21m2_mul_temp_0;
  input [13:0]p20m1_mul_temp_0;
  input [0:0]E;
  input [13:0]D;
  input IPCORE_CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire [13:0]B;
  wire [13:0]D;
  wire [0:0]E;
  wire IPCORE_CLK;
  wire [13:0]delayed_xout;
  wire [26:13]\^p20m1_mul_temp ;
  wire [13:0]p20m1_mul_temp_0;
  wire p20m1_mul_temp_i_10_n_0;
  wire p20m1_mul_temp_i_11_n_0;
  wire p20m1_mul_temp_i_12_n_0;
  wire p20m1_mul_temp_i_13_n_0;
  wire p20m1_mul_temp_i_14_n_0;
  wire p20m1_mul_temp_i_15_n_0;
  wire p20m1_mul_temp_i_16_n_0;
  wire p20m1_mul_temp_i_17_n_0;
  wire p20m1_mul_temp_i_18_n_0;
  wire p20m1_mul_temp_i_1_n_3;
  wire p20m1_mul_temp_i_1_n_6;
  wire p20m1_mul_temp_i_1_n_7;
  wire p20m1_mul_temp_i_2_n_0;
  wire p20m1_mul_temp_i_2_n_1;
  wire p20m1_mul_temp_i_2_n_2;
  wire p20m1_mul_temp_i_2_n_3;
  wire p20m1_mul_temp_i_2_n_4;
  wire p20m1_mul_temp_i_2_n_5;
  wire p20m1_mul_temp_i_2_n_6;
  wire p20m1_mul_temp_i_2_n_7;
  wire p20m1_mul_temp_i_3_n_0;
  wire p20m1_mul_temp_i_3_n_1;
  wire p20m1_mul_temp_i_3_n_2;
  wire p20m1_mul_temp_i_3_n_3;
  wire p20m1_mul_temp_i_3_n_4;
  wire p20m1_mul_temp_i_3_n_5;
  wire p20m1_mul_temp_i_3_n_6;
  wire p20m1_mul_temp_i_3_n_7;
  wire p20m1_mul_temp_i_4_n_0;
  wire p20m1_mul_temp_i_4_n_1;
  wire p20m1_mul_temp_i_4_n_2;
  wire p20m1_mul_temp_i_4_n_3;
  wire p20m1_mul_temp_i_4_n_4;
  wire p20m1_mul_temp_i_4_n_5;
  wire p20m1_mul_temp_i_4_n_6;
  wire p20m1_mul_temp_i_4_n_7;
  wire p20m1_mul_temp_i_5_n_0;
  wire p20m1_mul_temp_i_6_n_0;
  wire p20m1_mul_temp_i_7_n_0;
  wire p20m1_mul_temp_i_8_n_0;
  wire p20m1_mul_temp_i_9_n_0;
  wire p20m1_mul_temp_n_100;
  wire p20m1_mul_temp_n_101;
  wire p20m1_mul_temp_n_102;
  wire p20m1_mul_temp_n_103;
  wire p20m1_mul_temp_n_104;
  wire p20m1_mul_temp_n_105;
  wire p20m1_mul_temp_n_78;
  wire p20m1_mul_temp_n_93;
  wire p20m1_mul_temp_n_94;
  wire p20m1_mul_temp_n_95;
  wire p20m1_mul_temp_n_96;
  wire p20m1_mul_temp_n_97;
  wire p20m1_mul_temp_n_98;
  wire p20m1_mul_temp_n_99;
  wire [13:0]p21m2_mul_temp_0;
  wire p21m2_mul_temp_i_10_n_0;
  wire p21m2_mul_temp_i_11_n_0;
  wire p21m2_mul_temp_i_12_n_0;
  wire p21m2_mul_temp_i_13_n_0;
  wire p21m2_mul_temp_i_14_n_0;
  wire p21m2_mul_temp_i_15_n_0;
  wire p21m2_mul_temp_i_16_n_0;
  wire p21m2_mul_temp_i_17_n_0;
  wire p21m2_mul_temp_i_18_n_0;
  wire p21m2_mul_temp_i_1_n_3;
  wire p21m2_mul_temp_i_1_n_6;
  wire p21m2_mul_temp_i_1_n_7;
  wire p21m2_mul_temp_i_2_n_0;
  wire p21m2_mul_temp_i_2_n_1;
  wire p21m2_mul_temp_i_2_n_2;
  wire p21m2_mul_temp_i_2_n_3;
  wire p21m2_mul_temp_i_2_n_4;
  wire p21m2_mul_temp_i_2_n_5;
  wire p21m2_mul_temp_i_2_n_6;
  wire p21m2_mul_temp_i_2_n_7;
  wire p21m2_mul_temp_i_3_n_0;
  wire p21m2_mul_temp_i_3_n_1;
  wire p21m2_mul_temp_i_3_n_2;
  wire p21m2_mul_temp_i_3_n_3;
  wire p21m2_mul_temp_i_3_n_4;
  wire p21m2_mul_temp_i_3_n_5;
  wire p21m2_mul_temp_i_3_n_6;
  wire p21m2_mul_temp_i_3_n_7;
  wire p21m2_mul_temp_i_4_n_0;
  wire p21m2_mul_temp_i_4_n_1;
  wire p21m2_mul_temp_i_4_n_2;
  wire p21m2_mul_temp_i_4_n_3;
  wire p21m2_mul_temp_i_4_n_4;
  wire p21m2_mul_temp_i_4_n_5;
  wire p21m2_mul_temp_i_4_n_6;
  wire p21m2_mul_temp_i_4_n_7;
  wire p21m2_mul_temp_i_5_n_0;
  wire p21m2_mul_temp_i_6_n_0;
  wire p21m2_mul_temp_i_7_n_0;
  wire p21m2_mul_temp_i_8_n_0;
  wire p21m2_mul_temp_i_9_n_0;
  wire p21m2_mul_temp_n_100;
  wire p21m2_mul_temp_n_101;
  wire p21m2_mul_temp_n_102;
  wire p21m2_mul_temp_n_103;
  wire p21m2_mul_temp_n_104;
  wire p21m2_mul_temp_n_105;
  wire p21m2_mul_temp_n_78;
  wire p21m2_mul_temp_n_93;
  wire p21m2_mul_temp_n_94;
  wire p21m2_mul_temp_n_95;
  wire p21m2_mul_temp_n_96;
  wire p21m2_mul_temp_n_97;
  wire p21m2_mul_temp_n_98;
  wire p21m2_mul_temp_n_99;
  wire [13:0]p22m3_mul_temp_0;
  wire p22m3_mul_temp_i_10_n_0;
  wire p22m3_mul_temp_i_11_n_0;
  wire p22m3_mul_temp_i_12_n_0;
  wire p22m3_mul_temp_i_13_n_0;
  wire p22m3_mul_temp_i_14_n_0;
  wire p22m3_mul_temp_i_15_n_0;
  wire p22m3_mul_temp_i_16_n_0;
  wire p22m3_mul_temp_i_17_n_0;
  wire p22m3_mul_temp_i_18_n_0;
  wire p22m3_mul_temp_i_1_n_3;
  wire p22m3_mul_temp_i_1_n_6;
  wire p22m3_mul_temp_i_1_n_7;
  wire p22m3_mul_temp_i_2_n_0;
  wire p22m3_mul_temp_i_2_n_1;
  wire p22m3_mul_temp_i_2_n_2;
  wire p22m3_mul_temp_i_2_n_3;
  wire p22m3_mul_temp_i_2_n_4;
  wire p22m3_mul_temp_i_2_n_5;
  wire p22m3_mul_temp_i_2_n_6;
  wire p22m3_mul_temp_i_2_n_7;
  wire p22m3_mul_temp_i_3_n_0;
  wire p22m3_mul_temp_i_3_n_1;
  wire p22m3_mul_temp_i_3_n_2;
  wire p22m3_mul_temp_i_3_n_3;
  wire p22m3_mul_temp_i_3_n_4;
  wire p22m3_mul_temp_i_3_n_5;
  wire p22m3_mul_temp_i_3_n_6;
  wire p22m3_mul_temp_i_3_n_7;
  wire p22m3_mul_temp_i_4_n_0;
  wire p22m3_mul_temp_i_4_n_1;
  wire p22m3_mul_temp_i_4_n_2;
  wire p22m3_mul_temp_i_4_n_3;
  wire p22m3_mul_temp_i_4_n_4;
  wire p22m3_mul_temp_i_4_n_5;
  wire p22m3_mul_temp_i_4_n_6;
  wire p22m3_mul_temp_i_4_n_7;
  wire p22m3_mul_temp_i_5_n_0;
  wire p22m3_mul_temp_i_6_n_0;
  wire p22m3_mul_temp_i_7_n_0;
  wire p22m3_mul_temp_i_8_n_0;
  wire p22m3_mul_temp_i_9_n_0;
  wire p22m3_mul_temp_n_100;
  wire p22m3_mul_temp_n_101;
  wire p22m3_mul_temp_n_102;
  wire p22m3_mul_temp_n_103;
  wire p22m3_mul_temp_n_104;
  wire p22m3_mul_temp_n_105;
  wire p22m3_mul_temp_n_77;
  wire p22m3_mul_temp_n_92;
  wire p22m3_mul_temp_n_93;
  wire p22m3_mul_temp_n_94;
  wire p22m3_mul_temp_n_95;
  wire p22m3_mul_temp_n_96;
  wire p22m3_mul_temp_n_97;
  wire p22m3_mul_temp_n_98;
  wire p22m3_mul_temp_n_99;
  wire [26:13]\^p23m4_mul_temp ;
  wire [13:0]p23m4_mul_temp_0;
  wire p23m4_mul_temp_i_10_n_0;
  wire p23m4_mul_temp_i_11_n_0;
  wire p23m4_mul_temp_i_12_n_0;
  wire p23m4_mul_temp_i_13_n_0;
  wire p23m4_mul_temp_i_14_n_0;
  wire p23m4_mul_temp_i_15_n_0;
  wire p23m4_mul_temp_i_16_n_0;
  wire p23m4_mul_temp_i_17_n_0;
  wire p23m4_mul_temp_i_18_n_0;
  wire p23m4_mul_temp_i_19_n_0;
  wire p23m4_mul_temp_i_1_n_2;
  wire p23m4_mul_temp_i_1_n_3;
  wire p23m4_mul_temp_i_2_n_0;
  wire p23m4_mul_temp_i_2_n_1;
  wire p23m4_mul_temp_i_2_n_2;
  wire p23m4_mul_temp_i_2_n_3;
  wire p23m4_mul_temp_i_3_n_0;
  wire p23m4_mul_temp_i_3_n_1;
  wire p23m4_mul_temp_i_3_n_2;
  wire p23m4_mul_temp_i_3_n_3;
  wire p23m4_mul_temp_i_4_n_0;
  wire p23m4_mul_temp_i_4_n_1;
  wire p23m4_mul_temp_i_4_n_2;
  wire p23m4_mul_temp_i_4_n_3;
  wire p23m4_mul_temp_i_5_n_0;
  wire p23m4_mul_temp_i_6_n_0;
  wire p23m4_mul_temp_i_7_n_0;
  wire p23m4_mul_temp_i_8_n_0;
  wire p23m4_mul_temp_i_9_n_0;
  wire p23m4_mul_temp_n_100;
  wire p23m4_mul_temp_n_101;
  wire p23m4_mul_temp_n_102;
  wire p23m4_mul_temp_n_103;
  wire p23m4_mul_temp_n_104;
  wire p23m4_mul_temp_n_105;
  wire p23m4_mul_temp_n_77;
  wire p23m4_mul_temp_n_78;
  wire p23m4_mul_temp_n_93;
  wire p23m4_mul_temp_n_94;
  wire p23m4_mul_temp_n_95;
  wire p23m4_mul_temp_n_96;
  wire p23m4_mul_temp_n_97;
  wire p23m4_mul_temp_n_98;
  wire p23m4_mul_temp_n_99;
  wire [13:0]p24a5_add_cast_1;
  wire p24a5_add_temp_carry__0_i_1_n_0;
  wire p24a5_add_temp_carry__0_i_2_n_0;
  wire p24a5_add_temp_carry__0_i_3_n_0;
  wire p24a5_add_temp_carry__0_i_4_n_0;
  wire p24a5_add_temp_carry__0_n_0;
  wire p24a5_add_temp_carry__0_n_1;
  wire p24a5_add_temp_carry__0_n_2;
  wire p24a5_add_temp_carry__0_n_3;
  wire p24a5_add_temp_carry__1_i_1_n_0;
  wire p24a5_add_temp_carry__1_i_2_n_0;
  wire p24a5_add_temp_carry__1_i_3_n_0;
  wire p24a5_add_temp_carry__1_i_4_n_0;
  wire p24a5_add_temp_carry__1_n_0;
  wire p24a5_add_temp_carry__1_n_1;
  wire p24a5_add_temp_carry__1_n_2;
  wire p24a5_add_temp_carry__1_n_3;
  wire p24a5_add_temp_carry__2_i_1_n_0;
  wire p24a5_add_temp_carry__2_i_2_n_0;
  wire p24a5_add_temp_carry__2_i_3_n_0;
  wire p24a5_add_temp_carry__2_n_2;
  wire p24a5_add_temp_carry__2_n_3;
  wire p24a5_add_temp_carry_i_1_n_0;
  wire p24a5_add_temp_carry_i_2_n_0;
  wire p24a5_add_temp_carry_i_3_n_0;
  wire p24a5_add_temp_carry_n_0;
  wire p24a5_add_temp_carry_n_1;
  wire p24a5_add_temp_carry_n_2;
  wire p24a5_add_temp_carry_n_3;
  wire [13:0]p25a6_add_cast;
  wire [14:2]p25a6_add_temp;
  wire p25a6_add_temp_carry__0_i_1_n_0;
  wire p25a6_add_temp_carry__0_i_2_n_0;
  wire p25a6_add_temp_carry__0_i_3_n_0;
  wire p25a6_add_temp_carry__0_i_4_n_0;
  wire p25a6_add_temp_carry__0_n_0;
  wire p25a6_add_temp_carry__0_n_1;
  wire p25a6_add_temp_carry__0_n_2;
  wire p25a6_add_temp_carry__0_n_3;
  wire p25a6_add_temp_carry__1_i_1_n_0;
  wire p25a6_add_temp_carry__1_i_2_n_0;
  wire p25a6_add_temp_carry__1_i_3_n_0;
  wire p25a6_add_temp_carry__1_i_4_n_0;
  wire p25a6_add_temp_carry__1_n_0;
  wire p25a6_add_temp_carry__1_n_1;
  wire p25a6_add_temp_carry__1_n_2;
  wire p25a6_add_temp_carry__1_n_3;
  wire p25a6_add_temp_carry__2_i_1_n_0;
  wire p25a6_add_temp_carry__2_i_2_n_0;
  wire p25a6_add_temp_carry__2_i_3_n_0;
  wire p25a6_add_temp_carry__2_i_4_n_0;
  wire p25a6_add_temp_carry__2_n_0;
  wire p25a6_add_temp_carry__2_n_2;
  wire p25a6_add_temp_carry__2_n_3;
  wire p25a6_add_temp_carry_i_1_n_0;
  wire p25a6_add_temp_carry_i_2_n_0;
  wire p25a6_add_temp_carry_i_3_n_0;
  wire p25a6_add_temp_carry_n_0;
  wire p25a6_add_temp_carry_n_1;
  wire p25a6_add_temp_carry_n_2;
  wire p25a6_add_temp_carry_n_3;
  wire [13:1]p26y_out_add_cast;
  wire [13:0]p26y_out_add_temp;
  wire p26y_out_add_temp_carry__0_i_1_n_0;
  wire p26y_out_add_temp_carry__0_i_2_n_0;
  wire p26y_out_add_temp_carry__0_i_3_n_0;
  wire p26y_out_add_temp_carry__0_i_4_n_0;
  wire p26y_out_add_temp_carry__0_n_0;
  wire p26y_out_add_temp_carry__0_n_1;
  wire p26y_out_add_temp_carry__0_n_2;
  wire p26y_out_add_temp_carry__0_n_3;
  wire p26y_out_add_temp_carry__1_i_1_n_0;
  wire p26y_out_add_temp_carry__1_i_2_n_0;
  wire p26y_out_add_temp_carry__1_i_3_n_0;
  wire p26y_out_add_temp_carry__1_i_4_n_0;
  wire p26y_out_add_temp_carry__1_n_0;
  wire p26y_out_add_temp_carry__1_n_1;
  wire p26y_out_add_temp_carry__1_n_2;
  wire p26y_out_add_temp_carry__1_n_3;
  wire p26y_out_add_temp_carry__2_i_1_n_0;
  wire p26y_out_add_temp_carry__2_i_2_n_0;
  wire p26y_out_add_temp_carry__2_i_3_n_0;
  wire p26y_out_add_temp_carry__2_i_4_n_0;
  wire p26y_out_add_temp_carry__2_n_2;
  wire p26y_out_add_temp_carry__2_n_3;
  wire p26y_out_add_temp_carry_i_1_n_0;
  wire p26y_out_add_temp_carry_i_2_n_0;
  wire p26y_out_add_temp_carry_i_3_n_0;
  wire p26y_out_add_temp_carry_n_0;
  wire p26y_out_add_temp_carry_n_1;
  wire p26y_out_add_temp_carry_n_2;
  wire p26y_out_add_temp_carry_n_3;
  wire [13:0]ud1;
  wire [13:0]ud2;
  wire [13:0]ud3;
  wire [13:0]ud4;
  wire [13:0]ud5;
  wire [13:0]ud6;
  wire [13:0]ud7;
  wire NLW_p20m1_mul_temp_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p20m1_mul_temp_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p20m1_mul_temp_OVERFLOW_UNCONNECTED;
  wire NLW_p20m1_mul_temp_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p20m1_mul_temp_PATTERNDETECT_UNCONNECTED;
  wire NLW_p20m1_mul_temp_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p20m1_mul_temp_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p20m1_mul_temp_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p20m1_mul_temp_CARRYOUT_UNCONNECTED;
  wire [47:28]NLW_p20m1_mul_temp_P_UNCONNECTED;
  wire [47:0]NLW_p20m1_mul_temp_PCOUT_UNCONNECTED;
  wire [3:1]NLW_p20m1_mul_temp_i_1_CO_UNCONNECTED;
  wire [3:2]NLW_p20m1_mul_temp_i_1_O_UNCONNECTED;
  wire NLW_p21m2_mul_temp_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p21m2_mul_temp_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p21m2_mul_temp_OVERFLOW_UNCONNECTED;
  wire NLW_p21m2_mul_temp_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p21m2_mul_temp_PATTERNDETECT_UNCONNECTED;
  wire NLW_p21m2_mul_temp_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p21m2_mul_temp_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p21m2_mul_temp_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p21m2_mul_temp_CARRYOUT_UNCONNECTED;
  wire [47:28]NLW_p21m2_mul_temp_P_UNCONNECTED;
  wire [47:0]NLW_p21m2_mul_temp_PCOUT_UNCONNECTED;
  wire [3:1]NLW_p21m2_mul_temp_i_1_CO_UNCONNECTED;
  wire [3:2]NLW_p21m2_mul_temp_i_1_O_UNCONNECTED;
  wire NLW_p22m3_mul_temp_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p22m3_mul_temp_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p22m3_mul_temp_OVERFLOW_UNCONNECTED;
  wire NLW_p22m3_mul_temp_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p22m3_mul_temp_PATTERNDETECT_UNCONNECTED;
  wire NLW_p22m3_mul_temp_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p22m3_mul_temp_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p22m3_mul_temp_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p22m3_mul_temp_CARRYOUT_UNCONNECTED;
  wire [47:29]NLW_p22m3_mul_temp_P_UNCONNECTED;
  wire [47:0]NLW_p22m3_mul_temp_PCOUT_UNCONNECTED;
  wire [3:1]NLW_p22m3_mul_temp_i_1_CO_UNCONNECTED;
  wire [3:2]NLW_p22m3_mul_temp_i_1_O_UNCONNECTED;
  wire NLW_p23m4_mul_temp_CARRYCASCOUT_UNCONNECTED;
  wire NLW_p23m4_mul_temp_MULTSIGNOUT_UNCONNECTED;
  wire NLW_p23m4_mul_temp_OVERFLOW_UNCONNECTED;
  wire NLW_p23m4_mul_temp_PATTERNBDETECT_UNCONNECTED;
  wire NLW_p23m4_mul_temp_PATTERNDETECT_UNCONNECTED;
  wire NLW_p23m4_mul_temp_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_p23m4_mul_temp_ACOUT_UNCONNECTED;
  wire [17:0]NLW_p23m4_mul_temp_BCOUT_UNCONNECTED;
  wire [3:0]NLW_p23m4_mul_temp_CARRYOUT_UNCONNECTED;
  wire [47:29]NLW_p23m4_mul_temp_P_UNCONNECTED;
  wire [47:0]NLW_p23m4_mul_temp_PCOUT_UNCONNECTED;
  wire [3:2]NLW_p23m4_mul_temp_i_1_CO_UNCONNECTED;
  wire [3:3]NLW_p23m4_mul_temp_i_1_O_UNCONNECTED;
  wire [0:0]NLW_p23m4_mul_temp_i_4_O_UNCONNECTED;
  wire [1:0]NLW_p24a5_add_temp_carry_O_UNCONNECTED;
  wire [3:2]NLW_p24a5_add_temp_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_p24a5_add_temp_carry__2_O_UNCONNECTED;
  wire [1:0]NLW_p25a6_add_temp_carry_O_UNCONNECTED;
  wire [2:2]NLW_p25a6_add_temp_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_p25a6_add_temp_carry__2_O_UNCONNECTED;
  wire [0:0]NLW_p26y_out_add_temp_carry_O_UNCONNECTED;
  wire [3:2]NLW_p26y_out_add_temp_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_p26y_out_add_temp_carry__2_O_UNCONNECTED;

  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p20m1_mul_temp
       (.A({p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0[13],p20m1_mul_temp_0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_p20m1_mul_temp_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({p20m1_mul_temp_i_1_n_6,p20m1_mul_temp_i_1_n_6,p20m1_mul_temp_i_1_n_6,p20m1_mul_temp_i_1_n_6,p20m1_mul_temp_i_1_n_6,p20m1_mul_temp_i_1_n_7,p20m1_mul_temp_i_2_n_4,p20m1_mul_temp_i_2_n_5,p20m1_mul_temp_i_2_n_6,p20m1_mul_temp_i_2_n_7,p20m1_mul_temp_i_3_n_4,p20m1_mul_temp_i_3_n_5,p20m1_mul_temp_i_3_n_6,p20m1_mul_temp_i_3_n_7,p20m1_mul_temp_i_4_n_4,p20m1_mul_temp_i_4_n_5,p20m1_mul_temp_i_4_n_6,p20m1_mul_temp_i_4_n_7}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p20m1_mul_temp_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p20m1_mul_temp_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p20m1_mul_temp_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p20m1_mul_temp_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p20m1_mul_temp_OVERFLOW_UNCONNECTED),
        .P({NLW_p20m1_mul_temp_P_UNCONNECTED[47:28],p20m1_mul_temp_n_78,\^p20m1_mul_temp ,p20m1_mul_temp_n_93,p20m1_mul_temp_n_94,p20m1_mul_temp_n_95,p20m1_mul_temp_n_96,p20m1_mul_temp_n_97,p20m1_mul_temp_n_98,p20m1_mul_temp_n_99,p20m1_mul_temp_n_100,p20m1_mul_temp_n_101,p20m1_mul_temp_n_102,p20m1_mul_temp_n_103,p20m1_mul_temp_n_104,p20m1_mul_temp_n_105}),
        .PATTERNBDETECT(NLW_p20m1_mul_temp_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p20m1_mul_temp_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_p20m1_mul_temp_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p20m1_mul_temp_UNDERFLOW_UNCONNECTED));
  CARRY4 p20m1_mul_temp_i_1
       (.CI(p20m1_mul_temp_i_2_n_0),
        .CO({NLW_p20m1_mul_temp_i_1_CO_UNCONNECTED[3:1],p20m1_mul_temp_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,ud1[12]}),
        .O({NLW_p20m1_mul_temp_i_1_O_UNCONNECTED[3:2],p20m1_mul_temp_i_1_n_6,p20m1_mul_temp_i_1_n_7}),
        .S({1'b0,1'b0,p20m1_mul_temp_i_5_n_0,p20m1_mul_temp_i_6_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_10
       (.I0(ud1[8]),
        .I1(delayed_xout[8]),
        .O(p20m1_mul_temp_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_11
       (.I0(ud1[7]),
        .I1(delayed_xout[7]),
        .O(p20m1_mul_temp_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_12
       (.I0(ud1[6]),
        .I1(delayed_xout[6]),
        .O(p20m1_mul_temp_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_13
       (.I0(ud1[5]),
        .I1(delayed_xout[5]),
        .O(p20m1_mul_temp_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_14
       (.I0(ud1[4]),
        .I1(delayed_xout[4]),
        .O(p20m1_mul_temp_i_14_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_15
       (.I0(ud1[3]),
        .I1(delayed_xout[3]),
        .O(p20m1_mul_temp_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_16
       (.I0(ud1[2]),
        .I1(delayed_xout[2]),
        .O(p20m1_mul_temp_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_17
       (.I0(ud1[1]),
        .I1(delayed_xout[1]),
        .O(p20m1_mul_temp_i_17_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_18
       (.I0(ud1[0]),
        .I1(delayed_xout[0]),
        .O(p20m1_mul_temp_i_18_n_0));
  CARRY4 p20m1_mul_temp_i_2
       (.CI(p20m1_mul_temp_i_3_n_0),
        .CO({p20m1_mul_temp_i_2_n_0,p20m1_mul_temp_i_2_n_1,p20m1_mul_temp_i_2_n_2,p20m1_mul_temp_i_2_n_3}),
        .CYINIT(1'b0),
        .DI(ud1[11:8]),
        .O({p20m1_mul_temp_i_2_n_4,p20m1_mul_temp_i_2_n_5,p20m1_mul_temp_i_2_n_6,p20m1_mul_temp_i_2_n_7}),
        .S({p20m1_mul_temp_i_7_n_0,p20m1_mul_temp_i_8_n_0,p20m1_mul_temp_i_9_n_0,p20m1_mul_temp_i_10_n_0}));
  CARRY4 p20m1_mul_temp_i_3
       (.CI(p20m1_mul_temp_i_4_n_0),
        .CO({p20m1_mul_temp_i_3_n_0,p20m1_mul_temp_i_3_n_1,p20m1_mul_temp_i_3_n_2,p20m1_mul_temp_i_3_n_3}),
        .CYINIT(1'b0),
        .DI(ud1[7:4]),
        .O({p20m1_mul_temp_i_3_n_4,p20m1_mul_temp_i_3_n_5,p20m1_mul_temp_i_3_n_6,p20m1_mul_temp_i_3_n_7}),
        .S({p20m1_mul_temp_i_11_n_0,p20m1_mul_temp_i_12_n_0,p20m1_mul_temp_i_13_n_0,p20m1_mul_temp_i_14_n_0}));
  CARRY4 p20m1_mul_temp_i_4
       (.CI(1'b0),
        .CO({p20m1_mul_temp_i_4_n_0,p20m1_mul_temp_i_4_n_1,p20m1_mul_temp_i_4_n_2,p20m1_mul_temp_i_4_n_3}),
        .CYINIT(1'b0),
        .DI(ud1[3:0]),
        .O({p20m1_mul_temp_i_4_n_4,p20m1_mul_temp_i_4_n_5,p20m1_mul_temp_i_4_n_6,p20m1_mul_temp_i_4_n_7}),
        .S({p20m1_mul_temp_i_15_n_0,p20m1_mul_temp_i_16_n_0,p20m1_mul_temp_i_17_n_0,p20m1_mul_temp_i_18_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_5
       (.I0(ud1[13]),
        .I1(delayed_xout[13]),
        .O(p20m1_mul_temp_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_6
       (.I0(ud1[12]),
        .I1(delayed_xout[12]),
        .O(p20m1_mul_temp_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_7
       (.I0(ud1[11]),
        .I1(delayed_xout[11]),
        .O(p20m1_mul_temp_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_8
       (.I0(ud1[10]),
        .I1(delayed_xout[10]),
        .O(p20m1_mul_temp_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p20m1_mul_temp_i_9
       (.I0(ud1[9]),
        .I1(delayed_xout[9]),
        .O(p20m1_mul_temp_i_9_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p21m2_mul_temp
       (.A({p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0[13],p21m2_mul_temp_0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_p21m2_mul_temp_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({p21m2_mul_temp_i_1_n_6,p21m2_mul_temp_i_1_n_6,p21m2_mul_temp_i_1_n_6,p21m2_mul_temp_i_1_n_6,p21m2_mul_temp_i_1_n_6,p21m2_mul_temp_i_1_n_7,p21m2_mul_temp_i_2_n_4,p21m2_mul_temp_i_2_n_5,p21m2_mul_temp_i_2_n_6,p21m2_mul_temp_i_2_n_7,p21m2_mul_temp_i_3_n_4,p21m2_mul_temp_i_3_n_5,p21m2_mul_temp_i_3_n_6,p21m2_mul_temp_i_3_n_7,p21m2_mul_temp_i_4_n_4,p21m2_mul_temp_i_4_n_5,p21m2_mul_temp_i_4_n_6,p21m2_mul_temp_i_4_n_7}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p21m2_mul_temp_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p21m2_mul_temp_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p21m2_mul_temp_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p21m2_mul_temp_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p21m2_mul_temp_OVERFLOW_UNCONNECTED),
        .P({NLW_p21m2_mul_temp_P_UNCONNECTED[47:28],p21m2_mul_temp_n_78,p24a5_add_cast_1,p21m2_mul_temp_n_93,p21m2_mul_temp_n_94,p21m2_mul_temp_n_95,p21m2_mul_temp_n_96,p21m2_mul_temp_n_97,p21m2_mul_temp_n_98,p21m2_mul_temp_n_99,p21m2_mul_temp_n_100,p21m2_mul_temp_n_101,p21m2_mul_temp_n_102,p21m2_mul_temp_n_103,p21m2_mul_temp_n_104,p21m2_mul_temp_n_105}),
        .PATTERNBDETECT(NLW_p21m2_mul_temp_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p21m2_mul_temp_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_p21m2_mul_temp_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p21m2_mul_temp_UNDERFLOW_UNCONNECTED));
  CARRY4 p21m2_mul_temp_i_1
       (.CI(p21m2_mul_temp_i_2_n_0),
        .CO({NLW_p21m2_mul_temp_i_1_CO_UNCONNECTED[3:1],p21m2_mul_temp_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,ud2[12]}),
        .O({NLW_p21m2_mul_temp_i_1_O_UNCONNECTED[3:2],p21m2_mul_temp_i_1_n_6,p21m2_mul_temp_i_1_n_7}),
        .S({1'b0,1'b0,p21m2_mul_temp_i_5_n_0,p21m2_mul_temp_i_6_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_10
       (.I0(ud2[8]),
        .I1(ud7[8]),
        .O(p21m2_mul_temp_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_11
       (.I0(ud2[7]),
        .I1(ud7[7]),
        .O(p21m2_mul_temp_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_12
       (.I0(ud2[6]),
        .I1(ud7[6]),
        .O(p21m2_mul_temp_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_13
       (.I0(ud2[5]),
        .I1(ud7[5]),
        .O(p21m2_mul_temp_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_14
       (.I0(ud2[4]),
        .I1(ud7[4]),
        .O(p21m2_mul_temp_i_14_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_15
       (.I0(ud2[3]),
        .I1(ud7[3]),
        .O(p21m2_mul_temp_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_16
       (.I0(ud2[2]),
        .I1(ud7[2]),
        .O(p21m2_mul_temp_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_17
       (.I0(ud2[1]),
        .I1(ud7[1]),
        .O(p21m2_mul_temp_i_17_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_18
       (.I0(ud2[0]),
        .I1(ud7[0]),
        .O(p21m2_mul_temp_i_18_n_0));
  CARRY4 p21m2_mul_temp_i_2
       (.CI(p21m2_mul_temp_i_3_n_0),
        .CO({p21m2_mul_temp_i_2_n_0,p21m2_mul_temp_i_2_n_1,p21m2_mul_temp_i_2_n_2,p21m2_mul_temp_i_2_n_3}),
        .CYINIT(1'b0),
        .DI(ud2[11:8]),
        .O({p21m2_mul_temp_i_2_n_4,p21m2_mul_temp_i_2_n_5,p21m2_mul_temp_i_2_n_6,p21m2_mul_temp_i_2_n_7}),
        .S({p21m2_mul_temp_i_7_n_0,p21m2_mul_temp_i_8_n_0,p21m2_mul_temp_i_9_n_0,p21m2_mul_temp_i_10_n_0}));
  CARRY4 p21m2_mul_temp_i_3
       (.CI(p21m2_mul_temp_i_4_n_0),
        .CO({p21m2_mul_temp_i_3_n_0,p21m2_mul_temp_i_3_n_1,p21m2_mul_temp_i_3_n_2,p21m2_mul_temp_i_3_n_3}),
        .CYINIT(1'b0),
        .DI(ud2[7:4]),
        .O({p21m2_mul_temp_i_3_n_4,p21m2_mul_temp_i_3_n_5,p21m2_mul_temp_i_3_n_6,p21m2_mul_temp_i_3_n_7}),
        .S({p21m2_mul_temp_i_11_n_0,p21m2_mul_temp_i_12_n_0,p21m2_mul_temp_i_13_n_0,p21m2_mul_temp_i_14_n_0}));
  CARRY4 p21m2_mul_temp_i_4
       (.CI(1'b0),
        .CO({p21m2_mul_temp_i_4_n_0,p21m2_mul_temp_i_4_n_1,p21m2_mul_temp_i_4_n_2,p21m2_mul_temp_i_4_n_3}),
        .CYINIT(1'b0),
        .DI(ud2[3:0]),
        .O({p21m2_mul_temp_i_4_n_4,p21m2_mul_temp_i_4_n_5,p21m2_mul_temp_i_4_n_6,p21m2_mul_temp_i_4_n_7}),
        .S({p21m2_mul_temp_i_15_n_0,p21m2_mul_temp_i_16_n_0,p21m2_mul_temp_i_17_n_0,p21m2_mul_temp_i_18_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_5
       (.I0(ud2[13]),
        .I1(ud7[13]),
        .O(p21m2_mul_temp_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_6
       (.I0(ud2[12]),
        .I1(ud7[12]),
        .O(p21m2_mul_temp_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_7
       (.I0(ud2[11]),
        .I1(ud7[11]),
        .O(p21m2_mul_temp_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_8
       (.I0(ud2[10]),
        .I1(ud7[10]),
        .O(p21m2_mul_temp_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p21m2_mul_temp_i_9
       (.I0(ud2[9]),
        .I1(ud7[9]),
        .O(p21m2_mul_temp_i_9_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p22m3_mul_temp
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p22m3_mul_temp_0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_p22m3_mul_temp_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({p22m3_mul_temp_i_1_n_6,p22m3_mul_temp_i_1_n_6,p22m3_mul_temp_i_1_n_6,p22m3_mul_temp_i_1_n_6,p22m3_mul_temp_i_1_n_6,p22m3_mul_temp_i_1_n_7,p22m3_mul_temp_i_2_n_4,p22m3_mul_temp_i_2_n_5,p22m3_mul_temp_i_2_n_6,p22m3_mul_temp_i_2_n_7,p22m3_mul_temp_i_3_n_4,p22m3_mul_temp_i_3_n_5,p22m3_mul_temp_i_3_n_6,p22m3_mul_temp_i_3_n_7,p22m3_mul_temp_i_4_n_4,p22m3_mul_temp_i_4_n_5,p22m3_mul_temp_i_4_n_6,p22m3_mul_temp_i_4_n_7}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p22m3_mul_temp_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p22m3_mul_temp_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p22m3_mul_temp_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p22m3_mul_temp_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p22m3_mul_temp_OVERFLOW_UNCONNECTED),
        .P({NLW_p22m3_mul_temp_P_UNCONNECTED[47:29],p22m3_mul_temp_n_77,p25a6_add_cast,p22m3_mul_temp_n_92,p22m3_mul_temp_n_93,p22m3_mul_temp_n_94,p22m3_mul_temp_n_95,p22m3_mul_temp_n_96,p22m3_mul_temp_n_97,p22m3_mul_temp_n_98,p22m3_mul_temp_n_99,p22m3_mul_temp_n_100,p22m3_mul_temp_n_101,p22m3_mul_temp_n_102,p22m3_mul_temp_n_103,p22m3_mul_temp_n_104,p22m3_mul_temp_n_105}),
        .PATTERNBDETECT(NLW_p22m3_mul_temp_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p22m3_mul_temp_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_p22m3_mul_temp_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p22m3_mul_temp_UNDERFLOW_UNCONNECTED));
  CARRY4 p22m3_mul_temp_i_1
       (.CI(p22m3_mul_temp_i_2_n_0),
        .CO({NLW_p22m3_mul_temp_i_1_CO_UNCONNECTED[3:1],p22m3_mul_temp_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,ud3[12]}),
        .O({NLW_p22m3_mul_temp_i_1_O_UNCONNECTED[3:2],p22m3_mul_temp_i_1_n_6,p22m3_mul_temp_i_1_n_7}),
        .S({1'b0,1'b0,p22m3_mul_temp_i_5_n_0,p22m3_mul_temp_i_6_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_10
       (.I0(ud3[8]),
        .I1(ud6[8]),
        .O(p22m3_mul_temp_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_11
       (.I0(ud3[7]),
        .I1(ud6[7]),
        .O(p22m3_mul_temp_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_12
       (.I0(ud3[6]),
        .I1(ud6[6]),
        .O(p22m3_mul_temp_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_13
       (.I0(ud3[5]),
        .I1(ud6[5]),
        .O(p22m3_mul_temp_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_14
       (.I0(ud3[4]),
        .I1(ud6[4]),
        .O(p22m3_mul_temp_i_14_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_15
       (.I0(ud3[3]),
        .I1(ud6[3]),
        .O(p22m3_mul_temp_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_16
       (.I0(ud3[2]),
        .I1(ud6[2]),
        .O(p22m3_mul_temp_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_17
       (.I0(ud3[1]),
        .I1(ud6[1]),
        .O(p22m3_mul_temp_i_17_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_18
       (.I0(ud3[0]),
        .I1(ud6[0]),
        .O(p22m3_mul_temp_i_18_n_0));
  CARRY4 p22m3_mul_temp_i_2
       (.CI(p22m3_mul_temp_i_3_n_0),
        .CO({p22m3_mul_temp_i_2_n_0,p22m3_mul_temp_i_2_n_1,p22m3_mul_temp_i_2_n_2,p22m3_mul_temp_i_2_n_3}),
        .CYINIT(1'b0),
        .DI(ud3[11:8]),
        .O({p22m3_mul_temp_i_2_n_4,p22m3_mul_temp_i_2_n_5,p22m3_mul_temp_i_2_n_6,p22m3_mul_temp_i_2_n_7}),
        .S({p22m3_mul_temp_i_7_n_0,p22m3_mul_temp_i_8_n_0,p22m3_mul_temp_i_9_n_0,p22m3_mul_temp_i_10_n_0}));
  CARRY4 p22m3_mul_temp_i_3
       (.CI(p22m3_mul_temp_i_4_n_0),
        .CO({p22m3_mul_temp_i_3_n_0,p22m3_mul_temp_i_3_n_1,p22m3_mul_temp_i_3_n_2,p22m3_mul_temp_i_3_n_3}),
        .CYINIT(1'b0),
        .DI(ud3[7:4]),
        .O({p22m3_mul_temp_i_3_n_4,p22m3_mul_temp_i_3_n_5,p22m3_mul_temp_i_3_n_6,p22m3_mul_temp_i_3_n_7}),
        .S({p22m3_mul_temp_i_11_n_0,p22m3_mul_temp_i_12_n_0,p22m3_mul_temp_i_13_n_0,p22m3_mul_temp_i_14_n_0}));
  CARRY4 p22m3_mul_temp_i_4
       (.CI(1'b0),
        .CO({p22m3_mul_temp_i_4_n_0,p22m3_mul_temp_i_4_n_1,p22m3_mul_temp_i_4_n_2,p22m3_mul_temp_i_4_n_3}),
        .CYINIT(1'b0),
        .DI(ud3[3:0]),
        .O({p22m3_mul_temp_i_4_n_4,p22m3_mul_temp_i_4_n_5,p22m3_mul_temp_i_4_n_6,p22m3_mul_temp_i_4_n_7}),
        .S({p22m3_mul_temp_i_15_n_0,p22m3_mul_temp_i_16_n_0,p22m3_mul_temp_i_17_n_0,p22m3_mul_temp_i_18_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_5
       (.I0(ud3[13]),
        .I1(ud6[13]),
        .O(p22m3_mul_temp_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_6
       (.I0(ud3[12]),
        .I1(ud6[12]),
        .O(p22m3_mul_temp_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_7
       (.I0(ud3[11]),
        .I1(ud6[11]),
        .O(p22m3_mul_temp_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_8
       (.I0(ud3[10]),
        .I1(ud6[10]),
        .O(p22m3_mul_temp_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p22m3_mul_temp_i_9
       (.I0(ud3[9]),
        .I1(ud6[9]),
        .O(p22m3_mul_temp_i_9_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    p23m4_mul_temp
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,p23m4_mul_temp_0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_p23m4_mul_temp_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({B[13],B[13],B[13],B[13],B}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_p23m4_mul_temp_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_p23m4_mul_temp_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_p23m4_mul_temp_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_p23m4_mul_temp_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_p23m4_mul_temp_OVERFLOW_UNCONNECTED),
        .P({NLW_p23m4_mul_temp_P_UNCONNECTED[47:29],p23m4_mul_temp_n_77,p23m4_mul_temp_n_78,\^p23m4_mul_temp ,p23m4_mul_temp_n_93,p23m4_mul_temp_n_94,p23m4_mul_temp_n_95,p23m4_mul_temp_n_96,p23m4_mul_temp_n_97,p23m4_mul_temp_n_98,p23m4_mul_temp_n_99,p23m4_mul_temp_n_100,p23m4_mul_temp_n_101,p23m4_mul_temp_n_102,p23m4_mul_temp_n_103,p23m4_mul_temp_n_104,p23m4_mul_temp_n_105}),
        .PATTERNBDETECT(NLW_p23m4_mul_temp_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_p23m4_mul_temp_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_p23m4_mul_temp_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_p23m4_mul_temp_UNDERFLOW_UNCONNECTED));
  CARRY4 p23m4_mul_temp_i_1
       (.CI(p23m4_mul_temp_i_2_n_0),
        .CO({NLW_p23m4_mul_temp_i_1_CO_UNCONNECTED[3:2],p23m4_mul_temp_i_1_n_2,p23m4_mul_temp_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,p23m4_mul_temp_i_5_n_0,ud4[12]}),
        .O({NLW_p23m4_mul_temp_i_1_O_UNCONNECTED[3],B[13:11]}),
        .S({1'b0,1'b1,p23m4_mul_temp_i_6_n_0,p23m4_mul_temp_i_7_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_10
       (.I0(ud4[9]),
        .I1(ud5[9]),
        .O(p23m4_mul_temp_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_11
       (.I0(ud4[8]),
        .I1(ud5[8]),
        .O(p23m4_mul_temp_i_11_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_12
       (.I0(ud4[7]),
        .I1(ud5[7]),
        .O(p23m4_mul_temp_i_12_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_13
       (.I0(ud4[6]),
        .I1(ud5[6]),
        .O(p23m4_mul_temp_i_13_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_14
       (.I0(ud4[5]),
        .I1(ud5[5]),
        .O(p23m4_mul_temp_i_14_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_15
       (.I0(ud4[4]),
        .I1(ud5[4]),
        .O(p23m4_mul_temp_i_15_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_16
       (.I0(ud4[3]),
        .I1(ud5[3]),
        .O(p23m4_mul_temp_i_16_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_17
       (.I0(ud4[2]),
        .I1(ud5[2]),
        .O(p23m4_mul_temp_i_17_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_18
       (.I0(ud4[1]),
        .I1(ud5[1]),
        .O(p23m4_mul_temp_i_18_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_19
       (.I0(ud4[0]),
        .I1(ud5[0]),
        .O(p23m4_mul_temp_i_19_n_0));
  CARRY4 p23m4_mul_temp_i_2
       (.CI(p23m4_mul_temp_i_3_n_0),
        .CO({p23m4_mul_temp_i_2_n_0,p23m4_mul_temp_i_2_n_1,p23m4_mul_temp_i_2_n_2,p23m4_mul_temp_i_2_n_3}),
        .CYINIT(1'b0),
        .DI(ud4[11:8]),
        .O(B[10:7]),
        .S({p23m4_mul_temp_i_8_n_0,p23m4_mul_temp_i_9_n_0,p23m4_mul_temp_i_10_n_0,p23m4_mul_temp_i_11_n_0}));
  CARRY4 p23m4_mul_temp_i_3
       (.CI(p23m4_mul_temp_i_4_n_0),
        .CO({p23m4_mul_temp_i_3_n_0,p23m4_mul_temp_i_3_n_1,p23m4_mul_temp_i_3_n_2,p23m4_mul_temp_i_3_n_3}),
        .CYINIT(1'b0),
        .DI(ud4[7:4]),
        .O(B[6:3]),
        .S({p23m4_mul_temp_i_12_n_0,p23m4_mul_temp_i_13_n_0,p23m4_mul_temp_i_14_n_0,p23m4_mul_temp_i_15_n_0}));
  CARRY4 p23m4_mul_temp_i_4
       (.CI(1'b0),
        .CO({p23m4_mul_temp_i_4_n_0,p23m4_mul_temp_i_4_n_1,p23m4_mul_temp_i_4_n_2,p23m4_mul_temp_i_4_n_3}),
        .CYINIT(1'b0),
        .DI(ud4[3:0]),
        .O({B[2:0],NLW_p23m4_mul_temp_i_4_O_UNCONNECTED[0]}),
        .S({p23m4_mul_temp_i_16_n_0,p23m4_mul_temp_i_17_n_0,p23m4_mul_temp_i_18_n_0,p23m4_mul_temp_i_19_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    p23m4_mul_temp_i_5
       (.I0(ud4[13]),
        .O(p23m4_mul_temp_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_6
       (.I0(ud4[13]),
        .I1(ud5[13]),
        .O(p23m4_mul_temp_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_7
       (.I0(ud4[12]),
        .I1(ud5[12]),
        .O(p23m4_mul_temp_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_8
       (.I0(ud4[11]),
        .I1(ud5[11]),
        .O(p23m4_mul_temp_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p23m4_mul_temp_i_9
       (.I0(ud4[10]),
        .I1(ud5[10]),
        .O(p23m4_mul_temp_i_9_n_0));
  CARRY4 p24a5_add_temp_carry
       (.CI(1'b0),
        .CO({p24a5_add_temp_carry_n_0,p24a5_add_temp_carry_n_1,p24a5_add_temp_carry_n_2,p24a5_add_temp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({\^p20m1_mul_temp [15:13],1'b0}),
        .O({p26y_out_add_cast[2:1],NLW_p24a5_add_temp_carry_O_UNCONNECTED[1:0]}),
        .S({p24a5_add_temp_carry_i_1_n_0,p24a5_add_temp_carry_i_2_n_0,p24a5_add_temp_carry_i_3_n_0,p24a5_add_cast_1[0]}));
  CARRY4 p24a5_add_temp_carry__0
       (.CI(p24a5_add_temp_carry_n_0),
        .CO({p24a5_add_temp_carry__0_n_0,p24a5_add_temp_carry__0_n_1,p24a5_add_temp_carry__0_n_2,p24a5_add_temp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(\^p20m1_mul_temp [19:16]),
        .O(p26y_out_add_cast[6:3]),
        .S({p24a5_add_temp_carry__0_i_1_n_0,p24a5_add_temp_carry__0_i_2_n_0,p24a5_add_temp_carry__0_i_3_n_0,p24a5_add_temp_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__0_i_1
       (.I0(\^p20m1_mul_temp [19]),
        .I1(p24a5_add_cast_1[7]),
        .O(p24a5_add_temp_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__0_i_2
       (.I0(\^p20m1_mul_temp [18]),
        .I1(p24a5_add_cast_1[6]),
        .O(p24a5_add_temp_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__0_i_3
       (.I0(\^p20m1_mul_temp [17]),
        .I1(p24a5_add_cast_1[5]),
        .O(p24a5_add_temp_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__0_i_4
       (.I0(\^p20m1_mul_temp [16]),
        .I1(p24a5_add_cast_1[4]),
        .O(p24a5_add_temp_carry__0_i_4_n_0));
  CARRY4 p24a5_add_temp_carry__1
       (.CI(p24a5_add_temp_carry__0_n_0),
        .CO({p24a5_add_temp_carry__1_n_0,p24a5_add_temp_carry__1_n_1,p24a5_add_temp_carry__1_n_2,p24a5_add_temp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(\^p20m1_mul_temp [23:20]),
        .O(p26y_out_add_cast[10:7]),
        .S({p24a5_add_temp_carry__1_i_1_n_0,p24a5_add_temp_carry__1_i_2_n_0,p24a5_add_temp_carry__1_i_3_n_0,p24a5_add_temp_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__1_i_1
       (.I0(\^p20m1_mul_temp [23]),
        .I1(p24a5_add_cast_1[11]),
        .O(p24a5_add_temp_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__1_i_2
       (.I0(\^p20m1_mul_temp [22]),
        .I1(p24a5_add_cast_1[10]),
        .O(p24a5_add_temp_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__1_i_3
       (.I0(\^p20m1_mul_temp [21]),
        .I1(p24a5_add_cast_1[9]),
        .O(p24a5_add_temp_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__1_i_4
       (.I0(\^p20m1_mul_temp [20]),
        .I1(p24a5_add_cast_1[8]),
        .O(p24a5_add_temp_carry__1_i_4_n_0));
  CARRY4 p24a5_add_temp_carry__2
       (.CI(p24a5_add_temp_carry__1_n_0),
        .CO({NLW_p24a5_add_temp_carry__2_CO_UNCONNECTED[3:2],p24a5_add_temp_carry__2_n_2,p24a5_add_temp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,p24a5_add_cast_1[13],\^p20m1_mul_temp [24]}),
        .O({NLW_p24a5_add_temp_carry__2_O_UNCONNECTED[3],p26y_out_add_cast[13:11]}),
        .S({1'b0,p24a5_add_temp_carry__2_i_1_n_0,p24a5_add_temp_carry__2_i_2_n_0,p24a5_add_temp_carry__2_i_3_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__2_i_1
       (.I0(p24a5_add_cast_1[13]),
        .I1(\^p20m1_mul_temp [26]),
        .O(p24a5_add_temp_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__2_i_2
       (.I0(p24a5_add_cast_1[13]),
        .I1(\^p20m1_mul_temp [25]),
        .O(p24a5_add_temp_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry__2_i_3
       (.I0(\^p20m1_mul_temp [24]),
        .I1(p24a5_add_cast_1[12]),
        .O(p24a5_add_temp_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry_i_1
       (.I0(\^p20m1_mul_temp [15]),
        .I1(p24a5_add_cast_1[3]),
        .O(p24a5_add_temp_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry_i_2
       (.I0(\^p20m1_mul_temp [14]),
        .I1(p24a5_add_cast_1[2]),
        .O(p24a5_add_temp_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p24a5_add_temp_carry_i_3
       (.I0(\^p20m1_mul_temp [13]),
        .I1(p24a5_add_cast_1[1]),
        .O(p24a5_add_temp_carry_i_3_n_0));
  CARRY4 p25a6_add_temp_carry
       (.CI(1'b0),
        .CO({p25a6_add_temp_carry_n_0,p25a6_add_temp_carry_n_1,p25a6_add_temp_carry_n_2,p25a6_add_temp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({p25a6_add_cast[3:1],1'b0}),
        .O({p25a6_add_temp[3:2],NLW_p25a6_add_temp_carry_O_UNCONNECTED[1:0]}),
        .S({p25a6_add_temp_carry_i_1_n_0,p25a6_add_temp_carry_i_2_n_0,p25a6_add_temp_carry_i_3_n_0,p25a6_add_cast[0]}));
  CARRY4 p25a6_add_temp_carry__0
       (.CI(p25a6_add_temp_carry_n_0),
        .CO({p25a6_add_temp_carry__0_n_0,p25a6_add_temp_carry__0_n_1,p25a6_add_temp_carry__0_n_2,p25a6_add_temp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(p25a6_add_cast[7:4]),
        .O(p25a6_add_temp[7:4]),
        .S({p25a6_add_temp_carry__0_i_1_n_0,p25a6_add_temp_carry__0_i_2_n_0,p25a6_add_temp_carry__0_i_3_n_0,p25a6_add_temp_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__0_i_1
       (.I0(p25a6_add_cast[7]),
        .I1(\^p23m4_mul_temp [19]),
        .O(p25a6_add_temp_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__0_i_2
       (.I0(p25a6_add_cast[6]),
        .I1(\^p23m4_mul_temp [18]),
        .O(p25a6_add_temp_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__0_i_3
       (.I0(p25a6_add_cast[5]),
        .I1(\^p23m4_mul_temp [17]),
        .O(p25a6_add_temp_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__0_i_4
       (.I0(p25a6_add_cast[4]),
        .I1(\^p23m4_mul_temp [16]),
        .O(p25a6_add_temp_carry__0_i_4_n_0));
  CARRY4 p25a6_add_temp_carry__1
       (.CI(p25a6_add_temp_carry__0_n_0),
        .CO({p25a6_add_temp_carry__1_n_0,p25a6_add_temp_carry__1_n_1,p25a6_add_temp_carry__1_n_2,p25a6_add_temp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(p25a6_add_cast[11:8]),
        .O(p25a6_add_temp[11:8]),
        .S({p25a6_add_temp_carry__1_i_1_n_0,p25a6_add_temp_carry__1_i_2_n_0,p25a6_add_temp_carry__1_i_3_n_0,p25a6_add_temp_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__1_i_1
       (.I0(p25a6_add_cast[11]),
        .I1(\^p23m4_mul_temp [23]),
        .O(p25a6_add_temp_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__1_i_2
       (.I0(p25a6_add_cast[10]),
        .I1(\^p23m4_mul_temp [22]),
        .O(p25a6_add_temp_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__1_i_3
       (.I0(p25a6_add_cast[9]),
        .I1(\^p23m4_mul_temp [21]),
        .O(p25a6_add_temp_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__1_i_4
       (.I0(p25a6_add_cast[8]),
        .I1(\^p23m4_mul_temp [20]),
        .O(p25a6_add_temp_carry__1_i_4_n_0));
  CARRY4 p25a6_add_temp_carry__2
       (.CI(p25a6_add_temp_carry__1_n_0),
        .CO({p25a6_add_temp_carry__2_n_0,NLW_p25a6_add_temp_carry__2_CO_UNCONNECTED[2],p25a6_add_temp_carry__2_n_2,p25a6_add_temp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,\^p23m4_mul_temp [25],p25a6_add_temp_carry__2_i_1_n_0,p25a6_add_cast[12]}),
        .O({NLW_p25a6_add_temp_carry__2_O_UNCONNECTED[3],p25a6_add_temp[14:12]}),
        .S({1'b1,p25a6_add_temp_carry__2_i_2_n_0,p25a6_add_temp_carry__2_i_3_n_0,p25a6_add_temp_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    p25a6_add_temp_carry__2_i_1
       (.I0(\^p23m4_mul_temp [25]),
        .O(p25a6_add_temp_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    p25a6_add_temp_carry__2_i_2
       (.I0(\^p23m4_mul_temp [25]),
        .I1(\^p23m4_mul_temp [26]),
        .O(p25a6_add_temp_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__2_i_3
       (.I0(\^p23m4_mul_temp [25]),
        .I1(p25a6_add_cast[13]),
        .O(p25a6_add_temp_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry__2_i_4
       (.I0(p25a6_add_cast[12]),
        .I1(\^p23m4_mul_temp [24]),
        .O(p25a6_add_temp_carry__2_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry_i_1
       (.I0(p25a6_add_cast[3]),
        .I1(\^p23m4_mul_temp [15]),
        .O(p25a6_add_temp_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry_i_2
       (.I0(p25a6_add_cast[2]),
        .I1(\^p23m4_mul_temp [14]),
        .O(p25a6_add_temp_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p25a6_add_temp_carry_i_3
       (.I0(p25a6_add_cast[1]),
        .I1(\^p23m4_mul_temp [13]),
        .O(p25a6_add_temp_carry_i_3_n_0));
  CARRY4 p26y_out_add_temp_carry
       (.CI(1'b0),
        .CO({p26y_out_add_temp_carry_n_0,p26y_out_add_temp_carry_n_1,p26y_out_add_temp_carry_n_2,p26y_out_add_temp_carry_n_3}),
        .CYINIT(1'b0),
        .DI({p26y_out_add_cast[4:2],1'b0}),
        .O({p26y_out_add_temp[2:0],NLW_p26y_out_add_temp_carry_O_UNCONNECTED[0]}),
        .S({p26y_out_add_temp_carry_i_1_n_0,p26y_out_add_temp_carry_i_2_n_0,p26y_out_add_temp_carry_i_3_n_0,p26y_out_add_cast[1]}));
  CARRY4 p26y_out_add_temp_carry__0
       (.CI(p26y_out_add_temp_carry_n_0),
        .CO({p26y_out_add_temp_carry__0_n_0,p26y_out_add_temp_carry__0_n_1,p26y_out_add_temp_carry__0_n_2,p26y_out_add_temp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(p26y_out_add_cast[8:5]),
        .O(p26y_out_add_temp[6:3]),
        .S({p26y_out_add_temp_carry__0_i_1_n_0,p26y_out_add_temp_carry__0_i_2_n_0,p26y_out_add_temp_carry__0_i_3_n_0,p26y_out_add_temp_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__0_i_1
       (.I0(p26y_out_add_cast[8]),
        .I1(p25a6_add_temp[8]),
        .O(p26y_out_add_temp_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__0_i_2
       (.I0(p26y_out_add_cast[7]),
        .I1(p25a6_add_temp[7]),
        .O(p26y_out_add_temp_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__0_i_3
       (.I0(p26y_out_add_cast[6]),
        .I1(p25a6_add_temp[6]),
        .O(p26y_out_add_temp_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__0_i_4
       (.I0(p26y_out_add_cast[5]),
        .I1(p25a6_add_temp[5]),
        .O(p26y_out_add_temp_carry__0_i_4_n_0));
  CARRY4 p26y_out_add_temp_carry__1
       (.CI(p26y_out_add_temp_carry__0_n_0),
        .CO({p26y_out_add_temp_carry__1_n_0,p26y_out_add_temp_carry__1_n_1,p26y_out_add_temp_carry__1_n_2,p26y_out_add_temp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(p26y_out_add_cast[12:9]),
        .O(p26y_out_add_temp[10:7]),
        .S({p26y_out_add_temp_carry__1_i_1_n_0,p26y_out_add_temp_carry__1_i_2_n_0,p26y_out_add_temp_carry__1_i_3_n_0,p26y_out_add_temp_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__1_i_1
       (.I0(p26y_out_add_cast[12]),
        .I1(p25a6_add_temp[12]),
        .O(p26y_out_add_temp_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__1_i_2
       (.I0(p26y_out_add_cast[11]),
        .I1(p25a6_add_temp[11]),
        .O(p26y_out_add_temp_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__1_i_3
       (.I0(p26y_out_add_cast[10]),
        .I1(p25a6_add_temp[10]),
        .O(p26y_out_add_temp_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__1_i_4
       (.I0(p26y_out_add_cast[9]),
        .I1(p25a6_add_temp[9]),
        .O(p26y_out_add_temp_carry__1_i_4_n_0));
  CARRY4 p26y_out_add_temp_carry__2
       (.CI(p26y_out_add_temp_carry__1_n_0),
        .CO({NLW_p26y_out_add_temp_carry__2_CO_UNCONNECTED[3:2],p26y_out_add_temp_carry__2_n_2,p26y_out_add_temp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,p25a6_add_temp[13],p26y_out_add_temp_carry__2_i_1_n_0}),
        .O({NLW_p26y_out_add_temp_carry__2_O_UNCONNECTED[3],p26y_out_add_temp[13:11]}),
        .S({1'b0,p26y_out_add_temp_carry__2_i_2_n_0,p26y_out_add_temp_carry__2_i_3_n_0,p26y_out_add_temp_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    p26y_out_add_temp_carry__2_i_1
       (.I0(p25a6_add_temp[13]),
        .O(p26y_out_add_temp_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__2_i_2
       (.I0(p25a6_add_temp[14]),
        .I1(p25a6_add_temp_carry__2_n_0),
        .O(p26y_out_add_temp_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    p26y_out_add_temp_carry__2_i_3
       (.I0(p25a6_add_temp[13]),
        .I1(p25a6_add_temp[14]),
        .O(p26y_out_add_temp_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry__2_i_4
       (.I0(p25a6_add_temp[13]),
        .I1(p26y_out_add_cast[13]),
        .O(p26y_out_add_temp_carry__2_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry_i_1
       (.I0(p26y_out_add_cast[4]),
        .I1(p25a6_add_temp[4]),
        .O(p26y_out_add_temp_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry_i_2
       (.I0(p26y_out_add_cast[3]),
        .I1(p25a6_add_temp[3]),
        .O(p26y_out_add_temp_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    p26y_out_add_temp_carry_i_3
       (.I0(p26y_out_add_cast[2]),
        .I1(p25a6_add_temp[2]),
        .O(p26y_out_add_temp_carry_i_3_n_0));
  FDCE \ud1_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[0]),
        .Q(ud1[0]));
  FDCE \ud1_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[10]),
        .Q(ud1[10]));
  FDCE \ud1_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[11]),
        .Q(ud1[11]));
  FDCE \ud1_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[12]),
        .Q(ud1[12]));
  FDCE \ud1_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[13]),
        .Q(ud1[13]));
  FDCE \ud1_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[1]),
        .Q(ud1[1]));
  FDCE \ud1_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[2]),
        .Q(ud1[2]));
  FDCE \ud1_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[3]),
        .Q(ud1[3]));
  FDCE \ud1_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[4]),
        .Q(ud1[4]));
  FDCE \ud1_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[5]),
        .Q(ud1[5]));
  FDCE \ud1_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[6]),
        .Q(ud1[6]));
  FDCE \ud1_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[7]),
        .Q(ud1[7]));
  FDCE \ud1_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[8]),
        .Q(ud1[8]));
  FDCE \ud1_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(D[9]),
        .Q(ud1[9]));
  FDCE \ud2_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[0]),
        .Q(ud2[0]));
  FDCE \ud2_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[10]),
        .Q(ud2[10]));
  FDCE \ud2_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[11]),
        .Q(ud2[11]));
  FDCE \ud2_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[12]),
        .Q(ud2[12]));
  FDCE \ud2_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[13]),
        .Q(ud2[13]));
  FDCE \ud2_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[1]),
        .Q(ud2[1]));
  FDCE \ud2_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[2]),
        .Q(ud2[2]));
  FDCE \ud2_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[3]),
        .Q(ud2[3]));
  FDCE \ud2_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[4]),
        .Q(ud2[4]));
  FDCE \ud2_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[5]),
        .Q(ud2[5]));
  FDCE \ud2_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[6]),
        .Q(ud2[6]));
  FDCE \ud2_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[7]),
        .Q(ud2[7]));
  FDCE \ud2_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[8]),
        .Q(ud2[8]));
  FDCE \ud2_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud1[9]),
        .Q(ud2[9]));
  FDCE \ud3_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[0]),
        .Q(ud3[0]));
  FDCE \ud3_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[10]),
        .Q(ud3[10]));
  FDCE \ud3_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[11]),
        .Q(ud3[11]));
  FDCE \ud3_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[12]),
        .Q(ud3[12]));
  FDCE \ud3_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[13]),
        .Q(ud3[13]));
  FDCE \ud3_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[1]),
        .Q(ud3[1]));
  FDCE \ud3_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[2]),
        .Q(ud3[2]));
  FDCE \ud3_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[3]),
        .Q(ud3[3]));
  FDCE \ud3_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[4]),
        .Q(ud3[4]));
  FDCE \ud3_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[5]),
        .Q(ud3[5]));
  FDCE \ud3_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[6]),
        .Q(ud3[6]));
  FDCE \ud3_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[7]),
        .Q(ud3[7]));
  FDCE \ud3_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[8]),
        .Q(ud3[8]));
  FDCE \ud3_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud2[9]),
        .Q(ud3[9]));
  FDCE \ud4_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[0]),
        .Q(ud4[0]));
  FDCE \ud4_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[10]),
        .Q(ud4[10]));
  FDCE \ud4_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[11]),
        .Q(ud4[11]));
  FDCE \ud4_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[12]),
        .Q(ud4[12]));
  FDCE \ud4_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[13]),
        .Q(ud4[13]));
  FDCE \ud4_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[1]),
        .Q(ud4[1]));
  FDCE \ud4_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[2]),
        .Q(ud4[2]));
  FDCE \ud4_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[3]),
        .Q(ud4[3]));
  FDCE \ud4_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[4]),
        .Q(ud4[4]));
  FDCE \ud4_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[5]),
        .Q(ud4[5]));
  FDCE \ud4_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[6]),
        .Q(ud4[6]));
  FDCE \ud4_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[7]),
        .Q(ud4[7]));
  FDCE \ud4_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[8]),
        .Q(ud4[8]));
  FDCE \ud4_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud3[9]),
        .Q(ud4[9]));
  FDCE \ud5_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[0]),
        .Q(ud5[0]));
  FDCE \ud5_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[10]),
        .Q(ud5[10]));
  FDCE \ud5_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[11]),
        .Q(ud5[11]));
  FDCE \ud5_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[12]),
        .Q(ud5[12]));
  FDCE \ud5_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[13]),
        .Q(ud5[13]));
  FDCE \ud5_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[1]),
        .Q(ud5[1]));
  FDCE \ud5_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[2]),
        .Q(ud5[2]));
  FDCE \ud5_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[3]),
        .Q(ud5[3]));
  FDCE \ud5_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[4]),
        .Q(ud5[4]));
  FDCE \ud5_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[5]),
        .Q(ud5[5]));
  FDCE \ud5_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[6]),
        .Q(ud5[6]));
  FDCE \ud5_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[7]),
        .Q(ud5[7]));
  FDCE \ud5_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[8]),
        .Q(ud5[8]));
  FDCE \ud5_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud4[9]),
        .Q(ud5[9]));
  FDCE \ud6_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[0]),
        .Q(ud6[0]));
  FDCE \ud6_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[10]),
        .Q(ud6[10]));
  FDCE \ud6_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[11]),
        .Q(ud6[11]));
  FDCE \ud6_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[12]),
        .Q(ud6[12]));
  FDCE \ud6_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[13]),
        .Q(ud6[13]));
  FDCE \ud6_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[1]),
        .Q(ud6[1]));
  FDCE \ud6_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[2]),
        .Q(ud6[2]));
  FDCE \ud6_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[3]),
        .Q(ud6[3]));
  FDCE \ud6_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[4]),
        .Q(ud6[4]));
  FDCE \ud6_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[5]),
        .Q(ud6[5]));
  FDCE \ud6_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[6]),
        .Q(ud6[6]));
  FDCE \ud6_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[7]),
        .Q(ud6[7]));
  FDCE \ud6_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[8]),
        .Q(ud6[8]));
  FDCE \ud6_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud5[9]),
        .Q(ud6[9]));
  FDCE \ud7_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[0]),
        .Q(ud7[0]));
  FDCE \ud7_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[10]),
        .Q(ud7[10]));
  FDCE \ud7_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[11]),
        .Q(ud7[11]));
  FDCE \ud7_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[12]),
        .Q(ud7[12]));
  FDCE \ud7_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[13]),
        .Q(ud7[13]));
  FDCE \ud7_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[1]),
        .Q(ud7[1]));
  FDCE \ud7_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[2]),
        .Q(ud7[2]));
  FDCE \ud7_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[3]),
        .Q(ud7[3]));
  FDCE \ud7_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[4]),
        .Q(ud7[4]));
  FDCE \ud7_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[5]),
        .Q(ud7[5]));
  FDCE \ud7_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[6]),
        .Q(ud7[6]));
  FDCE \ud7_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[7]),
        .Q(ud7[7]));
  FDCE \ud7_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[8]),
        .Q(ud7[8]));
  FDCE \ud7_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud6[9]),
        .Q(ud7[9]));
  FDCE \ud8_reg[0] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[0]),
        .Q(delayed_xout[0]));
  FDCE \ud8_reg[10] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[10]),
        .Q(delayed_xout[10]));
  FDCE \ud8_reg[11] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[11]),
        .Q(delayed_xout[11]));
  FDCE \ud8_reg[12] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[12]),
        .Q(delayed_xout[12]));
  FDCE \ud8_reg[13] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[13]),
        .Q(delayed_xout[13]));
  FDCE \ud8_reg[1] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[1]),
        .Q(delayed_xout[1]));
  FDCE \ud8_reg[2] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[2]),
        .Q(delayed_xout[2]));
  FDCE \ud8_reg[3] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[3]),
        .Q(delayed_xout[3]));
  FDCE \ud8_reg[4] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[4]),
        .Q(delayed_xout[4]));
  FDCE \ud8_reg[5] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[5]),
        .Q(delayed_xout[5]));
  FDCE \ud8_reg[6] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[6]),
        .Q(delayed_xout[6]));
  FDCE \ud8_reg[7] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[7]),
        .Q(delayed_xout[7]));
  FDCE \ud8_reg[8] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[8]),
        .Q(delayed_xout[8]));
  FDCE \ud8_reg[9] 
       (.C(IPCORE_CLK),
        .CE(E),
        .CLR(AR),
        .D(ud7[9]),
        .Q(delayed_xout[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore
   (AXI4_Lite_ARREADY,
    AXI4_Lite_RVALID,
    AXI4_Lite_AWREADY,
    AXI4_Lite_BVALID,
    AXI4_Lite_WREADY,
    out_valid_reg,
    delayed_xout,
    AXI4_Stream_Master_TDATA,
    AXI4_Stream_Slave_TREADY,
    AXI4_Lite_RDATA,
    AXI4_Stream_Master_TLAST,
    AXI4_Lite_AWVALID,
    AXI4_Lite_ARVALID,
    AXI4_Lite_ARADDR,
    AXI4_Stream_Master_TREADY,
    AXI4_Stream_Slave_TVALID,
    AXI4_Lite_ACLK,
    AXI4_Lite_WDATA,
    AXI4_Lite_AWADDR,
    IPCORE_CLK,
    AXI4_Stream_Slave_TDATA,
    AXI4_Lite_RREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN);
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_RVALID;
  output AXI4_Lite_AWREADY;
  output AXI4_Lite_BVALID;
  output AXI4_Lite_WREADY;
  output out_valid_reg;
  output [13:0]delayed_xout;
  output [15:0]AXI4_Stream_Master_TDATA;
  output AXI4_Stream_Slave_TREADY;
  output [0:0]AXI4_Lite_RDATA;
  output AXI4_Stream_Master_TLAST;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Stream_Master_TREADY;
  input AXI4_Stream_Slave_TVALID;
  input AXI4_Lite_ACLK;
  input [31:0]AXI4_Lite_WDATA;
  input [13:0]AXI4_Lite_AWADDR;
  input IPCORE_CLK;
  input [15:0]AXI4_Stream_Slave_TDATA;
  input AXI4_Lite_RREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;

  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire AXI4_Lite_BVALID;
  wire [0:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire AXI4_Lite_RVALID;
  wire [31:0]AXI4_Lite_WDATA;
  wire AXI4_Lite_WREADY;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [15:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire [15:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire IPCORE_CLK;
  wire IPCORE_RESETN;
  wire [13:0]Out_1;
  wire auto_ready_axi4_stream_master;
  wire auto_ready_dut_enb;
  wire auto_tlast;
  wire [13:0]delayed_xout;
  wire dut_enable;
  wire out_valid_reg;
  wire relop_relop1;
  wire reset;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst_n_4;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_1;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_41;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_42;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_43;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_44;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_45;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_46;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_47;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_48;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_49;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_50;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_51;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_52;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_53;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_54;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_55;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_56;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_57;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_58;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_59;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_60;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_61;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_62;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_63;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_64;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_65;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_66;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_67;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_68;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_7;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_8;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_9;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_15;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_16;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_17;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_18;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_19;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_20;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_21;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_22;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_23;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_24;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_25;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_26;
  wire u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_27;
  wire \u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst/out_valid ;
  wire wr_din0;
  wire [13:0]write_reg_h_in1;
  wire [13:0]write_reg_h_in2;
  wire [13:0]write_reg_h_in3;
  wire [13:0]write_reg_h_in4;
  wire [30:0]write_reg_packet_size_axi4_stream_master;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master_inst
       (.AR(u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_1),
        .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),
        .AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .CO(relop_relop1),
        .IPCORE_CLK(IPCORE_CLK),
        .Q(write_reg_packet_size_axi4_stream_master),
        .S({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_65,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_66,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_67,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_68}),
        .auto_ready_axi4_stream_master(auto_ready_axi4_stream_master),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .auto_tlast(auto_tlast),
        .out_valid(\u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst/out_valid ),
        .out_valid_reg(out_valid_reg),
        .p26y_out_add_temp({wr_din0,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_15,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_16,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_17,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_18,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_19,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_20,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_21,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_22,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_23,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_24,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_25,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_26,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_27}),
        .relop_relop1_carry__0_i_1({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_45,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_46,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_47,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_48}),
        .relop_relop1_carry__0_i_3({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_49,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_50,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_51,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_52}),
        .relop_relop1_carry__0_i_4({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_53,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_54,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_55,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_56}),
        .relop_relop1_carry__1_i_2({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_7,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_8,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_9}),
        .relop_relop1_carry__1_i_3({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_41,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_42,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_43,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_44}),
        .relop_relop1_carry_i_1({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_57,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_58,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_59,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_60}),
        .relop_relop1_carry_i_3({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_61,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_62,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_63,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_64}),
        .reset(reset),
        .\tlast_counter_out_reg[31]_0 (u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst_n_4));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst
       (.AR(u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .CO(relop_relop1),
        .IPCORE_CLK(IPCORE_CLK),
        .Q(Out_1),
        .auto_ready_axi4_stream_master(auto_ready_axi4_stream_master),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .auto_tlast(auto_tlast),
        .out_valid(\u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst/out_valid ),
        .out_valid_reg(u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst_n_4),
        .reset(reset));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst
       (.AR(u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_1),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARESETN_0(reset),
        .AXI4_Lite_ARESETN_1(u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .E(dut_enable),
        .\FSM_onehot_axi_lite_wstate_reg[2] ({AXI4_Lite_BVALID,AXI4_Lite_WREADY}),
        .FSM_sequential_axi_lite_rstate_reg(AXI4_Lite_RVALID),
        .IPCORE_RESETN(IPCORE_RESETN),
        .Q(write_reg_packet_size_axi4_stream_master),
        .S({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_65,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_66,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_67,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_68}),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .\write_reg_h_in1_reg[13] (write_reg_h_in1),
        .\write_reg_h_in2_reg[13] (write_reg_h_in2),
        .\write_reg_h_in3_reg[13] (write_reg_h_in3),
        .\write_reg_h_in4_reg[13] (write_reg_h_in4),
        .\write_reg_packet_size_axi4_stream_master_reg[12] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_57,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_58,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_59,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_60}),
        .\write_reg_packet_size_axi4_stream_master_reg[16] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_53,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_54,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_55,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_56}),
        .\write_reg_packet_size_axi4_stream_master_reg[20] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_49,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_50,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_51,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_52}),
        .\write_reg_packet_size_axi4_stream_master_reg[24] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_45,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_46,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_47,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_48}),
        .\write_reg_packet_size_axi4_stream_master_reg[28] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_41,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_42,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_43,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_44}),
        .\write_reg_packet_size_axi4_stream_master_reg[31] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_7,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_8,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_9}),
        .\write_reg_packet_size_axi4_stream_master_reg[8] ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_61,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_62,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_63,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_64}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_dut u_mlhdlc_sfir_fixpt_ipcore_dut_inst
       (.AR(u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69),
        .D(Out_1),
        .E(dut_enable),
        .IPCORE_CLK(IPCORE_CLK),
        .delayed_xout(delayed_xout),
        .p20m1_mul_temp(write_reg_h_in1),
        .p21m2_mul_temp(write_reg_h_in2),
        .p22m3_mul_temp(write_reg_h_in3),
        .p23m4_mul_temp(write_reg_h_in4),
        .p26y_out_add_temp({wr_din0,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_15,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_16,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_17,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_18,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_19,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_20,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_21,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_22,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_23,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_24,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_25,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_26,u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_27}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic
   (D,
    data_int,
    \data_int_reg[13]_0 ,
    Q,
    \Out_1_reg[13] ,
    w_d1,
    cache_valid,
    AXI4_Stream_Slave_TVALID,
    \data_int_reg[1]_0 ,
    \data_int_reg[1]_1 ,
    \data_int_reg[1]_2 ,
    IPCORE_CLK,
    AXI4_Stream_Slave_TDATA,
    ADDRA,
    ADDRD);
  output [13:0]D;
  output [13:0]data_int;
  output [13:0]\data_int_reg[13]_0 ;
  input [13:0]Q;
  input [13:0]\Out_1_reg[13] ;
  input w_d1;
  input cache_valid;
  input AXI4_Stream_Slave_TVALID;
  input \data_int_reg[1]_0 ;
  input \data_int_reg[1]_1 ;
  input \data_int_reg[1]_2 ;
  input IPCORE_CLK;
  input [15:0]AXI4_Stream_Slave_TDATA;
  input [1:0]ADDRA;
  input [1:0]ADDRD;

  wire [1:0]ADDRA;
  wire [1:0]ADDRD;
  wire [15:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TVALID;
  wire [13:0]D;
  wire IPCORE_CLK;
  wire [13:0]\Out_1_reg[13] ;
  wire [13:0]Q;
  wire cache_valid;
  wire [13:0]data_int;
  wire [13:0]data_int0;
  wire [13:0]\data_int_reg[13]_0 ;
  wire \data_int_reg[1]_0 ;
  wire \data_int_reg[1]_1 ;
  wire \data_int_reg[1]_2 ;
  wire ram_reg_0_3_12_15_n_2;
  wire ram_reg_0_3_12_15_n_3;
  wire w_d1;
  wire w_we;
  wire [1:0]NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[0]_i_1 
       (.I0(Q[0]),
        .I1(data_int[0]),
        .I2(\Out_1_reg[13] [0]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[10]_i_1 
       (.I0(Q[10]),
        .I1(data_int[10]),
        .I2(\Out_1_reg[13] [10]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[11]_i_1 
       (.I0(Q[11]),
        .I1(data_int[11]),
        .I2(\Out_1_reg[13] [11]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[12]_i_1 
       (.I0(Q[12]),
        .I1(data_int[12]),
        .I2(\Out_1_reg[13] [12]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[13]_i_2 
       (.I0(Q[13]),
        .I1(data_int[13]),
        .I2(\Out_1_reg[13] [13]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[1]_i_1 
       (.I0(Q[1]),
        .I1(data_int[1]),
        .I2(\Out_1_reg[13] [1]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[2]_i_1 
       (.I0(Q[2]),
        .I1(data_int[2]),
        .I2(\Out_1_reg[13] [2]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[3]_i_1 
       (.I0(Q[3]),
        .I1(data_int[3]),
        .I2(\Out_1_reg[13] [3]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[4]_i_1 
       (.I0(Q[4]),
        .I1(data_int[4]),
        .I2(\Out_1_reg[13] [4]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[5]_i_1 
       (.I0(Q[5]),
        .I1(data_int[5]),
        .I2(\Out_1_reg[13] [5]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[6]_i_1 
       (.I0(Q[6]),
        .I1(data_int[6]),
        .I2(\Out_1_reg[13] [6]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[7]_i_1 
       (.I0(Q[7]),
        .I1(data_int[7]),
        .I2(\Out_1_reg[13] [7]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[8]_i_1 
       (.I0(Q[8]),
        .I1(data_int[8]),
        .I2(\Out_1_reg[13] [8]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[9]_i_1 
       (.I0(Q[9]),
        .I1(data_int[9]),
        .I2(\Out_1_reg[13] [9]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[0]_i_1 
       (.I0(data_int[0]),
        .I1(\Out_1_reg[13] [0]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[10]_i_1 
       (.I0(data_int[10]),
        .I1(\Out_1_reg[13] [10]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [10]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[11]_i_1 
       (.I0(data_int[11]),
        .I1(\Out_1_reg[13] [11]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [11]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[12]_i_1 
       (.I0(data_int[12]),
        .I1(\Out_1_reg[13] [12]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [12]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[13]_i_2 
       (.I0(data_int[13]),
        .I1(\Out_1_reg[13] [13]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [13]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[1]_i_1 
       (.I0(data_int[1]),
        .I1(\Out_1_reg[13] [1]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[2]_i_1 
       (.I0(data_int[2]),
        .I1(\Out_1_reg[13] [2]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[3]_i_1 
       (.I0(data_int[3]),
        .I1(\Out_1_reg[13] [3]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[4]_i_1 
       (.I0(data_int[4]),
        .I1(\Out_1_reg[13] [4]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[5]_i_1 
       (.I0(data_int[5]),
        .I1(\Out_1_reg[13] [5]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[6]_i_1 
       (.I0(data_int[6]),
        .I1(\Out_1_reg[13] [6]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[7]_i_1 
       (.I0(data_int[7]),
        .I1(\Out_1_reg[13] [7]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[8]_i_1 
       (.I0(data_int[8]),
        .I1(\Out_1_reg[13] [8]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[9]_i_1 
       (.I0(data_int[9]),
        .I1(\Out_1_reg[13] [9]),
        .I2(w_d1),
        .O(\data_int_reg[13]_0 [9]));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[0]),
        .Q(data_int[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[10] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[10]),
        .Q(data_int[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[11] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[11]),
        .Q(data_int[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[12] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[12]),
        .Q(data_int[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[13] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[13]),
        .Q(data_int[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[1]),
        .Q(data_int[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[2]),
        .Q(data_int[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[3] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[3]),
        .Q(data_int[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[4] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[4]),
        .Q(data_int[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[5] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[5]),
        .Q(data_int[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[6] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[6]),
        .Q(data_int[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[7] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[7]),
        .Q(data_int[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[8] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[8]),
        .Q(data_int[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[9] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0[9]),
        .Q(data_int[9]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_0_5
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[1:0]),
        .DIB(AXI4_Stream_Slave_TDATA[3:2]),
        .DIC(AXI4_Stream_Slave_TDATA[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(data_int0[1:0]),
        .DOB(data_int0[3:2]),
        .DOC(data_int0[5:4]),
        .DOD(NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
  LUT4 #(
    .INIT(16'hAA8A)) 
    ram_reg_0_3_0_5_i_1
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\data_int_reg[1]_0 ),
        .I2(\data_int_reg[1]_1 ),
        .I3(\data_int_reg[1]_2 ),
        .O(w_we));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "15" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_12_15
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[13:12]),
        .DIB(AXI4_Stream_Slave_TDATA[15:14]),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(data_int0[13:12]),
        .DOB({ram_reg_0_3_12_15_n_2,ram_reg_0_3_12_15_n_3}),
        .DOC(NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_6_11
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[7:6]),
        .DIB(AXI4_Stream_Slave_TDATA[9:8]),
        .DIC(AXI4_Stream_Slave_TDATA[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(data_int0[7:6]),
        .DOB(data_int0[9:8]),
        .DOC(data_int0[11:10]),
        .DOD(NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
endmodule

(* ORIG_REF_NAME = "mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0
   (D,
    data_int,
    \data_int_reg[15]_0 ,
    w_we,
    Q,
    \Out_1_reg[15] ,
    w_d1,
    cache_valid,
    auto_ready_dut_enb,
    out_valid,
    fifo_num,
    IPCORE_CLK,
    p26y_out_add_temp,
    ADDRA,
    ADDRD);
  output [15:0]D;
  output [15:0]data_int;
  output [15:0]\data_int_reg[15]_0 ;
  output w_we;
  input [15:0]Q;
  input [15:0]\Out_1_reg[15] ;
  input w_d1;
  input cache_valid;
  input auto_ready_dut_enb;
  input out_valid;
  input [2:0]fifo_num;
  input IPCORE_CLK;
  input [13:0]p26y_out_add_temp;
  input [1:0]ADDRA;
  input [1:0]ADDRD;

  wire [1:0]ADDRA;
  wire [1:0]ADDRD;
  wire [15:0]D;
  wire IPCORE_CLK;
  wire [15:0]\Out_1_reg[15] ;
  wire [15:0]Q;
  wire auto_ready_dut_enb;
  wire cache_valid;
  wire [15:0]data_int;
  wire [15:0]data_int0__0;
  wire [15:0]\data_int_reg[15]_0 ;
  wire [2:0]fifo_num;
  wire out_valid;
  wire [13:0]p26y_out_add_temp;
  wire w_d1;
  wire w_we;
  wire [1:0]NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[0]_i_1__0 
       (.I0(Q[0]),
        .I1(data_int[0]),
        .I2(\Out_1_reg[15] [0]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[10]_i_1__0 
       (.I0(Q[10]),
        .I1(data_int[10]),
        .I2(\Out_1_reg[15] [10]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[11]_i_1__0 
       (.I0(Q[11]),
        .I1(data_int[11]),
        .I2(\Out_1_reg[15] [11]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[12]_i_1__0 
       (.I0(Q[12]),
        .I1(data_int[12]),
        .I2(\Out_1_reg[15] [12]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[13]_i_1__0 
       (.I0(Q[13]),
        .I1(data_int[13]),
        .I2(\Out_1_reg[15] [13]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[14]_i_1 
       (.I0(Q[14]),
        .I1(data_int[14]),
        .I2(\Out_1_reg[15] [14]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[15]_i_2 
       (.I0(Q[15]),
        .I1(data_int[15]),
        .I2(\Out_1_reg[15] [15]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[1]_i_1__0 
       (.I0(Q[1]),
        .I1(data_int[1]),
        .I2(\Out_1_reg[15] [1]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[2]_i_1__0 
       (.I0(Q[2]),
        .I1(data_int[2]),
        .I2(\Out_1_reg[15] [2]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[3]_i_1__0 
       (.I0(Q[3]),
        .I1(data_int[3]),
        .I2(\Out_1_reg[15] [3]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[4]_i_1__0 
       (.I0(Q[4]),
        .I1(data_int[4]),
        .I2(\Out_1_reg[15] [4]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[5]_i_1__0 
       (.I0(Q[5]),
        .I1(data_int[5]),
        .I2(\Out_1_reg[15] [5]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[6]_i_1__0 
       (.I0(Q[6]),
        .I1(data_int[6]),
        .I2(\Out_1_reg[15] [6]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[7]_i_1__0 
       (.I0(Q[7]),
        .I1(data_int[7]),
        .I2(\Out_1_reg[15] [7]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[8]_i_1__0 
       (.I0(Q[8]),
        .I1(data_int[8]),
        .I2(\Out_1_reg[15] [8]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_1[9]_i_1__0 
       (.I0(Q[9]),
        .I1(data_int[9]),
        .I2(\Out_1_reg[15] [9]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[0]_i_1__0 
       (.I0(data_int[0]),
        .I1(\Out_1_reg[15] [0]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[10]_i_1__0 
       (.I0(data_int[10]),
        .I1(\Out_1_reg[15] [10]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [10]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[11]_i_1__0 
       (.I0(data_int[11]),
        .I1(\Out_1_reg[15] [11]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [11]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[12]_i_1__0 
       (.I0(data_int[12]),
        .I1(\Out_1_reg[15] [12]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [12]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[13]_i_1__0 
       (.I0(data_int[13]),
        .I1(\Out_1_reg[15] [13]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [13]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[14]_i_1 
       (.I0(data_int[14]),
        .I1(\Out_1_reg[15] [14]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [14]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[15]_i_2 
       (.I0(data_int[15]),
        .I1(\Out_1_reg[15] [15]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [15]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[1]_i_1__0 
       (.I0(data_int[1]),
        .I1(\Out_1_reg[15] [1]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[2]_i_1__0 
       (.I0(data_int[2]),
        .I1(\Out_1_reg[15] [2]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[3]_i_1__0 
       (.I0(data_int[3]),
        .I1(\Out_1_reg[15] [3]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[4]_i_1__0 
       (.I0(data_int[4]),
        .I1(\Out_1_reg[15] [4]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[5]_i_1__0 
       (.I0(data_int[5]),
        .I1(\Out_1_reg[15] [5]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[6]_i_1__0 
       (.I0(data_int[6]),
        .I1(\Out_1_reg[15] [6]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[7]_i_1__0 
       (.I0(data_int[7]),
        .I1(\Out_1_reg[15] [7]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[8]_i_1__0 
       (.I0(data_int[8]),
        .I1(\Out_1_reg[15] [8]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[9]_i_1__0 
       (.I0(data_int[9]),
        .I1(\Out_1_reg[15] [9]),
        .I2(w_d1),
        .O(\data_int_reg[15]_0 [9]));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[0]),
        .Q(data_int[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[10] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[10]),
        .Q(data_int[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[11] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[11]),
        .Q(data_int[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[12] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[12]),
        .Q(data_int[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[13] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[13]),
        .Q(data_int[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[14] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[14]),
        .Q(data_int[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[15] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[15]),
        .Q(data_int[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[1]),
        .Q(data_int[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[2]),
        .Q(data_int[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[3] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[3]),
        .Q(data_int[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[4] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[4]),
        .Q(data_int[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[5] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[5]),
        .Q(data_int[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[6] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[6]),
        .Q(data_int[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[7] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[7]),
        .Q(data_int[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[8] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[8]),
        .Q(data_int[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[9] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__0[9]),
        .Q(data_int[9]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_0_5
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(p26y_out_add_temp[1:0]),
        .DIB(p26y_out_add_temp[3:2]),
        .DIC(p26y_out_add_temp[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(data_int0__0[1:0]),
        .DOB(data_int0__0[3:2]),
        .DOC(data_int0__0[5:4]),
        .DOD(NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
  LUT5 #(
    .INIT(32'h88888088)) 
    ram_reg_0_3_0_5_i_1__0
       (.I0(auto_ready_dut_enb),
        .I1(out_valid),
        .I2(fifo_num[0]),
        .I3(fifo_num[2]),
        .I4(fifo_num[1]),
        .O(w_we));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "15" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_12_15
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(p26y_out_add_temp[13:12]),
        .DIB({p26y_out_add_temp[13],p26y_out_add_temp[13]}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(data_int0__0[13:12]),
        .DOB(data_int0__0[15:14]),
        .DOC(NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_6_11
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(p26y_out_add_temp[7:6]),
        .DIB(p26y_out_add_temp[9:8]),
        .DIC(p26y_out_add_temp[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(data_int0__0[7:6]),
        .DOB(data_int0__0[9:8]),
        .DOC(data_int0__0[11:10]),
        .DOD(NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_singlebit
   (S,
    \tlast_counter_out_reg[21] ,
    \tlast_counter_out_reg[30] ,
    w_we,
    data_int_reg_0,
    cache_data_reg,
    fifo_data_out,
    tlast_counter_out_reg,
    Q,
    tlast_size_value,
    auto_ready_dut_enb,
    out_valid,
    data_int_reg_1,
    data_int_reg_2,
    data_int_reg_3,
    w_d1,
    w_d2,
    cache_wr_en,
    Out_1_reg,
    cache_valid,
    out_wr_en,
    AXI4_Stream_Master_TLAST,
    IPCORE_CLK,
    auto_tlast,
    wr_addr,
    rd_addr);
  output [3:0]S;
  output [3:0]\tlast_counter_out_reg[21] ;
  output [2:0]\tlast_counter_out_reg[30] ;
  output w_we;
  output data_int_reg_0;
  output cache_data_reg;
  output fifo_data_out;
  input [31:0]tlast_counter_out_reg;
  input [0:0]Q;
  input [30:0]tlast_size_value;
  input auto_ready_dut_enb;
  input out_valid;
  input data_int_reg_1;
  input data_int_reg_2;
  input data_int_reg_3;
  input w_d1;
  input w_d2;
  input cache_wr_en;
  input Out_1_reg;
  input cache_valid;
  input out_wr_en;
  input AXI4_Stream_Master_TLAST;
  input IPCORE_CLK;
  input auto_tlast;
  input [1:0]wr_addr;
  input [1:0]rd_addr;

  wire AXI4_Stream_Master_TLAST;
  wire IPCORE_CLK;
  wire Out_1_reg;
  wire [0:0]Q;
  wire [3:0]S;
  wire auto_ready_dut_enb;
  wire auto_tlast;
  wire cache_data_reg;
  wire cache_valid;
  wire cache_wr_en;
  wire data_int0__1;
  wire data_int_reg_0;
  wire data_int_reg_1;
  wire data_int_reg_2;
  wire data_int_reg_3;
  wire fifo_data_out;
  wire out_valid;
  wire out_wr_en;
  wire [1:0]rd_addr;
  wire [31:0]tlast_counter_out_reg;
  wire [3:0]\tlast_counter_out_reg[21] ;
  wire [2:0]\tlast_counter_out_reg[30] ;
  wire [30:0]tlast_size_value;
  wire w_d1;
  wire w_d2;
  wire w_waddr_1;
  wire w_we;
  wire [1:0]wr_addr;
  wire NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED;

  LUT5 #(
    .INIT(32'hACFFAC00)) 
    Out_1_i_1
       (.I0(Out_1_reg),
        .I1(fifo_data_out),
        .I2(cache_valid),
        .I3(out_wr_en),
        .I4(AXI4_Stream_Master_TLAST),
        .O(cache_data_reg));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    cache_data_i_1
       (.I0(w_waddr_1),
        .I1(w_d1),
        .I2(w_d2),
        .I3(cache_wr_en),
        .I4(Out_1_reg),
        .O(data_int_reg_0));
  FDRE #(
    .INIT(1'b0)) 
    data_int_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(data_int0__1),
        .Q(w_waddr_1),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAM32X1D #(
    .INIT(32'h00000000)) 
    ram_reg_0_3_0_0
       (.A0(wr_addr[0]),
        .A1(wr_addr[1]),
        .A2(1'b0),
        .A3(1'b0),
        .A4(1'b0),
        .D(auto_tlast),
        .DPO(data_int0__1),
        .DPRA0(rd_addr[0]),
        .DPRA1(rd_addr[1]),
        .DPRA2(1'b0),
        .DPRA3(1'b0),
        .DPRA4(1'b0),
        .SPO(NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED),
        .WCLK(IPCORE_CLK),
        .WE(w_we));
  LUT5 #(
    .INIT(32'h88888088)) 
    ram_reg_0_3_0_0_i_2
       (.I0(auto_ready_dut_enb),
        .I1(out_valid),
        .I2(data_int_reg_1),
        .I3(data_int_reg_2),
        .I4(data_int_reg_3),
        .O(w_we));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry__0_i_1
       (.I0(tlast_counter_out_reg[21]),
        .I1(tlast_size_value[20]),
        .I2(tlast_size_value[22]),
        .I3(tlast_counter_out_reg[23]),
        .I4(tlast_size_value[21]),
        .I5(tlast_counter_out_reg[22]),
        .O(\tlast_counter_out_reg[21] [3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry__0_i_2
       (.I0(tlast_counter_out_reg[18]),
        .I1(tlast_size_value[17]),
        .I2(tlast_size_value[19]),
        .I3(tlast_counter_out_reg[20]),
        .I4(tlast_size_value[18]),
        .I5(tlast_counter_out_reg[19]),
        .O(\tlast_counter_out_reg[21] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry__0_i_3
       (.I0(tlast_counter_out_reg[15]),
        .I1(tlast_size_value[14]),
        .I2(tlast_size_value[16]),
        .I3(tlast_counter_out_reg[17]),
        .I4(tlast_size_value[15]),
        .I5(tlast_counter_out_reg[16]),
        .O(\tlast_counter_out_reg[21] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry__0_i_4
       (.I0(tlast_counter_out_reg[12]),
        .I1(tlast_size_value[11]),
        .I2(tlast_size_value[13]),
        .I3(tlast_counter_out_reg[14]),
        .I4(tlast_size_value[12]),
        .I5(tlast_counter_out_reg[13]),
        .O(\tlast_counter_out_reg[21] [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    relop_relop1_carry__1_i_1
       (.I0(tlast_counter_out_reg[30]),
        .I1(tlast_size_value[29]),
        .I2(tlast_counter_out_reg[31]),
        .I3(tlast_size_value[30]),
        .O(\tlast_counter_out_reg[30] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry__1_i_2
       (.I0(tlast_counter_out_reg[27]),
        .I1(tlast_size_value[26]),
        .I2(tlast_size_value[28]),
        .I3(tlast_counter_out_reg[29]),
        .I4(tlast_size_value[27]),
        .I5(tlast_counter_out_reg[28]),
        .O(\tlast_counter_out_reg[30] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry__1_i_3
       (.I0(tlast_counter_out_reg[24]),
        .I1(tlast_size_value[23]),
        .I2(tlast_size_value[25]),
        .I3(tlast_counter_out_reg[26]),
        .I4(tlast_size_value[24]),
        .I5(tlast_counter_out_reg[25]),
        .O(\tlast_counter_out_reg[30] [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry_i_1
       (.I0(tlast_counter_out_reg[9]),
        .I1(tlast_size_value[8]),
        .I2(tlast_size_value[10]),
        .I3(tlast_counter_out_reg[11]),
        .I4(tlast_size_value[9]),
        .I5(tlast_counter_out_reg[10]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry_i_2
       (.I0(tlast_counter_out_reg[6]),
        .I1(tlast_size_value[5]),
        .I2(tlast_size_value[7]),
        .I3(tlast_counter_out_reg[8]),
        .I4(tlast_size_value[6]),
        .I5(tlast_counter_out_reg[7]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    relop_relop1_carry_i_3
       (.I0(tlast_counter_out_reg[3]),
        .I1(tlast_size_value[2]),
        .I2(tlast_size_value[4]),
        .I3(tlast_counter_out_reg[5]),
        .I4(tlast_size_value[3]),
        .I5(tlast_counter_out_reg[4]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    relop_relop1_carry_i_4
       (.I0(tlast_counter_out_reg[0]),
        .I1(Q),
        .I2(tlast_size_value[1]),
        .I3(tlast_counter_out_reg[2]),
        .I4(tlast_size_value[0]),
        .I5(tlast_counter_out_reg[1]),
        .O(S[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    w_d2_i_1
       (.I0(w_waddr_1),
        .I1(w_d1),
        .I2(w_d2),
        .O(fifo_data_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_addr_decoder
   (read_reg_ip_timestamp,
    write_axi_enable,
    E,
    \write_reg_packet_size_axi4_stream_master_reg[31]_0 ,
    Q,
    \write_reg_packet_size_axi4_stream_master_reg[28]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[24]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[20]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[16]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[12]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[8]_0 ,
    S,
    \write_reg_h_in4_reg[13]_0 ,
    \write_reg_h_in3_reg[13]_0 ,
    \write_reg_h_in2_reg[13]_0 ,
    \write_reg_h_in1_reg[13]_0 ,
    AXI4_Lite_ACLK,
    AR,
    write_reg_axi_enable_reg_0,
    AS,
    auto_ready_dut_enb,
    \write_reg_h_in4_reg[13]_1 ,
    \write_reg_packet_size_axi4_stream_master_reg[31]_1 ,
    \write_reg_h_in1_reg[0]_0 ,
    \write_reg_h_in3_reg[13]_1 ,
    \write_reg_h_in2_reg[13]_1 ,
    \write_reg_h_in1_reg[13]_1 ,
    \write_reg_packet_size_axi4_stream_master_reg[31]_2 );
  output [0:0]read_reg_ip_timestamp;
  output write_axi_enable;
  output [0:0]E;
  output [2:0]\write_reg_packet_size_axi4_stream_master_reg[31]_0 ;
  output [30:0]Q;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[28]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[24]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[20]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[16]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[12]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[8]_0 ;
  output [3:0]S;
  output [13:0]\write_reg_h_in4_reg[13]_0 ;
  output [13:0]\write_reg_h_in3_reg[13]_0 ;
  output [13:0]\write_reg_h_in2_reg[13]_0 ;
  output [13:0]\write_reg_h_in1_reg[13]_0 ;
  input AXI4_Lite_ACLK;
  input [0:0]AR;
  input write_reg_axi_enable_reg_0;
  input [0:0]AS;
  input auto_ready_dut_enb;
  input [0:0]\write_reg_h_in4_reg[13]_1 ;
  input [31:0]\write_reg_packet_size_axi4_stream_master_reg[31]_1 ;
  input [0:0]\write_reg_h_in1_reg[0]_0 ;
  input [0:0]\write_reg_h_in3_reg[13]_1 ;
  input [0:0]\write_reg_h_in2_reg[13]_1 ;
  input [0:0]\write_reg_h_in1_reg[13]_1 ;
  input [0:0]\write_reg_packet_size_axi4_stream_master_reg[31]_2 ;

  wire [0:0]AR;
  wire [0:0]AS;
  wire AXI4_Lite_ACLK;
  wire [0:0]E;
  wire [30:0]Q;
  wire [3:0]S;
  wire auto_ready_dut_enb;
  wire [0:0]read_reg_ip_timestamp;
  wire write_axi_enable;
  wire write_reg_axi_enable_reg_0;
  wire [0:0]\write_reg_h_in1_reg[0]_0 ;
  wire [13:0]\write_reg_h_in1_reg[13]_0 ;
  wire [0:0]\write_reg_h_in1_reg[13]_1 ;
  wire [13:0]\write_reg_h_in2_reg[13]_0 ;
  wire [0:0]\write_reg_h_in2_reg[13]_1 ;
  wire [13:0]\write_reg_h_in3_reg[13]_0 ;
  wire [0:0]\write_reg_h_in3_reg[13]_1 ;
  wire [13:0]\write_reg_h_in4_reg[13]_0 ;
  wire [0:0]\write_reg_h_in4_reg[13]_1 ;
  wire [31:31]write_reg_packet_size_axi4_stream_master;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[12]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[16]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[20]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[24]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[28]_0 ;
  wire [2:0]\write_reg_packet_size_axi4_stream_master_reg[31]_0 ;
  wire [31:0]\write_reg_packet_size_axi4_stream_master_reg[31]_1 ;
  wire [0:0]\write_reg_packet_size_axi4_stream_master_reg[31]_2 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[8]_0 ;

  FDCE \read_reg_ip_timestamp_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(1'b1),
        .Q(read_reg_ip_timestamp));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_1
       (.I0(Q[8]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_2
       (.I0(Q[7]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_3
       (.I0(Q[6]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_4
       (.I0(Q[5]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_1
       (.I0(Q[12]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_2
       (.I0(Q[11]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_3
       (.I0(Q[10]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_4
       (.I0(Q[9]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_1
       (.I0(Q[16]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_2
       (.I0(Q[15]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_3
       (.I0(Q[14]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_4
       (.I0(Q[13]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_1
       (.I0(Q[20]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_2
       (.I0(Q[19]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_3
       (.I0(Q[18]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_4
       (.I0(Q[17]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_1
       (.I0(Q[24]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_2
       (.I0(Q[23]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_3
       (.I0(Q[22]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_4
       (.I0(Q[21]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_1
       (.I0(Q[28]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_2
       (.I0(Q[27]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_3
       (.I0(Q[26]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_4
       (.I0(Q[25]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__6_i_1
       (.I0(write_reg_packet_size_axi4_stream_master),
        .O(\write_reg_packet_size_axi4_stream_master_reg[31]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__6_i_2
       (.I0(Q[30]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[31]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__6_i_3
       (.I0(Q[29]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[31]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_1
       (.I0(Q[4]),
        .O(S[3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_2
       (.I0(Q[3]),
        .O(S[2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_3
       (.I0(Q[2]),
        .O(S[1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_4
       (.I0(Q[1]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \ud8[13]_i_1 
       (.I0(write_axi_enable),
        .I1(auto_ready_dut_enb),
        .O(E));
  FDPE write_reg_axi_enable_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .D(write_reg_axi_enable_reg_0),
        .PRE(AS),
        .Q(write_axi_enable));
  FDCE \write_reg_h_in1_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [0]),
        .Q(\write_reg_h_in1_reg[13]_0 [0]));
  FDCE \write_reg_h_in1_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [10]),
        .Q(\write_reg_h_in1_reg[13]_0 [10]));
  FDCE \write_reg_h_in1_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [11]),
        .Q(\write_reg_h_in1_reg[13]_0 [11]));
  FDCE \write_reg_h_in1_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [12]),
        .Q(\write_reg_h_in1_reg[13]_0 [12]));
  FDCE \write_reg_h_in1_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [13]),
        .Q(\write_reg_h_in1_reg[13]_0 [13]));
  FDCE \write_reg_h_in1_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [1]),
        .Q(\write_reg_h_in1_reg[13]_0 [1]));
  FDCE \write_reg_h_in1_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [2]),
        .Q(\write_reg_h_in1_reg[13]_0 [2]));
  FDCE \write_reg_h_in1_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [3]),
        .Q(\write_reg_h_in1_reg[13]_0 [3]));
  FDCE \write_reg_h_in1_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [4]),
        .Q(\write_reg_h_in1_reg[13]_0 [4]));
  FDCE \write_reg_h_in1_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [5]),
        .Q(\write_reg_h_in1_reg[13]_0 [5]));
  FDCE \write_reg_h_in1_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [6]),
        .Q(\write_reg_h_in1_reg[13]_0 [6]));
  FDCE \write_reg_h_in1_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [7]),
        .Q(\write_reg_h_in1_reg[13]_0 [7]));
  FDCE \write_reg_h_in1_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [8]),
        .Q(\write_reg_h_in1_reg[13]_0 [8]));
  FDCE \write_reg_h_in1_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in1_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [9]),
        .Q(\write_reg_h_in1_reg[13]_0 [9]));
  FDCE \write_reg_h_in2_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [0]),
        .Q(\write_reg_h_in2_reg[13]_0 [0]));
  FDCE \write_reg_h_in2_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [10]),
        .Q(\write_reg_h_in2_reg[13]_0 [10]));
  FDCE \write_reg_h_in2_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [11]),
        .Q(\write_reg_h_in2_reg[13]_0 [11]));
  FDCE \write_reg_h_in2_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [12]),
        .Q(\write_reg_h_in2_reg[13]_0 [12]));
  FDCE \write_reg_h_in2_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [13]),
        .Q(\write_reg_h_in2_reg[13]_0 [13]));
  FDCE \write_reg_h_in2_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [1]),
        .Q(\write_reg_h_in2_reg[13]_0 [1]));
  FDCE \write_reg_h_in2_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [2]),
        .Q(\write_reg_h_in2_reg[13]_0 [2]));
  FDCE \write_reg_h_in2_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [3]),
        .Q(\write_reg_h_in2_reg[13]_0 [3]));
  FDCE \write_reg_h_in2_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [4]),
        .Q(\write_reg_h_in2_reg[13]_0 [4]));
  FDCE \write_reg_h_in2_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [5]),
        .Q(\write_reg_h_in2_reg[13]_0 [5]));
  FDCE \write_reg_h_in2_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [6]),
        .Q(\write_reg_h_in2_reg[13]_0 [6]));
  FDCE \write_reg_h_in2_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [7]),
        .Q(\write_reg_h_in2_reg[13]_0 [7]));
  FDCE \write_reg_h_in2_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [8]),
        .Q(\write_reg_h_in2_reg[13]_0 [8]));
  FDCE \write_reg_h_in2_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in2_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [9]),
        .Q(\write_reg_h_in2_reg[13]_0 [9]));
  FDCE \write_reg_h_in3_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [0]),
        .Q(\write_reg_h_in3_reg[13]_0 [0]));
  FDCE \write_reg_h_in3_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [10]),
        .Q(\write_reg_h_in3_reg[13]_0 [10]));
  FDCE \write_reg_h_in3_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [11]),
        .Q(\write_reg_h_in3_reg[13]_0 [11]));
  FDCE \write_reg_h_in3_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [12]),
        .Q(\write_reg_h_in3_reg[13]_0 [12]));
  FDCE \write_reg_h_in3_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [13]),
        .Q(\write_reg_h_in3_reg[13]_0 [13]));
  FDCE \write_reg_h_in3_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [1]),
        .Q(\write_reg_h_in3_reg[13]_0 [1]));
  FDCE \write_reg_h_in3_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [2]),
        .Q(\write_reg_h_in3_reg[13]_0 [2]));
  FDCE \write_reg_h_in3_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [3]),
        .Q(\write_reg_h_in3_reg[13]_0 [3]));
  FDCE \write_reg_h_in3_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [4]),
        .Q(\write_reg_h_in3_reg[13]_0 [4]));
  FDCE \write_reg_h_in3_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [5]),
        .Q(\write_reg_h_in3_reg[13]_0 [5]));
  FDCE \write_reg_h_in3_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [6]),
        .Q(\write_reg_h_in3_reg[13]_0 [6]));
  FDCE \write_reg_h_in3_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [7]),
        .Q(\write_reg_h_in3_reg[13]_0 [7]));
  FDCE \write_reg_h_in3_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [8]),
        .Q(\write_reg_h_in3_reg[13]_0 [8]));
  FDCE \write_reg_h_in3_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in3_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [9]),
        .Q(\write_reg_h_in3_reg[13]_0 [9]));
  FDCE \write_reg_h_in4_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [0]),
        .Q(\write_reg_h_in4_reg[13]_0 [0]));
  FDCE \write_reg_h_in4_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [10]),
        .Q(\write_reg_h_in4_reg[13]_0 [10]));
  FDCE \write_reg_h_in4_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [11]),
        .Q(\write_reg_h_in4_reg[13]_0 [11]));
  FDCE \write_reg_h_in4_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [12]),
        .Q(\write_reg_h_in4_reg[13]_0 [12]));
  FDCE \write_reg_h_in4_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [13]),
        .Q(\write_reg_h_in4_reg[13]_0 [13]));
  FDCE \write_reg_h_in4_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [1]),
        .Q(\write_reg_h_in4_reg[13]_0 [1]));
  FDCE \write_reg_h_in4_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [2]),
        .Q(\write_reg_h_in4_reg[13]_0 [2]));
  FDCE \write_reg_h_in4_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [3]),
        .Q(\write_reg_h_in4_reg[13]_0 [3]));
  FDCE \write_reg_h_in4_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [4]),
        .Q(\write_reg_h_in4_reg[13]_0 [4]));
  FDCE \write_reg_h_in4_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [5]),
        .Q(\write_reg_h_in4_reg[13]_0 [5]));
  FDCE \write_reg_h_in4_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [6]),
        .Q(\write_reg_h_in4_reg[13]_0 [6]));
  FDCE \write_reg_h_in4_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [7]),
        .Q(\write_reg_h_in4_reg[13]_0 [7]));
  FDCE \write_reg_h_in4_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [8]),
        .Q(\write_reg_h_in4_reg[13]_0 [8]));
  FDCE \write_reg_h_in4_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_h_in4_reg[13]_1 ),
        .CLR(\write_reg_h_in1_reg[0]_0 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [9]),
        .Q(\write_reg_h_in4_reg[13]_0 [9]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [0]),
        .Q(Q[0]));
  FDPE \write_reg_packet_size_axi4_stream_master_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [10]),
        .PRE(AS),
        .Q(Q[10]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [11]),
        .Q(Q[11]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [12]),
        .Q(Q[12]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [13]),
        .Q(Q[13]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [14]),
        .Q(Q[14]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [15]),
        .Q(Q[15]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [16]),
        .Q(Q[16]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [17]),
        .Q(Q[17]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [18]),
        .Q(Q[18]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [19]),
        .Q(Q[19]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [1]),
        .Q(Q[1]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [20]),
        .Q(Q[20]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [21]),
        .Q(Q[21]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [22]),
        .Q(Q[22]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [23]),
        .Q(Q[23]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [24]),
        .Q(Q[24]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [25]),
        .Q(Q[25]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [26]),
        .Q(Q[26]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [27]),
        .Q(Q[27]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [28]),
        .Q(Q[28]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [29]),
        .Q(Q[29]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [2]),
        .Q(Q[2]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [30]),
        .Q(Q[30]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[31] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [31]),
        .Q(write_reg_packet_size_axi4_stream_master));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [3]),
        .Q(Q[3]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [4]),
        .Q(Q[4]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [5]),
        .Q(Q[5]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [6]),
        .Q(Q[6]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [7]),
        .Q(Q[7]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [8]),
        .Q(Q[8]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(\write_reg_packet_size_axi4_stream_master_reg[31]_2 ),
        .CLR(AS),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [9]),
        .Q(Q[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master
   (out_valid_reg,
    CO,
    AXI4_Stream_Master_TLAST,
    auto_ready_axi4_stream_master,
    AXI4_Stream_Master_TDATA,
    IPCORE_CLK,
    reset,
    \tlast_counter_out_reg[31]_0 ,
    Q,
    S,
    relop_relop1_carry_i_3,
    relop_relop1_carry_i_1,
    relop_relop1_carry__0_i_4,
    relop_relop1_carry__0_i_3,
    relop_relop1_carry__0_i_1,
    relop_relop1_carry__1_i_3,
    relop_relop1_carry__1_i_2,
    auto_ready_dut_enb,
    out_valid,
    AXI4_Stream_Master_TREADY,
    p26y_out_add_temp,
    AR,
    auto_tlast);
  output out_valid_reg;
  output [0:0]CO;
  output AXI4_Stream_Master_TLAST;
  output auto_ready_axi4_stream_master;
  output [15:0]AXI4_Stream_Master_TDATA;
  input IPCORE_CLK;
  input reset;
  input \tlast_counter_out_reg[31]_0 ;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]relop_relop1_carry_i_3;
  input [3:0]relop_relop1_carry_i_1;
  input [3:0]relop_relop1_carry__0_i_4;
  input [3:0]relop_relop1_carry__0_i_3;
  input [3:0]relop_relop1_carry__0_i_1;
  input [3:0]relop_relop1_carry__1_i_3;
  input [2:0]relop_relop1_carry__1_i_2;
  input auto_ready_dut_enb;
  input out_valid;
  input AXI4_Stream_Master_TREADY;
  input [13:0]p26y_out_add_temp;
  input [0:0]AR;
  input auto_tlast;

  wire [0:0]AR;
  wire [15:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire [0:0]CO;
  wire IPCORE_CLK;
  wire [30:0]Q;
  wire [3:0]S;
  wire auto_ready_axi4_stream_master;
  wire auto_ready_dut_enb;
  wire auto_tlast;
  wire out_valid;
  wire out_valid_reg;
  wire [13:0]p26y_out_add_temp;
  wire [3:0]relop_relop1_carry__0_i_1;
  wire [3:0]relop_relop1_carry__0_i_3;
  wire [3:0]relop_relop1_carry__0_i_4;
  wire relop_relop1_carry__0_n_0;
  wire relop_relop1_carry__0_n_1;
  wire relop_relop1_carry__0_n_2;
  wire relop_relop1_carry__0_n_3;
  wire [2:0]relop_relop1_carry__1_i_2;
  wire [3:0]relop_relop1_carry__1_i_3;
  wire relop_relop1_carry__1_n_2;
  wire relop_relop1_carry__1_n_3;
  wire [3:0]relop_relop1_carry_i_1;
  wire [3:0]relop_relop1_carry_i_3;
  wire relop_relop1_carry_n_0;
  wire relop_relop1_carry_n_1;
  wire relop_relop1_carry_n_2;
  wire relop_relop1_carry_n_3;
  wire reset;
  wire \tlast_counter_out[0]_i_3_n_0 ;
  wire \tlast_counter_out[0]_i_4_n_0 ;
  wire \tlast_counter_out[0]_i_5_n_0 ;
  wire \tlast_counter_out[0]_i_6_n_0 ;
  wire \tlast_counter_out[0]_i_7_n_0 ;
  wire \tlast_counter_out[12]_i_2_n_0 ;
  wire \tlast_counter_out[12]_i_3_n_0 ;
  wire \tlast_counter_out[12]_i_4_n_0 ;
  wire \tlast_counter_out[12]_i_5_n_0 ;
  wire \tlast_counter_out[16]_i_2_n_0 ;
  wire \tlast_counter_out[16]_i_3_n_0 ;
  wire \tlast_counter_out[16]_i_4_n_0 ;
  wire \tlast_counter_out[16]_i_5_n_0 ;
  wire \tlast_counter_out[20]_i_2_n_0 ;
  wire \tlast_counter_out[20]_i_3_n_0 ;
  wire \tlast_counter_out[20]_i_4_n_0 ;
  wire \tlast_counter_out[20]_i_5_n_0 ;
  wire \tlast_counter_out[24]_i_2_n_0 ;
  wire \tlast_counter_out[24]_i_3_n_0 ;
  wire \tlast_counter_out[24]_i_4_n_0 ;
  wire \tlast_counter_out[24]_i_5_n_0 ;
  wire \tlast_counter_out[28]_i_2_n_0 ;
  wire \tlast_counter_out[28]_i_3_n_0 ;
  wire \tlast_counter_out[28]_i_4_n_0 ;
  wire \tlast_counter_out[28]_i_5_n_0 ;
  wire \tlast_counter_out[4]_i_2_n_0 ;
  wire \tlast_counter_out[4]_i_3_n_0 ;
  wire \tlast_counter_out[4]_i_4_n_0 ;
  wire \tlast_counter_out[4]_i_5_n_0 ;
  wire \tlast_counter_out[8]_i_2_n_0 ;
  wire \tlast_counter_out[8]_i_3_n_0 ;
  wire \tlast_counter_out[8]_i_4_n_0 ;
  wire \tlast_counter_out[8]_i_5_n_0 ;
  wire [31:0]tlast_counter_out_reg;
  wire \tlast_counter_out_reg[0]_i_2_n_0 ;
  wire \tlast_counter_out_reg[0]_i_2_n_1 ;
  wire \tlast_counter_out_reg[0]_i_2_n_2 ;
  wire \tlast_counter_out_reg[0]_i_2_n_3 ;
  wire \tlast_counter_out_reg[0]_i_2_n_4 ;
  wire \tlast_counter_out_reg[0]_i_2_n_5 ;
  wire \tlast_counter_out_reg[0]_i_2_n_6 ;
  wire \tlast_counter_out_reg[0]_i_2_n_7 ;
  wire \tlast_counter_out_reg[12]_i_1_n_0 ;
  wire \tlast_counter_out_reg[12]_i_1_n_1 ;
  wire \tlast_counter_out_reg[12]_i_1_n_2 ;
  wire \tlast_counter_out_reg[12]_i_1_n_3 ;
  wire \tlast_counter_out_reg[12]_i_1_n_4 ;
  wire \tlast_counter_out_reg[12]_i_1_n_5 ;
  wire \tlast_counter_out_reg[12]_i_1_n_6 ;
  wire \tlast_counter_out_reg[12]_i_1_n_7 ;
  wire \tlast_counter_out_reg[16]_i_1_n_0 ;
  wire \tlast_counter_out_reg[16]_i_1_n_1 ;
  wire \tlast_counter_out_reg[16]_i_1_n_2 ;
  wire \tlast_counter_out_reg[16]_i_1_n_3 ;
  wire \tlast_counter_out_reg[16]_i_1_n_4 ;
  wire \tlast_counter_out_reg[16]_i_1_n_5 ;
  wire \tlast_counter_out_reg[16]_i_1_n_6 ;
  wire \tlast_counter_out_reg[16]_i_1_n_7 ;
  wire \tlast_counter_out_reg[20]_i_1_n_0 ;
  wire \tlast_counter_out_reg[20]_i_1_n_1 ;
  wire \tlast_counter_out_reg[20]_i_1_n_2 ;
  wire \tlast_counter_out_reg[20]_i_1_n_3 ;
  wire \tlast_counter_out_reg[20]_i_1_n_4 ;
  wire \tlast_counter_out_reg[20]_i_1_n_5 ;
  wire \tlast_counter_out_reg[20]_i_1_n_6 ;
  wire \tlast_counter_out_reg[20]_i_1_n_7 ;
  wire \tlast_counter_out_reg[24]_i_1_n_0 ;
  wire \tlast_counter_out_reg[24]_i_1_n_1 ;
  wire \tlast_counter_out_reg[24]_i_1_n_2 ;
  wire \tlast_counter_out_reg[24]_i_1_n_3 ;
  wire \tlast_counter_out_reg[24]_i_1_n_4 ;
  wire \tlast_counter_out_reg[24]_i_1_n_5 ;
  wire \tlast_counter_out_reg[24]_i_1_n_6 ;
  wire \tlast_counter_out_reg[24]_i_1_n_7 ;
  wire \tlast_counter_out_reg[28]_i_1_n_1 ;
  wire \tlast_counter_out_reg[28]_i_1_n_2 ;
  wire \tlast_counter_out_reg[28]_i_1_n_3 ;
  wire \tlast_counter_out_reg[28]_i_1_n_4 ;
  wire \tlast_counter_out_reg[28]_i_1_n_5 ;
  wire \tlast_counter_out_reg[28]_i_1_n_6 ;
  wire \tlast_counter_out_reg[28]_i_1_n_7 ;
  wire \tlast_counter_out_reg[31]_0 ;
  wire \tlast_counter_out_reg[4]_i_1_n_0 ;
  wire \tlast_counter_out_reg[4]_i_1_n_1 ;
  wire \tlast_counter_out_reg[4]_i_1_n_2 ;
  wire \tlast_counter_out_reg[4]_i_1_n_3 ;
  wire \tlast_counter_out_reg[4]_i_1_n_4 ;
  wire \tlast_counter_out_reg[4]_i_1_n_5 ;
  wire \tlast_counter_out_reg[4]_i_1_n_6 ;
  wire \tlast_counter_out_reg[4]_i_1_n_7 ;
  wire \tlast_counter_out_reg[8]_i_1_n_0 ;
  wire \tlast_counter_out_reg[8]_i_1_n_1 ;
  wire \tlast_counter_out_reg[8]_i_1_n_2 ;
  wire \tlast_counter_out_reg[8]_i_1_n_3 ;
  wire \tlast_counter_out_reg[8]_i_1_n_4 ;
  wire \tlast_counter_out_reg[8]_i_1_n_5 ;
  wire \tlast_counter_out_reg[8]_i_1_n_6 ;
  wire \tlast_counter_out_reg[8]_i_1_n_7 ;
  wire [31:1]tlast_size_value;
  wire tlast_size_value_carry__0_n_0;
  wire tlast_size_value_carry__0_n_1;
  wire tlast_size_value_carry__0_n_2;
  wire tlast_size_value_carry__0_n_3;
  wire tlast_size_value_carry__1_n_0;
  wire tlast_size_value_carry__1_n_1;
  wire tlast_size_value_carry__1_n_2;
  wire tlast_size_value_carry__1_n_3;
  wire tlast_size_value_carry__2_n_0;
  wire tlast_size_value_carry__2_n_1;
  wire tlast_size_value_carry__2_n_2;
  wire tlast_size_value_carry__2_n_3;
  wire tlast_size_value_carry__3_n_0;
  wire tlast_size_value_carry__3_n_1;
  wire tlast_size_value_carry__3_n_2;
  wire tlast_size_value_carry__3_n_3;
  wire tlast_size_value_carry__4_n_0;
  wire tlast_size_value_carry__4_n_1;
  wire tlast_size_value_carry__4_n_2;
  wire tlast_size_value_carry__4_n_3;
  wire tlast_size_value_carry__5_n_0;
  wire tlast_size_value_carry__5_n_1;
  wire tlast_size_value_carry__5_n_2;
  wire tlast_size_value_carry__5_n_3;
  wire tlast_size_value_carry__6_n_2;
  wire tlast_size_value_carry__6_n_3;
  wire tlast_size_value_carry_n_0;
  wire tlast_size_value_carry_n_1;
  wire tlast_size_value_carry_n_2;
  wire tlast_size_value_carry_n_3;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_1;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_10;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_11;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_2;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_3;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_4;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_5;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_6;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_7;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_8;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_9;
  wire [3:0]NLW_relop_relop1_carry_O_UNCONNECTED;
  wire [3:0]NLW_relop_relop1_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_relop_relop1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_relop_relop1_carry__1_O_UNCONNECTED;
  wire [3:3]\NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]NLW_tlast_size_value_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_tlast_size_value_carry__6_O_UNCONNECTED;

  CARRY4 relop_relop1_carry
       (.CI(1'b0),
        .CO({relop_relop1_carry_n_0,relop_relop1_carry_n_1,relop_relop1_carry_n_2,relop_relop1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_relop_relop1_carry_O_UNCONNECTED[3:0]),
        .S({u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_1,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_2,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_3,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_4}));
  CARRY4 relop_relop1_carry__0
       (.CI(relop_relop1_carry_n_0),
        .CO({relop_relop1_carry__0_n_0,relop_relop1_carry__0_n_1,relop_relop1_carry__0_n_2,relop_relop1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_relop_relop1_carry__0_O_UNCONNECTED[3:0]),
        .S({u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_5,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_6,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_7,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_8}));
  CARRY4 relop_relop1_carry__1
       (.CI(relop_relop1_carry__0_n_0),
        .CO({NLW_relop_relop1_carry__1_CO_UNCONNECTED[3],CO,relop_relop1_carry__1_n_2,relop_relop1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_relop_relop1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_9,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_10,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_11}));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_3 
       (.I0(tlast_counter_out_reg[0]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_4 
       (.I0(tlast_counter_out_reg[3]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_5 
       (.I0(tlast_counter_out_reg[2]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_6 
       (.I0(tlast_counter_out_reg[1]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h1555)) 
    \tlast_counter_out[0]_i_7 
       (.I0(tlast_counter_out_reg[0]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_2 
       (.I0(tlast_counter_out_reg[15]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_3 
       (.I0(tlast_counter_out_reg[14]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_4 
       (.I0(tlast_counter_out_reg[13]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_5 
       (.I0(tlast_counter_out_reg[12]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_2 
       (.I0(tlast_counter_out_reg[19]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_3 
       (.I0(tlast_counter_out_reg[18]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_4 
       (.I0(tlast_counter_out_reg[17]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_5 
       (.I0(tlast_counter_out_reg[16]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_2 
       (.I0(tlast_counter_out_reg[23]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_3 
       (.I0(tlast_counter_out_reg[22]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_4 
       (.I0(tlast_counter_out_reg[21]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_5 
       (.I0(tlast_counter_out_reg[20]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_2 
       (.I0(tlast_counter_out_reg[27]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_3 
       (.I0(tlast_counter_out_reg[26]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_4 
       (.I0(tlast_counter_out_reg[25]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_5 
       (.I0(tlast_counter_out_reg[24]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_2 
       (.I0(tlast_counter_out_reg[31]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_3 
       (.I0(tlast_counter_out_reg[30]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_4 
       (.I0(tlast_counter_out_reg[29]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_5 
       (.I0(tlast_counter_out_reg[28]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_2 
       (.I0(tlast_counter_out_reg[7]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_3 
       (.I0(tlast_counter_out_reg[6]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_4 
       (.I0(tlast_counter_out_reg[5]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_5 
       (.I0(tlast_counter_out_reg[4]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_2 
       (.I0(tlast_counter_out_reg[11]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_3 
       (.I0(tlast_counter_out_reg[10]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_4 
       (.I0(tlast_counter_out_reg[9]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_5 
       (.I0(tlast_counter_out_reg[8]),
        .I1(CO),
        .I2(auto_ready_dut_enb),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_5_n_0 ));
  FDCE \tlast_counter_out_reg[0] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[0]_i_2_n_7 ),
        .Q(tlast_counter_out_reg[0]));
  CARRY4 \tlast_counter_out_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\tlast_counter_out_reg[0]_i_2_n_0 ,\tlast_counter_out_reg[0]_i_2_n_1 ,\tlast_counter_out_reg[0]_i_2_n_2 ,\tlast_counter_out_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tlast_counter_out[0]_i_3_n_0 }),
        .O({\tlast_counter_out_reg[0]_i_2_n_4 ,\tlast_counter_out_reg[0]_i_2_n_5 ,\tlast_counter_out_reg[0]_i_2_n_6 ,\tlast_counter_out_reg[0]_i_2_n_7 }),
        .S({\tlast_counter_out[0]_i_4_n_0 ,\tlast_counter_out[0]_i_5_n_0 ,\tlast_counter_out[0]_i_6_n_0 ,\tlast_counter_out[0]_i_7_n_0 }));
  FDCE \tlast_counter_out_reg[10] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[8]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[10]));
  FDCE \tlast_counter_out_reg[11] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[8]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[11]));
  FDCE \tlast_counter_out_reg[12] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[12]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[12]));
  CARRY4 \tlast_counter_out_reg[12]_i_1 
       (.CI(\tlast_counter_out_reg[8]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[12]_i_1_n_0 ,\tlast_counter_out_reg[12]_i_1_n_1 ,\tlast_counter_out_reg[12]_i_1_n_2 ,\tlast_counter_out_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[12]_i_1_n_4 ,\tlast_counter_out_reg[12]_i_1_n_5 ,\tlast_counter_out_reg[12]_i_1_n_6 ,\tlast_counter_out_reg[12]_i_1_n_7 }),
        .S({\tlast_counter_out[12]_i_2_n_0 ,\tlast_counter_out[12]_i_3_n_0 ,\tlast_counter_out[12]_i_4_n_0 ,\tlast_counter_out[12]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[13] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[12]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[13]));
  FDCE \tlast_counter_out_reg[14] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[12]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[14]));
  FDCE \tlast_counter_out_reg[15] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[12]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[15]));
  FDCE \tlast_counter_out_reg[16] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[16]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[16]));
  CARRY4 \tlast_counter_out_reg[16]_i_1 
       (.CI(\tlast_counter_out_reg[12]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[16]_i_1_n_0 ,\tlast_counter_out_reg[16]_i_1_n_1 ,\tlast_counter_out_reg[16]_i_1_n_2 ,\tlast_counter_out_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[16]_i_1_n_4 ,\tlast_counter_out_reg[16]_i_1_n_5 ,\tlast_counter_out_reg[16]_i_1_n_6 ,\tlast_counter_out_reg[16]_i_1_n_7 }),
        .S({\tlast_counter_out[16]_i_2_n_0 ,\tlast_counter_out[16]_i_3_n_0 ,\tlast_counter_out[16]_i_4_n_0 ,\tlast_counter_out[16]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[17] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[16]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[17]));
  FDCE \tlast_counter_out_reg[18] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[16]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[18]));
  FDCE \tlast_counter_out_reg[19] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[16]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[19]));
  FDCE \tlast_counter_out_reg[1] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[0]_i_2_n_6 ),
        .Q(tlast_counter_out_reg[1]));
  FDCE \tlast_counter_out_reg[20] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[20]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[20]));
  CARRY4 \tlast_counter_out_reg[20]_i_1 
       (.CI(\tlast_counter_out_reg[16]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[20]_i_1_n_0 ,\tlast_counter_out_reg[20]_i_1_n_1 ,\tlast_counter_out_reg[20]_i_1_n_2 ,\tlast_counter_out_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[20]_i_1_n_4 ,\tlast_counter_out_reg[20]_i_1_n_5 ,\tlast_counter_out_reg[20]_i_1_n_6 ,\tlast_counter_out_reg[20]_i_1_n_7 }),
        .S({\tlast_counter_out[20]_i_2_n_0 ,\tlast_counter_out[20]_i_3_n_0 ,\tlast_counter_out[20]_i_4_n_0 ,\tlast_counter_out[20]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[21] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[20]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[21]));
  FDCE \tlast_counter_out_reg[22] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[20]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[22]));
  FDCE \tlast_counter_out_reg[23] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[20]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[23]));
  FDCE \tlast_counter_out_reg[24] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[24]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[24]));
  CARRY4 \tlast_counter_out_reg[24]_i_1 
       (.CI(\tlast_counter_out_reg[20]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[24]_i_1_n_0 ,\tlast_counter_out_reg[24]_i_1_n_1 ,\tlast_counter_out_reg[24]_i_1_n_2 ,\tlast_counter_out_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[24]_i_1_n_4 ,\tlast_counter_out_reg[24]_i_1_n_5 ,\tlast_counter_out_reg[24]_i_1_n_6 ,\tlast_counter_out_reg[24]_i_1_n_7 }),
        .S({\tlast_counter_out[24]_i_2_n_0 ,\tlast_counter_out[24]_i_3_n_0 ,\tlast_counter_out[24]_i_4_n_0 ,\tlast_counter_out[24]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[25] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[24]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[25]));
  FDCE \tlast_counter_out_reg[26] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[24]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[26]));
  FDCE \tlast_counter_out_reg[27] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[24]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[27]));
  FDCE \tlast_counter_out_reg[28] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[28]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[28]));
  CARRY4 \tlast_counter_out_reg[28]_i_1 
       (.CI(\tlast_counter_out_reg[24]_i_1_n_0 ),
        .CO({\NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED [3],\tlast_counter_out_reg[28]_i_1_n_1 ,\tlast_counter_out_reg[28]_i_1_n_2 ,\tlast_counter_out_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[28]_i_1_n_4 ,\tlast_counter_out_reg[28]_i_1_n_5 ,\tlast_counter_out_reg[28]_i_1_n_6 ,\tlast_counter_out_reg[28]_i_1_n_7 }),
        .S({\tlast_counter_out[28]_i_2_n_0 ,\tlast_counter_out[28]_i_3_n_0 ,\tlast_counter_out[28]_i_4_n_0 ,\tlast_counter_out[28]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[29] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[28]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[29]));
  FDCE \tlast_counter_out_reg[2] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[0]_i_2_n_5 ),
        .Q(tlast_counter_out_reg[2]));
  FDCE \tlast_counter_out_reg[30] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[28]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[30]));
  FDCE \tlast_counter_out_reg[31] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[28]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[31]));
  FDCE \tlast_counter_out_reg[3] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[0]_i_2_n_4 ),
        .Q(tlast_counter_out_reg[3]));
  FDCE \tlast_counter_out_reg[4] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[4]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[4]));
  CARRY4 \tlast_counter_out_reg[4]_i_1 
       (.CI(\tlast_counter_out_reg[0]_i_2_n_0 ),
        .CO({\tlast_counter_out_reg[4]_i_1_n_0 ,\tlast_counter_out_reg[4]_i_1_n_1 ,\tlast_counter_out_reg[4]_i_1_n_2 ,\tlast_counter_out_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[4]_i_1_n_4 ,\tlast_counter_out_reg[4]_i_1_n_5 ,\tlast_counter_out_reg[4]_i_1_n_6 ,\tlast_counter_out_reg[4]_i_1_n_7 }),
        .S({\tlast_counter_out[4]_i_2_n_0 ,\tlast_counter_out[4]_i_3_n_0 ,\tlast_counter_out[4]_i_4_n_0 ,\tlast_counter_out[4]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[5] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[4]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[5]));
  FDCE \tlast_counter_out_reg[6] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[4]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[6]));
  FDCE \tlast_counter_out_reg[7] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[4]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[7]));
  FDCE \tlast_counter_out_reg[8] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[8]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[8]));
  CARRY4 \tlast_counter_out_reg[8]_i_1 
       (.CI(\tlast_counter_out_reg[4]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[8]_i_1_n_0 ,\tlast_counter_out_reg[8]_i_1_n_1 ,\tlast_counter_out_reg[8]_i_1_n_2 ,\tlast_counter_out_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[8]_i_1_n_4 ,\tlast_counter_out_reg[8]_i_1_n_5 ,\tlast_counter_out_reg[8]_i_1_n_6 ,\tlast_counter_out_reg[8]_i_1_n_7 }),
        .S({\tlast_counter_out[8]_i_2_n_0 ,\tlast_counter_out[8]_i_3_n_0 ,\tlast_counter_out[8]_i_4_n_0 ,\tlast_counter_out[8]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[9] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(reset),
        .D(\tlast_counter_out_reg[8]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[9]));
  CARRY4 tlast_size_value_carry
       (.CI(1'b0),
        .CO({tlast_size_value_carry_n_0,tlast_size_value_carry_n_1,tlast_size_value_carry_n_2,tlast_size_value_carry_n_3}),
        .CYINIT(Q[0]),
        .DI(Q[4:1]),
        .O(tlast_size_value[4:1]),
        .S(S));
  CARRY4 tlast_size_value_carry__0
       (.CI(tlast_size_value_carry_n_0),
        .CO({tlast_size_value_carry__0_n_0,tlast_size_value_carry__0_n_1,tlast_size_value_carry__0_n_2,tlast_size_value_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O(tlast_size_value[8:5]),
        .S(relop_relop1_carry_i_3));
  CARRY4 tlast_size_value_carry__1
       (.CI(tlast_size_value_carry__0_n_0),
        .CO({tlast_size_value_carry__1_n_0,tlast_size_value_carry__1_n_1,tlast_size_value_carry__1_n_2,tlast_size_value_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O(tlast_size_value[12:9]),
        .S(relop_relop1_carry_i_1));
  CARRY4 tlast_size_value_carry__2
       (.CI(tlast_size_value_carry__1_n_0),
        .CO({tlast_size_value_carry__2_n_0,tlast_size_value_carry__2_n_1,tlast_size_value_carry__2_n_2,tlast_size_value_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[16:13]),
        .O(tlast_size_value[16:13]),
        .S(relop_relop1_carry__0_i_4));
  CARRY4 tlast_size_value_carry__3
       (.CI(tlast_size_value_carry__2_n_0),
        .CO({tlast_size_value_carry__3_n_0,tlast_size_value_carry__3_n_1,tlast_size_value_carry__3_n_2,tlast_size_value_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[20:17]),
        .O(tlast_size_value[20:17]),
        .S(relop_relop1_carry__0_i_3));
  CARRY4 tlast_size_value_carry__4
       (.CI(tlast_size_value_carry__3_n_0),
        .CO({tlast_size_value_carry__4_n_0,tlast_size_value_carry__4_n_1,tlast_size_value_carry__4_n_2,tlast_size_value_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(Q[24:21]),
        .O(tlast_size_value[24:21]),
        .S(relop_relop1_carry__0_i_1));
  CARRY4 tlast_size_value_carry__5
       (.CI(tlast_size_value_carry__4_n_0),
        .CO({tlast_size_value_carry__5_n_0,tlast_size_value_carry__5_n_1,tlast_size_value_carry__5_n_2,tlast_size_value_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(Q[28:25]),
        .O(tlast_size_value[28:25]),
        .S(relop_relop1_carry__1_i_3));
  CARRY4 tlast_size_value_carry__6
       (.CI(tlast_size_value_carry__5_n_0),
        .CO({NLW_tlast_size_value_carry__6_CO_UNCONNECTED[3:2],tlast_size_value_carry__6_n_2,tlast_size_value_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,Q[30:29]}),
        .O({NLW_tlast_size_value_carry__6_O_UNCONNECTED[3],tlast_size_value[31:29]}),
        .S({1'b0,relop_relop1_carry__1_i_2}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst
       (.AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .IPCORE_CLK(IPCORE_CLK),
        .Q(Q[0]),
        .S({u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_1,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_2,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_3,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_4}),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .auto_tlast(auto_tlast),
        .out_valid(out_valid),
        .reset(reset),
        .tlast_counter_out_reg(tlast_counter_out_reg),
        .\tlast_counter_out_reg[21] ({u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_5,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_6,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_7,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_8}),
        .\tlast_counter_out_reg[30] ({u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_9,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_10,u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_11}),
        .tlast_size_value(tlast_size_value));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_inst
       (.AR(AR),
        .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .IPCORE_CLK(IPCORE_CLK),
        .auto_ready_axi4_stream_master(auto_ready_axi4_stream_master),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .out_valid(out_valid),
        .out_valid_reg_0(out_valid_reg),
        .p26y_out_add_temp(p26y_out_add_temp),
        .reset(reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave
   (out_valid,
    auto_ready_dut_enb,
    auto_tlast,
    AXI4_Stream_Slave_TREADY,
    out_valid_reg,
    Q,
    IPCORE_CLK,
    reset,
    auto_ready_axi4_stream_master,
    AXI4_Stream_Slave_TVALID,
    CO,
    AXI4_Stream_Slave_TDATA,
    AR);
  output out_valid;
  output auto_ready_dut_enb;
  output auto_tlast;
  output AXI4_Stream_Slave_TREADY;
  output out_valid_reg;
  output [13:0]Q;
  input IPCORE_CLK;
  input reset;
  input auto_ready_axi4_stream_master;
  input AXI4_Stream_Slave_TVALID;
  input [0:0]CO;
  input [15:0]AXI4_Stream_Slave_TDATA;
  input [0:0]AR;

  wire [0:0]AR;
  wire [15:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire [0:0]CO;
  wire IPCORE_CLK;
  wire [13:0]Q;
  wire auto_ready_axi4_stream_master;
  wire auto_ready_dut_enb;
  wire auto_tlast;
  wire out_valid;
  wire out_valid_reg;
  wire reset;

  FDCE fifo_rd_ack_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(auto_ready_axi4_stream_master),
        .Q(auto_ready_dut_enb));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst
       (.AR(AR),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .CO(CO),
        .IPCORE_CLK(IPCORE_CLK),
        .Q(Q),
        .auto_tlast(auto_tlast),
        .fifo_valid_reg_0(auto_ready_dut_enb),
        .out_valid_reg_0(out_valid),
        .out_valid_reg_1(out_valid_reg),
        .reset(reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite
   (FSM_sequential_axi_lite_rstate_reg,
    AR,
    AXI4_Lite_ARESETN_0,
    AXI4_Lite_RDATA,
    AXI4_Lite_ARREADY,
    AXI4_Lite_AWREADY,
    E,
    \write_reg_packet_size_axi4_stream_master_reg[31] ,
    Q,
    \write_reg_packet_size_axi4_stream_master_reg[28] ,
    \write_reg_packet_size_axi4_stream_master_reg[24] ,
    \write_reg_packet_size_axi4_stream_master_reg[20] ,
    \write_reg_packet_size_axi4_stream_master_reg[16] ,
    \write_reg_packet_size_axi4_stream_master_reg[12] ,
    \write_reg_packet_size_axi4_stream_master_reg[8] ,
    S,
    AXI4_Lite_ARESETN_1,
    \FSM_onehot_axi_lite_wstate_reg[2] ,
    \write_reg_h_in4_reg[13] ,
    \write_reg_h_in3_reg[13] ,
    \write_reg_h_in2_reg[13] ,
    \write_reg_h_in1_reg[13] ,
    AXI4_Lite_ACLK,
    AXI4_Lite_AWVALID,
    AXI4_Lite_ARVALID,
    AXI4_Lite_ARADDR,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN,
    auto_ready_dut_enb,
    AXI4_Lite_WDATA,
    AXI4_Lite_AWADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY);
  output FSM_sequential_axi_lite_rstate_reg;
  output [0:0]AR;
  output [0:0]AXI4_Lite_ARESETN_0;
  output [0:0]AXI4_Lite_RDATA;
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_AWREADY;
  output [0:0]E;
  output [2:0]\write_reg_packet_size_axi4_stream_master_reg[31] ;
  output [30:0]Q;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[28] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[24] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[20] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[16] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[12] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[8] ;
  output [3:0]S;
  output [0:0]AXI4_Lite_ARESETN_1;
  output [1:0]\FSM_onehot_axi_lite_wstate_reg[2] ;
  output [13:0]\write_reg_h_in4_reg[13] ;
  output [13:0]\write_reg_h_in3_reg[13] ;
  output [13:0]\write_reg_h_in2_reg[13] ;
  output [13:0]\write_reg_h_in1_reg[13] ;
  input AXI4_Lite_ACLK;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;
  input auto_ready_dut_enb;
  input [31:0]AXI4_Lite_WDATA;
  input [13:0]AXI4_Lite_AWADDR;
  input AXI4_Lite_RREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;

  wire [0:0]AR;
  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire [0:0]AXI4_Lite_ARESETN_0;
  wire [0:0]AXI4_Lite_ARESETN_1;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire [0:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire [31:0]AXI4_Lite_WDATA;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [0:0]E;
  wire [1:0]\FSM_onehot_axi_lite_wstate_reg[2] ;
  wire FSM_sequential_axi_lite_rstate_reg;
  wire IPCORE_RESETN;
  wire [30:0]Q;
  wire [3:0]S;
  wire auto_ready_dut_enb;
  wire [30:30]read_reg_ip_timestamp;
  wire reg_enb_h_in1;
  wire reg_enb_h_in2;
  wire reg_enb_h_in3;
  wire reg_enb_h_in4;
  wire reg_enb_packet_size_axi4_stream_master;
  wire [0:0]top_data_write;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_10;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_11;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_12;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_13;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_14;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_15;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_16;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_17;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_18;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_19;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_20;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_21;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_22;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_23;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_24;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_25;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_26;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_27;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_28;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_29;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_30;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_31;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_32;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_33;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_34;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_35;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_36;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_37;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_38;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_7;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_8;
  wire u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_9;
  wire write_axi_enable;
  wire [13:0]\write_reg_h_in1_reg[13] ;
  wire [13:0]\write_reg_h_in2_reg[13] ;
  wire [13:0]\write_reg_h_in3_reg[13] ;
  wire [13:0]\write_reg_h_in4_reg[13] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[12] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[16] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[20] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[24] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[28] ;
  wire [2:0]\write_reg_packet_size_axi4_stream_master_reg[31] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[8] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_addr_decoder u_mlhdlc_sfir_fixpt_ipcore_addr_decoder_inst
       (.AR(AR),
        .AS(AXI4_Lite_ARESETN_0),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .E(E),
        .Q(Q),
        .S(S),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .read_reg_ip_timestamp(read_reg_ip_timestamp),
        .write_axi_enable(write_axi_enable),
        .write_reg_axi_enable_reg_0(u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_7),
        .\write_reg_h_in1_reg[0]_0 (AXI4_Lite_ARESETN_1),
        .\write_reg_h_in1_reg[13]_0 (\write_reg_h_in1_reg[13] ),
        .\write_reg_h_in1_reg[13]_1 (reg_enb_h_in1),
        .\write_reg_h_in2_reg[13]_0 (\write_reg_h_in2_reg[13] ),
        .\write_reg_h_in2_reg[13]_1 (reg_enb_h_in2),
        .\write_reg_h_in3_reg[13]_0 (\write_reg_h_in3_reg[13] ),
        .\write_reg_h_in3_reg[13]_1 (reg_enb_h_in3),
        .\write_reg_h_in4_reg[13]_0 (\write_reg_h_in4_reg[13] ),
        .\write_reg_h_in4_reg[13]_1 (reg_enb_h_in4),
        .\write_reg_packet_size_axi4_stream_master_reg[12]_0 (\write_reg_packet_size_axi4_stream_master_reg[12] ),
        .\write_reg_packet_size_axi4_stream_master_reg[16]_0 (\write_reg_packet_size_axi4_stream_master_reg[16] ),
        .\write_reg_packet_size_axi4_stream_master_reg[20]_0 (\write_reg_packet_size_axi4_stream_master_reg[20] ),
        .\write_reg_packet_size_axi4_stream_master_reg[24]_0 (\write_reg_packet_size_axi4_stream_master_reg[24] ),
        .\write_reg_packet_size_axi4_stream_master_reg[28]_0 (\write_reg_packet_size_axi4_stream_master_reg[28] ),
        .\write_reg_packet_size_axi4_stream_master_reg[31]_0 (\write_reg_packet_size_axi4_stream_master_reg[31] ),
        .\write_reg_packet_size_axi4_stream_master_reg[31]_1 ({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_8,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_9,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_10,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_11,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_12,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_13,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_14,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_15,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_16,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_17,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_18,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_19,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_20,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_21,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_22,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_23,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_24,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_25,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_26,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_27,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_28,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_29,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_30,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_31,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_32,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_33,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_34,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_35,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_36,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_37,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_38,top_data_write}),
        .\write_reg_packet_size_axi4_stream_master_reg[31]_2 (reg_enb_packet_size_axi4_stream_master),
        .\write_reg_packet_size_axi4_stream_master_reg[8]_0 (\write_reg_packet_size_axi4_stream_master_reg[8] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite_module u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst
       (.AR(AR),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),
        .\AXI4_Lite_ARADDR[2] (reg_enb_packet_size_axi4_stream_master),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARESETN_0(AXI4_Lite_ARESETN_0),
        .AXI4_Lite_ARESETN_1(AXI4_Lite_ARESETN_1),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_ARVALID_0(reg_enb_h_in1),
        .AXI4_Lite_ARVALID_1(reg_enb_h_in2),
        .AXI4_Lite_ARVALID_2(reg_enb_h_in3),
        .AXI4_Lite_ARVALID_3(reg_enb_h_in4),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .\FSM_onehot_axi_lite_wstate_reg[2]_0 (\FSM_onehot_axi_lite_wstate_reg[2] ),
        .FSM_sequential_axi_lite_rstate_reg_0(FSM_sequential_axi_lite_rstate_reg),
        .IPCORE_RESETN(IPCORE_RESETN),
        .Q({u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_8,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_9,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_10,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_11,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_12,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_13,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_14,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_15,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_16,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_17,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_18,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_19,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_20,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_21,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_22,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_23,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_24,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_25,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_26,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_27,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_28,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_29,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_30,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_31,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_32,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_33,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_34,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_35,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_36,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_37,u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_38,top_data_write}),
        .read_reg_ip_timestamp(read_reg_ip_timestamp),
        .\wdata_reg[0]_0 (u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_7),
        .write_axi_enable(write_axi_enable));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite_module
   (FSM_sequential_axi_lite_rstate_reg_0,
    AXI4_Lite_RDATA,
    AXI4_Lite_ARREADY,
    AXI4_Lite_AWREADY,
    AXI4_Lite_ARESETN_0,
    AR,
    AXI4_Lite_ARESETN_1,
    \wdata_reg[0]_0 ,
    Q,
    \FSM_onehot_axi_lite_wstate_reg[2]_0 ,
    \AXI4_Lite_ARADDR[2] ,
    AXI4_Lite_ARVALID_0,
    AXI4_Lite_ARVALID_1,
    AXI4_Lite_ARVALID_2,
    AXI4_Lite_ARVALID_3,
    AXI4_Lite_ACLK,
    AXI4_Lite_AWVALID,
    AXI4_Lite_ARVALID,
    AXI4_Lite_ARADDR,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN,
    write_axi_enable,
    read_reg_ip_timestamp,
    AXI4_Lite_WDATA,
    AXI4_Lite_AWADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY);
  output FSM_sequential_axi_lite_rstate_reg_0;
  output [0:0]AXI4_Lite_RDATA;
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_AWREADY;
  output [0:0]AXI4_Lite_ARESETN_0;
  output [0:0]AR;
  output [0:0]AXI4_Lite_ARESETN_1;
  output \wdata_reg[0]_0 ;
  output [31:0]Q;
  output [1:0]\FSM_onehot_axi_lite_wstate_reg[2]_0 ;
  output [0:0]\AXI4_Lite_ARADDR[2] ;
  output [0:0]AXI4_Lite_ARVALID_0;
  output [0:0]AXI4_Lite_ARVALID_1;
  output [0:0]AXI4_Lite_ARVALID_2;
  output [0:0]AXI4_Lite_ARVALID_3;
  input AXI4_Lite_ACLK;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;
  input write_axi_enable;
  input [0:0]read_reg_ip_timestamp;
  input [31:0]AXI4_Lite_WDATA;
  input [13:0]AXI4_Lite_AWADDR;
  input AXI4_Lite_RREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;

  wire [0:0]AR;
  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire [0:0]\AXI4_Lite_ARADDR[2] ;
  wire AXI4_Lite_ARESETN;
  wire [0:0]AXI4_Lite_ARESETN_0;
  wire [0:0]AXI4_Lite_ARESETN_1;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [0:0]AXI4_Lite_ARVALID_0;
  wire [0:0]AXI4_Lite_ARVALID_1;
  wire [0:0]AXI4_Lite_ARVALID_2;
  wire [0:0]AXI4_Lite_ARVALID_3;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire [0:0]AXI4_Lite_RDATA;
  wire \AXI4_Lite_RDATA_1[30]_i_10_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_1_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_2_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_4_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_5_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_7_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_8_n_0 ;
  wire \AXI4_Lite_RDATA_1[30]_i_9_n_0 ;
  wire AXI4_Lite_RREADY;
  wire [31:0]AXI4_Lite_WDATA;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [1:0]\FSM_onehot_axi_lite_wstate_reg[2]_0 ;
  wire \FSM_onehot_axi_lite_wstate_reg_n_0_[0] ;
  wire FSM_sequential_axi_lite_rstate_reg_0;
  wire IPCORE_RESETN;
  wire [31:0]Q;
  wire aw_transfer;
  wire axi_lite_rstate_next;
  wire [2:0]axi_lite_wstate_next;
  wire [0:0]read_reg_ip_timestamp;
  wire reset;
  wire soft_reset;
  wire soft_reset_i_2_n_0;
  wire soft_reset_i_3_n_0;
  wire soft_reset_i_4_n_0;
  wire strobe_sw;
  wire [12:0]top_addr_sel;
  wire top_wr_enb;
  wire w_transfer;
  wire w_transfer_and_wstrb;
  wire [13:0]waddr_sel;
  wire \wdata_reg[0]_0 ;
  wire write_axi_enable;
  wire write_reg_axi_enable_i_2_n_0;
  wire \write_reg_h_in4[13]_i_2_n_0 ;

  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'h04)) 
    AXI4_Lite_ARREADY_INST_0
       (.I0(AXI4_Lite_AWVALID),
        .I1(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I2(FSM_sequential_axi_lite_rstate_reg_0),
        .O(AXI4_Lite_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT2 #(
    .INIT(4'h2)) 
    AXI4_Lite_AWREADY_INST_0
       (.I0(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .O(AXI4_Lite_AWREADY));
  LUT6 #(
    .INIT(64'h80FFFFFF80000000)) 
    \AXI4_Lite_RDATA_1[30]_i_1 
       (.I0(\AXI4_Lite_RDATA_1[30]_i_2_n_0 ),
        .I1(top_addr_sel[0]),
        .I2(read_reg_ip_timestamp),
        .I3(AXI4_Lite_ARVALID),
        .I4(AXI4_Lite_ARREADY),
        .I5(AXI4_Lite_RDATA),
        .O(\AXI4_Lite_RDATA_1[30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFACFCA)) 
    \AXI4_Lite_RDATA_1[30]_i_10 
       (.I0(waddr_sel[8]),
        .I1(AXI4_Lite_ARADDR[8]),
        .I2(AXI4_Lite_ARVALID),
        .I3(waddr_sel[7]),
        .I4(AXI4_Lite_ARADDR[7]),
        .O(\AXI4_Lite_RDATA_1[30]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0C0C00AA00000000)) 
    \AXI4_Lite_RDATA_1[30]_i_2 
       (.I0(waddr_sel[1]),
        .I1(AXI4_Lite_ARADDR[1]),
        .I2(AXI4_Lite_ARADDR[6]),
        .I3(waddr_sel[6]),
        .I4(AXI4_Lite_ARVALID),
        .I5(\AXI4_Lite_RDATA_1[30]_i_4_n_0 ),
        .O(\AXI4_Lite_RDATA_1[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \AXI4_Lite_RDATA_1[30]_i_3 
       (.I0(AXI4_Lite_ARADDR[0]),
        .I1(waddr_sel[0]),
        .I2(AXI4_Lite_ARVALID),
        .O(top_addr_sel[0]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \AXI4_Lite_RDATA_1[30]_i_4 
       (.I0(\AXI4_Lite_RDATA_1[30]_i_5_n_0 ),
        .I1(top_addr_sel[12]),
        .I2(\AXI4_Lite_RDATA_1[30]_i_7_n_0 ),
        .I3(\AXI4_Lite_RDATA_1[30]_i_8_n_0 ),
        .I4(\AXI4_Lite_RDATA_1[30]_i_9_n_0 ),
        .I5(\AXI4_Lite_RDATA_1[30]_i_10_n_0 ),
        .O(\AXI4_Lite_RDATA_1[30]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFACFCA)) 
    \AXI4_Lite_RDATA_1[30]_i_5 
       (.I0(waddr_sel[13]),
        .I1(AXI4_Lite_ARADDR[13]),
        .I2(AXI4_Lite_ARVALID),
        .I3(waddr_sel[11]),
        .I4(AXI4_Lite_ARADDR[11]),
        .O(\AXI4_Lite_RDATA_1[30]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \AXI4_Lite_RDATA_1[30]_i_6 
       (.I0(AXI4_Lite_ARADDR[12]),
        .I1(waddr_sel[12]),
        .I2(AXI4_Lite_ARVALID),
        .O(top_addr_sel[12]));
  LUT5 #(
    .INIT(32'hFFFACFCA)) 
    \AXI4_Lite_RDATA_1[30]_i_7 
       (.I0(waddr_sel[5]),
        .I1(AXI4_Lite_ARADDR[5]),
        .I2(AXI4_Lite_ARVALID),
        .I3(waddr_sel[4]),
        .I4(AXI4_Lite_ARADDR[4]),
        .O(\AXI4_Lite_RDATA_1[30]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFACFCA)) 
    \AXI4_Lite_RDATA_1[30]_i_8 
       (.I0(waddr_sel[3]),
        .I1(AXI4_Lite_ARADDR[3]),
        .I2(AXI4_Lite_ARVALID),
        .I3(waddr_sel[2]),
        .I4(AXI4_Lite_ARADDR[2]),
        .O(\AXI4_Lite_RDATA_1[30]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hFFFACFCA)) 
    \AXI4_Lite_RDATA_1[30]_i_9 
       (.I0(waddr_sel[10]),
        .I1(AXI4_Lite_ARADDR[10]),
        .I2(AXI4_Lite_ARVALID),
        .I3(waddr_sel[9]),
        .I4(AXI4_Lite_ARADDR[9]),
        .O(\AXI4_Lite_RDATA_1[30]_i_9_n_0 ));
  FDCE \AXI4_Lite_RDATA_1_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\AXI4_Lite_RDATA_1[30]_i_1_n_0 ),
        .Q(AXI4_Lite_RDATA));
  LUT5 #(
    .INIT(32'hFFD0D0D0)) 
    \FSM_onehot_axi_lite_wstate[0]_i_1 
       (.I0(AXI4_Lite_AWVALID),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .I2(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I3(AXI4_Lite_BREADY),
        .I4(\FSM_onehot_axi_lite_wstate_reg[2]_0 [1]),
        .O(axi_lite_wstate_next[0]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'h20FF2020)) 
    \FSM_onehot_axi_lite_wstate[1]_i_1 
       (.I0(AXI4_Lite_AWVALID),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .I2(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I3(AXI4_Lite_WVALID),
        .I4(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .O(axi_lite_wstate_next[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_axi_lite_wstate[1]_i_2 
       (.I0(AXI4_Lite_ARESETN),
        .O(reset));
  LUT4 #(
    .INIT(16'h8F88)) 
    \FSM_onehot_axi_lite_wstate[2]_i_1 
       (.I0(AXI4_Lite_WVALID),
        .I1(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .I2(AXI4_Lite_BREADY),
        .I3(\FSM_onehot_axi_lite_wstate_reg[2]_0 [1]),
        .O(axi_lite_wstate_next[2]));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_axi_lite_wstate_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .D(axi_lite_wstate_next[0]),
        .PRE(reset),
        .Q(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_axi_lite_wstate_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_wstate_next[1]),
        .Q(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_axi_lite_wstate_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_wstate_next[2]),
        .Q(\FSM_onehot_axi_lite_wstate_reg[2]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h47444444)) 
    FSM_sequential_axi_lite_rstate_i_1
       (.I0(AXI4_Lite_RREADY),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .I2(AXI4_Lite_AWVALID),
        .I3(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I4(AXI4_Lite_ARVALID),
        .O(axi_lite_rstate_next));
  (* FSM_ENCODED_STATES = "iSTATE:0,iSTATE0:1" *) 
  FDCE FSM_sequential_axi_lite_rstate_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_rstate_next),
        .Q(FSM_sequential_axi_lite_rstate_reg_0));
  LUT3 #(
    .INIT(8'hDF)) 
    \Out_1[15]_i_3 
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AR));
  LUT3 #(
    .INIT(8'hDF)) 
    out_valid_i_2
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AXI4_Lite_ARESETN_0));
  LUT5 #(
    .INIT(32'h00000008)) 
    soft_reset_i_1
       (.I0(top_wr_enb),
        .I1(Q[0]),
        .I2(soft_reset_i_2_n_0),
        .I3(soft_reset_i_3_n_0),
        .I4(soft_reset_i_4_n_0),
        .O(strobe_sw));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    soft_reset_i_2
       (.I0(waddr_sel[13]),
        .I1(waddr_sel[12]),
        .I2(waddr_sel[9]),
        .I3(waddr_sel[8]),
        .I4(waddr_sel[11]),
        .I5(waddr_sel[10]),
        .O(soft_reset_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    soft_reset_i_3
       (.I0(waddr_sel[2]),
        .I1(waddr_sel[3]),
        .I2(waddr_sel[0]),
        .I3(waddr_sel[1]),
        .O(soft_reset_i_3_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    soft_reset_i_4
       (.I0(waddr_sel[6]),
        .I1(waddr_sel[7]),
        .I2(waddr_sel[4]),
        .I3(waddr_sel[5]),
        .O(soft_reset_i_4_n_0));
  FDCE soft_reset_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(strobe_sw),
        .Q(soft_reset));
  LUT3 #(
    .INIT(8'hDF)) 
    \ud8[13]_i_2 
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AXI4_Lite_ARESETN_1));
  LUT3 #(
    .INIT(8'h40)) 
    \waddr[15]_i_1 
       (.I0(FSM_sequential_axi_lite_rstate_reg_0),
        .I1(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I2(AXI4_Lite_AWVALID),
        .O(aw_transfer));
  FDCE \waddr_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[8]),
        .Q(waddr_sel[8]));
  FDCE \waddr_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[9]),
        .Q(waddr_sel[9]));
  FDCE \waddr_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[10]),
        .Q(waddr_sel[10]));
  FDCE \waddr_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[11]),
        .Q(waddr_sel[11]));
  FDCE \waddr_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[12]),
        .Q(waddr_sel[12]));
  FDCE \waddr_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[13]),
        .Q(waddr_sel[13]));
  FDCE \waddr_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[0]),
        .Q(waddr_sel[0]));
  FDCE \waddr_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[1]),
        .Q(waddr_sel[1]));
  FDCE \waddr_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[2]),
        .Q(waddr_sel[2]));
  FDCE \waddr_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[3]),
        .Q(waddr_sel[3]));
  FDCE \waddr_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[4]),
        .Q(waddr_sel[4]));
  FDCE \waddr_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[5]),
        .Q(waddr_sel[5]));
  FDCE \waddr_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[6]),
        .Q(waddr_sel[6]));
  FDCE \waddr_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[7]),
        .Q(waddr_sel[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \wdata[31]_i_1 
       (.I0(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .I1(AXI4_Lite_WVALID),
        .O(w_transfer));
  FDCE \wdata_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[0]),
        .Q(Q[0]));
  FDCE \wdata_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[10]),
        .Q(Q[10]));
  FDCE \wdata_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[11]),
        .Q(Q[11]));
  FDCE \wdata_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[12]),
        .Q(Q[12]));
  FDCE \wdata_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[13]),
        .Q(Q[13]));
  FDCE \wdata_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[14]),
        .Q(Q[14]));
  FDCE \wdata_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[15]),
        .Q(Q[15]));
  FDCE \wdata_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[16]),
        .Q(Q[16]));
  FDCE \wdata_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[17]),
        .Q(Q[17]));
  FDCE \wdata_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[18]),
        .Q(Q[18]));
  FDCE \wdata_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[19]),
        .Q(Q[19]));
  FDCE \wdata_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[1]),
        .Q(Q[1]));
  FDCE \wdata_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[20]),
        .Q(Q[20]));
  FDCE \wdata_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[21]),
        .Q(Q[21]));
  FDCE \wdata_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[22]),
        .Q(Q[22]));
  FDCE \wdata_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[23]),
        .Q(Q[23]));
  FDCE \wdata_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[24]),
        .Q(Q[24]));
  FDCE \wdata_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[25]),
        .Q(Q[25]));
  FDCE \wdata_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[26]),
        .Q(Q[26]));
  FDCE \wdata_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[27]),
        .Q(Q[27]));
  FDCE \wdata_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[28]),
        .Q(Q[28]));
  FDCE \wdata_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[29]),
        .Q(Q[29]));
  FDCE \wdata_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[2]),
        .Q(Q[2]));
  FDCE \wdata_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[30]),
        .Q(Q[30]));
  FDCE \wdata_reg[31] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[31]),
        .Q(Q[31]));
  FDCE \wdata_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[3]),
        .Q(Q[3]));
  FDCE \wdata_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[4]),
        .Q(Q[4]));
  FDCE \wdata_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[5]),
        .Q(Q[5]));
  FDCE \wdata_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[6]),
        .Q(Q[6]));
  FDCE \wdata_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[7]),
        .Q(Q[7]));
  FDCE \wdata_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[8]),
        .Q(Q[8]));
  FDCE \wdata_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[9]),
        .Q(Q[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    wr_enb_1_i_1
       (.I0(AXI4_Lite_WSTRB[2]),
        .I1(AXI4_Lite_WSTRB[3]),
        .I2(AXI4_Lite_WSTRB[0]),
        .I3(AXI4_Lite_WSTRB[1]),
        .I4(AXI4_Lite_WVALID),
        .I5(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .O(w_transfer_and_wstrb));
  FDCE wr_enb_1_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(w_transfer_and_wstrb),
        .Q(top_wr_enb));
  LUT6 #(
    .INIT(64'hFFBFFFFF00800000)) 
    write_reg_axi_enable_i_1
       (.I0(Q[0]),
        .I1(write_reg_axi_enable_i_2_n_0),
        .I2(top_addr_sel[0]),
        .I3(top_addr_sel[1]),
        .I4(top_wr_enb),
        .I5(write_axi_enable),
        .O(\wdata_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'h028A)) 
    write_reg_axi_enable_i_2
       (.I0(\AXI4_Lite_RDATA_1[30]_i_4_n_0 ),
        .I1(AXI4_Lite_ARVALID),
        .I2(waddr_sel[6]),
        .I3(AXI4_Lite_ARADDR[6]),
        .O(write_reg_axi_enable_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    write_reg_axi_enable_i_3
       (.I0(AXI4_Lite_ARADDR[1]),
        .I1(waddr_sel[1]),
        .I2(AXI4_Lite_ARVALID),
        .O(top_addr_sel[1]));
  LUT6 #(
    .INIT(64'h0004404400000000)) 
    \write_reg_h_in1[13]_i_1 
       (.I0(top_addr_sel[0]),
        .I1(\write_reg_h_in4[13]_i_2_n_0 ),
        .I2(AXI4_Lite_ARVALID),
        .I3(waddr_sel[1]),
        .I4(AXI4_Lite_ARADDR[1]),
        .I5(top_wr_enb),
        .O(AXI4_Lite_ARVALID_0));
  LUT6 #(
    .INIT(64'h028A000000000000)) 
    \write_reg_h_in2[13]_i_1 
       (.I0(\write_reg_h_in4[13]_i_2_n_0 ),
        .I1(AXI4_Lite_ARVALID),
        .I2(waddr_sel[1]),
        .I3(AXI4_Lite_ARADDR[1]),
        .I4(top_addr_sel[0]),
        .I5(top_wr_enb),
        .O(AXI4_Lite_ARVALID_1));
  LUT6 #(
    .INIT(64'h5410000000000000)) 
    \write_reg_h_in3[13]_i_1 
       (.I0(top_addr_sel[0]),
        .I1(AXI4_Lite_ARVALID),
        .I2(waddr_sel[1]),
        .I3(AXI4_Lite_ARADDR[1]),
        .I4(\write_reg_h_in4[13]_i_2_n_0 ),
        .I5(top_wr_enb),
        .O(AXI4_Lite_ARVALID_2));
  LUT6 #(
    .INIT(64'hE400000000000000)) 
    \write_reg_h_in4[13]_i_1 
       (.I0(AXI4_Lite_ARVALID),
        .I1(waddr_sel[1]),
        .I2(AXI4_Lite_ARADDR[1]),
        .I3(\write_reg_h_in4[13]_i_2_n_0 ),
        .I4(top_addr_sel[0]),
        .I5(top_wr_enb),
        .O(AXI4_Lite_ARVALID_3));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT4 #(
    .INIT(16'hE400)) 
    \write_reg_h_in4[13]_i_2 
       (.I0(AXI4_Lite_ARVALID),
        .I1(waddr_sel[6]),
        .I2(AXI4_Lite_ARADDR[6]),
        .I3(\AXI4_Lite_RDATA_1[30]_i_4_n_0 ),
        .O(\write_reg_h_in4[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'h53000000)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_1 
       (.I0(AXI4_Lite_ARADDR[0]),
        .I1(waddr_sel[0]),
        .I2(AXI4_Lite_ARVALID),
        .I3(\AXI4_Lite_RDATA_1[30]_i_2_n_0 ),
        .I4(top_wr_enb),
        .O(\AXI4_Lite_ARADDR[2] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_dut
   (delayed_xout,
    p26y_out_add_temp,
    p23m4_mul_temp,
    p22m3_mul_temp,
    p21m2_mul_temp,
    p20m1_mul_temp,
    E,
    D,
    IPCORE_CLK,
    AR);
  output [13:0]delayed_xout;
  output [13:0]p26y_out_add_temp;
  input [13:0]p23m4_mul_temp;
  input [13:0]p22m3_mul_temp;
  input [13:0]p21m2_mul_temp;
  input [13:0]p20m1_mul_temp;
  input [0:0]E;
  input [13:0]D;
  input IPCORE_CLK;
  input [0:0]AR;

  wire [0:0]AR;
  wire [13:0]D;
  wire [0:0]E;
  wire IPCORE_CLK;
  wire [13:0]delayed_xout;
  wire [13:0]p20m1_mul_temp;
  wire [13:0]p21m2_mul_temp;
  wire [13:0]p22m3_mul_temp;
  wire [13:0]p23m4_mul_temp;
  wire [13:0]p26y_out_add_temp;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt u_mlhdlc_sfir_fixpt
       (.AR(AR),
        .D(D),
        .E(E),
        .IPCORE_CLK(IPCORE_CLK),
        .delayed_xout(delayed_xout),
        .p20m1_mul_temp_0(p20m1_mul_temp),
        .p21m2_mul_temp_0(p21m2_mul_temp),
        .p22m3_mul_temp_0(p22m3_mul_temp),
        .p23m4_mul_temp_0(p23m4_mul_temp),
        .p26y_out_add_temp(p26y_out_add_temp));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT
   (AXI4_Stream_Master_TLAST,
    S,
    \tlast_counter_out_reg[21] ,
    \tlast_counter_out_reg[30] ,
    IPCORE_CLK,
    reset,
    AXI4_Stream_Master_TREADY,
    tlast_counter_out_reg,
    Q,
    tlast_size_value,
    auto_ready_dut_enb,
    out_valid,
    auto_tlast);
  output AXI4_Stream_Master_TLAST;
  output [3:0]S;
  output [3:0]\tlast_counter_out_reg[21] ;
  output [2:0]\tlast_counter_out_reg[30] ;
  input IPCORE_CLK;
  input reset;
  input AXI4_Stream_Master_TREADY;
  input [31:0]tlast_counter_out_reg;
  input [0:0]Q;
  input [30:0]tlast_size_value;
  input auto_ready_dut_enb;
  input out_valid;
  input auto_tlast;

  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire IPCORE_CLK;
  wire [0:0]Q;
  wire Q_next;
  wire Q_next_1;
  wire [3:0]S;
  wire auto_ready_dut_enb;
  wire auto_tlast;
  wire cache_data_reg_n_0;
  wire cache_valid;
  wire cache_wr_en;
  wire fifo_valid;
  wire out_valid;
  wire out_valid_0;
  wire out_valid_i_1__1_n_0;
  wire out_wr_en;
  wire reset;
  wire [31:0]tlast_counter_out_reg;
  wire [3:0]\tlast_counter_out_reg[21] ;
  wire [2:0]\tlast_counter_out_reg[30] ;
  wire [30:0]tlast_size_value;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_12;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_13;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hDDD0)) 
    Out_1_i_2
       (.I0(out_valid_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(out_wr_en));
  FDCE Out_1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_13),
        .Q(AXI4_Stream_Master_TLAST));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hA600)) 
    cache_data_i_2
       (.I0(cache_valid),
        .I1(out_valid_0),
        .I2(AXI4_Stream_Master_TREADY),
        .I3(fifo_valid),
        .O(cache_wr_en));
  FDCE cache_data_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_12),
        .Q(cache_data_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hF220)) 
    cache_valid_i_1__1
       (.I0(out_valid_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(Q_next));
  FDCE cache_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next),
        .Q(cache_valid));
  FDCE fifo_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next_1),
        .Q(fifo_valid));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hEEFE)) 
    out_valid_i_1__1
       (.I0(fifo_valid),
        .I1(cache_valid),
        .I2(out_valid_0),
        .I3(AXI4_Stream_Master_TREADY),
        .O(out_valid_i_1__1_n_0));
  FDCE out_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(out_valid_i_1__1_n_0),
        .Q(out_valid_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst
       (.AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .IPCORE_CLK(IPCORE_CLK),
        .Out_1_reg(cache_data_reg_n_0),
        .Q(Q),
        .Q_next_1(Q_next_1),
        .S(S),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .auto_tlast(auto_tlast),
        .cache_data_reg(u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_13),
        .cache_valid(cache_valid),
        .cache_wr_en(cache_wr_en),
        .data_int_reg(u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_12),
        .fifo_valid(fifo_valid),
        .out_valid(out_valid),
        .out_valid_0(out_valid_0),
        .out_wr_en(out_wr_en),
        .reset(reset),
        .tlast_counter_out_reg(tlast_counter_out_reg),
        .\tlast_counter_out_reg[21] (\tlast_counter_out_reg[21] ),
        .\tlast_counter_out_reg[30] (\tlast_counter_out_reg[30] ),
        .tlast_size_value(tlast_size_value));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic
   (Q_next_1,
    S,
    \tlast_counter_out_reg[21] ,
    \tlast_counter_out_reg[30] ,
    data_int_reg,
    cache_data_reg,
    IPCORE_CLK,
    reset,
    cache_valid,
    out_valid_0,
    fifo_valid,
    AXI4_Stream_Master_TREADY,
    tlast_counter_out_reg,
    Q,
    tlast_size_value,
    auto_ready_dut_enb,
    out_valid,
    cache_wr_en,
    Out_1_reg,
    out_wr_en,
    AXI4_Stream_Master_TLAST,
    auto_tlast);
  output Q_next_1;
  output [3:0]S;
  output [3:0]\tlast_counter_out_reg[21] ;
  output [2:0]\tlast_counter_out_reg[30] ;
  output data_int_reg;
  output cache_data_reg;
  input IPCORE_CLK;
  input reset;
  input cache_valid;
  input out_valid_0;
  input fifo_valid;
  input AXI4_Stream_Master_TREADY;
  input [31:0]tlast_counter_out_reg;
  input [0:0]Q;
  input [30:0]tlast_size_value;
  input auto_ready_dut_enb;
  input out_valid;
  input cache_wr_en;
  input Out_1_reg;
  input out_wr_en;
  input AXI4_Stream_Master_TLAST;
  input auto_tlast;

  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire IPCORE_CLK;
  wire Out_1_reg;
  wire [0:0]Q;
  wire Q_next_1;
  wire [3:0]S;
  wire auto_ready_dut_enb;
  wire auto_tlast;
  wire cache_data_reg;
  wire cache_valid;
  wire cache_wr_en;
  wire data_int_reg;
  wire \fifo_back_indx[0]_i_1_n_0 ;
  wire \fifo_back_indx[1]_i_1_n_0 ;
  wire \fifo_back_indx_reg_n_0_[0] ;
  wire \fifo_back_indx_reg_n_0_[1] ;
  wire fifo_data_out;
  wire \fifo_front_indx[0]_i_1_n_0 ;
  wire \fifo_front_indx[1]_i_1_n_0 ;
  wire \fifo_front_indx_reg_n_0_[0] ;
  wire \fifo_front_indx_reg_n_0_[1] ;
  wire fifo_pop__2;
  wire fifo_read_enable;
  wire \fifo_sample_count[0]_i_1_n_0 ;
  wire \fifo_sample_count[1]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_1_n_0 ;
  wire \fifo_sample_count_reg_n_0_[0] ;
  wire \fifo_sample_count_reg_n_0_[1] ;
  wire \fifo_sample_count_reg_n_0_[2] ;
  wire fifo_valid;
  wire out_valid;
  wire out_valid_0;
  wire out_wr_en;
  wire reset;
  wire [31:0]tlast_counter_out_reg;
  wire [3:0]\tlast_counter_out_reg[21] ;
  wire [2:0]\tlast_counter_out_reg[30] ;
  wire [30:0]tlast_size_value;
  wire w_d1;
  wire w_d2;
  wire w_mux1;
  wire w_we;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_back_indx[0]_i_1 
       (.I0(w_we),
        .I1(\fifo_back_indx_reg_n_0_[0] ),
        .O(\fifo_back_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_back_indx[1]_i_1 
       (.I0(\fifo_back_indx_reg_n_0_[0] ),
        .I1(w_we),
        .I2(\fifo_back_indx_reg_n_0_[1] ),
        .O(\fifo_back_indx[1]_i_1_n_0 ));
  FDCE \fifo_back_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_back_indx[0]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[0] ));
  FDCE \fifo_back_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_back_indx[1]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_front_indx[0]_i_1 
       (.I0(fifo_read_enable),
        .I1(\fifo_front_indx_reg_n_0_[0] ),
        .O(\fifo_front_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_front_indx[1]_i_1 
       (.I0(\fifo_front_indx_reg_n_0_[0] ),
        .I1(fifo_read_enable),
        .I2(\fifo_front_indx_reg_n_0_[1] ),
        .O(\fifo_front_indx[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    \fifo_front_indx[1]_i_2__1 
       (.I0(cache_valid),
        .I1(out_valid_0),
        .I2(fifo_valid),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .I5(\fifo_sample_count_reg_n_0_[2] ),
        .O(fifo_read_enable));
  FDCE \fifo_front_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_front_indx[0]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[0] ));
  FDCE \fifo_front_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_front_indx[1]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAA55555567A8A8A8)) 
    \fifo_sample_count[0]_i_1 
       (.I0(fifo_pop__2),
        .I1(\fifo_sample_count_reg_n_0_[1] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(out_valid),
        .I4(auto_ready_dut_enb),
        .I5(\fifo_sample_count_reg_n_0_[0] ),
        .O(\fifo_sample_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8F80F007F7F8080)) 
    \fifo_sample_count[1]_i_1 
       (.I0(auto_ready_dut_enb),
        .I1(out_valid),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[2] ),
        .I4(\fifo_sample_count_reg_n_0_[1] ),
        .I5(fifo_pop__2),
        .O(\fifo_sample_count[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F0007F80FF00)) 
    \fifo_sample_count[2]_i_1 
       (.I0(auto_ready_dut_enb),
        .I1(out_valid),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[2] ),
        .I4(\fifo_sample_count_reg_n_0_[1] ),
        .I5(fifo_pop__2),
        .O(\fifo_sample_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FEFEFEFEFEFEFE)) 
    \fifo_sample_count[2]_i_2__1 
       (.I0(\fifo_sample_count_reg_n_0_[1] ),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(fifo_valid),
        .I4(out_valid_0),
        .I5(cache_valid),
        .O(fifo_pop__2));
  FDCE \fifo_sample_count_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[0]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[0] ));
  FDCE \fifo_sample_count_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[1]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[1] ));
  FDCE \fifo_sample_count_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[2]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[2] ));
  LUT5 #(
    .INIT(32'hAAEAAAAA)) 
    fifo_valid_i_1__1
       (.I0(fifo_pop__2),
        .I1(fifo_valid),
        .I2(cache_valid),
        .I3(AXI4_Stream_Master_TREADY),
        .I4(out_valid_0),
        .O(Q_next_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_singlebit u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_ram_singlebit
       (.AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .IPCORE_CLK(IPCORE_CLK),
        .Out_1_reg(Out_1_reg),
        .Q(Q),
        .S(S),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .auto_tlast(auto_tlast),
        .cache_data_reg(cache_data_reg),
        .cache_valid(cache_valid),
        .cache_wr_en(cache_wr_en),
        .data_int_reg_0(data_int_reg),
        .data_int_reg_1(\fifo_sample_count_reg_n_0_[0] ),
        .data_int_reg_2(\fifo_sample_count_reg_n_0_[2] ),
        .data_int_reg_3(\fifo_sample_count_reg_n_0_[1] ),
        .fifo_data_out(fifo_data_out),
        .out_valid(out_valid),
        .out_wr_en(out_wr_en),
        .rd_addr({\fifo_front_indx_reg_n_0_[1] ,\fifo_front_indx_reg_n_0_[0] }),
        .tlast_counter_out_reg(tlast_counter_out_reg),
        .\tlast_counter_out_reg[21] (\tlast_counter_out_reg[21] ),
        .\tlast_counter_out_reg[30] (\tlast_counter_out_reg[30] ),
        .tlast_size_value(tlast_size_value),
        .w_d1(w_d1),
        .w_d2(w_d2),
        .w_we(w_we),
        .wr_addr({\fifo_back_indx_reg_n_0_[1] ,\fifo_back_indx_reg_n_0_[0] }));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    w_d1_i_1__1
       (.I0(cache_valid),
        .I1(out_valid_0),
        .I2(fifo_valid),
        .I3(\fifo_sample_count_reg_n_0_[2] ),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .I5(\fifo_sample_count_reg_n_0_[1] ),
        .O(w_mux1));
  FDCE w_d1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(w_mux1),
        .Q(w_d1));
  FDCE w_d2_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(fifo_data_out),
        .Q(w_d2));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data
   (out_valid_reg_0,
    auto_tlast,
    AXI4_Stream_Slave_TREADY,
    out_valid_reg_1,
    Q,
    IPCORE_CLK,
    reset,
    AXI4_Stream_Slave_TVALID,
    fifo_valid_reg_0,
    CO,
    AXI4_Stream_Slave_TDATA,
    AR);
  output out_valid_reg_0;
  output auto_tlast;
  output AXI4_Stream_Slave_TREADY;
  output out_valid_reg_1;
  output [13:0]Q;
  input IPCORE_CLK;
  input reset;
  input AXI4_Stream_Slave_TVALID;
  input fifo_valid_reg_0;
  input [0:0]CO;
  input [15:0]AXI4_Stream_Slave_TDATA;
  input [0:0]AR;

  wire [0:0]AR;
  wire [15:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire [0:0]CO;
  wire IPCORE_CLK;
  wire [13:0]Q;
  wire Q_next;
  wire Q_next_1;
  wire Q_next_2;
  wire auto_tlast;
  wire [13:0]cache_data;
  wire cache_valid;
  wire cache_wr_en;
  wire [13:0]data_out_next;
  wire [13:0]fifo_data_out;
  wire fifo_valid;
  wire fifo_valid_reg_0;
  wire out_valid_reg_0;
  wire out_valid_reg_1;
  wire out_wr_en;
  wire reset;

  LUT4 #(
    .INIT(16'hDDD0)) 
    \Out_1[13]_i_1 
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(out_wr_en));
  FDCE \Out_1_reg[0] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[0]),
        .Q(Q[0]));
  FDCE \Out_1_reg[10] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[10]),
        .Q(Q[10]));
  FDCE \Out_1_reg[11] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[11]),
        .Q(Q[11]));
  FDCE \Out_1_reg[12] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[12]),
        .Q(Q[12]));
  FDCE \Out_1_reg[13] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[13]),
        .Q(Q[13]));
  FDCE \Out_1_reg[1] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[1]),
        .Q(Q[1]));
  FDCE \Out_1_reg[2] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[2]),
        .Q(Q[2]));
  FDCE \Out_1_reg[3] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[3]),
        .Q(Q[3]));
  FDCE \Out_1_reg[4] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[4]),
        .Q(Q[4]));
  FDCE \Out_1_reg[5] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[5]),
        .Q(Q[5]));
  FDCE \Out_1_reg[6] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[6]),
        .Q(Q[6]));
  FDCE \Out_1_reg[7] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[7]),
        .Q(Q[7]));
  FDCE \Out_1_reg[8] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[8]),
        .Q(Q[8]));
  FDCE \Out_1_reg[9] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(data_out_next[9]),
        .Q(Q[9]));
  LUT4 #(
    .INIT(16'hA600)) 
    \cache_data[13]_i_1 
       (.I0(cache_valid),
        .I1(out_valid_reg_0),
        .I2(fifo_valid_reg_0),
        .I3(fifo_valid),
        .O(cache_wr_en));
  FDCE \cache_data_reg[0] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[0]),
        .Q(cache_data[0]));
  FDCE \cache_data_reg[10] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[10]),
        .Q(cache_data[10]));
  FDCE \cache_data_reg[11] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[11]),
        .Q(cache_data[11]));
  FDCE \cache_data_reg[12] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[12]),
        .Q(cache_data[12]));
  FDCE \cache_data_reg[13] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[13]),
        .Q(cache_data[13]));
  FDCE \cache_data_reg[1] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[1]),
        .Q(cache_data[1]));
  FDCE \cache_data_reg[2] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[2]),
        .Q(cache_data[2]));
  FDCE \cache_data_reg[3] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[3]),
        .Q(cache_data[3]));
  FDCE \cache_data_reg[4] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[4]),
        .Q(cache_data[4]));
  FDCE \cache_data_reg[5] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[5]),
        .Q(cache_data[5]));
  FDCE \cache_data_reg[6] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[6]),
        .Q(cache_data[6]));
  FDCE \cache_data_reg[7] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[7]),
        .Q(cache_data[7]));
  FDCE \cache_data_reg[8] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[8]),
        .Q(cache_data[8]));
  FDCE \cache_data_reg[9] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(fifo_data_out[9]),
        .Q(cache_data[9]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hF220)) 
    cache_valid_i_1
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(Q_next));
  FDCE cache_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next),
        .Q(cache_valid));
  FDCE fifo_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next_1),
        .Q(fifo_valid));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT4 #(
    .INIT(16'hEFEE)) 
    out_valid_i_1
       (.I0(fifo_valid),
        .I1(cache_valid),
        .I2(fifo_valid_reg_0),
        .I3(out_valid_reg_0),
        .O(Q_next_2));
  FDCE out_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next_2),
        .Q(out_valid_reg_0));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_3_0_0_i_1
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .I2(CO),
        .O(auto_tlast));
  LUT2 #(
    .INIT(4'h8)) 
    \tlast_counter_out[0]_i_1 
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .O(out_valid_reg_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic u_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic_inst
       (.AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .D(data_out_next),
        .IPCORE_CLK(IPCORE_CLK),
        .Q(cache_data),
        .Q_next_1(Q_next_1),
        .cache_valid(cache_valid),
        .\data_int_reg[13] (fifo_data_out),
        .fifo_valid(fifo_valid),
        .fifo_valid_reg(out_valid_reg_0),
        .fifo_valid_reg_0(fifo_valid_reg_0),
        .reset(reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT
   (out_valid_reg_0,
    auto_ready_axi4_stream_master,
    AXI4_Stream_Master_TDATA,
    IPCORE_CLK,
    reset,
    AXI4_Stream_Master_TREADY,
    auto_ready_dut_enb,
    out_valid,
    p26y_out_add_temp,
    AR);
  output out_valid_reg_0;
  output auto_ready_axi4_stream_master;
  output [15:0]AXI4_Stream_Master_TDATA;
  input IPCORE_CLK;
  input reset;
  input AXI4_Stream_Master_TREADY;
  input auto_ready_dut_enb;
  input out_valid;
  input [13:0]p26y_out_add_temp;
  input [0:0]AR;

  wire [0:0]AR;
  wire [15:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TREADY;
  wire IPCORE_CLK;
  wire Q_next;
  wire Q_next_1;
  wire Q_next_2;
  wire auto_ready_axi4_stream_master;
  wire auto_ready_dut_enb;
  wire \cache_data_reg_n_0_[0] ;
  wire \cache_data_reg_n_0_[10] ;
  wire \cache_data_reg_n_0_[11] ;
  wire \cache_data_reg_n_0_[12] ;
  wire \cache_data_reg_n_0_[13] ;
  wire \cache_data_reg_n_0_[14] ;
  wire \cache_data_reg_n_0_[15] ;
  wire \cache_data_reg_n_0_[1] ;
  wire \cache_data_reg_n_0_[2] ;
  wire \cache_data_reg_n_0_[3] ;
  wire \cache_data_reg_n_0_[4] ;
  wire \cache_data_reg_n_0_[5] ;
  wire \cache_data_reg_n_0_[6] ;
  wire \cache_data_reg_n_0_[7] ;
  wire \cache_data_reg_n_0_[8] ;
  wire \cache_data_reg_n_0_[9] ;
  wire cache_valid;
  wire cache_wr_en;
  wire fifo_valid;
  wire out_valid;
  wire out_valid_reg_0;
  wire out_wr_en;
  wire [13:0]p26y_out_add_temp;
  wire reset;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_10;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_11;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_12;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_13;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_14;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_15;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_16;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_17;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_18;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_19;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_2;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_20;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_21;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_22;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_23;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_24;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_25;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_26;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_27;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_28;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_29;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_3;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_30;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_31;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_32;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_33;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_4;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_5;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_6;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_7;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_8;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_9;

  LUT4 #(
    .INIT(16'hDDD0)) 
    \Out_1[15]_i_1 
       (.I0(out_valid_reg_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(out_wr_en));
  FDCE \Out_1_reg[0] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_17),
        .Q(AXI4_Stream_Master_TDATA[0]));
  FDCE \Out_1_reg[10] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_7),
        .Q(AXI4_Stream_Master_TDATA[10]));
  FDCE \Out_1_reg[11] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_6),
        .Q(AXI4_Stream_Master_TDATA[11]));
  FDCE \Out_1_reg[12] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_5),
        .Q(AXI4_Stream_Master_TDATA[12]));
  FDCE \Out_1_reg[13] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_4),
        .Q(AXI4_Stream_Master_TDATA[13]));
  FDCE \Out_1_reg[14] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_3),
        .Q(AXI4_Stream_Master_TDATA[14]));
  FDCE \Out_1_reg[15] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_2),
        .Q(AXI4_Stream_Master_TDATA[15]));
  FDCE \Out_1_reg[1] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_16),
        .Q(AXI4_Stream_Master_TDATA[1]));
  FDCE \Out_1_reg[2] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_15),
        .Q(AXI4_Stream_Master_TDATA[2]));
  FDCE \Out_1_reg[3] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_14),
        .Q(AXI4_Stream_Master_TDATA[3]));
  FDCE \Out_1_reg[4] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_13),
        .Q(AXI4_Stream_Master_TDATA[4]));
  FDCE \Out_1_reg[5] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_12),
        .Q(AXI4_Stream_Master_TDATA[5]));
  FDCE \Out_1_reg[6] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_11),
        .Q(AXI4_Stream_Master_TDATA[6]));
  FDCE \Out_1_reg[7] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_10),
        .Q(AXI4_Stream_Master_TDATA[7]));
  FDCE \Out_1_reg[8] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_9),
        .Q(AXI4_Stream_Master_TDATA[8]));
  FDCE \Out_1_reg[9] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_8),
        .Q(AXI4_Stream_Master_TDATA[9]));
  LUT4 #(
    .INIT(16'hA600)) 
    \cache_data[15]_i_1 
       (.I0(cache_valid),
        .I1(out_valid_reg_0),
        .I2(AXI4_Stream_Master_TREADY),
        .I3(fifo_valid),
        .O(cache_wr_en));
  FDCE \cache_data_reg[0] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_33),
        .Q(\cache_data_reg_n_0_[0] ));
  FDCE \cache_data_reg[10] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_23),
        .Q(\cache_data_reg_n_0_[10] ));
  FDCE \cache_data_reg[11] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_22),
        .Q(\cache_data_reg_n_0_[11] ));
  FDCE \cache_data_reg[12] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_21),
        .Q(\cache_data_reg_n_0_[12] ));
  FDCE \cache_data_reg[13] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_20),
        .Q(\cache_data_reg_n_0_[13] ));
  FDCE \cache_data_reg[14] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_19),
        .Q(\cache_data_reg_n_0_[14] ));
  FDCE \cache_data_reg[15] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_18),
        .Q(\cache_data_reg_n_0_[15] ));
  FDCE \cache_data_reg[1] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_32),
        .Q(\cache_data_reg_n_0_[1] ));
  FDCE \cache_data_reg[2] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_31),
        .Q(\cache_data_reg_n_0_[2] ));
  FDCE \cache_data_reg[3] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_30),
        .Q(\cache_data_reg_n_0_[3] ));
  FDCE \cache_data_reg[4] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_29),
        .Q(\cache_data_reg_n_0_[4] ));
  FDCE \cache_data_reg[5] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_28),
        .Q(\cache_data_reg_n_0_[5] ));
  FDCE \cache_data_reg[6] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_27),
        .Q(\cache_data_reg_n_0_[6] ));
  FDCE \cache_data_reg[7] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_26),
        .Q(\cache_data_reg_n_0_[7] ));
  FDCE \cache_data_reg[8] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_25),
        .Q(\cache_data_reg_n_0_[8] ));
  FDCE \cache_data_reg[9] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_24),
        .Q(\cache_data_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'hF220)) 
    cache_valid_i_1__0
       (.I0(out_valid_reg_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(Q_next));
  FDCE cache_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next),
        .Q(cache_valid));
  FDCE fifo_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next_1),
        .Q(fifo_valid));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'hEFEE)) 
    out_valid_i_1__0
       (.I0(fifo_valid),
        .I1(cache_valid),
        .I2(AXI4_Stream_Master_TREADY),
        .I3(out_valid_reg_0),
        .O(Q_next_2));
  FDCE out_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(Q_next_2),
        .Q(out_valid_reg_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst
       (.AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .D({u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_2,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_3,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_4,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_5,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_6,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_7,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_8,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_9,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_10,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_11,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_12,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_13,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_14,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_15,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_16,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_17}),
        .IPCORE_CLK(IPCORE_CLK),
        .Q({\cache_data_reg_n_0_[15] ,\cache_data_reg_n_0_[14] ,\cache_data_reg_n_0_[13] ,\cache_data_reg_n_0_[12] ,\cache_data_reg_n_0_[11] ,\cache_data_reg_n_0_[10] ,\cache_data_reg_n_0_[9] ,\cache_data_reg_n_0_[8] ,\cache_data_reg_n_0_[7] ,\cache_data_reg_n_0_[6] ,\cache_data_reg_n_0_[5] ,\cache_data_reg_n_0_[4] ,\cache_data_reg_n_0_[3] ,\cache_data_reg_n_0_[2] ,\cache_data_reg_n_0_[1] ,\cache_data_reg_n_0_[0] }),
        .Q_next_1(Q_next_1),
        .auto_ready_axi4_stream_master(auto_ready_axi4_stream_master),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .cache_valid(cache_valid),
        .\data_int_reg[15] ({u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_18,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_19,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_20,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_21,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_22,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_23,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_24,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_25,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_26,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_27,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_28,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_29,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_30,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_31,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_32,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_33}),
        .fifo_valid(fifo_valid),
        .out_valid(out_valid),
        .p26y_out_add_temp(p26y_out_add_temp),
        .reset(reset),
        .w_d1_reg_0(out_valid_reg_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic
   (Q_next_1,
    auto_ready_axi4_stream_master,
    D,
    \data_int_reg[15] ,
    IPCORE_CLK,
    reset,
    cache_valid,
    w_d1_reg_0,
    fifo_valid,
    AXI4_Stream_Master_TREADY,
    Q,
    auto_ready_dut_enb,
    out_valid,
    p26y_out_add_temp);
  output Q_next_1;
  output auto_ready_axi4_stream_master;
  output [15:0]D;
  output [15:0]\data_int_reg[15] ;
  input IPCORE_CLK;
  input reset;
  input cache_valid;
  input w_d1_reg_0;
  input fifo_valid;
  input AXI4_Stream_Master_TREADY;
  input [15:0]Q;
  input auto_ready_dut_enb;
  input out_valid;
  input [13:0]p26y_out_add_temp;

  wire AXI4_Stream_Master_TREADY;
  wire [15:0]D;
  wire IPCORE_CLK;
  wire [15:0]Q;
  wire Q_next_1;
  wire auto_ready_axi4_stream_master;
  wire auto_ready_dut_enb;
  wire cache_valid;
  wire [15:0]\data_int_reg[15] ;
  wire \fifo_back_indx[0]_i_1_n_0 ;
  wire \fifo_back_indx[1]_i_1_n_0 ;
  wire \fifo_back_indx_reg_n_0_[0] ;
  wire \fifo_back_indx_reg_n_0_[1] ;
  wire \fifo_front_indx[0]_i_1_n_0 ;
  wire \fifo_front_indx[1]_i_1_n_0 ;
  wire \fifo_front_indx_reg_n_0_[0] ;
  wire \fifo_front_indx_reg_n_0_[1] ;
  wire [2:0]fifo_num;
  wire fifo_pop__2;
  wire fifo_read_enable;
  wire \fifo_sample_count[0]_i_1_n_0 ;
  wire \fifo_sample_count[1]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_1_n_0 ;
  wire fifo_valid;
  wire out_valid;
  wire [13:0]p26y_out_add_temp;
  wire reset;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_16;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_17;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_18;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_19;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_20;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_21;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_22;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_23;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_24;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_25;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_26;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_27;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_28;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_29;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_30;
  wire u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_31;
  wire w_d1;
  wire w_d1_reg_0;
  wire \w_d2_reg_n_0_[0] ;
  wire \w_d2_reg_n_0_[10] ;
  wire \w_d2_reg_n_0_[11] ;
  wire \w_d2_reg_n_0_[12] ;
  wire \w_d2_reg_n_0_[13] ;
  wire \w_d2_reg_n_0_[14] ;
  wire \w_d2_reg_n_0_[15] ;
  wire \w_d2_reg_n_0_[1] ;
  wire \w_d2_reg_n_0_[2] ;
  wire \w_d2_reg_n_0_[3] ;
  wire \w_d2_reg_n_0_[4] ;
  wire \w_d2_reg_n_0_[5] ;
  wire \w_d2_reg_n_0_[6] ;
  wire \w_d2_reg_n_0_[7] ;
  wire \w_d2_reg_n_0_[8] ;
  wire \w_d2_reg_n_0_[9] ;
  wire w_mux1;
  wire w_we;

  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_back_indx[0]_i_1 
       (.I0(w_we),
        .I1(\fifo_back_indx_reg_n_0_[0] ),
        .O(\fifo_back_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_back_indx[1]_i_1 
       (.I0(\fifo_back_indx_reg_n_0_[0] ),
        .I1(w_we),
        .I2(\fifo_back_indx_reg_n_0_[1] ),
        .O(\fifo_back_indx[1]_i_1_n_0 ));
  FDCE \fifo_back_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_back_indx[0]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[0] ));
  FDCE \fifo_back_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_back_indx[1]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_front_indx[0]_i_1 
       (.I0(fifo_read_enable),
        .I1(\fifo_front_indx_reg_n_0_[0] ),
        .O(\fifo_front_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_front_indx[1]_i_1 
       (.I0(\fifo_front_indx_reg_n_0_[0] ),
        .I1(fifo_read_enable),
        .I2(\fifo_front_indx_reg_n_0_[1] ),
        .O(\fifo_front_indx[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    \fifo_front_indx[1]_i_2__0 
       (.I0(cache_valid),
        .I1(w_d1_reg_0),
        .I2(fifo_valid),
        .I3(fifo_num[1]),
        .I4(fifo_num[0]),
        .I5(fifo_num[2]),
        .O(fifo_read_enable));
  FDCE \fifo_front_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_front_indx[0]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[0] ));
  FDCE \fifo_front_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_front_indx[1]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[1] ));
  LUT3 #(
    .INIT(8'h15)) 
    fifo_rd_ack_i_1
       (.I0(fifo_num[2]),
        .I1(fifo_num[0]),
        .I2(fifo_num[1]),
        .O(auto_ready_axi4_stream_master));
  LUT6 #(
    .INIT(64'hAA55555567A8A8A8)) 
    \fifo_sample_count[0]_i_1 
       (.I0(fifo_pop__2),
        .I1(fifo_num[1]),
        .I2(fifo_num[2]),
        .I3(out_valid),
        .I4(auto_ready_dut_enb),
        .I5(fifo_num[0]),
        .O(\fifo_sample_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8F80F007F7F8080)) 
    \fifo_sample_count[1]_i_1 
       (.I0(auto_ready_dut_enb),
        .I1(out_valid),
        .I2(fifo_num[0]),
        .I3(fifo_num[2]),
        .I4(fifo_num[1]),
        .I5(fifo_pop__2),
        .O(\fifo_sample_count[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F0007F80FF00)) 
    \fifo_sample_count[2]_i_1 
       (.I0(auto_ready_dut_enb),
        .I1(out_valid),
        .I2(fifo_num[0]),
        .I3(fifo_num[2]),
        .I4(fifo_num[1]),
        .I5(fifo_pop__2),
        .O(\fifo_sample_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FEFEFEFEFEFEFE)) 
    \fifo_sample_count[2]_i_2__0 
       (.I0(fifo_num[1]),
        .I1(fifo_num[0]),
        .I2(fifo_num[2]),
        .I3(fifo_valid),
        .I4(w_d1_reg_0),
        .I5(cache_valid),
        .O(fifo_pop__2));
  FDCE \fifo_sample_count_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[0]_i_1_n_0 ),
        .Q(fifo_num[0]));
  FDCE \fifo_sample_count_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[1]_i_1_n_0 ),
        .Q(fifo_num[1]));
  FDCE \fifo_sample_count_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[2]_i_1_n_0 ),
        .Q(fifo_num[2]));
  LUT5 #(
    .INIT(32'hAAEAAAAA)) 
    fifo_valid_i_1__0
       (.I0(fifo_pop__2),
        .I1(fifo_valid),
        .I2(cache_valid),
        .I3(AXI4_Stream_Master_TREADY),
        .I4(w_d1_reg_0),
        .O(Q_next_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0 u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic
       (.ADDRA({\fifo_front_indx_reg_n_0_[1] ,\fifo_front_indx_reg_n_0_[0] }),
        .ADDRD({\fifo_back_indx_reg_n_0_[1] ,\fifo_back_indx_reg_n_0_[0] }),
        .D(D),
        .IPCORE_CLK(IPCORE_CLK),
        .\Out_1_reg[15] ({\w_d2_reg_n_0_[15] ,\w_d2_reg_n_0_[14] ,\w_d2_reg_n_0_[13] ,\w_d2_reg_n_0_[12] ,\w_d2_reg_n_0_[11] ,\w_d2_reg_n_0_[10] ,\w_d2_reg_n_0_[9] ,\w_d2_reg_n_0_[8] ,\w_d2_reg_n_0_[7] ,\w_d2_reg_n_0_[6] ,\w_d2_reg_n_0_[5] ,\w_d2_reg_n_0_[4] ,\w_d2_reg_n_0_[3] ,\w_d2_reg_n_0_[2] ,\w_d2_reg_n_0_[1] ,\w_d2_reg_n_0_[0] }),
        .Q(Q),
        .auto_ready_dut_enb(auto_ready_dut_enb),
        .cache_valid(cache_valid),
        .data_int({u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_16,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_17,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_18,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_19,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_20,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_21,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_22,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_23,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_24,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_25,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_26,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_27,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_28,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_29,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_30,u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_31}),
        .\data_int_reg[15]_0 (\data_int_reg[15] ),
        .fifo_num(fifo_num),
        .out_valid(out_valid),
        .p26y_out_add_temp(p26y_out_add_temp),
        .w_d1(w_d1),
        .w_we(w_we));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    w_d1_i_1__0
       (.I0(cache_valid),
        .I1(w_d1_reg_0),
        .I2(fifo_valid),
        .I3(fifo_num[2]),
        .I4(fifo_num[0]),
        .I5(fifo_num[1]),
        .O(w_mux1));
  FDCE w_d1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(w_mux1),
        .Q(w_d1));
  FDCE \w_d2_reg[0] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_31),
        .Q(\w_d2_reg_n_0_[0] ));
  FDCE \w_d2_reg[10] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_21),
        .Q(\w_d2_reg_n_0_[10] ));
  FDCE \w_d2_reg[11] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_20),
        .Q(\w_d2_reg_n_0_[11] ));
  FDCE \w_d2_reg[12] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_19),
        .Q(\w_d2_reg_n_0_[12] ));
  FDCE \w_d2_reg[13] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_18),
        .Q(\w_d2_reg_n_0_[13] ));
  FDCE \w_d2_reg[14] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_17),
        .Q(\w_d2_reg_n_0_[14] ));
  FDCE \w_d2_reg[15] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_16),
        .Q(\w_d2_reg_n_0_[15] ));
  FDCE \w_d2_reg[1] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_30),
        .Q(\w_d2_reg_n_0_[1] ));
  FDCE \w_d2_reg[2] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_29),
        .Q(\w_d2_reg_n_0_[2] ));
  FDCE \w_d2_reg[3] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_28),
        .Q(\w_d2_reg_n_0_[3] ));
  FDCE \w_d2_reg[4] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_27),
        .Q(\w_d2_reg_n_0_[4] ));
  FDCE \w_d2_reg[5] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_26),
        .Q(\w_d2_reg_n_0_[5] ));
  FDCE \w_d2_reg[6] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_25),
        .Q(\w_d2_reg_n_0_[6] ));
  FDCE \w_d2_reg[7] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_24),
        .Q(\w_d2_reg_n_0_[7] ));
  FDCE \w_d2_reg[8] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_23),
        .Q(\w_d2_reg_n_0_[8] ));
  FDCE \w_d2_reg[9] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_22),
        .Q(\w_d2_reg_n_0_[9] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic
   (Q_next_1,
    AXI4_Stream_Slave_TREADY,
    D,
    \data_int_reg[13] ,
    IPCORE_CLK,
    reset,
    AXI4_Stream_Slave_TVALID,
    cache_valid,
    fifo_valid_reg,
    fifo_valid,
    fifo_valid_reg_0,
    Q,
    AXI4_Stream_Slave_TDATA);
  output Q_next_1;
  output AXI4_Stream_Slave_TREADY;
  output [13:0]D;
  output [13:0]\data_int_reg[13] ;
  input IPCORE_CLK;
  input reset;
  input AXI4_Stream_Slave_TVALID;
  input cache_valid;
  input fifo_valid_reg;
  input fifo_valid;
  input fifo_valid_reg_0;
  input [13:0]Q;
  input [15:0]AXI4_Stream_Slave_TDATA;

  wire [15:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire [13:0]D;
  wire IPCORE_CLK;
  wire [13:0]Q;
  wire Q_next_1;
  wire cache_valid;
  wire [13:0]data_int;
  wire [13:0]\data_int_reg[13] ;
  wire \fifo_back_indx[0]_i_1_n_0 ;
  wire \fifo_back_indx[1]_i_1_n_0 ;
  wire \fifo_front_indx[0]_i_1_n_0 ;
  wire \fifo_front_indx[1]_i_1_n_0 ;
  wire fifo_pop__2;
  wire fifo_read_enable;
  wire \fifo_sample_count[0]_i_1_n_0 ;
  wire \fifo_sample_count[1]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_1_n_0 ;
  wire \fifo_sample_count_reg_n_0_[0] ;
  wire \fifo_sample_count_reg_n_0_[1] ;
  wire \fifo_sample_count_reg_n_0_[2] ;
  wire fifo_valid;
  wire fifo_valid_reg;
  wire fifo_valid_reg_0;
  wire [1:0]rd_addr;
  wire reset;
  wire w_d1;
  wire [13:0]w_d2;
  wire w_mux1;
  wire [1:0]wr_addr;

  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'hFB)) 
    AXI4_Stream_Slave_TREADY_INST_0
       (.I0(\fifo_sample_count_reg_n_0_[1] ),
        .I1(\fifo_sample_count_reg_n_0_[2] ),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .O(AXI4_Stream_Slave_TREADY));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT5 #(
    .INIT(32'h5575AA8A)) 
    \fifo_back_indx[0]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(wr_addr[0]),
        .O(\fifo_back_indx[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h5575FFFFAA8A0000)) 
    \fifo_back_indx[1]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(wr_addr[0]),
        .I5(wr_addr[1]),
        .O(\fifo_back_indx[1]_i_1_n_0 ));
  FDCE \fifo_back_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_back_indx[0]_i_1_n_0 ),
        .Q(wr_addr[0]));
  FDCE \fifo_back_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_back_indx[1]_i_1_n_0 ),
        .Q(wr_addr[1]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_front_indx[0]_i_1 
       (.I0(fifo_read_enable),
        .I1(rd_addr[0]),
        .O(\fifo_front_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_front_indx[1]_i_1 
       (.I0(rd_addr[0]),
        .I1(fifo_read_enable),
        .I2(rd_addr[1]),
        .O(\fifo_front_indx[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    \fifo_front_indx[1]_i_2 
       (.I0(cache_valid),
        .I1(fifo_valid_reg),
        .I2(fifo_valid),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .I5(\fifo_sample_count_reg_n_0_[2] ),
        .O(fifo_read_enable));
  FDCE \fifo_front_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_front_indx[0]_i_1_n_0 ),
        .Q(rd_addr[0]));
  FDCE \fifo_front_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_front_indx[1]_i_1_n_0 ),
        .Q(rd_addr[1]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hAA5567A8)) 
    \fifo_sample_count[0]_i_1 
       (.I0(fifo_pop__2),
        .I1(\fifo_sample_count_reg_n_0_[1] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(AXI4_Stream_Slave_TVALID),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .O(\fifo_sample_count[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hEE307788)) 
    \fifo_sample_count[1]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(fifo_pop__2),
        .O(\fifo_sample_count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT5 #(
    .INIT(32'hF0C078F0)) 
    \fifo_sample_count[2]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(fifo_pop__2),
        .O(\fifo_sample_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FEFEFEFEFEFEFE)) 
    \fifo_sample_count[2]_i_2 
       (.I0(\fifo_sample_count_reg_n_0_[1] ),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(fifo_valid),
        .I4(fifo_valid_reg),
        .I5(cache_valid),
        .O(fifo_pop__2));
  FDCE \fifo_sample_count_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[0]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[0] ));
  FDCE \fifo_sample_count_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[1]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[1] ));
  FDCE \fifo_sample_count_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\fifo_sample_count[2]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[2] ));
  LUT5 #(
    .INIT(32'hAAEAAAAA)) 
    fifo_valid_i_1
       (.I0(fifo_pop__2),
        .I1(fifo_valid),
        .I2(cache_valid),
        .I3(fifo_valid_reg_0),
        .I4(fifo_valid_reg),
        .O(Q_next_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic u_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic_ram
       (.ADDRA(rd_addr),
        .ADDRD(wr_addr),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .D(D),
        .IPCORE_CLK(IPCORE_CLK),
        .\Out_1_reg[13] (w_d2),
        .Q(Q),
        .cache_valid(cache_valid),
        .data_int(data_int),
        .\data_int_reg[13]_0 (\data_int_reg[13] ),
        .\data_int_reg[1]_0 (\fifo_sample_count_reg_n_0_[0] ),
        .\data_int_reg[1]_1 (\fifo_sample_count_reg_n_0_[2] ),
        .\data_int_reg[1]_2 (\fifo_sample_count_reg_n_0_[1] ),
        .w_d1(w_d1));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    w_d1_i_1
       (.I0(cache_valid),
        .I1(fifo_valid_reg),
        .I2(fifo_valid),
        .I3(\fifo_sample_count_reg_n_0_[2] ),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .I5(\fifo_sample_count_reg_n_0_[1] ),
        .O(w_mux1));
  FDCE w_d1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(reset),
        .D(w_mux1),
        .Q(w_d1));
  FDCE \w_d2_reg[0] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[0]),
        .Q(w_d2[0]));
  FDCE \w_d2_reg[10] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[10]),
        .Q(w_d2[10]));
  FDCE \w_d2_reg[11] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[11]),
        .Q(w_d2[11]));
  FDCE \w_d2_reg[12] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[12]),
        .Q(w_d2[12]));
  FDCE \w_d2_reg[13] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[13]),
        .Q(w_d2[13]));
  FDCE \w_d2_reg[1] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[1]),
        .Q(w_d2[1]));
  FDCE \w_d2_reg[2] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[2]),
        .Q(w_d2[2]));
  FDCE \w_d2_reg[3] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[3]),
        .Q(w_d2[3]));
  FDCE \w_d2_reg[4] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[4]),
        .Q(w_d2[4]));
  FDCE \w_d2_reg[5] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[5]),
        .Q(w_d2[5]));
  FDCE \w_d2_reg[6] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[6]),
        .Q(w_d2[6]));
  FDCE \w_d2_reg[7] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[7]),
        .Q(w_d2[7]));
  FDCE \w_d2_reg[8] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[8]),
        .Q(w_d2[8]));
  FDCE \w_d2_reg[9] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(reset),
        .D(data_int[9]),
        .Q(w_d2[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
