-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Sun Apr  7 08:01:54 2019
-- Host        : DESKTOP-MC6SQ8S running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ System_mlhdlc_sfir_fixpt_ip_0_0_sim_netlist.vhdl
-- Design      : System_mlhdlc_sfir_fixpt_ip_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt is
  port (
    delayed_xout : out STD_LOGIC_VECTOR ( 13 downto 0 );
    p26y_out_add_temp : out STD_LOGIC_VECTOR ( 13 downto 0 );
    p23m4_mul_temp_0 : in STD_LOGIC_VECTOR ( 13 downto 0 );
    p22m3_mul_temp_0 : in STD_LOGIC_VECTOR ( 13 downto 0 );
    p21m2_mul_temp_0 : in STD_LOGIC_VECTOR ( 13 downto 0 );
    p20m1_mul_temp_0 : in STD_LOGIC_VECTOR ( 13 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 13 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt is
  signal B : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \^delayed_xout\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \^p20m1_mul_temp\ : STD_LOGIC_VECTOR ( 26 downto 13 );
  signal p20m1_mul_temp_i_10_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_11_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_12_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_13_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_14_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_15_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_16_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_17_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_18_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_1_n_3 : STD_LOGIC;
  signal p20m1_mul_temp_i_1_n_6 : STD_LOGIC;
  signal p20m1_mul_temp_i_1_n_7 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_1 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_2 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_3 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_4 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_5 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_6 : STD_LOGIC;
  signal p20m1_mul_temp_i_2_n_7 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_1 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_2 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_3 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_4 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_5 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_6 : STD_LOGIC;
  signal p20m1_mul_temp_i_3_n_7 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_1 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_2 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_3 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_4 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_5 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_6 : STD_LOGIC;
  signal p20m1_mul_temp_i_4_n_7 : STD_LOGIC;
  signal p20m1_mul_temp_i_5_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_6_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_7_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_8_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_i_9_n_0 : STD_LOGIC;
  signal p20m1_mul_temp_n_100 : STD_LOGIC;
  signal p20m1_mul_temp_n_101 : STD_LOGIC;
  signal p20m1_mul_temp_n_102 : STD_LOGIC;
  signal p20m1_mul_temp_n_103 : STD_LOGIC;
  signal p20m1_mul_temp_n_104 : STD_LOGIC;
  signal p20m1_mul_temp_n_105 : STD_LOGIC;
  signal p20m1_mul_temp_n_78 : STD_LOGIC;
  signal p20m1_mul_temp_n_93 : STD_LOGIC;
  signal p20m1_mul_temp_n_94 : STD_LOGIC;
  signal p20m1_mul_temp_n_95 : STD_LOGIC;
  signal p20m1_mul_temp_n_96 : STD_LOGIC;
  signal p20m1_mul_temp_n_97 : STD_LOGIC;
  signal p20m1_mul_temp_n_98 : STD_LOGIC;
  signal p20m1_mul_temp_n_99 : STD_LOGIC;
  signal p21m2_mul_temp_i_10_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_11_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_12_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_13_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_14_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_15_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_16_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_17_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_18_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_1_n_3 : STD_LOGIC;
  signal p21m2_mul_temp_i_1_n_6 : STD_LOGIC;
  signal p21m2_mul_temp_i_1_n_7 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_1 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_2 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_3 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_4 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_5 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_6 : STD_LOGIC;
  signal p21m2_mul_temp_i_2_n_7 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_1 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_2 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_3 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_4 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_5 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_6 : STD_LOGIC;
  signal p21m2_mul_temp_i_3_n_7 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_1 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_2 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_3 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_4 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_5 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_6 : STD_LOGIC;
  signal p21m2_mul_temp_i_4_n_7 : STD_LOGIC;
  signal p21m2_mul_temp_i_5_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_6_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_7_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_8_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_i_9_n_0 : STD_LOGIC;
  signal p21m2_mul_temp_n_100 : STD_LOGIC;
  signal p21m2_mul_temp_n_101 : STD_LOGIC;
  signal p21m2_mul_temp_n_102 : STD_LOGIC;
  signal p21m2_mul_temp_n_103 : STD_LOGIC;
  signal p21m2_mul_temp_n_104 : STD_LOGIC;
  signal p21m2_mul_temp_n_105 : STD_LOGIC;
  signal p21m2_mul_temp_n_78 : STD_LOGIC;
  signal p21m2_mul_temp_n_93 : STD_LOGIC;
  signal p21m2_mul_temp_n_94 : STD_LOGIC;
  signal p21m2_mul_temp_n_95 : STD_LOGIC;
  signal p21m2_mul_temp_n_96 : STD_LOGIC;
  signal p21m2_mul_temp_n_97 : STD_LOGIC;
  signal p21m2_mul_temp_n_98 : STD_LOGIC;
  signal p21m2_mul_temp_n_99 : STD_LOGIC;
  signal p22m3_mul_temp_i_10_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_11_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_12_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_13_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_14_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_15_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_16_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_17_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_18_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_1_n_3 : STD_LOGIC;
  signal p22m3_mul_temp_i_1_n_6 : STD_LOGIC;
  signal p22m3_mul_temp_i_1_n_7 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_1 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_2 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_3 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_4 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_5 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_6 : STD_LOGIC;
  signal p22m3_mul_temp_i_2_n_7 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_1 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_2 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_3 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_4 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_5 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_6 : STD_LOGIC;
  signal p22m3_mul_temp_i_3_n_7 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_1 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_2 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_3 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_4 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_5 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_6 : STD_LOGIC;
  signal p22m3_mul_temp_i_4_n_7 : STD_LOGIC;
  signal p22m3_mul_temp_i_5_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_6_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_7_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_8_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_i_9_n_0 : STD_LOGIC;
  signal p22m3_mul_temp_n_100 : STD_LOGIC;
  signal p22m3_mul_temp_n_101 : STD_LOGIC;
  signal p22m3_mul_temp_n_102 : STD_LOGIC;
  signal p22m3_mul_temp_n_103 : STD_LOGIC;
  signal p22m3_mul_temp_n_104 : STD_LOGIC;
  signal p22m3_mul_temp_n_105 : STD_LOGIC;
  signal p22m3_mul_temp_n_77 : STD_LOGIC;
  signal p22m3_mul_temp_n_92 : STD_LOGIC;
  signal p22m3_mul_temp_n_93 : STD_LOGIC;
  signal p22m3_mul_temp_n_94 : STD_LOGIC;
  signal p22m3_mul_temp_n_95 : STD_LOGIC;
  signal p22m3_mul_temp_n_96 : STD_LOGIC;
  signal p22m3_mul_temp_n_97 : STD_LOGIC;
  signal p22m3_mul_temp_n_98 : STD_LOGIC;
  signal p22m3_mul_temp_n_99 : STD_LOGIC;
  signal \^p23m4_mul_temp\ : STD_LOGIC_VECTOR ( 26 downto 13 );
  signal p23m4_mul_temp_i_10_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_11_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_12_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_13_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_14_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_15_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_16_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_17_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_18_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_19_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_1_n_2 : STD_LOGIC;
  signal p23m4_mul_temp_i_1_n_3 : STD_LOGIC;
  signal p23m4_mul_temp_i_2_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_2_n_1 : STD_LOGIC;
  signal p23m4_mul_temp_i_2_n_2 : STD_LOGIC;
  signal p23m4_mul_temp_i_2_n_3 : STD_LOGIC;
  signal p23m4_mul_temp_i_3_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_3_n_1 : STD_LOGIC;
  signal p23m4_mul_temp_i_3_n_2 : STD_LOGIC;
  signal p23m4_mul_temp_i_3_n_3 : STD_LOGIC;
  signal p23m4_mul_temp_i_4_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_4_n_1 : STD_LOGIC;
  signal p23m4_mul_temp_i_4_n_2 : STD_LOGIC;
  signal p23m4_mul_temp_i_4_n_3 : STD_LOGIC;
  signal p23m4_mul_temp_i_5_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_6_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_7_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_8_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_i_9_n_0 : STD_LOGIC;
  signal p23m4_mul_temp_n_100 : STD_LOGIC;
  signal p23m4_mul_temp_n_101 : STD_LOGIC;
  signal p23m4_mul_temp_n_102 : STD_LOGIC;
  signal p23m4_mul_temp_n_103 : STD_LOGIC;
  signal p23m4_mul_temp_n_104 : STD_LOGIC;
  signal p23m4_mul_temp_n_105 : STD_LOGIC;
  signal p23m4_mul_temp_n_77 : STD_LOGIC;
  signal p23m4_mul_temp_n_78 : STD_LOGIC;
  signal p23m4_mul_temp_n_93 : STD_LOGIC;
  signal p23m4_mul_temp_n_94 : STD_LOGIC;
  signal p23m4_mul_temp_n_95 : STD_LOGIC;
  signal p23m4_mul_temp_n_96 : STD_LOGIC;
  signal p23m4_mul_temp_n_97 : STD_LOGIC;
  signal p23m4_mul_temp_n_98 : STD_LOGIC;
  signal p23m4_mul_temp_n_99 : STD_LOGIC;
  signal p24a5_add_cast_1 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \p24a5_add_temp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_n_1\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_n_2\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__0_n_3\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_n_1\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_n_2\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__1_n_3\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__2_n_2\ : STD_LOGIC;
  signal \p24a5_add_temp_carry__2_n_3\ : STD_LOGIC;
  signal p24a5_add_temp_carry_i_1_n_0 : STD_LOGIC;
  signal p24a5_add_temp_carry_i_2_n_0 : STD_LOGIC;
  signal p24a5_add_temp_carry_i_3_n_0 : STD_LOGIC;
  signal p24a5_add_temp_carry_n_0 : STD_LOGIC;
  signal p24a5_add_temp_carry_n_1 : STD_LOGIC;
  signal p24a5_add_temp_carry_n_2 : STD_LOGIC;
  signal p24a5_add_temp_carry_n_3 : STD_LOGIC;
  signal p25a6_add_cast : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal p25a6_add_temp : STD_LOGIC_VECTOR ( 14 downto 2 );
  signal \p25a6_add_temp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_n_1\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_n_2\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__0_n_3\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_n_1\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_n_2\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__1_n_3\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_n_0\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_n_2\ : STD_LOGIC;
  signal \p25a6_add_temp_carry__2_n_3\ : STD_LOGIC;
  signal p25a6_add_temp_carry_i_1_n_0 : STD_LOGIC;
  signal p25a6_add_temp_carry_i_2_n_0 : STD_LOGIC;
  signal p25a6_add_temp_carry_i_3_n_0 : STD_LOGIC;
  signal p25a6_add_temp_carry_n_0 : STD_LOGIC;
  signal p25a6_add_temp_carry_n_1 : STD_LOGIC;
  signal p25a6_add_temp_carry_n_2 : STD_LOGIC;
  signal p25a6_add_temp_carry_n_3 : STD_LOGIC;
  signal p26y_out_add_cast : STD_LOGIC_VECTOR ( 13 downto 1 );
  signal \p26y_out_add_temp_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_n_1\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_n_2\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__0_n_3\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_n_1\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_n_2\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__1_n_3\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__2_n_2\ : STD_LOGIC;
  signal \p26y_out_add_temp_carry__2_n_3\ : STD_LOGIC;
  signal p26y_out_add_temp_carry_i_1_n_0 : STD_LOGIC;
  signal p26y_out_add_temp_carry_i_2_n_0 : STD_LOGIC;
  signal p26y_out_add_temp_carry_i_3_n_0 : STD_LOGIC;
  signal p26y_out_add_temp_carry_n_0 : STD_LOGIC;
  signal p26y_out_add_temp_carry_n_1 : STD_LOGIC;
  signal p26y_out_add_temp_carry_n_2 : STD_LOGIC;
  signal p26y_out_add_temp_carry_n_3 : STD_LOGIC;
  signal ud1 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ud2 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ud3 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ud4 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ud5 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ud6 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ud7 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal NLW_p20m1_mul_temp_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p20m1_mul_temp_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p20m1_mul_temp_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p20m1_mul_temp_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p20m1_mul_temp_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p20m1_mul_temp_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p20m1_mul_temp_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p20m1_mul_temp_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p20m1_mul_temp_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p20m1_mul_temp_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 28 );
  signal NLW_p20m1_mul_temp_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_p20m1_mul_temp_i_1_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_p20m1_mul_temp_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_p21m2_mul_temp_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p21m2_mul_temp_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p21m2_mul_temp_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p21m2_mul_temp_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p21m2_mul_temp_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p21m2_mul_temp_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p21m2_mul_temp_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p21m2_mul_temp_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p21m2_mul_temp_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p21m2_mul_temp_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 28 );
  signal NLW_p21m2_mul_temp_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_p21m2_mul_temp_i_1_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_p21m2_mul_temp_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_p22m3_mul_temp_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p22m3_mul_temp_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p22m3_mul_temp_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p22m3_mul_temp_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p22m3_mul_temp_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p22m3_mul_temp_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p22m3_mul_temp_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p22m3_mul_temp_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p22m3_mul_temp_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p22m3_mul_temp_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 29 );
  signal NLW_p22m3_mul_temp_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_p22m3_mul_temp_i_1_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_p22m3_mul_temp_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_p23m4_mul_temp_CARRYCASCOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p23m4_mul_temp_MULTSIGNOUT_UNCONNECTED : STD_LOGIC;
  signal NLW_p23m4_mul_temp_OVERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p23m4_mul_temp_PATTERNBDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p23m4_mul_temp_PATTERNDETECT_UNCONNECTED : STD_LOGIC;
  signal NLW_p23m4_mul_temp_UNDERFLOW_UNCONNECTED : STD_LOGIC;
  signal NLW_p23m4_mul_temp_ACOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 29 downto 0 );
  signal NLW_p23m4_mul_temp_BCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 17 downto 0 );
  signal NLW_p23m4_mul_temp_CARRYOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_p23m4_mul_temp_P_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 29 );
  signal NLW_p23m4_mul_temp_PCOUT_UNCONNECTED : STD_LOGIC_VECTOR ( 47 downto 0 );
  signal NLW_p23m4_mul_temp_i_1_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal NLW_p23m4_mul_temp_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_p23m4_mul_temp_i_4_O_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_p24a5_add_temp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_p24a5_add_temp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_p24a5_add_temp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_p25a6_add_temp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_p25a6_add_temp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_p25a6_add_temp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_p26y_out_add_temp_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \NLW_p26y_out_add_temp_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_p26y_out_add_temp_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of p20m1_mul_temp : label is "{SYNTH-13 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of p21m2_mul_temp : label is "{SYNTH-13 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of p22m3_mul_temp : label is "{SYNTH-13 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of p23m4_mul_temp : label is "{SYNTH-13 {cell *THIS*}}";
begin
  delayed_xout(13 downto 0) <= \^delayed_xout\(13 downto 0);
p20m1_mul_temp: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => p20m1_mul_temp_0(13),
      A(28) => p20m1_mul_temp_0(13),
      A(27) => p20m1_mul_temp_0(13),
      A(26) => p20m1_mul_temp_0(13),
      A(25) => p20m1_mul_temp_0(13),
      A(24) => p20m1_mul_temp_0(13),
      A(23) => p20m1_mul_temp_0(13),
      A(22) => p20m1_mul_temp_0(13),
      A(21) => p20m1_mul_temp_0(13),
      A(20) => p20m1_mul_temp_0(13),
      A(19) => p20m1_mul_temp_0(13),
      A(18) => p20m1_mul_temp_0(13),
      A(17) => p20m1_mul_temp_0(13),
      A(16) => p20m1_mul_temp_0(13),
      A(15) => p20m1_mul_temp_0(13),
      A(14) => p20m1_mul_temp_0(13),
      A(13 downto 0) => p20m1_mul_temp_0(13 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_p20m1_mul_temp_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => p20m1_mul_temp_i_1_n_6,
      B(16) => p20m1_mul_temp_i_1_n_6,
      B(15) => p20m1_mul_temp_i_1_n_6,
      B(14) => p20m1_mul_temp_i_1_n_6,
      B(13) => p20m1_mul_temp_i_1_n_6,
      B(12) => p20m1_mul_temp_i_1_n_7,
      B(11) => p20m1_mul_temp_i_2_n_4,
      B(10) => p20m1_mul_temp_i_2_n_5,
      B(9) => p20m1_mul_temp_i_2_n_6,
      B(8) => p20m1_mul_temp_i_2_n_7,
      B(7) => p20m1_mul_temp_i_3_n_4,
      B(6) => p20m1_mul_temp_i_3_n_5,
      B(5) => p20m1_mul_temp_i_3_n_6,
      B(4) => p20m1_mul_temp_i_3_n_7,
      B(3) => p20m1_mul_temp_i_4_n_4,
      B(2) => p20m1_mul_temp_i_4_n_5,
      B(1) => p20m1_mul_temp_i_4_n_6,
      B(0) => p20m1_mul_temp_i_4_n_7,
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p20m1_mul_temp_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p20m1_mul_temp_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p20m1_mul_temp_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p20m1_mul_temp_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_p20m1_mul_temp_OVERFLOW_UNCONNECTED,
      P(47 downto 28) => NLW_p20m1_mul_temp_P_UNCONNECTED(47 downto 28),
      P(27) => p20m1_mul_temp_n_78,
      P(26 downto 13) => \^p20m1_mul_temp\(26 downto 13),
      P(12) => p20m1_mul_temp_n_93,
      P(11) => p20m1_mul_temp_n_94,
      P(10) => p20m1_mul_temp_n_95,
      P(9) => p20m1_mul_temp_n_96,
      P(8) => p20m1_mul_temp_n_97,
      P(7) => p20m1_mul_temp_n_98,
      P(6) => p20m1_mul_temp_n_99,
      P(5) => p20m1_mul_temp_n_100,
      P(4) => p20m1_mul_temp_n_101,
      P(3) => p20m1_mul_temp_n_102,
      P(2) => p20m1_mul_temp_n_103,
      P(1) => p20m1_mul_temp_n_104,
      P(0) => p20m1_mul_temp_n_105,
      PATTERNBDETECT => NLW_p20m1_mul_temp_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p20m1_mul_temp_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_p20m1_mul_temp_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p20m1_mul_temp_UNDERFLOW_UNCONNECTED
    );
p20m1_mul_temp_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => p20m1_mul_temp_i_2_n_0,
      CO(3 downto 1) => NLW_p20m1_mul_temp_i_1_CO_UNCONNECTED(3 downto 1),
      CO(0) => p20m1_mul_temp_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => ud1(12),
      O(3 downto 2) => NLW_p20m1_mul_temp_i_1_O_UNCONNECTED(3 downto 2),
      O(1) => p20m1_mul_temp_i_1_n_6,
      O(0) => p20m1_mul_temp_i_1_n_7,
      S(3 downto 2) => B"00",
      S(1) => p20m1_mul_temp_i_5_n_0,
      S(0) => p20m1_mul_temp_i_6_n_0
    );
p20m1_mul_temp_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(8),
      I1 => \^delayed_xout\(8),
      O => p20m1_mul_temp_i_10_n_0
    );
p20m1_mul_temp_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(7),
      I1 => \^delayed_xout\(7),
      O => p20m1_mul_temp_i_11_n_0
    );
p20m1_mul_temp_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(6),
      I1 => \^delayed_xout\(6),
      O => p20m1_mul_temp_i_12_n_0
    );
p20m1_mul_temp_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(5),
      I1 => \^delayed_xout\(5),
      O => p20m1_mul_temp_i_13_n_0
    );
p20m1_mul_temp_i_14: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(4),
      I1 => \^delayed_xout\(4),
      O => p20m1_mul_temp_i_14_n_0
    );
p20m1_mul_temp_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(3),
      I1 => \^delayed_xout\(3),
      O => p20m1_mul_temp_i_15_n_0
    );
p20m1_mul_temp_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(2),
      I1 => \^delayed_xout\(2),
      O => p20m1_mul_temp_i_16_n_0
    );
p20m1_mul_temp_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(1),
      I1 => \^delayed_xout\(1),
      O => p20m1_mul_temp_i_17_n_0
    );
p20m1_mul_temp_i_18: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(0),
      I1 => \^delayed_xout\(0),
      O => p20m1_mul_temp_i_18_n_0
    );
p20m1_mul_temp_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => p20m1_mul_temp_i_3_n_0,
      CO(3) => p20m1_mul_temp_i_2_n_0,
      CO(2) => p20m1_mul_temp_i_2_n_1,
      CO(1) => p20m1_mul_temp_i_2_n_2,
      CO(0) => p20m1_mul_temp_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud1(11 downto 8),
      O(3) => p20m1_mul_temp_i_2_n_4,
      O(2) => p20m1_mul_temp_i_2_n_5,
      O(1) => p20m1_mul_temp_i_2_n_6,
      O(0) => p20m1_mul_temp_i_2_n_7,
      S(3) => p20m1_mul_temp_i_7_n_0,
      S(2) => p20m1_mul_temp_i_8_n_0,
      S(1) => p20m1_mul_temp_i_9_n_0,
      S(0) => p20m1_mul_temp_i_10_n_0
    );
p20m1_mul_temp_i_3: unisim.vcomponents.CARRY4
     port map (
      CI => p20m1_mul_temp_i_4_n_0,
      CO(3) => p20m1_mul_temp_i_3_n_0,
      CO(2) => p20m1_mul_temp_i_3_n_1,
      CO(1) => p20m1_mul_temp_i_3_n_2,
      CO(0) => p20m1_mul_temp_i_3_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud1(7 downto 4),
      O(3) => p20m1_mul_temp_i_3_n_4,
      O(2) => p20m1_mul_temp_i_3_n_5,
      O(1) => p20m1_mul_temp_i_3_n_6,
      O(0) => p20m1_mul_temp_i_3_n_7,
      S(3) => p20m1_mul_temp_i_11_n_0,
      S(2) => p20m1_mul_temp_i_12_n_0,
      S(1) => p20m1_mul_temp_i_13_n_0,
      S(0) => p20m1_mul_temp_i_14_n_0
    );
p20m1_mul_temp_i_4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p20m1_mul_temp_i_4_n_0,
      CO(2) => p20m1_mul_temp_i_4_n_1,
      CO(1) => p20m1_mul_temp_i_4_n_2,
      CO(0) => p20m1_mul_temp_i_4_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud1(3 downto 0),
      O(3) => p20m1_mul_temp_i_4_n_4,
      O(2) => p20m1_mul_temp_i_4_n_5,
      O(1) => p20m1_mul_temp_i_4_n_6,
      O(0) => p20m1_mul_temp_i_4_n_7,
      S(3) => p20m1_mul_temp_i_15_n_0,
      S(2) => p20m1_mul_temp_i_16_n_0,
      S(1) => p20m1_mul_temp_i_17_n_0,
      S(0) => p20m1_mul_temp_i_18_n_0
    );
p20m1_mul_temp_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(13),
      I1 => \^delayed_xout\(13),
      O => p20m1_mul_temp_i_5_n_0
    );
p20m1_mul_temp_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(12),
      I1 => \^delayed_xout\(12),
      O => p20m1_mul_temp_i_6_n_0
    );
p20m1_mul_temp_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(11),
      I1 => \^delayed_xout\(11),
      O => p20m1_mul_temp_i_7_n_0
    );
p20m1_mul_temp_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(10),
      I1 => \^delayed_xout\(10),
      O => p20m1_mul_temp_i_8_n_0
    );
p20m1_mul_temp_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud1(9),
      I1 => \^delayed_xout\(9),
      O => p20m1_mul_temp_i_9_n_0
    );
p21m2_mul_temp: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29) => p21m2_mul_temp_0(13),
      A(28) => p21m2_mul_temp_0(13),
      A(27) => p21m2_mul_temp_0(13),
      A(26) => p21m2_mul_temp_0(13),
      A(25) => p21m2_mul_temp_0(13),
      A(24) => p21m2_mul_temp_0(13),
      A(23) => p21m2_mul_temp_0(13),
      A(22) => p21m2_mul_temp_0(13),
      A(21) => p21m2_mul_temp_0(13),
      A(20) => p21m2_mul_temp_0(13),
      A(19) => p21m2_mul_temp_0(13),
      A(18) => p21m2_mul_temp_0(13),
      A(17) => p21m2_mul_temp_0(13),
      A(16) => p21m2_mul_temp_0(13),
      A(15) => p21m2_mul_temp_0(13),
      A(14) => p21m2_mul_temp_0(13),
      A(13 downto 0) => p21m2_mul_temp_0(13 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_p21m2_mul_temp_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => p21m2_mul_temp_i_1_n_6,
      B(16) => p21m2_mul_temp_i_1_n_6,
      B(15) => p21m2_mul_temp_i_1_n_6,
      B(14) => p21m2_mul_temp_i_1_n_6,
      B(13) => p21m2_mul_temp_i_1_n_6,
      B(12) => p21m2_mul_temp_i_1_n_7,
      B(11) => p21m2_mul_temp_i_2_n_4,
      B(10) => p21m2_mul_temp_i_2_n_5,
      B(9) => p21m2_mul_temp_i_2_n_6,
      B(8) => p21m2_mul_temp_i_2_n_7,
      B(7) => p21m2_mul_temp_i_3_n_4,
      B(6) => p21m2_mul_temp_i_3_n_5,
      B(5) => p21m2_mul_temp_i_3_n_6,
      B(4) => p21m2_mul_temp_i_3_n_7,
      B(3) => p21m2_mul_temp_i_4_n_4,
      B(2) => p21m2_mul_temp_i_4_n_5,
      B(1) => p21m2_mul_temp_i_4_n_6,
      B(0) => p21m2_mul_temp_i_4_n_7,
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p21m2_mul_temp_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p21m2_mul_temp_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p21m2_mul_temp_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p21m2_mul_temp_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_p21m2_mul_temp_OVERFLOW_UNCONNECTED,
      P(47 downto 28) => NLW_p21m2_mul_temp_P_UNCONNECTED(47 downto 28),
      P(27) => p21m2_mul_temp_n_78,
      P(26 downto 13) => p24a5_add_cast_1(13 downto 0),
      P(12) => p21m2_mul_temp_n_93,
      P(11) => p21m2_mul_temp_n_94,
      P(10) => p21m2_mul_temp_n_95,
      P(9) => p21m2_mul_temp_n_96,
      P(8) => p21m2_mul_temp_n_97,
      P(7) => p21m2_mul_temp_n_98,
      P(6) => p21m2_mul_temp_n_99,
      P(5) => p21m2_mul_temp_n_100,
      P(4) => p21m2_mul_temp_n_101,
      P(3) => p21m2_mul_temp_n_102,
      P(2) => p21m2_mul_temp_n_103,
      P(1) => p21m2_mul_temp_n_104,
      P(0) => p21m2_mul_temp_n_105,
      PATTERNBDETECT => NLW_p21m2_mul_temp_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p21m2_mul_temp_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_p21m2_mul_temp_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p21m2_mul_temp_UNDERFLOW_UNCONNECTED
    );
p21m2_mul_temp_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => p21m2_mul_temp_i_2_n_0,
      CO(3 downto 1) => NLW_p21m2_mul_temp_i_1_CO_UNCONNECTED(3 downto 1),
      CO(0) => p21m2_mul_temp_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => ud2(12),
      O(3 downto 2) => NLW_p21m2_mul_temp_i_1_O_UNCONNECTED(3 downto 2),
      O(1) => p21m2_mul_temp_i_1_n_6,
      O(0) => p21m2_mul_temp_i_1_n_7,
      S(3 downto 2) => B"00",
      S(1) => p21m2_mul_temp_i_5_n_0,
      S(0) => p21m2_mul_temp_i_6_n_0
    );
p21m2_mul_temp_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(8),
      I1 => ud7(8),
      O => p21m2_mul_temp_i_10_n_0
    );
p21m2_mul_temp_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(7),
      I1 => ud7(7),
      O => p21m2_mul_temp_i_11_n_0
    );
p21m2_mul_temp_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(6),
      I1 => ud7(6),
      O => p21m2_mul_temp_i_12_n_0
    );
p21m2_mul_temp_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(5),
      I1 => ud7(5),
      O => p21m2_mul_temp_i_13_n_0
    );
p21m2_mul_temp_i_14: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(4),
      I1 => ud7(4),
      O => p21m2_mul_temp_i_14_n_0
    );
p21m2_mul_temp_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(3),
      I1 => ud7(3),
      O => p21m2_mul_temp_i_15_n_0
    );
p21m2_mul_temp_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(2),
      I1 => ud7(2),
      O => p21m2_mul_temp_i_16_n_0
    );
p21m2_mul_temp_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(1),
      I1 => ud7(1),
      O => p21m2_mul_temp_i_17_n_0
    );
p21m2_mul_temp_i_18: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(0),
      I1 => ud7(0),
      O => p21m2_mul_temp_i_18_n_0
    );
p21m2_mul_temp_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => p21m2_mul_temp_i_3_n_0,
      CO(3) => p21m2_mul_temp_i_2_n_0,
      CO(2) => p21m2_mul_temp_i_2_n_1,
      CO(1) => p21m2_mul_temp_i_2_n_2,
      CO(0) => p21m2_mul_temp_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud2(11 downto 8),
      O(3) => p21m2_mul_temp_i_2_n_4,
      O(2) => p21m2_mul_temp_i_2_n_5,
      O(1) => p21m2_mul_temp_i_2_n_6,
      O(0) => p21m2_mul_temp_i_2_n_7,
      S(3) => p21m2_mul_temp_i_7_n_0,
      S(2) => p21m2_mul_temp_i_8_n_0,
      S(1) => p21m2_mul_temp_i_9_n_0,
      S(0) => p21m2_mul_temp_i_10_n_0
    );
p21m2_mul_temp_i_3: unisim.vcomponents.CARRY4
     port map (
      CI => p21m2_mul_temp_i_4_n_0,
      CO(3) => p21m2_mul_temp_i_3_n_0,
      CO(2) => p21m2_mul_temp_i_3_n_1,
      CO(1) => p21m2_mul_temp_i_3_n_2,
      CO(0) => p21m2_mul_temp_i_3_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud2(7 downto 4),
      O(3) => p21m2_mul_temp_i_3_n_4,
      O(2) => p21m2_mul_temp_i_3_n_5,
      O(1) => p21m2_mul_temp_i_3_n_6,
      O(0) => p21m2_mul_temp_i_3_n_7,
      S(3) => p21m2_mul_temp_i_11_n_0,
      S(2) => p21m2_mul_temp_i_12_n_0,
      S(1) => p21m2_mul_temp_i_13_n_0,
      S(0) => p21m2_mul_temp_i_14_n_0
    );
p21m2_mul_temp_i_4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p21m2_mul_temp_i_4_n_0,
      CO(2) => p21m2_mul_temp_i_4_n_1,
      CO(1) => p21m2_mul_temp_i_4_n_2,
      CO(0) => p21m2_mul_temp_i_4_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud2(3 downto 0),
      O(3) => p21m2_mul_temp_i_4_n_4,
      O(2) => p21m2_mul_temp_i_4_n_5,
      O(1) => p21m2_mul_temp_i_4_n_6,
      O(0) => p21m2_mul_temp_i_4_n_7,
      S(3) => p21m2_mul_temp_i_15_n_0,
      S(2) => p21m2_mul_temp_i_16_n_0,
      S(1) => p21m2_mul_temp_i_17_n_0,
      S(0) => p21m2_mul_temp_i_18_n_0
    );
p21m2_mul_temp_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(13),
      I1 => ud7(13),
      O => p21m2_mul_temp_i_5_n_0
    );
p21m2_mul_temp_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(12),
      I1 => ud7(12),
      O => p21m2_mul_temp_i_6_n_0
    );
p21m2_mul_temp_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(11),
      I1 => ud7(11),
      O => p21m2_mul_temp_i_7_n_0
    );
p21m2_mul_temp_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(10),
      I1 => ud7(10),
      O => p21m2_mul_temp_i_8_n_0
    );
p21m2_mul_temp_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud2(9),
      I1 => ud7(9),
      O => p21m2_mul_temp_i_9_n_0
    );
p22m3_mul_temp: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 14) => B"0000000000000000",
      A(13 downto 0) => p22m3_mul_temp_0(13 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_p22m3_mul_temp_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => p22m3_mul_temp_i_1_n_6,
      B(16) => p22m3_mul_temp_i_1_n_6,
      B(15) => p22m3_mul_temp_i_1_n_6,
      B(14) => p22m3_mul_temp_i_1_n_6,
      B(13) => p22m3_mul_temp_i_1_n_6,
      B(12) => p22m3_mul_temp_i_1_n_7,
      B(11) => p22m3_mul_temp_i_2_n_4,
      B(10) => p22m3_mul_temp_i_2_n_5,
      B(9) => p22m3_mul_temp_i_2_n_6,
      B(8) => p22m3_mul_temp_i_2_n_7,
      B(7) => p22m3_mul_temp_i_3_n_4,
      B(6) => p22m3_mul_temp_i_3_n_5,
      B(5) => p22m3_mul_temp_i_3_n_6,
      B(4) => p22m3_mul_temp_i_3_n_7,
      B(3) => p22m3_mul_temp_i_4_n_4,
      B(2) => p22m3_mul_temp_i_4_n_5,
      B(1) => p22m3_mul_temp_i_4_n_6,
      B(0) => p22m3_mul_temp_i_4_n_7,
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p22m3_mul_temp_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p22m3_mul_temp_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p22m3_mul_temp_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p22m3_mul_temp_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_p22m3_mul_temp_OVERFLOW_UNCONNECTED,
      P(47 downto 29) => NLW_p22m3_mul_temp_P_UNCONNECTED(47 downto 29),
      P(28) => p22m3_mul_temp_n_77,
      P(27 downto 14) => p25a6_add_cast(13 downto 0),
      P(13) => p22m3_mul_temp_n_92,
      P(12) => p22m3_mul_temp_n_93,
      P(11) => p22m3_mul_temp_n_94,
      P(10) => p22m3_mul_temp_n_95,
      P(9) => p22m3_mul_temp_n_96,
      P(8) => p22m3_mul_temp_n_97,
      P(7) => p22m3_mul_temp_n_98,
      P(6) => p22m3_mul_temp_n_99,
      P(5) => p22m3_mul_temp_n_100,
      P(4) => p22m3_mul_temp_n_101,
      P(3) => p22m3_mul_temp_n_102,
      P(2) => p22m3_mul_temp_n_103,
      P(1) => p22m3_mul_temp_n_104,
      P(0) => p22m3_mul_temp_n_105,
      PATTERNBDETECT => NLW_p22m3_mul_temp_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p22m3_mul_temp_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_p22m3_mul_temp_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p22m3_mul_temp_UNDERFLOW_UNCONNECTED
    );
p22m3_mul_temp_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => p22m3_mul_temp_i_2_n_0,
      CO(3 downto 1) => NLW_p22m3_mul_temp_i_1_CO_UNCONNECTED(3 downto 1),
      CO(0) => p22m3_mul_temp_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => ud3(12),
      O(3 downto 2) => NLW_p22m3_mul_temp_i_1_O_UNCONNECTED(3 downto 2),
      O(1) => p22m3_mul_temp_i_1_n_6,
      O(0) => p22m3_mul_temp_i_1_n_7,
      S(3 downto 2) => B"00",
      S(1) => p22m3_mul_temp_i_5_n_0,
      S(0) => p22m3_mul_temp_i_6_n_0
    );
p22m3_mul_temp_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(8),
      I1 => ud6(8),
      O => p22m3_mul_temp_i_10_n_0
    );
p22m3_mul_temp_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(7),
      I1 => ud6(7),
      O => p22m3_mul_temp_i_11_n_0
    );
p22m3_mul_temp_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(6),
      I1 => ud6(6),
      O => p22m3_mul_temp_i_12_n_0
    );
p22m3_mul_temp_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(5),
      I1 => ud6(5),
      O => p22m3_mul_temp_i_13_n_0
    );
p22m3_mul_temp_i_14: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(4),
      I1 => ud6(4),
      O => p22m3_mul_temp_i_14_n_0
    );
p22m3_mul_temp_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(3),
      I1 => ud6(3),
      O => p22m3_mul_temp_i_15_n_0
    );
p22m3_mul_temp_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(2),
      I1 => ud6(2),
      O => p22m3_mul_temp_i_16_n_0
    );
p22m3_mul_temp_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(1),
      I1 => ud6(1),
      O => p22m3_mul_temp_i_17_n_0
    );
p22m3_mul_temp_i_18: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(0),
      I1 => ud6(0),
      O => p22m3_mul_temp_i_18_n_0
    );
p22m3_mul_temp_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => p22m3_mul_temp_i_3_n_0,
      CO(3) => p22m3_mul_temp_i_2_n_0,
      CO(2) => p22m3_mul_temp_i_2_n_1,
      CO(1) => p22m3_mul_temp_i_2_n_2,
      CO(0) => p22m3_mul_temp_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud3(11 downto 8),
      O(3) => p22m3_mul_temp_i_2_n_4,
      O(2) => p22m3_mul_temp_i_2_n_5,
      O(1) => p22m3_mul_temp_i_2_n_6,
      O(0) => p22m3_mul_temp_i_2_n_7,
      S(3) => p22m3_mul_temp_i_7_n_0,
      S(2) => p22m3_mul_temp_i_8_n_0,
      S(1) => p22m3_mul_temp_i_9_n_0,
      S(0) => p22m3_mul_temp_i_10_n_0
    );
p22m3_mul_temp_i_3: unisim.vcomponents.CARRY4
     port map (
      CI => p22m3_mul_temp_i_4_n_0,
      CO(3) => p22m3_mul_temp_i_3_n_0,
      CO(2) => p22m3_mul_temp_i_3_n_1,
      CO(1) => p22m3_mul_temp_i_3_n_2,
      CO(0) => p22m3_mul_temp_i_3_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud3(7 downto 4),
      O(3) => p22m3_mul_temp_i_3_n_4,
      O(2) => p22m3_mul_temp_i_3_n_5,
      O(1) => p22m3_mul_temp_i_3_n_6,
      O(0) => p22m3_mul_temp_i_3_n_7,
      S(3) => p22m3_mul_temp_i_11_n_0,
      S(2) => p22m3_mul_temp_i_12_n_0,
      S(1) => p22m3_mul_temp_i_13_n_0,
      S(0) => p22m3_mul_temp_i_14_n_0
    );
p22m3_mul_temp_i_4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p22m3_mul_temp_i_4_n_0,
      CO(2) => p22m3_mul_temp_i_4_n_1,
      CO(1) => p22m3_mul_temp_i_4_n_2,
      CO(0) => p22m3_mul_temp_i_4_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud3(3 downto 0),
      O(3) => p22m3_mul_temp_i_4_n_4,
      O(2) => p22m3_mul_temp_i_4_n_5,
      O(1) => p22m3_mul_temp_i_4_n_6,
      O(0) => p22m3_mul_temp_i_4_n_7,
      S(3) => p22m3_mul_temp_i_15_n_0,
      S(2) => p22m3_mul_temp_i_16_n_0,
      S(1) => p22m3_mul_temp_i_17_n_0,
      S(0) => p22m3_mul_temp_i_18_n_0
    );
p22m3_mul_temp_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(13),
      I1 => ud6(13),
      O => p22m3_mul_temp_i_5_n_0
    );
p22m3_mul_temp_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(12),
      I1 => ud6(12),
      O => p22m3_mul_temp_i_6_n_0
    );
p22m3_mul_temp_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(11),
      I1 => ud6(11),
      O => p22m3_mul_temp_i_7_n_0
    );
p22m3_mul_temp_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(10),
      I1 => ud6(10),
      O => p22m3_mul_temp_i_8_n_0
    );
p22m3_mul_temp_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud3(9),
      I1 => ud6(9),
      O => p22m3_mul_temp_i_9_n_0
    );
p23m4_mul_temp: unisim.vcomponents.DSP48E1
    generic map(
      ACASCREG => 0,
      ADREG => 1,
      ALUMODEREG => 0,
      AREG => 0,
      AUTORESET_PATDET => "NO_RESET",
      A_INPUT => "DIRECT",
      BCASCREG => 0,
      BREG => 0,
      B_INPUT => "DIRECT",
      CARRYINREG => 0,
      CARRYINSELREG => 0,
      CREG => 1,
      DREG => 1,
      INMODEREG => 0,
      MASK => X"3FFFFFFFFFFF",
      MREG => 0,
      OPMODEREG => 0,
      PATTERN => X"000000000000",
      PREG => 0,
      SEL_MASK => "MASK",
      SEL_PATTERN => "PATTERN",
      USE_DPORT => false,
      USE_MULT => "MULTIPLY",
      USE_PATTERN_DETECT => "NO_PATDET",
      USE_SIMD => "ONE48"
    )
        port map (
      A(29 downto 14) => B"0000000000000000",
      A(13 downto 0) => p23m4_mul_temp_0(13 downto 0),
      ACIN(29 downto 0) => B"000000000000000000000000000000",
      ACOUT(29 downto 0) => NLW_p23m4_mul_temp_ACOUT_UNCONNECTED(29 downto 0),
      ALUMODE(3 downto 0) => B"0000",
      B(17) => B(13),
      B(16) => B(13),
      B(15) => B(13),
      B(14) => B(13),
      B(13 downto 0) => B(13 downto 0),
      BCIN(17 downto 0) => B"000000000000000000",
      BCOUT(17 downto 0) => NLW_p23m4_mul_temp_BCOUT_UNCONNECTED(17 downto 0),
      C(47 downto 0) => B"111111111111111111111111111111111111111111111111",
      CARRYCASCIN => '0',
      CARRYCASCOUT => NLW_p23m4_mul_temp_CARRYCASCOUT_UNCONNECTED,
      CARRYIN => '0',
      CARRYINSEL(2 downto 0) => B"000",
      CARRYOUT(3 downto 0) => NLW_p23m4_mul_temp_CARRYOUT_UNCONNECTED(3 downto 0),
      CEA1 => '0',
      CEA2 => '0',
      CEAD => '0',
      CEALUMODE => '0',
      CEB1 => '0',
      CEB2 => '0',
      CEC => '0',
      CECARRYIN => '0',
      CECTRL => '0',
      CED => '0',
      CEINMODE => '0',
      CEM => '0',
      CEP => '0',
      CLK => '0',
      D(24 downto 0) => B"0000000000000000000000000",
      INMODE(4 downto 0) => B"00000",
      MULTSIGNIN => '0',
      MULTSIGNOUT => NLW_p23m4_mul_temp_MULTSIGNOUT_UNCONNECTED,
      OPMODE(6 downto 0) => B"0000101",
      OVERFLOW => NLW_p23m4_mul_temp_OVERFLOW_UNCONNECTED,
      P(47 downto 29) => NLW_p23m4_mul_temp_P_UNCONNECTED(47 downto 29),
      P(28) => p23m4_mul_temp_n_77,
      P(27) => p23m4_mul_temp_n_78,
      P(26 downto 13) => \^p23m4_mul_temp\(26 downto 13),
      P(12) => p23m4_mul_temp_n_93,
      P(11) => p23m4_mul_temp_n_94,
      P(10) => p23m4_mul_temp_n_95,
      P(9) => p23m4_mul_temp_n_96,
      P(8) => p23m4_mul_temp_n_97,
      P(7) => p23m4_mul_temp_n_98,
      P(6) => p23m4_mul_temp_n_99,
      P(5) => p23m4_mul_temp_n_100,
      P(4) => p23m4_mul_temp_n_101,
      P(3) => p23m4_mul_temp_n_102,
      P(2) => p23m4_mul_temp_n_103,
      P(1) => p23m4_mul_temp_n_104,
      P(0) => p23m4_mul_temp_n_105,
      PATTERNBDETECT => NLW_p23m4_mul_temp_PATTERNBDETECT_UNCONNECTED,
      PATTERNDETECT => NLW_p23m4_mul_temp_PATTERNDETECT_UNCONNECTED,
      PCIN(47 downto 0) => B"000000000000000000000000000000000000000000000000",
      PCOUT(47 downto 0) => NLW_p23m4_mul_temp_PCOUT_UNCONNECTED(47 downto 0),
      RSTA => '0',
      RSTALLCARRYIN => '0',
      RSTALUMODE => '0',
      RSTB => '0',
      RSTC => '0',
      RSTCTRL => '0',
      RSTD => '0',
      RSTINMODE => '0',
      RSTM => '0',
      RSTP => '0',
      UNDERFLOW => NLW_p23m4_mul_temp_UNDERFLOW_UNCONNECTED
    );
p23m4_mul_temp_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => p23m4_mul_temp_i_2_n_0,
      CO(3 downto 2) => NLW_p23m4_mul_temp_i_1_CO_UNCONNECTED(3 downto 2),
      CO(1) => p23m4_mul_temp_i_1_n_2,
      CO(0) => p23m4_mul_temp_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => p23m4_mul_temp_i_5_n_0,
      DI(0) => ud4(12),
      O(3) => NLW_p23m4_mul_temp_i_1_O_UNCONNECTED(3),
      O(2 downto 0) => B(13 downto 11),
      S(3 downto 2) => B"01",
      S(1) => p23m4_mul_temp_i_6_n_0,
      S(0) => p23m4_mul_temp_i_7_n_0
    );
p23m4_mul_temp_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(9),
      I1 => ud5(9),
      O => p23m4_mul_temp_i_10_n_0
    );
p23m4_mul_temp_i_11: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(8),
      I1 => ud5(8),
      O => p23m4_mul_temp_i_11_n_0
    );
p23m4_mul_temp_i_12: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(7),
      I1 => ud5(7),
      O => p23m4_mul_temp_i_12_n_0
    );
p23m4_mul_temp_i_13: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(6),
      I1 => ud5(6),
      O => p23m4_mul_temp_i_13_n_0
    );
p23m4_mul_temp_i_14: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(5),
      I1 => ud5(5),
      O => p23m4_mul_temp_i_14_n_0
    );
p23m4_mul_temp_i_15: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(4),
      I1 => ud5(4),
      O => p23m4_mul_temp_i_15_n_0
    );
p23m4_mul_temp_i_16: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(3),
      I1 => ud5(3),
      O => p23m4_mul_temp_i_16_n_0
    );
p23m4_mul_temp_i_17: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(2),
      I1 => ud5(2),
      O => p23m4_mul_temp_i_17_n_0
    );
p23m4_mul_temp_i_18: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(1),
      I1 => ud5(1),
      O => p23m4_mul_temp_i_18_n_0
    );
p23m4_mul_temp_i_19: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(0),
      I1 => ud5(0),
      O => p23m4_mul_temp_i_19_n_0
    );
p23m4_mul_temp_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => p23m4_mul_temp_i_3_n_0,
      CO(3) => p23m4_mul_temp_i_2_n_0,
      CO(2) => p23m4_mul_temp_i_2_n_1,
      CO(1) => p23m4_mul_temp_i_2_n_2,
      CO(0) => p23m4_mul_temp_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud4(11 downto 8),
      O(3 downto 0) => B(10 downto 7),
      S(3) => p23m4_mul_temp_i_8_n_0,
      S(2) => p23m4_mul_temp_i_9_n_0,
      S(1) => p23m4_mul_temp_i_10_n_0,
      S(0) => p23m4_mul_temp_i_11_n_0
    );
p23m4_mul_temp_i_3: unisim.vcomponents.CARRY4
     port map (
      CI => p23m4_mul_temp_i_4_n_0,
      CO(3) => p23m4_mul_temp_i_3_n_0,
      CO(2) => p23m4_mul_temp_i_3_n_1,
      CO(1) => p23m4_mul_temp_i_3_n_2,
      CO(0) => p23m4_mul_temp_i_3_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud4(7 downto 4),
      O(3 downto 0) => B(6 downto 3),
      S(3) => p23m4_mul_temp_i_12_n_0,
      S(2) => p23m4_mul_temp_i_13_n_0,
      S(1) => p23m4_mul_temp_i_14_n_0,
      S(0) => p23m4_mul_temp_i_15_n_0
    );
p23m4_mul_temp_i_4: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p23m4_mul_temp_i_4_n_0,
      CO(2) => p23m4_mul_temp_i_4_n_1,
      CO(1) => p23m4_mul_temp_i_4_n_2,
      CO(0) => p23m4_mul_temp_i_4_n_3,
      CYINIT => '0',
      DI(3 downto 0) => ud4(3 downto 0),
      O(3 downto 1) => B(2 downto 0),
      O(0) => NLW_p23m4_mul_temp_i_4_O_UNCONNECTED(0),
      S(3) => p23m4_mul_temp_i_16_n_0,
      S(2) => p23m4_mul_temp_i_17_n_0,
      S(1) => p23m4_mul_temp_i_18_n_0,
      S(0) => p23m4_mul_temp_i_19_n_0
    );
p23m4_mul_temp_i_5: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ud4(13),
      O => p23m4_mul_temp_i_5_n_0
    );
p23m4_mul_temp_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(13),
      I1 => ud5(13),
      O => p23m4_mul_temp_i_6_n_0
    );
p23m4_mul_temp_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(12),
      I1 => ud5(12),
      O => p23m4_mul_temp_i_7_n_0
    );
p23m4_mul_temp_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(11),
      I1 => ud5(11),
      O => p23m4_mul_temp_i_8_n_0
    );
p23m4_mul_temp_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => ud4(10),
      I1 => ud5(10),
      O => p23m4_mul_temp_i_9_n_0
    );
p24a5_add_temp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p24a5_add_temp_carry_n_0,
      CO(2) => p24a5_add_temp_carry_n_1,
      CO(1) => p24a5_add_temp_carry_n_2,
      CO(0) => p24a5_add_temp_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => \^p20m1_mul_temp\(15 downto 13),
      DI(0) => '0',
      O(3 downto 2) => p26y_out_add_cast(2 downto 1),
      O(1 downto 0) => NLW_p24a5_add_temp_carry_O_UNCONNECTED(1 downto 0),
      S(3) => p24a5_add_temp_carry_i_1_n_0,
      S(2) => p24a5_add_temp_carry_i_2_n_0,
      S(1) => p24a5_add_temp_carry_i_3_n_0,
      S(0) => p24a5_add_cast_1(0)
    );
\p24a5_add_temp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => p24a5_add_temp_carry_n_0,
      CO(3) => \p24a5_add_temp_carry__0_n_0\,
      CO(2) => \p24a5_add_temp_carry__0_n_1\,
      CO(1) => \p24a5_add_temp_carry__0_n_2\,
      CO(0) => \p24a5_add_temp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^p20m1_mul_temp\(19 downto 16),
      O(3 downto 0) => p26y_out_add_cast(6 downto 3),
      S(3) => \p24a5_add_temp_carry__0_i_1_n_0\,
      S(2) => \p24a5_add_temp_carry__0_i_2_n_0\,
      S(1) => \p24a5_add_temp_carry__0_i_3_n_0\,
      S(0) => \p24a5_add_temp_carry__0_i_4_n_0\
    );
\p24a5_add_temp_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(19),
      I1 => p24a5_add_cast_1(7),
      O => \p24a5_add_temp_carry__0_i_1_n_0\
    );
\p24a5_add_temp_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(18),
      I1 => p24a5_add_cast_1(6),
      O => \p24a5_add_temp_carry__0_i_2_n_0\
    );
\p24a5_add_temp_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(17),
      I1 => p24a5_add_cast_1(5),
      O => \p24a5_add_temp_carry__0_i_3_n_0\
    );
\p24a5_add_temp_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(16),
      I1 => p24a5_add_cast_1(4),
      O => \p24a5_add_temp_carry__0_i_4_n_0\
    );
\p24a5_add_temp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \p24a5_add_temp_carry__0_n_0\,
      CO(3) => \p24a5_add_temp_carry__1_n_0\,
      CO(2) => \p24a5_add_temp_carry__1_n_1\,
      CO(1) => \p24a5_add_temp_carry__1_n_2\,
      CO(0) => \p24a5_add_temp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \^p20m1_mul_temp\(23 downto 20),
      O(3 downto 0) => p26y_out_add_cast(10 downto 7),
      S(3) => \p24a5_add_temp_carry__1_i_1_n_0\,
      S(2) => \p24a5_add_temp_carry__1_i_2_n_0\,
      S(1) => \p24a5_add_temp_carry__1_i_3_n_0\,
      S(0) => \p24a5_add_temp_carry__1_i_4_n_0\
    );
\p24a5_add_temp_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(23),
      I1 => p24a5_add_cast_1(11),
      O => \p24a5_add_temp_carry__1_i_1_n_0\
    );
\p24a5_add_temp_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(22),
      I1 => p24a5_add_cast_1(10),
      O => \p24a5_add_temp_carry__1_i_2_n_0\
    );
\p24a5_add_temp_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(21),
      I1 => p24a5_add_cast_1(9),
      O => \p24a5_add_temp_carry__1_i_3_n_0\
    );
\p24a5_add_temp_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(20),
      I1 => p24a5_add_cast_1(8),
      O => \p24a5_add_temp_carry__1_i_4_n_0\
    );
\p24a5_add_temp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \p24a5_add_temp_carry__1_n_0\,
      CO(3 downto 2) => \NLW_p24a5_add_temp_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \p24a5_add_temp_carry__2_n_2\,
      CO(0) => \p24a5_add_temp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => p24a5_add_cast_1(13),
      DI(0) => \^p20m1_mul_temp\(24),
      O(3) => \NLW_p24a5_add_temp_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => p26y_out_add_cast(13 downto 11),
      S(3) => '0',
      S(2) => \p24a5_add_temp_carry__2_i_1_n_0\,
      S(1) => \p24a5_add_temp_carry__2_i_2_n_0\,
      S(0) => \p24a5_add_temp_carry__2_i_3_n_0\
    );
\p24a5_add_temp_carry__2_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p24a5_add_cast_1(13),
      I1 => \^p20m1_mul_temp\(26),
      O => \p24a5_add_temp_carry__2_i_1_n_0\
    );
\p24a5_add_temp_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p24a5_add_cast_1(13),
      I1 => \^p20m1_mul_temp\(25),
      O => \p24a5_add_temp_carry__2_i_2_n_0\
    );
\p24a5_add_temp_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(24),
      I1 => p24a5_add_cast_1(12),
      O => \p24a5_add_temp_carry__2_i_3_n_0\
    );
p24a5_add_temp_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(15),
      I1 => p24a5_add_cast_1(3),
      O => p24a5_add_temp_carry_i_1_n_0
    );
p24a5_add_temp_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(14),
      I1 => p24a5_add_cast_1(2),
      O => p24a5_add_temp_carry_i_2_n_0
    );
p24a5_add_temp_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p20m1_mul_temp\(13),
      I1 => p24a5_add_cast_1(1),
      O => p24a5_add_temp_carry_i_3_n_0
    );
p25a6_add_temp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p25a6_add_temp_carry_n_0,
      CO(2) => p25a6_add_temp_carry_n_1,
      CO(1) => p25a6_add_temp_carry_n_2,
      CO(0) => p25a6_add_temp_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => p25a6_add_cast(3 downto 1),
      DI(0) => '0',
      O(3 downto 2) => p25a6_add_temp(3 downto 2),
      O(1 downto 0) => NLW_p25a6_add_temp_carry_O_UNCONNECTED(1 downto 0),
      S(3) => p25a6_add_temp_carry_i_1_n_0,
      S(2) => p25a6_add_temp_carry_i_2_n_0,
      S(1) => p25a6_add_temp_carry_i_3_n_0,
      S(0) => p25a6_add_cast(0)
    );
\p25a6_add_temp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => p25a6_add_temp_carry_n_0,
      CO(3) => \p25a6_add_temp_carry__0_n_0\,
      CO(2) => \p25a6_add_temp_carry__0_n_1\,
      CO(1) => \p25a6_add_temp_carry__0_n_2\,
      CO(0) => \p25a6_add_temp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => p25a6_add_cast(7 downto 4),
      O(3 downto 0) => p25a6_add_temp(7 downto 4),
      S(3) => \p25a6_add_temp_carry__0_i_1_n_0\,
      S(2) => \p25a6_add_temp_carry__0_i_2_n_0\,
      S(1) => \p25a6_add_temp_carry__0_i_3_n_0\,
      S(0) => \p25a6_add_temp_carry__0_i_4_n_0\
    );
\p25a6_add_temp_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(7),
      I1 => \^p23m4_mul_temp\(19),
      O => \p25a6_add_temp_carry__0_i_1_n_0\
    );
\p25a6_add_temp_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(6),
      I1 => \^p23m4_mul_temp\(18),
      O => \p25a6_add_temp_carry__0_i_2_n_0\
    );
\p25a6_add_temp_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(5),
      I1 => \^p23m4_mul_temp\(17),
      O => \p25a6_add_temp_carry__0_i_3_n_0\
    );
\p25a6_add_temp_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(4),
      I1 => \^p23m4_mul_temp\(16),
      O => \p25a6_add_temp_carry__0_i_4_n_0\
    );
\p25a6_add_temp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \p25a6_add_temp_carry__0_n_0\,
      CO(3) => \p25a6_add_temp_carry__1_n_0\,
      CO(2) => \p25a6_add_temp_carry__1_n_1\,
      CO(1) => \p25a6_add_temp_carry__1_n_2\,
      CO(0) => \p25a6_add_temp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => p25a6_add_cast(11 downto 8),
      O(3 downto 0) => p25a6_add_temp(11 downto 8),
      S(3) => \p25a6_add_temp_carry__1_i_1_n_0\,
      S(2) => \p25a6_add_temp_carry__1_i_2_n_0\,
      S(1) => \p25a6_add_temp_carry__1_i_3_n_0\,
      S(0) => \p25a6_add_temp_carry__1_i_4_n_0\
    );
\p25a6_add_temp_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(11),
      I1 => \^p23m4_mul_temp\(23),
      O => \p25a6_add_temp_carry__1_i_1_n_0\
    );
\p25a6_add_temp_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(10),
      I1 => \^p23m4_mul_temp\(22),
      O => \p25a6_add_temp_carry__1_i_2_n_0\
    );
\p25a6_add_temp_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(9),
      I1 => \^p23m4_mul_temp\(21),
      O => \p25a6_add_temp_carry__1_i_3_n_0\
    );
\p25a6_add_temp_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(8),
      I1 => \^p23m4_mul_temp\(20),
      O => \p25a6_add_temp_carry__1_i_4_n_0\
    );
\p25a6_add_temp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \p25a6_add_temp_carry__1_n_0\,
      CO(3) => \p25a6_add_temp_carry__2_n_0\,
      CO(2) => \NLW_p25a6_add_temp_carry__2_CO_UNCONNECTED\(2),
      CO(1) => \p25a6_add_temp_carry__2_n_2\,
      CO(0) => \p25a6_add_temp_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2) => \^p23m4_mul_temp\(25),
      DI(1) => \p25a6_add_temp_carry__2_i_1_n_0\,
      DI(0) => p25a6_add_cast(12),
      O(3) => \NLW_p25a6_add_temp_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => p25a6_add_temp(14 downto 12),
      S(3) => '1',
      S(2) => \p25a6_add_temp_carry__2_i_2_n_0\,
      S(1) => \p25a6_add_temp_carry__2_i_3_n_0\,
      S(0) => \p25a6_add_temp_carry__2_i_4_n_0\
    );
\p25a6_add_temp_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^p23m4_mul_temp\(25),
      O => \p25a6_add_temp_carry__2_i_1_n_0\
    );
\p25a6_add_temp_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \^p23m4_mul_temp\(25),
      I1 => \^p23m4_mul_temp\(26),
      O => \p25a6_add_temp_carry__2_i_2_n_0\
    );
\p25a6_add_temp_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^p23m4_mul_temp\(25),
      I1 => p25a6_add_cast(13),
      O => \p25a6_add_temp_carry__2_i_3_n_0\
    );
\p25a6_add_temp_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(12),
      I1 => \^p23m4_mul_temp\(24),
      O => \p25a6_add_temp_carry__2_i_4_n_0\
    );
p25a6_add_temp_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(3),
      I1 => \^p23m4_mul_temp\(15),
      O => p25a6_add_temp_carry_i_1_n_0
    );
p25a6_add_temp_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(2),
      I1 => \^p23m4_mul_temp\(14),
      O => p25a6_add_temp_carry_i_2_n_0
    );
p25a6_add_temp_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_cast(1),
      I1 => \^p23m4_mul_temp\(13),
      O => p25a6_add_temp_carry_i_3_n_0
    );
p26y_out_add_temp_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => p26y_out_add_temp_carry_n_0,
      CO(2) => p26y_out_add_temp_carry_n_1,
      CO(1) => p26y_out_add_temp_carry_n_2,
      CO(0) => p26y_out_add_temp_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => p26y_out_add_cast(4 downto 2),
      DI(0) => '0',
      O(3 downto 1) => p26y_out_add_temp(2 downto 0),
      O(0) => NLW_p26y_out_add_temp_carry_O_UNCONNECTED(0),
      S(3) => p26y_out_add_temp_carry_i_1_n_0,
      S(2) => p26y_out_add_temp_carry_i_2_n_0,
      S(1) => p26y_out_add_temp_carry_i_3_n_0,
      S(0) => p26y_out_add_cast(1)
    );
\p26y_out_add_temp_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => p26y_out_add_temp_carry_n_0,
      CO(3) => \p26y_out_add_temp_carry__0_n_0\,
      CO(2) => \p26y_out_add_temp_carry__0_n_1\,
      CO(1) => \p26y_out_add_temp_carry__0_n_2\,
      CO(0) => \p26y_out_add_temp_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => p26y_out_add_cast(8 downto 5),
      O(3 downto 0) => p26y_out_add_temp(6 downto 3),
      S(3) => \p26y_out_add_temp_carry__0_i_1_n_0\,
      S(2) => \p26y_out_add_temp_carry__0_i_2_n_0\,
      S(1) => \p26y_out_add_temp_carry__0_i_3_n_0\,
      S(0) => \p26y_out_add_temp_carry__0_i_4_n_0\
    );
\p26y_out_add_temp_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(8),
      I1 => p25a6_add_temp(8),
      O => \p26y_out_add_temp_carry__0_i_1_n_0\
    );
\p26y_out_add_temp_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(7),
      I1 => p25a6_add_temp(7),
      O => \p26y_out_add_temp_carry__0_i_2_n_0\
    );
\p26y_out_add_temp_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(6),
      I1 => p25a6_add_temp(6),
      O => \p26y_out_add_temp_carry__0_i_3_n_0\
    );
\p26y_out_add_temp_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(5),
      I1 => p25a6_add_temp(5),
      O => \p26y_out_add_temp_carry__0_i_4_n_0\
    );
\p26y_out_add_temp_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \p26y_out_add_temp_carry__0_n_0\,
      CO(3) => \p26y_out_add_temp_carry__1_n_0\,
      CO(2) => \p26y_out_add_temp_carry__1_n_1\,
      CO(1) => \p26y_out_add_temp_carry__1_n_2\,
      CO(0) => \p26y_out_add_temp_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => p26y_out_add_cast(12 downto 9),
      O(3 downto 0) => p26y_out_add_temp(10 downto 7),
      S(3) => \p26y_out_add_temp_carry__1_i_1_n_0\,
      S(2) => \p26y_out_add_temp_carry__1_i_2_n_0\,
      S(1) => \p26y_out_add_temp_carry__1_i_3_n_0\,
      S(0) => \p26y_out_add_temp_carry__1_i_4_n_0\
    );
\p26y_out_add_temp_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(12),
      I1 => p25a6_add_temp(12),
      O => \p26y_out_add_temp_carry__1_i_1_n_0\
    );
\p26y_out_add_temp_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(11),
      I1 => p25a6_add_temp(11),
      O => \p26y_out_add_temp_carry__1_i_2_n_0\
    );
\p26y_out_add_temp_carry__1_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(10),
      I1 => p25a6_add_temp(10),
      O => \p26y_out_add_temp_carry__1_i_3_n_0\
    );
\p26y_out_add_temp_carry__1_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(9),
      I1 => p25a6_add_temp(9),
      O => \p26y_out_add_temp_carry__1_i_4_n_0\
    );
\p26y_out_add_temp_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \p26y_out_add_temp_carry__1_n_0\,
      CO(3 downto 2) => \NLW_p26y_out_add_temp_carry__2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \p26y_out_add_temp_carry__2_n_2\,
      CO(0) => \p26y_out_add_temp_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => p25a6_add_temp(13),
      DI(0) => \p26y_out_add_temp_carry__2_i_1_n_0\,
      O(3) => \NLW_p26y_out_add_temp_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => p26y_out_add_temp(13 downto 11),
      S(3) => '0',
      S(2) => \p26y_out_add_temp_carry__2_i_2_n_0\,
      S(1) => \p26y_out_add_temp_carry__2_i_3_n_0\,
      S(0) => \p26y_out_add_temp_carry__2_i_4_n_0\
    );
\p26y_out_add_temp_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => p25a6_add_temp(13),
      O => \p26y_out_add_temp_carry__2_i_1_n_0\
    );
\p26y_out_add_temp_carry__2_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_temp(14),
      I1 => \p25a6_add_temp_carry__2_n_0\,
      O => \p26y_out_add_temp_carry__2_i_2_n_0\
    );
\p26y_out_add_temp_carry__2_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => p25a6_add_temp(13),
      I1 => p25a6_add_temp(14),
      O => \p26y_out_add_temp_carry__2_i_3_n_0\
    );
\p26y_out_add_temp_carry__2_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p25a6_add_temp(13),
      I1 => p26y_out_add_cast(13),
      O => \p26y_out_add_temp_carry__2_i_4_n_0\
    );
p26y_out_add_temp_carry_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(4),
      I1 => p25a6_add_temp(4),
      O => p26y_out_add_temp_carry_i_1_n_0
    );
p26y_out_add_temp_carry_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(3),
      I1 => p25a6_add_temp(3),
      O => p26y_out_add_temp_carry_i_2_n_0
    );
p26y_out_add_temp_carry_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => p26y_out_add_cast(2),
      I1 => p25a6_add_temp(2),
      O => p26y_out_add_temp_carry_i_3_n_0
    );
\ud1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(0),
      Q => ud1(0)
    );
\ud1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(10),
      Q => ud1(10)
    );
\ud1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(11),
      Q => ud1(11)
    );
\ud1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(12),
      Q => ud1(12)
    );
\ud1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(13),
      Q => ud1(13)
    );
\ud1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(1),
      Q => ud1(1)
    );
\ud1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(2),
      Q => ud1(2)
    );
\ud1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(3),
      Q => ud1(3)
    );
\ud1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(4),
      Q => ud1(4)
    );
\ud1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(5),
      Q => ud1(5)
    );
\ud1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(6),
      Q => ud1(6)
    );
\ud1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(7),
      Q => ud1(7)
    );
\ud1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(8),
      Q => ud1(8)
    );
\ud1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => D(9),
      Q => ud1(9)
    );
\ud2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(0),
      Q => ud2(0)
    );
\ud2_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(10),
      Q => ud2(10)
    );
\ud2_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(11),
      Q => ud2(11)
    );
\ud2_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(12),
      Q => ud2(12)
    );
\ud2_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(13),
      Q => ud2(13)
    );
\ud2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(1),
      Q => ud2(1)
    );
\ud2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(2),
      Q => ud2(2)
    );
\ud2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(3),
      Q => ud2(3)
    );
\ud2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(4),
      Q => ud2(4)
    );
\ud2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(5),
      Q => ud2(5)
    );
\ud2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(6),
      Q => ud2(6)
    );
\ud2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(7),
      Q => ud2(7)
    );
\ud2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(8),
      Q => ud2(8)
    );
\ud2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud1(9),
      Q => ud2(9)
    );
\ud3_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(0),
      Q => ud3(0)
    );
\ud3_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(10),
      Q => ud3(10)
    );
\ud3_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(11),
      Q => ud3(11)
    );
\ud3_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(12),
      Q => ud3(12)
    );
\ud3_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(13),
      Q => ud3(13)
    );
\ud3_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(1),
      Q => ud3(1)
    );
\ud3_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(2),
      Q => ud3(2)
    );
\ud3_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(3),
      Q => ud3(3)
    );
\ud3_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(4),
      Q => ud3(4)
    );
\ud3_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(5),
      Q => ud3(5)
    );
\ud3_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(6),
      Q => ud3(6)
    );
\ud3_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(7),
      Q => ud3(7)
    );
\ud3_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(8),
      Q => ud3(8)
    );
\ud3_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud2(9),
      Q => ud3(9)
    );
\ud4_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(0),
      Q => ud4(0)
    );
\ud4_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(10),
      Q => ud4(10)
    );
\ud4_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(11),
      Q => ud4(11)
    );
\ud4_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(12),
      Q => ud4(12)
    );
\ud4_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(13),
      Q => ud4(13)
    );
\ud4_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(1),
      Q => ud4(1)
    );
\ud4_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(2),
      Q => ud4(2)
    );
\ud4_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(3),
      Q => ud4(3)
    );
\ud4_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(4),
      Q => ud4(4)
    );
\ud4_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(5),
      Q => ud4(5)
    );
\ud4_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(6),
      Q => ud4(6)
    );
\ud4_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(7),
      Q => ud4(7)
    );
\ud4_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(8),
      Q => ud4(8)
    );
\ud4_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud3(9),
      Q => ud4(9)
    );
\ud5_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(0),
      Q => ud5(0)
    );
\ud5_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(10),
      Q => ud5(10)
    );
\ud5_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(11),
      Q => ud5(11)
    );
\ud5_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(12),
      Q => ud5(12)
    );
\ud5_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(13),
      Q => ud5(13)
    );
\ud5_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(1),
      Q => ud5(1)
    );
\ud5_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(2),
      Q => ud5(2)
    );
\ud5_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(3),
      Q => ud5(3)
    );
\ud5_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(4),
      Q => ud5(4)
    );
\ud5_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(5),
      Q => ud5(5)
    );
\ud5_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(6),
      Q => ud5(6)
    );
\ud5_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(7),
      Q => ud5(7)
    );
\ud5_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(8),
      Q => ud5(8)
    );
\ud5_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud4(9),
      Q => ud5(9)
    );
\ud6_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(0),
      Q => ud6(0)
    );
\ud6_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(10),
      Q => ud6(10)
    );
\ud6_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(11),
      Q => ud6(11)
    );
\ud6_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(12),
      Q => ud6(12)
    );
\ud6_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(13),
      Q => ud6(13)
    );
\ud6_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(1),
      Q => ud6(1)
    );
\ud6_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(2),
      Q => ud6(2)
    );
\ud6_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(3),
      Q => ud6(3)
    );
\ud6_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(4),
      Q => ud6(4)
    );
\ud6_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(5),
      Q => ud6(5)
    );
\ud6_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(6),
      Q => ud6(6)
    );
\ud6_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(7),
      Q => ud6(7)
    );
\ud6_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(8),
      Q => ud6(8)
    );
\ud6_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud5(9),
      Q => ud6(9)
    );
\ud7_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(0),
      Q => ud7(0)
    );
\ud7_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(10),
      Q => ud7(10)
    );
\ud7_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(11),
      Q => ud7(11)
    );
\ud7_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(12),
      Q => ud7(12)
    );
\ud7_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(13),
      Q => ud7(13)
    );
\ud7_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(1),
      Q => ud7(1)
    );
\ud7_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(2),
      Q => ud7(2)
    );
\ud7_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(3),
      Q => ud7(3)
    );
\ud7_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(4),
      Q => ud7(4)
    );
\ud7_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(5),
      Q => ud7(5)
    );
\ud7_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(6),
      Q => ud7(6)
    );
\ud7_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(7),
      Q => ud7(7)
    );
\ud7_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(8),
      Q => ud7(8)
    );
\ud7_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud6(9),
      Q => ud7(9)
    );
\ud8_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(0),
      Q => \^delayed_xout\(0)
    );
\ud8_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(10),
      Q => \^delayed_xout\(10)
    );
\ud8_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(11),
      Q => \^delayed_xout\(11)
    );
\ud8_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(12),
      Q => \^delayed_xout\(12)
    );
\ud8_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(13),
      Q => \^delayed_xout\(13)
    );
\ud8_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(1),
      Q => \^delayed_xout\(1)
    );
\ud8_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(2),
      Q => \^delayed_xout\(2)
    );
\ud8_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(3),
      Q => \^delayed_xout\(3)
    );
\ud8_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(4),
      Q => \^delayed_xout\(4)
    );
\ud8_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(5),
      Q => \^delayed_xout\(5)
    );
\ud8_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(6),
      Q => \^delayed_xout\(6)
    );
\ud8_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(7),
      Q => \^delayed_xout\(7)
    );
\ud8_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(8),
      Q => \^delayed_xout\(8)
    );
\ud8_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => E(0),
      CLR => AR(0),
      D => ud7(9),
      Q => \^delayed_xout\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic is
  port (
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    data_int : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \data_int_reg[13]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    \Out_1_reg[13]\ : in STD_LOGIC_VECTOR ( 13 downto 0 );
    w_d1 : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    \data_int_reg[1]_0\ : in STD_LOGIC;
    \data_int_reg[1]_1\ : in STD_LOGIC;
    \data_int_reg[1]_2\ : in STD_LOGIC;
    IPCORE_CLK : in STD_LOGIC;
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ADDRD : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic is
  signal \^data_int\ : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal data_int0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal ram_reg_0_3_12_15_n_2 : STD_LOGIC;
  signal ram_reg_0_3_12_15_n_3 : STD_LOGIC;
  signal w_we : STD_LOGIC;
  signal NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Out_1[0]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \Out_1[10]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \Out_1[11]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \Out_1[12]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \Out_1[13]_i_2\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \Out_1[1]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \Out_1[2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \Out_1[3]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \Out_1[4]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \Out_1[5]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \Out_1[6]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \Out_1[7]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \Out_1[8]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \Out_1[9]_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \cache_data[0]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \cache_data[10]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \cache_data[11]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \cache_data[12]_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \cache_data[13]_i_2\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \cache_data[1]_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \cache_data[2]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \cache_data[3]_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \cache_data[4]_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \cache_data[5]_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \cache_data[6]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \cache_data[7]_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \cache_data[8]_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \cache_data[9]_i_1\ : label is "soft_lutpair36";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_0_5 : label is "";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_3_0_5 : label is 3;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_3_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_12_15 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_12_15 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_12_15 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_12_15 : label is 12;
  attribute ram_slice_end of ram_reg_0_3_12_15 : label is 15;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_6_11 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_6_11 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_6_11 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_6_11 : label is 6;
  attribute ram_slice_end of ram_reg_0_3_6_11 : label is 11;
begin
  data_int(13 downto 0) <= \^data_int\(13 downto 0);
\Out_1[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(0),
      I1 => \^data_int\(0),
      I2 => \Out_1_reg[13]\(0),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(0)
    );
\Out_1[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(10),
      I1 => \^data_int\(10),
      I2 => \Out_1_reg[13]\(10),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(10)
    );
\Out_1[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(11),
      I1 => \^data_int\(11),
      I2 => \Out_1_reg[13]\(11),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(11)
    );
\Out_1[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(12),
      I1 => \^data_int\(12),
      I2 => \Out_1_reg[13]\(12),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(12)
    );
\Out_1[13]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(13),
      I1 => \^data_int\(13),
      I2 => \Out_1_reg[13]\(13),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(13)
    );
\Out_1[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(1),
      I1 => \^data_int\(1),
      I2 => \Out_1_reg[13]\(1),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(1)
    );
\Out_1[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(2),
      I1 => \^data_int\(2),
      I2 => \Out_1_reg[13]\(2),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(2)
    );
\Out_1[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(3),
      I1 => \^data_int\(3),
      I2 => \Out_1_reg[13]\(3),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(3)
    );
\Out_1[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(4),
      I1 => \^data_int\(4),
      I2 => \Out_1_reg[13]\(4),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(4)
    );
\Out_1[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(5),
      I1 => \^data_int\(5),
      I2 => \Out_1_reg[13]\(5),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(5)
    );
\Out_1[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(6),
      I1 => \^data_int\(6),
      I2 => \Out_1_reg[13]\(6),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(6)
    );
\Out_1[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(7),
      I1 => \^data_int\(7),
      I2 => \Out_1_reg[13]\(7),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(7)
    );
\Out_1[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(8),
      I1 => \^data_int\(8),
      I2 => \Out_1_reg[13]\(8),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(8)
    );
\Out_1[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(9),
      I1 => \^data_int\(9),
      I2 => \Out_1_reg[13]\(9),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(9)
    );
\cache_data[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(0),
      I1 => \Out_1_reg[13]\(0),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(0)
    );
\cache_data[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(10),
      I1 => \Out_1_reg[13]\(10),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(10)
    );
\cache_data[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(11),
      I1 => \Out_1_reg[13]\(11),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(11)
    );
\cache_data[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(12),
      I1 => \Out_1_reg[13]\(12),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(12)
    );
\cache_data[13]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(13),
      I1 => \Out_1_reg[13]\(13),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(13)
    );
\cache_data[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(1),
      I1 => \Out_1_reg[13]\(1),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(1)
    );
\cache_data[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(2),
      I1 => \Out_1_reg[13]\(2),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(2)
    );
\cache_data[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(3),
      I1 => \Out_1_reg[13]\(3),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(3)
    );
\cache_data[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(4),
      I1 => \Out_1_reg[13]\(4),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(4)
    );
\cache_data[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(5),
      I1 => \Out_1_reg[13]\(5),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(5)
    );
\cache_data[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(6),
      I1 => \Out_1_reg[13]\(6),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(6)
    );
\cache_data[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(7),
      I1 => \Out_1_reg[13]\(7),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(7)
    );
\cache_data[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(8),
      I1 => \Out_1_reg[13]\(8),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(8)
    );
\cache_data[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(9),
      I1 => \Out_1_reg[13]\(9),
      I2 => w_d1,
      O => \data_int_reg[13]_0\(9)
    );
\data_int_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(0),
      Q => \^data_int\(0),
      R => '0'
    );
\data_int_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(10),
      Q => \^data_int\(10),
      R => '0'
    );
\data_int_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(11),
      Q => \^data_int\(11),
      R => '0'
    );
\data_int_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(12),
      Q => \^data_int\(12),
      R => '0'
    );
\data_int_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(13),
      Q => \^data_int\(13),
      R => '0'
    );
\data_int_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(1),
      Q => \^data_int\(1),
      R => '0'
    );
\data_int_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(2),
      Q => \^data_int\(2),
      R => '0'
    );
\data_int_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(3),
      Q => \^data_int\(3),
      R => '0'
    );
\data_int_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(4),
      Q => \^data_int\(4),
      R => '0'
    );
\data_int_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(5),
      Q => \^data_int\(5),
      R => '0'
    );
\data_int_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(6),
      Q => \^data_int\(6),
      R => '0'
    );
\data_int_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(7),
      Q => \^data_int\(7),
      R => '0'
    );
\data_int_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(8),
      Q => \^data_int\(8),
      R => '0'
    );
\data_int_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => data_int0(9),
      Q => \^data_int\(9),
      R => '0'
    );
ram_reg_0_3_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(1 downto 0),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(3 downto 2),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => data_int0(1 downto 0),
      DOB(1 downto 0) => data_int0(3 downto 2),
      DOC(1 downto 0) => data_int0(5 downto 4),
      DOD(1 downto 0) => NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => w_we
    );
ram_reg_0_3_0_5_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA8A"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \data_int_reg[1]_0\,
      I2 => \data_int_reg[1]_1\,
      I3 => \data_int_reg[1]_2\,
      O => w_we
    );
ram_reg_0_3_12_15: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(13 downto 12),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 14),
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => data_int0(13 downto 12),
      DOB(1) => ram_reg_0_3_12_15_n_2,
      DOB(0) => ram_reg_0_3_12_15_n_3,
      DOC(1 downto 0) => NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => w_we
    );
ram_reg_0_3_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(7 downto 6),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(9 downto 8),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => data_int0(7 downto 6),
      DOB(1 downto 0) => data_int0(9 downto 8),
      DOC(1 downto 0) => data_int0(11 downto 10),
      DOD(1 downto 0) => NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => w_we
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0 is
  port (
    D : out STD_LOGIC_VECTOR ( 15 downto 0 );
    data_int : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \data_int_reg[15]_0\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    w_we : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    \Out_1_reg[15]\ : in STD_LOGIC_VECTOR ( 15 downto 0 );
    w_d1 : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    fifo_num : in STD_LOGIC_VECTOR ( 2 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    p26y_out_add_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ADDRD : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0 : entity is "mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0 is
  signal \^data_int\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \data_int0__0\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^w_we\ : STD_LOGIC;
  signal NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Out_1[0]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \Out_1[10]_i_1__0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \Out_1[11]_i_1__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \Out_1[12]_i_1__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \Out_1[13]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \Out_1[14]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \Out_1[15]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \Out_1[1]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \Out_1[2]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \Out_1[3]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Out_1[4]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \Out_1[5]_i_1__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \Out_1[6]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \Out_1[7]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \Out_1[8]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \Out_1[9]_i_1__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \cache_data[0]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cache_data[10]_i_1__0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \cache_data[11]_i_1__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \cache_data[12]_i_1__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \cache_data[13]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \cache_data[14]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cache_data[15]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cache_data[1]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cache_data[2]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cache_data[3]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \cache_data[4]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cache_data[5]_i_1__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \cache_data[6]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \cache_data[7]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cache_data[8]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \cache_data[9]_i_1__0\ : label is "soft_lutpair16";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_0_5 : label is "";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_3_0_5 : label is 3;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_3_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_12_15 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_12_15 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_12_15 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_12_15 : label is 12;
  attribute ram_slice_end of ram_reg_0_3_12_15 : label is 15;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_6_11 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_6_11 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_6_11 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_6_11 : label is 6;
  attribute ram_slice_end of ram_reg_0_3_6_11 : label is 11;
begin
  data_int(15 downto 0) <= \^data_int\(15 downto 0);
  w_we <= \^w_we\;
\Out_1[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(0),
      I1 => \^data_int\(0),
      I2 => \Out_1_reg[15]\(0),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(0)
    );
\Out_1[10]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(10),
      I1 => \^data_int\(10),
      I2 => \Out_1_reg[15]\(10),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(10)
    );
\Out_1[11]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(11),
      I1 => \^data_int\(11),
      I2 => \Out_1_reg[15]\(11),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(11)
    );
\Out_1[12]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(12),
      I1 => \^data_int\(12),
      I2 => \Out_1_reg[15]\(12),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(12)
    );
\Out_1[13]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(13),
      I1 => \^data_int\(13),
      I2 => \Out_1_reg[15]\(13),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(13)
    );
\Out_1[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(14),
      I1 => \^data_int\(14),
      I2 => \Out_1_reg[15]\(14),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(14)
    );
\Out_1[15]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(15),
      I1 => \^data_int\(15),
      I2 => \Out_1_reg[15]\(15),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(15)
    );
\Out_1[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(1),
      I1 => \^data_int\(1),
      I2 => \Out_1_reg[15]\(1),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(1)
    );
\Out_1[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(2),
      I1 => \^data_int\(2),
      I2 => \Out_1_reg[15]\(2),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(2)
    );
\Out_1[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(3),
      I1 => \^data_int\(3),
      I2 => \Out_1_reg[15]\(3),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(3)
    );
\Out_1[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(4),
      I1 => \^data_int\(4),
      I2 => \Out_1_reg[15]\(4),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(4)
    );
\Out_1[5]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(5),
      I1 => \^data_int\(5),
      I2 => \Out_1_reg[15]\(5),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(5)
    );
\Out_1[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(6),
      I1 => \^data_int\(6),
      I2 => \Out_1_reg[15]\(6),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(6)
    );
\Out_1[7]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(7),
      I1 => \^data_int\(7),
      I2 => \Out_1_reg[15]\(7),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(7)
    );
\Out_1[8]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(8),
      I1 => \^data_int\(8),
      I2 => \Out_1_reg[15]\(8),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(8)
    );
\Out_1[9]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(9),
      I1 => \^data_int\(9),
      I2 => \Out_1_reg[15]\(9),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(9)
    );
\cache_data[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(0),
      I1 => \Out_1_reg[15]\(0),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(0)
    );
\cache_data[10]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(10),
      I1 => \Out_1_reg[15]\(10),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(10)
    );
\cache_data[11]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(11),
      I1 => \Out_1_reg[15]\(11),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(11)
    );
\cache_data[12]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(12),
      I1 => \Out_1_reg[15]\(12),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(12)
    );
\cache_data[13]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(13),
      I1 => \Out_1_reg[15]\(13),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(13)
    );
\cache_data[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(14),
      I1 => \Out_1_reg[15]\(14),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(14)
    );
\cache_data[15]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(15),
      I1 => \Out_1_reg[15]\(15),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(15)
    );
\cache_data[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(1),
      I1 => \Out_1_reg[15]\(1),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(1)
    );
\cache_data[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(2),
      I1 => \Out_1_reg[15]\(2),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(2)
    );
\cache_data[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(3),
      I1 => \Out_1_reg[15]\(3),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(3)
    );
\cache_data[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(4),
      I1 => \Out_1_reg[15]\(4),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(4)
    );
\cache_data[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(5),
      I1 => \Out_1_reg[15]\(5),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(5)
    );
\cache_data[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(6),
      I1 => \Out_1_reg[15]\(6),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(6)
    );
\cache_data[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(7),
      I1 => \Out_1_reg[15]\(7),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(7)
    );
\cache_data[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(8),
      I1 => \Out_1_reg[15]\(8),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(8)
    );
\cache_data[9]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(9),
      I1 => \Out_1_reg[15]\(9),
      I2 => w_d1,
      O => \data_int_reg[15]_0\(9)
    );
\data_int_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(0),
      Q => \^data_int\(0),
      R => '0'
    );
\data_int_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(10),
      Q => \^data_int\(10),
      R => '0'
    );
\data_int_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(11),
      Q => \^data_int\(11),
      R => '0'
    );
\data_int_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(12),
      Q => \^data_int\(12),
      R => '0'
    );
\data_int_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(13),
      Q => \^data_int\(13),
      R => '0'
    );
\data_int_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(14),
      Q => \^data_int\(14),
      R => '0'
    );
\data_int_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(15),
      Q => \^data_int\(15),
      R => '0'
    );
\data_int_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(1),
      Q => \^data_int\(1),
      R => '0'
    );
\data_int_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(2),
      Q => \^data_int\(2),
      R => '0'
    );
\data_int_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(3),
      Q => \^data_int\(3),
      R => '0'
    );
\data_int_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(4),
      Q => \^data_int\(4),
      R => '0'
    );
\data_int_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(5),
      Q => \^data_int\(5),
      R => '0'
    );
\data_int_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(6),
      Q => \^data_int\(6),
      R => '0'
    );
\data_int_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(7),
      Q => \^data_int\(7),
      R => '0'
    );
\data_int_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(8),
      Q => \^data_int\(8),
      R => '0'
    );
\data_int_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__0\(9),
      Q => \^data_int\(9),
      R => '0'
    );
ram_reg_0_3_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => p26y_out_add_temp(1 downto 0),
      DIB(1 downto 0) => p26y_out_add_temp(3 downto 2),
      DIC(1 downto 0) => p26y_out_add_temp(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \data_int0__0\(1 downto 0),
      DOB(1 downto 0) => \data_int0__0\(3 downto 2),
      DOC(1 downto 0) => \data_int0__0\(5 downto 4),
      DOD(1 downto 0) => NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^w_we\
    );
\ram_reg_0_3_0_5_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888088"
    )
        port map (
      I0 => auto_ready_dut_enb,
      I1 => out_valid,
      I2 => fifo_num(0),
      I3 => fifo_num(2),
      I4 => fifo_num(1),
      O => \^w_we\
    );
ram_reg_0_3_12_15: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => p26y_out_add_temp(13 downto 12),
      DIB(1) => p26y_out_add_temp(13),
      DIB(0) => p26y_out_add_temp(13),
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \data_int0__0\(13 downto 12),
      DOB(1 downto 0) => \data_int0__0\(15 downto 14),
      DOC(1 downto 0) => NLW_ram_reg_0_3_12_15_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_ram_reg_0_3_12_15_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^w_we\
    );
ram_reg_0_3_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => p26y_out_add_temp(7 downto 6),
      DIB(1 downto 0) => p26y_out_add_temp(9 downto 8),
      DIC(1 downto 0) => p26y_out_add_temp(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \data_int0__0\(7 downto 6),
      DOB(1 downto 0) => \data_int0__0\(9 downto 8),
      DOC(1 downto 0) => \data_int0__0\(11 downto 10),
      DOD(1 downto 0) => NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^w_we\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_singlebit is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[21]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[30]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    w_we : out STD_LOGIC;
    data_int_reg_0 : out STD_LOGIC;
    cache_data_reg : out STD_LOGIC;
    fifo_data_out : out STD_LOGIC;
    tlast_counter_out_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    tlast_size_value : in STD_LOGIC_VECTOR ( 30 downto 0 );
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    data_int_reg_1 : in STD_LOGIC;
    data_int_reg_2 : in STD_LOGIC;
    data_int_reg_3 : in STD_LOGIC;
    w_d1 : in STD_LOGIC;
    w_d2 : in STD_LOGIC;
    cache_wr_en : in STD_LOGIC;
    Out_1_reg : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    out_wr_en : in STD_LOGIC;
    AXI4_Stream_Master_TLAST : in STD_LOGIC;
    IPCORE_CLK : in STD_LOGIC;
    auto_tlast : in STD_LOGIC;
    wr_addr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    rd_addr : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_singlebit;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_singlebit is
  signal \data_int0__1\ : STD_LOGIC;
  signal \^fifo_data_out\ : STD_LOGIC;
  signal w_waddr_1 : STD_LOGIC;
  signal \^w_we\ : STD_LOGIC;
  signal NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of cache_data_i_1 : label is "soft_lutpair0";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_3_0_0 : label is "RAM16X1D";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_3_0_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_3_0_0 : label is 3;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_3_0_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_3_0_0 : label is 0;
  attribute SOFT_HLUTNM of w_d2_i_1 : label is "soft_lutpair0";
begin
  fifo_data_out <= \^fifo_data_out\;
  w_we <= \^w_we\;
Out_1_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ACFFAC00"
    )
        port map (
      I0 => Out_1_reg,
      I1 => \^fifo_data_out\,
      I2 => cache_valid,
      I3 => out_wr_en,
      I4 => AXI4_Stream_Master_TLAST,
      O => cache_data_reg
    );
cache_data_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => w_waddr_1,
      I1 => w_d1,
      I2 => w_d2,
      I3 => cache_wr_en,
      I4 => Out_1_reg,
      O => data_int_reg_0
    );
data_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \data_int0__1\,
      Q => w_waddr_1,
      R => '0'
    );
ram_reg_0_3_0_0: unisim.vcomponents.RAM32X1D
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => wr_addr(0),
      A1 => wr_addr(1),
      A2 => '0',
      A3 => '0',
      A4 => '0',
      D => auto_tlast,
      DPO => \data_int0__1\,
      DPRA0 => rd_addr(0),
      DPRA1 => rd_addr(1),
      DPRA2 => '0',
      DPRA3 => '0',
      DPRA4 => '0',
      SPO => NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED,
      WCLK => IPCORE_CLK,
      WE => \^w_we\
    );
ram_reg_0_3_0_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888088"
    )
        port map (
      I0 => auto_ready_dut_enb,
      I1 => out_valid,
      I2 => data_int_reg_1,
      I3 => data_int_reg_2,
      I4 => data_int_reg_3,
      O => \^w_we\
    );
\relop_relop1_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(21),
      I1 => tlast_size_value(20),
      I2 => tlast_size_value(22),
      I3 => tlast_counter_out_reg(23),
      I4 => tlast_size_value(21),
      I5 => tlast_counter_out_reg(22),
      O => \tlast_counter_out_reg[21]\(3)
    );
\relop_relop1_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(18),
      I1 => tlast_size_value(17),
      I2 => tlast_size_value(19),
      I3 => tlast_counter_out_reg(20),
      I4 => tlast_size_value(18),
      I5 => tlast_counter_out_reg(19),
      O => \tlast_counter_out_reg[21]\(2)
    );
\relop_relop1_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(15),
      I1 => tlast_size_value(14),
      I2 => tlast_size_value(16),
      I3 => tlast_counter_out_reg(17),
      I4 => tlast_size_value(15),
      I5 => tlast_counter_out_reg(16),
      O => \tlast_counter_out_reg[21]\(1)
    );
\relop_relop1_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(12),
      I1 => tlast_size_value(11),
      I2 => tlast_size_value(13),
      I3 => tlast_counter_out_reg(14),
      I4 => tlast_size_value(12),
      I5 => tlast_counter_out_reg(13),
      O => \tlast_counter_out_reg[21]\(0)
    );
\relop_relop1_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => tlast_counter_out_reg(30),
      I1 => tlast_size_value(29),
      I2 => tlast_counter_out_reg(31),
      I3 => tlast_size_value(30),
      O => \tlast_counter_out_reg[30]\(2)
    );
\relop_relop1_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(27),
      I1 => tlast_size_value(26),
      I2 => tlast_size_value(28),
      I3 => tlast_counter_out_reg(29),
      I4 => tlast_size_value(27),
      I5 => tlast_counter_out_reg(28),
      O => \tlast_counter_out_reg[30]\(1)
    );
\relop_relop1_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(24),
      I1 => tlast_size_value(23),
      I2 => tlast_size_value(25),
      I3 => tlast_counter_out_reg(26),
      I4 => tlast_size_value(24),
      I5 => tlast_counter_out_reg(25),
      O => \tlast_counter_out_reg[30]\(0)
    );
relop_relop1_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(9),
      I1 => tlast_size_value(8),
      I2 => tlast_size_value(10),
      I3 => tlast_counter_out_reg(11),
      I4 => tlast_size_value(9),
      I5 => tlast_counter_out_reg(10),
      O => S(3)
    );
relop_relop1_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(6),
      I1 => tlast_size_value(5),
      I2 => tlast_size_value(7),
      I3 => tlast_counter_out_reg(8),
      I4 => tlast_size_value(6),
      I5 => tlast_counter_out_reg(7),
      O => S(2)
    );
relop_relop1_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(3),
      I1 => tlast_size_value(2),
      I2 => tlast_size_value(4),
      I3 => tlast_counter_out_reg(5),
      I4 => tlast_size_value(3),
      I5 => tlast_counter_out_reg(4),
      O => S(1)
    );
relop_relop1_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => tlast_counter_out_reg(0),
      I1 => Q(0),
      I2 => tlast_size_value(1),
      I3 => tlast_counter_out_reg(2),
      I4 => tlast_size_value(0),
      I5 => tlast_counter_out_reg(1),
      O => S(0)
    );
w_d2_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => w_waddr_1,
      I1 => w_d1,
      I2 => w_d2,
      O => \^fifo_data_out\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_addr_decoder is
  port (
    read_reg_ip_timestamp : out STD_LOGIC_VECTOR ( 0 to 0 );
    write_axi_enable : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[28]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[24]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[20]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[16]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[12]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_h_in4_reg[13]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \write_reg_h_in3_reg[13]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \write_reg_h_in2_reg[13]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \write_reg_h_in1_reg[13]_0\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_ACLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    write_reg_axi_enable_reg_0 : in STD_LOGIC;
    AS : in STD_LOGIC_VECTOR ( 0 to 0 );
    auto_ready_dut_enb : in STD_LOGIC;
    \write_reg_h_in4_reg[13]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]_1\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \write_reg_h_in1_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_h_in3_reg[13]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_h_in2_reg[13]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_h_in1_reg[13]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_addr_decoder;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_addr_decoder is
  signal \^q\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \^write_axi_enable\ : STD_LOGIC;
  signal write_reg_packet_size_axi4_stream_master : STD_LOGIC_VECTOR ( 31 to 31 );
begin
  Q(30 downto 0) <= \^q\(30 downto 0);
  write_axi_enable <= \^write_axi_enable\;
\read_reg_ip_timestamp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => '1',
      Q => read_reg_ip_timestamp(0)
    );
\tlast_size_value_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(8),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(3)
    );
\tlast_size_value_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(7),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(2)
    );
\tlast_size_value_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(6),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(1)
    );
\tlast_size_value_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(5),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(0)
    );
\tlast_size_value_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(12),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(3)
    );
\tlast_size_value_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(11),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(2)
    );
\tlast_size_value_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(10),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(1)
    );
\tlast_size_value_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(9),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(0)
    );
\tlast_size_value_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(16),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(3)
    );
\tlast_size_value_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(15),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(2)
    );
\tlast_size_value_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(14),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(1)
    );
\tlast_size_value_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(13),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(0)
    );
\tlast_size_value_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(20),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(3)
    );
\tlast_size_value_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(19),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(2)
    );
\tlast_size_value_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(18),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(1)
    );
\tlast_size_value_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(17),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(0)
    );
\tlast_size_value_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(24),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(3)
    );
\tlast_size_value_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(23),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(2)
    );
\tlast_size_value_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(22),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(1)
    );
\tlast_size_value_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(21),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(0)
    );
\tlast_size_value_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(28),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(3)
    );
\tlast_size_value_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(27),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(2)
    );
\tlast_size_value_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(26),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(1)
    );
\tlast_size_value_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(25),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(0)
    );
\tlast_size_value_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_reg_packet_size_axi4_stream_master(31),
      O => \write_reg_packet_size_axi4_stream_master_reg[31]_0\(2)
    );
\tlast_size_value_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(30),
      O => \write_reg_packet_size_axi4_stream_master_reg[31]_0\(1)
    );
\tlast_size_value_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(29),
      O => \write_reg_packet_size_axi4_stream_master_reg[31]_0\(0)
    );
tlast_size_value_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(4),
      O => S(3)
    );
tlast_size_value_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(3),
      O => S(2)
    );
tlast_size_value_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(2),
      O => S(1)
    );
tlast_size_value_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(1),
      O => S(0)
    );
\ud8[13]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^write_axi_enable\,
      I1 => auto_ready_dut_enb,
      O => E(0)
    );
write_reg_axi_enable_reg: unisim.vcomponents.FDPE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      D => write_reg_axi_enable_reg_0,
      PRE => AS(0),
      Q => \^write_axi_enable\
    );
\write_reg_h_in1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0),
      Q => \write_reg_h_in1_reg[13]_0\(0)
    );
\write_reg_h_in1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10),
      Q => \write_reg_h_in1_reg[13]_0\(10)
    );
\write_reg_h_in1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11),
      Q => \write_reg_h_in1_reg[13]_0\(11)
    );
\write_reg_h_in1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12),
      Q => \write_reg_h_in1_reg[13]_0\(12)
    );
\write_reg_h_in1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13),
      Q => \write_reg_h_in1_reg[13]_0\(13)
    );
\write_reg_h_in1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1),
      Q => \write_reg_h_in1_reg[13]_0\(1)
    );
\write_reg_h_in1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2),
      Q => \write_reg_h_in1_reg[13]_0\(2)
    );
\write_reg_h_in1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3),
      Q => \write_reg_h_in1_reg[13]_0\(3)
    );
\write_reg_h_in1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4),
      Q => \write_reg_h_in1_reg[13]_0\(4)
    );
\write_reg_h_in1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5),
      Q => \write_reg_h_in1_reg[13]_0\(5)
    );
\write_reg_h_in1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6),
      Q => \write_reg_h_in1_reg[13]_0\(6)
    );
\write_reg_h_in1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7),
      Q => \write_reg_h_in1_reg[13]_0\(7)
    );
\write_reg_h_in1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8),
      Q => \write_reg_h_in1_reg[13]_0\(8)
    );
\write_reg_h_in1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in1_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9),
      Q => \write_reg_h_in1_reg[13]_0\(9)
    );
\write_reg_h_in2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0),
      Q => \write_reg_h_in2_reg[13]_0\(0)
    );
\write_reg_h_in2_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10),
      Q => \write_reg_h_in2_reg[13]_0\(10)
    );
\write_reg_h_in2_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11),
      Q => \write_reg_h_in2_reg[13]_0\(11)
    );
\write_reg_h_in2_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12),
      Q => \write_reg_h_in2_reg[13]_0\(12)
    );
\write_reg_h_in2_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13),
      Q => \write_reg_h_in2_reg[13]_0\(13)
    );
\write_reg_h_in2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1),
      Q => \write_reg_h_in2_reg[13]_0\(1)
    );
\write_reg_h_in2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2),
      Q => \write_reg_h_in2_reg[13]_0\(2)
    );
\write_reg_h_in2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3),
      Q => \write_reg_h_in2_reg[13]_0\(3)
    );
\write_reg_h_in2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4),
      Q => \write_reg_h_in2_reg[13]_0\(4)
    );
\write_reg_h_in2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5),
      Q => \write_reg_h_in2_reg[13]_0\(5)
    );
\write_reg_h_in2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6),
      Q => \write_reg_h_in2_reg[13]_0\(6)
    );
\write_reg_h_in2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7),
      Q => \write_reg_h_in2_reg[13]_0\(7)
    );
\write_reg_h_in2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8),
      Q => \write_reg_h_in2_reg[13]_0\(8)
    );
\write_reg_h_in2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in2_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9),
      Q => \write_reg_h_in2_reg[13]_0\(9)
    );
\write_reg_h_in3_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0),
      Q => \write_reg_h_in3_reg[13]_0\(0)
    );
\write_reg_h_in3_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10),
      Q => \write_reg_h_in3_reg[13]_0\(10)
    );
\write_reg_h_in3_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11),
      Q => \write_reg_h_in3_reg[13]_0\(11)
    );
\write_reg_h_in3_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12),
      Q => \write_reg_h_in3_reg[13]_0\(12)
    );
\write_reg_h_in3_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13),
      Q => \write_reg_h_in3_reg[13]_0\(13)
    );
\write_reg_h_in3_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1),
      Q => \write_reg_h_in3_reg[13]_0\(1)
    );
\write_reg_h_in3_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2),
      Q => \write_reg_h_in3_reg[13]_0\(2)
    );
\write_reg_h_in3_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3),
      Q => \write_reg_h_in3_reg[13]_0\(3)
    );
\write_reg_h_in3_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4),
      Q => \write_reg_h_in3_reg[13]_0\(4)
    );
\write_reg_h_in3_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5),
      Q => \write_reg_h_in3_reg[13]_0\(5)
    );
\write_reg_h_in3_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6),
      Q => \write_reg_h_in3_reg[13]_0\(6)
    );
\write_reg_h_in3_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7),
      Q => \write_reg_h_in3_reg[13]_0\(7)
    );
\write_reg_h_in3_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8),
      Q => \write_reg_h_in3_reg[13]_0\(8)
    );
\write_reg_h_in3_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in3_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9),
      Q => \write_reg_h_in3_reg[13]_0\(9)
    );
\write_reg_h_in4_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0),
      Q => \write_reg_h_in4_reg[13]_0\(0)
    );
\write_reg_h_in4_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10),
      Q => \write_reg_h_in4_reg[13]_0\(10)
    );
\write_reg_h_in4_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11),
      Q => \write_reg_h_in4_reg[13]_0\(11)
    );
\write_reg_h_in4_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12),
      Q => \write_reg_h_in4_reg[13]_0\(12)
    );
\write_reg_h_in4_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13),
      Q => \write_reg_h_in4_reg[13]_0\(13)
    );
\write_reg_h_in4_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1),
      Q => \write_reg_h_in4_reg[13]_0\(1)
    );
\write_reg_h_in4_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2),
      Q => \write_reg_h_in4_reg[13]_0\(2)
    );
\write_reg_h_in4_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3),
      Q => \write_reg_h_in4_reg[13]_0\(3)
    );
\write_reg_h_in4_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4),
      Q => \write_reg_h_in4_reg[13]_0\(4)
    );
\write_reg_h_in4_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5),
      Q => \write_reg_h_in4_reg[13]_0\(5)
    );
\write_reg_h_in4_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6),
      Q => \write_reg_h_in4_reg[13]_0\(6)
    );
\write_reg_h_in4_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7),
      Q => \write_reg_h_in4_reg[13]_0\(7)
    );
\write_reg_h_in4_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8),
      Q => \write_reg_h_in4_reg[13]_0\(8)
    );
\write_reg_h_in4_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_h_in4_reg[13]_1\(0),
      CLR => \write_reg_h_in1_reg[0]_0\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9),
      Q => \write_reg_h_in4_reg[13]_0\(9)
    );
\write_reg_packet_size_axi4_stream_master_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0),
      Q => \^q\(0)
    );
\write_reg_packet_size_axi4_stream_master_reg[10]\: unisim.vcomponents.FDPE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10),
      PRE => AS(0),
      Q => \^q\(10)
    );
\write_reg_packet_size_axi4_stream_master_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11),
      Q => \^q\(11)
    );
\write_reg_packet_size_axi4_stream_master_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12),
      Q => \^q\(12)
    );
\write_reg_packet_size_axi4_stream_master_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13),
      Q => \^q\(13)
    );
\write_reg_packet_size_axi4_stream_master_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(14),
      Q => \^q\(14)
    );
\write_reg_packet_size_axi4_stream_master_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(15),
      Q => \^q\(15)
    );
\write_reg_packet_size_axi4_stream_master_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(16),
      Q => \^q\(16)
    );
\write_reg_packet_size_axi4_stream_master_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(17),
      Q => \^q\(17)
    );
\write_reg_packet_size_axi4_stream_master_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(18),
      Q => \^q\(18)
    );
\write_reg_packet_size_axi4_stream_master_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(19),
      Q => \^q\(19)
    );
\write_reg_packet_size_axi4_stream_master_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1),
      Q => \^q\(1)
    );
\write_reg_packet_size_axi4_stream_master_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(20),
      Q => \^q\(20)
    );
\write_reg_packet_size_axi4_stream_master_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(21),
      Q => \^q\(21)
    );
\write_reg_packet_size_axi4_stream_master_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(22),
      Q => \^q\(22)
    );
\write_reg_packet_size_axi4_stream_master_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(23),
      Q => \^q\(23)
    );
\write_reg_packet_size_axi4_stream_master_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(24),
      Q => \^q\(24)
    );
\write_reg_packet_size_axi4_stream_master_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(25),
      Q => \^q\(25)
    );
\write_reg_packet_size_axi4_stream_master_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(26),
      Q => \^q\(26)
    );
\write_reg_packet_size_axi4_stream_master_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(27),
      Q => \^q\(27)
    );
\write_reg_packet_size_axi4_stream_master_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(28),
      Q => \^q\(28)
    );
\write_reg_packet_size_axi4_stream_master_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(29),
      Q => \^q\(29)
    );
\write_reg_packet_size_axi4_stream_master_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2),
      Q => \^q\(2)
    );
\write_reg_packet_size_axi4_stream_master_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(30),
      Q => \^q\(30)
    );
\write_reg_packet_size_axi4_stream_master_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(31),
      Q => write_reg_packet_size_axi4_stream_master(31)
    );
\write_reg_packet_size_axi4_stream_master_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3),
      Q => \^q\(3)
    );
\write_reg_packet_size_axi4_stream_master_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4),
      Q => \^q\(4)
    );
\write_reg_packet_size_axi4_stream_master_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5),
      Q => \^q\(5)
    );
\write_reg_packet_size_axi4_stream_master_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6),
      Q => \^q\(6)
    );
\write_reg_packet_size_axi4_stream_master_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7),
      Q => \^q\(7)
    );
\write_reg_packet_size_axi4_stream_master_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8),
      Q => \^q\(8)
    );
\write_reg_packet_size_axi4_stream_master_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0),
      CLR => AS(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9),
      Q => \^q\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite_module is
  port (
    FSM_sequential_axi_lite_rstate_reg_0 : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_ARESETN_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARESETN_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    \wdata_reg[0]_0\ : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \FSM_onehot_axi_lite_wstate_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \AXI4_Lite_ARADDR[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARVALID_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARVALID_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARVALID_2 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARVALID_3 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    write_axi_enable : in STD_LOGIC;
    read_reg_ip_timestamp : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite_module;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite_module is
  signal \^axi4_lite_arready\ : STD_LOGIC;
  signal \^axi4_lite_rdata\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \AXI4_Lite_RDATA_1[30]_i_10_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_1_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_2_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_4_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_5_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_7_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_8_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_1[30]_i_9_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_axi_lite_wstate_reg[2]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\ : STD_LOGIC;
  signal \^fsm_sequential_axi_lite_rstate_reg_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal aw_transfer : STD_LOGIC;
  signal axi_lite_rstate_next : STD_LOGIC;
  signal axi_lite_wstate_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reset : STD_LOGIC;
  signal soft_reset : STD_LOGIC;
  signal soft_reset_i_2_n_0 : STD_LOGIC;
  signal soft_reset_i_3_n_0 : STD_LOGIC;
  signal soft_reset_i_4_n_0 : STD_LOGIC;
  signal strobe_sw : STD_LOGIC;
  signal top_addr_sel : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal top_wr_enb : STD_LOGIC;
  signal w_transfer : STD_LOGIC;
  signal w_transfer_and_wstrb : STD_LOGIC;
  signal waddr_sel : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal write_reg_axi_enable_i_2_n_0 : STD_LOGIC;
  signal \write_reg_h_in4[13]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI4_Lite_ARREADY_INST_0 : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of AXI4_Lite_AWREADY_INST_0 : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \AXI4_Lite_RDATA_1[30]_i_3\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \AXI4_Lite_RDATA_1[30]_i_6\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \FSM_onehot_axi_lite_wstate[1]_i_1\ : label is "soft_lutpair44";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[0]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[1]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[2]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute SOFT_HLUTNM of FSM_sequential_axi_lite_rstate_i_1 : label is "soft_lutpair43";
  attribute FSM_ENCODED_STATES of FSM_sequential_axi_lite_rstate_reg : label is "iSTATE:0,iSTATE0:1";
  attribute SOFT_HLUTNM of write_reg_axi_enable_i_2 : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of write_reg_axi_enable_i_3 : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \write_reg_h_in4[13]_i_2\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \write_reg_packet_size_axi4_stream_master[31]_i_1\ : label is "soft_lutpair42";
begin
  AXI4_Lite_ARREADY <= \^axi4_lite_arready\;
  AXI4_Lite_RDATA(0) <= \^axi4_lite_rdata\(0);
  \FSM_onehot_axi_lite_wstate_reg[2]_0\(1 downto 0) <= \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1 downto 0);
  FSM_sequential_axi_lite_rstate_reg_0 <= \^fsm_sequential_axi_lite_rstate_reg_0\;
  Q(31 downto 0) <= \^q\(31 downto 0);
AXI4_Lite_ARREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => AXI4_Lite_AWVALID,
      I1 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I2 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => \^axi4_lite_arready\
    );
AXI4_Lite_AWREADY_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => AXI4_Lite_AWREADY
    );
\AXI4_Lite_RDATA_1[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"80FFFFFF80000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_1[30]_i_2_n_0\,
      I1 => top_addr_sel(0),
      I2 => read_reg_ip_timestamp(0),
      I3 => AXI4_Lite_ARVALID,
      I4 => \^axi4_lite_arready\,
      I5 => \^axi4_lite_rdata\(0),
      O => \AXI4_Lite_RDATA_1[30]_i_1_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFACFCA"
    )
        port map (
      I0 => waddr_sel(8),
      I1 => AXI4_Lite_ARADDR(8),
      I2 => AXI4_Lite_ARVALID,
      I3 => waddr_sel(7),
      I4 => AXI4_Lite_ARADDR(7),
      O => \AXI4_Lite_RDATA_1[30]_i_10_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0C00AA00000000"
    )
        port map (
      I0 => waddr_sel(1),
      I1 => AXI4_Lite_ARADDR(1),
      I2 => AXI4_Lite_ARADDR(6),
      I3 => waddr_sel(6),
      I4 => AXI4_Lite_ARVALID,
      I5 => \AXI4_Lite_RDATA_1[30]_i_4_n_0\,
      O => \AXI4_Lite_RDATA_1[30]_i_2_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(0),
      I1 => waddr_sel(0),
      I2 => AXI4_Lite_ARVALID,
      O => top_addr_sel(0)
    );
\AXI4_Lite_RDATA_1[30]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_1[30]_i_5_n_0\,
      I1 => top_addr_sel(12),
      I2 => \AXI4_Lite_RDATA_1[30]_i_7_n_0\,
      I3 => \AXI4_Lite_RDATA_1[30]_i_8_n_0\,
      I4 => \AXI4_Lite_RDATA_1[30]_i_9_n_0\,
      I5 => \AXI4_Lite_RDATA_1[30]_i_10_n_0\,
      O => \AXI4_Lite_RDATA_1[30]_i_4_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFACFCA"
    )
        port map (
      I0 => waddr_sel(13),
      I1 => AXI4_Lite_ARADDR(13),
      I2 => AXI4_Lite_ARVALID,
      I3 => waddr_sel(11),
      I4 => AXI4_Lite_ARADDR(11),
      O => \AXI4_Lite_RDATA_1[30]_i_5_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(12),
      I1 => waddr_sel(12),
      I2 => AXI4_Lite_ARVALID,
      O => top_addr_sel(12)
    );
\AXI4_Lite_RDATA_1[30]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFACFCA"
    )
        port map (
      I0 => waddr_sel(5),
      I1 => AXI4_Lite_ARADDR(5),
      I2 => AXI4_Lite_ARVALID,
      I3 => waddr_sel(4),
      I4 => AXI4_Lite_ARADDR(4),
      O => \AXI4_Lite_RDATA_1[30]_i_7_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFACFCA"
    )
        port map (
      I0 => waddr_sel(3),
      I1 => AXI4_Lite_ARADDR(3),
      I2 => AXI4_Lite_ARVALID,
      I3 => waddr_sel(2),
      I4 => AXI4_Lite_ARADDR(2),
      O => \AXI4_Lite_RDATA_1[30]_i_8_n_0\
    );
\AXI4_Lite_RDATA_1[30]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFACFCA"
    )
        port map (
      I0 => waddr_sel(10),
      I1 => AXI4_Lite_ARADDR(10),
      I2 => AXI4_Lite_ARVALID,
      I3 => waddr_sel(9),
      I4 => AXI4_Lite_ARADDR(9),
      O => \AXI4_Lite_RDATA_1[30]_i_9_n_0\
    );
\AXI4_Lite_RDATA_1_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => \AXI4_Lite_RDATA_1[30]_i_1_n_0\,
      Q => \^axi4_lite_rdata\(0)
    );
\FSM_onehot_axi_lite_wstate[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFD0D0D0"
    )
        port map (
      I0 => AXI4_Lite_AWVALID,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I2 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I3 => AXI4_Lite_BREADY,
      I4 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1),
      O => axi_lite_wstate_next(0)
    );
\FSM_onehot_axi_lite_wstate[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20FF2020"
    )
        port map (
      I0 => AXI4_Lite_AWVALID,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I2 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I3 => AXI4_Lite_WVALID,
      I4 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      O => axi_lite_wstate_next(1)
    );
\FSM_onehot_axi_lite_wstate[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      O => reset
    );
\FSM_onehot_axi_lite_wstate[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => AXI4_Lite_WVALID,
      I1 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      I2 => AXI4_Lite_BREADY,
      I3 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1),
      O => axi_lite_wstate_next(2)
    );
\FSM_onehot_axi_lite_wstate_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      D => axi_lite_wstate_next(0),
      PRE => reset,
      Q => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\
    );
\FSM_onehot_axi_lite_wstate_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_wstate_next(1),
      Q => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0)
    );
\FSM_onehot_axi_lite_wstate_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_wstate_next(2),
      Q => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1)
    );
FSM_sequential_axi_lite_rstate_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"47444444"
    )
        port map (
      I0 => AXI4_Lite_RREADY,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I2 => AXI4_Lite_AWVALID,
      I3 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I4 => AXI4_Lite_ARVALID,
      O => axi_lite_rstate_next
    );
FSM_sequential_axi_lite_rstate_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_rstate_next,
      Q => \^fsm_sequential_axi_lite_rstate_reg_0\
    );
\Out_1[15]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AR(0)
    );
out_valid_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AXI4_Lite_ARESETN_0(0)
    );
soft_reset_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => top_wr_enb,
      I1 => \^q\(0),
      I2 => soft_reset_i_2_n_0,
      I3 => soft_reset_i_3_n_0,
      I4 => soft_reset_i_4_n_0,
      O => strobe_sw
    );
soft_reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => waddr_sel(13),
      I1 => waddr_sel(12),
      I2 => waddr_sel(9),
      I3 => waddr_sel(8),
      I4 => waddr_sel(11),
      I5 => waddr_sel(10),
      O => soft_reset_i_2_n_0
    );
soft_reset_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => waddr_sel(2),
      I1 => waddr_sel(3),
      I2 => waddr_sel(0),
      I3 => waddr_sel(1),
      O => soft_reset_i_3_n_0
    );
soft_reset_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => waddr_sel(6),
      I1 => waddr_sel(7),
      I2 => waddr_sel(4),
      I3 => waddr_sel(5),
      O => soft_reset_i_4_n_0
    );
soft_reset_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => strobe_sw,
      Q => soft_reset
    );
\ud8[13]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AXI4_Lite_ARESETN_1(0)
    );
\waddr[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I1 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I2 => AXI4_Lite_AWVALID,
      O => aw_transfer
    );
\waddr_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(8),
      Q => waddr_sel(8)
    );
\waddr_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(9),
      Q => waddr_sel(9)
    );
\waddr_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(10),
      Q => waddr_sel(10)
    );
\waddr_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(11),
      Q => waddr_sel(11)
    );
\waddr_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(12),
      Q => waddr_sel(12)
    );
\waddr_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(13),
      Q => waddr_sel(13)
    );
\waddr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(0),
      Q => waddr_sel(0)
    );
\waddr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(1),
      Q => waddr_sel(1)
    );
\waddr_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(2),
      Q => waddr_sel(2)
    );
\waddr_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(3),
      Q => waddr_sel(3)
    );
\waddr_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(4),
      Q => waddr_sel(4)
    );
\waddr_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(5),
      Q => waddr_sel(5)
    );
\waddr_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(6),
      Q => waddr_sel(6)
    );
\waddr_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(7),
      Q => waddr_sel(7)
    );
\wdata[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      I1 => AXI4_Lite_WVALID,
      O => w_transfer
    );
\wdata_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(0),
      Q => \^q\(0)
    );
\wdata_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(10),
      Q => \^q\(10)
    );
\wdata_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(11),
      Q => \^q\(11)
    );
\wdata_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(12),
      Q => \^q\(12)
    );
\wdata_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(13),
      Q => \^q\(13)
    );
\wdata_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(14),
      Q => \^q\(14)
    );
\wdata_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(15),
      Q => \^q\(15)
    );
\wdata_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(16),
      Q => \^q\(16)
    );
\wdata_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(17),
      Q => \^q\(17)
    );
\wdata_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(18),
      Q => \^q\(18)
    );
\wdata_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(19),
      Q => \^q\(19)
    );
\wdata_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(1),
      Q => \^q\(1)
    );
\wdata_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(20),
      Q => \^q\(20)
    );
\wdata_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(21),
      Q => \^q\(21)
    );
\wdata_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(22),
      Q => \^q\(22)
    );
\wdata_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(23),
      Q => \^q\(23)
    );
\wdata_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(24),
      Q => \^q\(24)
    );
\wdata_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(25),
      Q => \^q\(25)
    );
\wdata_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(26),
      Q => \^q\(26)
    );
\wdata_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(27),
      Q => \^q\(27)
    );
\wdata_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(28),
      Q => \^q\(28)
    );
\wdata_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(29),
      Q => \^q\(29)
    );
\wdata_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(2),
      Q => \^q\(2)
    );
\wdata_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(30),
      Q => \^q\(30)
    );
\wdata_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(31),
      Q => \^q\(31)
    );
\wdata_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(3),
      Q => \^q\(3)
    );
\wdata_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(4),
      Q => \^q\(4)
    );
\wdata_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(5),
      Q => \^q\(5)
    );
\wdata_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(6),
      Q => \^q\(6)
    );
\wdata_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(7),
      Q => \^q\(7)
    );
\wdata_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(8),
      Q => \^q\(8)
    );
\wdata_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(9),
      Q => \^q\(9)
    );
wr_enb_1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => AXI4_Lite_WSTRB(2),
      I1 => AXI4_Lite_WSTRB(3),
      I2 => AXI4_Lite_WSTRB(0),
      I3 => AXI4_Lite_WSTRB(1),
      I4 => AXI4_Lite_WVALID,
      I5 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      O => w_transfer_and_wstrb
    );
wr_enb_1_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => w_transfer_and_wstrb,
      Q => top_wr_enb
    );
write_reg_axi_enable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFBFFFFF00800000"
    )
        port map (
      I0 => \^q\(0),
      I1 => write_reg_axi_enable_i_2_n_0,
      I2 => top_addr_sel(0),
      I3 => top_addr_sel(1),
      I4 => top_wr_enb,
      I5 => write_axi_enable,
      O => \wdata_reg[0]_0\
    );
write_reg_axi_enable_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"028A"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_1[30]_i_4_n_0\,
      I1 => AXI4_Lite_ARVALID,
      I2 => waddr_sel(6),
      I3 => AXI4_Lite_ARADDR(6),
      O => write_reg_axi_enable_i_2_n_0
    );
write_reg_axi_enable_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(1),
      I1 => waddr_sel(1),
      I2 => AXI4_Lite_ARVALID,
      O => top_addr_sel(1)
    );
\write_reg_h_in1[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004404400000000"
    )
        port map (
      I0 => top_addr_sel(0),
      I1 => \write_reg_h_in4[13]_i_2_n_0\,
      I2 => AXI4_Lite_ARVALID,
      I3 => waddr_sel(1),
      I4 => AXI4_Lite_ARADDR(1),
      I5 => top_wr_enb,
      O => AXI4_Lite_ARVALID_0(0)
    );
\write_reg_h_in2[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"028A000000000000"
    )
        port map (
      I0 => \write_reg_h_in4[13]_i_2_n_0\,
      I1 => AXI4_Lite_ARVALID,
      I2 => waddr_sel(1),
      I3 => AXI4_Lite_ARADDR(1),
      I4 => top_addr_sel(0),
      I5 => top_wr_enb,
      O => AXI4_Lite_ARVALID_1(0)
    );
\write_reg_h_in3[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5410000000000000"
    )
        port map (
      I0 => top_addr_sel(0),
      I1 => AXI4_Lite_ARVALID,
      I2 => waddr_sel(1),
      I3 => AXI4_Lite_ARADDR(1),
      I4 => \write_reg_h_in4[13]_i_2_n_0\,
      I5 => top_wr_enb,
      O => AXI4_Lite_ARVALID_2(0)
    );
\write_reg_h_in4[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E400000000000000"
    )
        port map (
      I0 => AXI4_Lite_ARVALID,
      I1 => waddr_sel(1),
      I2 => AXI4_Lite_ARADDR(1),
      I3 => \write_reg_h_in4[13]_i_2_n_0\,
      I4 => top_addr_sel(0),
      I5 => top_wr_enb,
      O => AXI4_Lite_ARVALID_3(0)
    );
\write_reg_h_in4[13]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E400"
    )
        port map (
      I0 => AXI4_Lite_ARVALID,
      I1 => waddr_sel(6),
      I2 => AXI4_Lite_ARADDR(6),
      I3 => \AXI4_Lite_RDATA_1[30]_i_4_n_0\,
      O => \write_reg_h_in4[13]_i_2_n_0\
    );
\write_reg_packet_size_axi4_stream_master[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"53000000"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(0),
      I1 => waddr_sel(0),
      I2 => AXI4_Lite_ARVALID,
      I3 => \AXI4_Lite_RDATA_1[30]_i_2_n_0\,
      I4 => top_wr_enb,
      O => \AXI4_Lite_ARADDR[2]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite is
  port (
    FSM_sequential_axi_lite_rstate_reg : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARESETN_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[28]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[24]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[20]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[16]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[12]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[8]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_ARESETN_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    \FSM_onehot_axi_lite_wstate_reg[2]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \write_reg_h_in4_reg[13]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \write_reg_h_in3_reg[13]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \write_reg_h_in2_reg[13]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \write_reg_h_in1_reg[13]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    auto_ready_dut_enb : in STD_LOGIC;
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite is
  signal \^ar\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^axi4_lite_aresetn_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^axi4_lite_aresetn_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal read_reg_ip_timestamp : STD_LOGIC_VECTOR ( 30 to 30 );
  signal reg_enb_h_in1 : STD_LOGIC;
  signal reg_enb_h_in2 : STD_LOGIC;
  signal reg_enb_h_in3 : STD_LOGIC;
  signal reg_enb_h_in4 : STD_LOGIC;
  signal reg_enb_packet_size_axi4_stream_master : STD_LOGIC;
  signal top_data_write : STD_LOGIC_VECTOR ( 0 to 0 );
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_10 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_11 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_12 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_13 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_14 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_15 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_16 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_17 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_18 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_19 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_20 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_21 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_22 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_23 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_24 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_25 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_26 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_27 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_28 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_29 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_30 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_31 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_32 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_33 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_34 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_35 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_36 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_37 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_38 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_7 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_8 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_9 : STD_LOGIC;
  signal write_axi_enable : STD_LOGIC;
begin
  AR(0) <= \^ar\(0);
  AXI4_Lite_ARESETN_0(0) <= \^axi4_lite_aresetn_0\(0);
  AXI4_Lite_ARESETN_1(0) <= \^axi4_lite_aresetn_1\(0);
u_mlhdlc_sfir_fixpt_ipcore_addr_decoder_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_addr_decoder
     port map (
      AR(0) => \^ar\(0),
      AS(0) => \^axi4_lite_aresetn_0\(0),
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      E(0) => E(0),
      Q(30 downto 0) => Q(30 downto 0),
      S(3 downto 0) => S(3 downto 0),
      auto_ready_dut_enb => auto_ready_dut_enb,
      read_reg_ip_timestamp(0) => read_reg_ip_timestamp(30),
      write_axi_enable => write_axi_enable,
      write_reg_axi_enable_reg_0 => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_7,
      \write_reg_h_in1_reg[0]_0\(0) => \^axi4_lite_aresetn_1\(0),
      \write_reg_h_in1_reg[13]_0\(13 downto 0) => \write_reg_h_in1_reg[13]\(13 downto 0),
      \write_reg_h_in1_reg[13]_1\(0) => reg_enb_h_in1,
      \write_reg_h_in2_reg[13]_0\(13 downto 0) => \write_reg_h_in2_reg[13]\(13 downto 0),
      \write_reg_h_in2_reg[13]_1\(0) => reg_enb_h_in2,
      \write_reg_h_in3_reg[13]_0\(13 downto 0) => \write_reg_h_in3_reg[13]\(13 downto 0),
      \write_reg_h_in3_reg[13]_1\(0) => reg_enb_h_in3,
      \write_reg_h_in4_reg[13]_0\(13 downto 0) => \write_reg_h_in4_reg[13]\(13 downto 0),
      \write_reg_h_in4_reg[13]_1\(0) => reg_enb_h_in4,
      \write_reg_packet_size_axi4_stream_master_reg[12]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[12]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[16]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[16]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[20]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[20]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[24]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[24]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[28]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[28]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[31]_0\(2 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[31]\(2 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(31) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_8,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(30) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_9,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(29) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_10,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(28) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_11,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(27) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_12,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(26) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_13,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(25) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_14,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(24) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_15,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(23) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_16,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(22) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_17,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(21) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_18,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(20) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_19,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(19) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_20,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(18) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_21,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(17) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_22,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(16) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_23,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(15) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_24,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(14) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_25,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_26,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_27,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_28,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_29,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_30,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_31,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_32,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_33,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_34,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_35,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_36,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_37,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_38,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0) => top_data_write(0),
      \write_reg_packet_size_axi4_stream_master_reg[31]_2\(0) => reg_enb_packet_size_axi4_stream_master,
      \write_reg_packet_size_axi4_stream_master_reg[8]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[8]\(3 downto 0)
    );
u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite_module
     port map (
      AR(0) => \^ar\(0),
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(13 downto 0),
      \AXI4_Lite_ARADDR[2]\(0) => reg_enb_packet_size_axi4_stream_master,
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARESETN_0(0) => \^axi4_lite_aresetn_0\(0),
      AXI4_Lite_ARESETN_1(0) => \^axi4_lite_aresetn_1\(0),
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_ARVALID_0(0) => reg_enb_h_in1,
      AXI4_Lite_ARVALID_1(0) => reg_enb_h_in2,
      AXI4_Lite_ARVALID_2(0) => reg_enb_h_in3,
      AXI4_Lite_ARVALID_3(0) => reg_enb_h_in4,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(13 downto 0),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_RDATA(0) => AXI4_Lite_RDATA(0),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_WDATA(31 downto 0) => AXI4_Lite_WDATA(31 downto 0),
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      \FSM_onehot_axi_lite_wstate_reg[2]_0\(1 downto 0) => \FSM_onehot_axi_lite_wstate_reg[2]\(1 downto 0),
      FSM_sequential_axi_lite_rstate_reg_0 => FSM_sequential_axi_lite_rstate_reg,
      IPCORE_RESETN => IPCORE_RESETN,
      Q(31) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_8,
      Q(30) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_9,
      Q(29) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_10,
      Q(28) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_11,
      Q(27) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_12,
      Q(26) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_13,
      Q(25) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_14,
      Q(24) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_15,
      Q(23) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_16,
      Q(22) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_17,
      Q(21) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_18,
      Q(20) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_19,
      Q(19) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_20,
      Q(18) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_21,
      Q(17) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_22,
      Q(16) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_23,
      Q(15) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_24,
      Q(14) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_25,
      Q(13) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_26,
      Q(12) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_27,
      Q(11) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_28,
      Q(10) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_29,
      Q(9) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_30,
      Q(8) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_31,
      Q(7) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_32,
      Q(6) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_33,
      Q(5) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_34,
      Q(4) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_35,
      Q(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_36,
      Q(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_37,
      Q(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_38,
      Q(0) => top_data_write(0),
      read_reg_ip_timestamp(0) => read_reg_ip_timestamp(30),
      \wdata_reg[0]_0\ => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_module_inst_n_7,
      write_axi_enable => write_axi_enable
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_dut is
  port (
    delayed_xout : out STD_LOGIC_VECTOR ( 13 downto 0 );
    p26y_out_add_temp : out STD_LOGIC_VECTOR ( 13 downto 0 );
    p23m4_mul_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    p22m3_mul_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    p21m2_mul_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    p20m1_mul_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 13 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_dut;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_dut is
begin
u_mlhdlc_sfir_fixpt: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt
     port map (
      AR(0) => AR(0),
      D(13 downto 0) => D(13 downto 0),
      E(0) => E(0),
      IPCORE_CLK => IPCORE_CLK,
      delayed_xout(13 downto 0) => delayed_xout(13 downto 0),
      p20m1_mul_temp_0(13 downto 0) => p20m1_mul_temp(13 downto 0),
      p21m2_mul_temp_0(13 downto 0) => p21m2_mul_temp(13 downto 0),
      p22m3_mul_temp_0(13 downto 0) => p22m3_mul_temp(13 downto 0),
      p23m4_mul_temp_0(13 downto 0) => p23m4_mul_temp(13 downto 0),
      p26y_out_add_temp(13 downto 0) => p26y_out_add_temp(13 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic is
  port (
    Q_next_1 : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[21]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[30]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    data_int_reg : out STD_LOGIC;
    cache_data_reg : out STD_LOGIC;
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    out_valid_0 : in STD_LOGIC;
    fifo_valid : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    tlast_counter_out_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    tlast_size_value : in STD_LOGIC_VECTOR ( 30 downto 0 );
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    cache_wr_en : in STD_LOGIC;
    Out_1_reg : in STD_LOGIC;
    out_wr_en : in STD_LOGIC;
    AXI4_Stream_Master_TLAST : in STD_LOGIC;
    auto_tlast : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic is
  signal \fifo_back_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal fifo_data_out : STD_LOGIC;
  signal \fifo_front_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_pop__2\ : STD_LOGIC;
  signal fifo_read_enable : STD_LOGIC;
  signal \fifo_sample_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[2]\ : STD_LOGIC;
  signal w_d1 : STD_LOGIC;
  signal w_d2 : STD_LOGIC;
  signal w_mux1 : STD_LOGIC;
  signal w_we : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \fifo_back_indx[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \fifo_back_indx[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \fifo_front_indx[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \fifo_front_indx[1]_i_1\ : label is "soft_lutpair2";
begin
\fifo_back_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => w_we,
      I1 => \fifo_back_indx_reg_n_0_[0]\,
      O => \fifo_back_indx[0]_i_1_n_0\
    );
\fifo_back_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_back_indx_reg_n_0_[0]\,
      I1 => w_we,
      I2 => \fifo_back_indx_reg_n_0_[1]\,
      O => \fifo_back_indx[1]_i_1_n_0\
    );
\fifo_back_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_back_indx[0]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[0]\
    );
\fifo_back_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_back_indx[1]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[1]\
    );
\fifo_front_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => fifo_read_enable,
      I1 => \fifo_front_indx_reg_n_0_[0]\,
      O => \fifo_front_indx[0]_i_1_n_0\
    );
\fifo_front_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_front_indx_reg_n_0_[0]\,
      I1 => fifo_read_enable,
      I2 => \fifo_front_indx_reg_n_0_[1]\,
      O => \fifo_front_indx[1]_i_1_n_0\
    );
\fifo_front_indx[1]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => out_valid_0,
      I2 => fifo_valid,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      I5 => \fifo_sample_count_reg_n_0_[2]\,
      O => fifo_read_enable
    );
\fifo_front_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_front_indx[0]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[0]\
    );
\fifo_front_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_front_indx[1]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[1]\
    );
\fifo_sample_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55555567A8A8A8"
    )
        port map (
      I0 => \fifo_pop__2\,
      I1 => \fifo_sample_count_reg_n_0_[1]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => out_valid,
      I4 => auto_ready_dut_enb,
      I5 => \fifo_sample_count_reg_n_0_[0]\,
      O => \fifo_sample_count[0]_i_1_n_0\
    );
\fifo_sample_count[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F80F007F7F8080"
    )
        port map (
      I0 => auto_ready_dut_enb,
      I1 => out_valid,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[2]\,
      I4 => \fifo_sample_count_reg_n_0_[1]\,
      I5 => \fifo_pop__2\,
      O => \fifo_sample_count[1]_i_1_n_0\
    );
\fifo_sample_count[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00F0007F80FF00"
    )
        port map (
      I0 => auto_ready_dut_enb,
      I1 => out_valid,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[2]\,
      I4 => \fifo_sample_count_reg_n_0_[1]\,
      I5 => \fifo_pop__2\,
      O => \fifo_sample_count[2]_i_1_n_0\
    );
\fifo_sample_count[2]_i_2__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFEFEFEFEFEFE"
    )
        port map (
      I0 => \fifo_sample_count_reg_n_0_[1]\,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => fifo_valid,
      I4 => out_valid_0,
      I5 => cache_valid,
      O => \fifo_pop__2\
    );
\fifo_sample_count_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[0]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[0]\
    );
\fifo_sample_count_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[1]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[1]\
    );
\fifo_sample_count_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[2]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[2]\
    );
\fifo_valid_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEAAAAA"
    )
        port map (
      I0 => \fifo_pop__2\,
      I1 => fifo_valid,
      I2 => cache_valid,
      I3 => AXI4_Stream_Master_TREADY,
      I4 => out_valid_0,
      O => Q_next_1
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_ram_singlebit: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_singlebit
     port map (
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      IPCORE_CLK => IPCORE_CLK,
      Out_1_reg => Out_1_reg,
      Q(0) => Q(0),
      S(3 downto 0) => S(3 downto 0),
      auto_ready_dut_enb => auto_ready_dut_enb,
      auto_tlast => auto_tlast,
      cache_data_reg => cache_data_reg,
      cache_valid => cache_valid,
      cache_wr_en => cache_wr_en,
      data_int_reg_0 => data_int_reg,
      data_int_reg_1 => \fifo_sample_count_reg_n_0_[0]\,
      data_int_reg_2 => \fifo_sample_count_reg_n_0_[2]\,
      data_int_reg_3 => \fifo_sample_count_reg_n_0_[1]\,
      fifo_data_out => fifo_data_out,
      out_valid => out_valid,
      out_wr_en => out_wr_en,
      rd_addr(1) => \fifo_front_indx_reg_n_0_[1]\,
      rd_addr(0) => \fifo_front_indx_reg_n_0_[0]\,
      tlast_counter_out_reg(31 downto 0) => tlast_counter_out_reg(31 downto 0),
      \tlast_counter_out_reg[21]\(3 downto 0) => \tlast_counter_out_reg[21]\(3 downto 0),
      \tlast_counter_out_reg[30]\(2 downto 0) => \tlast_counter_out_reg[30]\(2 downto 0),
      tlast_size_value(30 downto 0) => tlast_size_value(30 downto 0),
      w_d1 => w_d1,
      w_d2 => w_d2,
      w_we => w_we,
      wr_addr(1) => \fifo_back_indx_reg_n_0_[1]\,
      wr_addr(0) => \fifo_back_indx_reg_n_0_[0]\
    );
\w_d1_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => out_valid_0,
      I2 => fifo_valid,
      I3 => \fifo_sample_count_reg_n_0_[2]\,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      I5 => \fifo_sample_count_reg_n_0_[1]\,
      O => w_mux1
    );
w_d1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => w_mux1,
      Q => w_d1
    );
w_d2_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => fifo_data_out,
      Q => w_d2
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic is
  port (
    Q_next_1 : out STD_LOGIC;
    auto_ready_axi4_stream_master : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 15 downto 0 );
    \data_int_reg[15]\ : out STD_LOGIC_VECTOR ( 15 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    w_d1_reg_0 : in STD_LOGIC;
    fifo_valid : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 15 downto 0 );
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    p26y_out_add_temp : in STD_LOGIC_VECTOR ( 13 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic is
  signal \fifo_back_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_front_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal fifo_num : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \fifo_pop__2\ : STD_LOGIC;
  signal fifo_read_enable : STD_LOGIC;
  signal \fifo_sample_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_1_n_0\ : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_16 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_17 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_18 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_19 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_20 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_21 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_22 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_23 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_24 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_25 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_26 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_27 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_28 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_29 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_30 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_31 : STD_LOGIC;
  signal w_d1 : STD_LOGIC;
  signal \w_d2_reg_n_0_[0]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[10]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[11]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[12]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[13]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[14]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[15]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[1]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[2]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[3]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[4]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[5]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[6]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[7]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[8]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[9]\ : STD_LOGIC;
  signal w_mux1 : STD_LOGIC;
  signal w_we : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \fifo_back_indx[0]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \fifo_back_indx[1]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \fifo_front_indx[0]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \fifo_front_indx[1]_i_1\ : label is "soft_lutpair22";
begin
\fifo_back_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => w_we,
      I1 => \fifo_back_indx_reg_n_0_[0]\,
      O => \fifo_back_indx[0]_i_1_n_0\
    );
\fifo_back_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_back_indx_reg_n_0_[0]\,
      I1 => w_we,
      I2 => \fifo_back_indx_reg_n_0_[1]\,
      O => \fifo_back_indx[1]_i_1_n_0\
    );
\fifo_back_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_back_indx[0]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[0]\
    );
\fifo_back_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_back_indx[1]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[1]\
    );
\fifo_front_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => fifo_read_enable,
      I1 => \fifo_front_indx_reg_n_0_[0]\,
      O => \fifo_front_indx[0]_i_1_n_0\
    );
\fifo_front_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_front_indx_reg_n_0_[0]\,
      I1 => fifo_read_enable,
      I2 => \fifo_front_indx_reg_n_0_[1]\,
      O => \fifo_front_indx[1]_i_1_n_0\
    );
\fifo_front_indx[1]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => w_d1_reg_0,
      I2 => fifo_valid,
      I3 => fifo_num(1),
      I4 => fifo_num(0),
      I5 => fifo_num(2),
      O => fifo_read_enable
    );
\fifo_front_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_front_indx[0]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[0]\
    );
\fifo_front_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_front_indx[1]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[1]\
    );
fifo_rd_ack_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"15"
    )
        port map (
      I0 => fifo_num(2),
      I1 => fifo_num(0),
      I2 => fifo_num(1),
      O => auto_ready_axi4_stream_master
    );
\fifo_sample_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55555567A8A8A8"
    )
        port map (
      I0 => \fifo_pop__2\,
      I1 => fifo_num(1),
      I2 => fifo_num(2),
      I3 => out_valid,
      I4 => auto_ready_dut_enb,
      I5 => fifo_num(0),
      O => \fifo_sample_count[0]_i_1_n_0\
    );
\fifo_sample_count[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F80F007F7F8080"
    )
        port map (
      I0 => auto_ready_dut_enb,
      I1 => out_valid,
      I2 => fifo_num(0),
      I3 => fifo_num(2),
      I4 => fifo_num(1),
      I5 => \fifo_pop__2\,
      O => \fifo_sample_count[1]_i_1_n_0\
    );
\fifo_sample_count[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00F0007F80FF00"
    )
        port map (
      I0 => auto_ready_dut_enb,
      I1 => out_valid,
      I2 => fifo_num(0),
      I3 => fifo_num(2),
      I4 => fifo_num(1),
      I5 => \fifo_pop__2\,
      O => \fifo_sample_count[2]_i_1_n_0\
    );
\fifo_sample_count[2]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFEFEFEFEFEFE"
    )
        port map (
      I0 => fifo_num(1),
      I1 => fifo_num(0),
      I2 => fifo_num(2),
      I3 => fifo_valid,
      I4 => w_d1_reg_0,
      I5 => cache_valid,
      O => \fifo_pop__2\
    );
\fifo_sample_count_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[0]_i_1_n_0\,
      Q => fifo_num(0)
    );
\fifo_sample_count_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[1]_i_1_n_0\,
      Q => fifo_num(1)
    );
\fifo_sample_count_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[2]_i_1_n_0\,
      Q => fifo_num(2)
    );
\fifo_valid_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEAAAAA"
    )
        port map (
      I0 => \fifo_pop__2\,
      I1 => fifo_valid,
      I2 => cache_valid,
      I3 => AXI4_Stream_Master_TREADY,
      I4 => w_d1_reg_0,
      O => Q_next_1
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic_0
     port map (
      ADDRA(1) => \fifo_front_indx_reg_n_0_[1]\,
      ADDRA(0) => \fifo_front_indx_reg_n_0_[0]\,
      ADDRD(1) => \fifo_back_indx_reg_n_0_[1]\,
      ADDRD(0) => \fifo_back_indx_reg_n_0_[0]\,
      D(15 downto 0) => D(15 downto 0),
      IPCORE_CLK => IPCORE_CLK,
      \Out_1_reg[15]\(15) => \w_d2_reg_n_0_[15]\,
      \Out_1_reg[15]\(14) => \w_d2_reg_n_0_[14]\,
      \Out_1_reg[15]\(13) => \w_d2_reg_n_0_[13]\,
      \Out_1_reg[15]\(12) => \w_d2_reg_n_0_[12]\,
      \Out_1_reg[15]\(11) => \w_d2_reg_n_0_[11]\,
      \Out_1_reg[15]\(10) => \w_d2_reg_n_0_[10]\,
      \Out_1_reg[15]\(9) => \w_d2_reg_n_0_[9]\,
      \Out_1_reg[15]\(8) => \w_d2_reg_n_0_[8]\,
      \Out_1_reg[15]\(7) => \w_d2_reg_n_0_[7]\,
      \Out_1_reg[15]\(6) => \w_d2_reg_n_0_[6]\,
      \Out_1_reg[15]\(5) => \w_d2_reg_n_0_[5]\,
      \Out_1_reg[15]\(4) => \w_d2_reg_n_0_[4]\,
      \Out_1_reg[15]\(3) => \w_d2_reg_n_0_[3]\,
      \Out_1_reg[15]\(2) => \w_d2_reg_n_0_[2]\,
      \Out_1_reg[15]\(1) => \w_d2_reg_n_0_[1]\,
      \Out_1_reg[15]\(0) => \w_d2_reg_n_0_[0]\,
      Q(15 downto 0) => Q(15 downto 0),
      auto_ready_dut_enb => auto_ready_dut_enb,
      cache_valid => cache_valid,
      data_int(15) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_16,
      data_int(14) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_17,
      data_int(13) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_18,
      data_int(12) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_19,
      data_int(11) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_20,
      data_int(10) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_21,
      data_int(9) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_22,
      data_int(8) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_23,
      data_int(7) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_24,
      data_int(6) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_25,
      data_int(5) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_26,
      data_int(4) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_27,
      data_int(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_28,
      data_int(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_29,
      data_int(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_30,
      data_int(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_31,
      \data_int_reg[15]_0\(15 downto 0) => \data_int_reg[15]\(15 downto 0),
      fifo_num(2 downto 0) => fifo_num(2 downto 0),
      out_valid => out_valid,
      p26y_out_add_temp(13 downto 0) => p26y_out_add_temp(13 downto 0),
      w_d1 => w_d1,
      w_we => w_we
    );
\w_d1_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => w_d1_reg_0,
      I2 => fifo_valid,
      I3 => fifo_num(2),
      I4 => fifo_num(0),
      I5 => fifo_num(1),
      O => w_mux1
    );
w_d1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => w_mux1,
      Q => w_d1
    );
\w_d2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_31,
      Q => \w_d2_reg_n_0_[0]\
    );
\w_d2_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_21,
      Q => \w_d2_reg_n_0_[10]\
    );
\w_d2_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_20,
      Q => \w_d2_reg_n_0_[11]\
    );
\w_d2_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_19,
      Q => \w_d2_reg_n_0_[12]\
    );
\w_d2_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_18,
      Q => \w_d2_reg_n_0_[13]\
    );
\w_d2_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_17,
      Q => \w_d2_reg_n_0_[14]\
    );
\w_d2_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_16,
      Q => \w_d2_reg_n_0_[15]\
    );
\w_d2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_30,
      Q => \w_d2_reg_n_0_[1]\
    );
\w_d2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_29,
      Q => \w_d2_reg_n_0_[2]\
    );
\w_d2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_28,
      Q => \w_d2_reg_n_0_[3]\
    );
\w_d2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_27,
      Q => \w_d2_reg_n_0_[4]\
    );
\w_d2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_26,
      Q => \w_d2_reg_n_0_[5]\
    );
\w_d2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_25,
      Q => \w_d2_reg_n_0_[6]\
    );
\w_d2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_24,
      Q => \w_d2_reg_n_0_[7]\
    );
\w_d2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_23,
      Q => \w_d2_reg_n_0_[8]\
    );
\w_d2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_ram_generic_n_22,
      Q => \w_d2_reg_n_0_[9]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic is
  port (
    Q_next_1 : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 13 downto 0 );
    \data_int_reg[13]\ : out STD_LOGIC_VECTOR ( 13 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    fifo_valid_reg : in STD_LOGIC;
    fifo_valid : in STD_LOGIC;
    fifo_valid_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic is
  signal data_int : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \fifo_back_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_pop__2\ : STD_LOGIC;
  signal fifo_read_enable : STD_LOGIC;
  signal \fifo_sample_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[2]\ : STD_LOGIC;
  signal rd_addr : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal w_d1 : STD_LOGIC;
  signal w_d2 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal w_mux1 : STD_LOGIC;
  signal wr_addr : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI4_Stream_Slave_TREADY_INST_0 : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \fifo_back_indx[0]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \fifo_front_indx[0]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \fifo_front_indx[1]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \fifo_sample_count[0]_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \fifo_sample_count[2]_i_1\ : label is "soft_lutpair39";
begin
AXI4_Stream_Slave_TREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FB"
    )
        port map (
      I0 => \fifo_sample_count_reg_n_0_[1]\,
      I1 => \fifo_sample_count_reg_n_0_[2]\,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      O => AXI4_Stream_Slave_TREADY
    );
\fifo_back_indx[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5575AA8A"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => wr_addr(0),
      O => \fifo_back_indx[0]_i_1_n_0\
    );
\fifo_back_indx[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5575FFFFAA8A0000"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => wr_addr(0),
      I5 => wr_addr(1),
      O => \fifo_back_indx[1]_i_1_n_0\
    );
\fifo_back_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_back_indx[0]_i_1_n_0\,
      Q => wr_addr(0)
    );
\fifo_back_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_back_indx[1]_i_1_n_0\,
      Q => wr_addr(1)
    );
\fifo_front_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => fifo_read_enable,
      I1 => rd_addr(0),
      O => \fifo_front_indx[0]_i_1_n_0\
    );
\fifo_front_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => rd_addr(0),
      I1 => fifo_read_enable,
      I2 => rd_addr(1),
      O => \fifo_front_indx[1]_i_1_n_0\
    );
\fifo_front_indx[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => fifo_valid_reg,
      I2 => fifo_valid,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      I5 => \fifo_sample_count_reg_n_0_[2]\,
      O => fifo_read_enable
    );
\fifo_front_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_front_indx[0]_i_1_n_0\,
      Q => rd_addr(0)
    );
\fifo_front_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_front_indx[1]_i_1_n_0\,
      Q => rd_addr(1)
    );
\fifo_sample_count[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA5567A8"
    )
        port map (
      I0 => \fifo_pop__2\,
      I1 => \fifo_sample_count_reg_n_0_[1]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => AXI4_Stream_Slave_TVALID,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      O => \fifo_sample_count[0]_i_1_n_0\
    );
\fifo_sample_count[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EE307788"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => \fifo_pop__2\,
      O => \fifo_sample_count[1]_i_1_n_0\
    );
\fifo_sample_count[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F0C078F0"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => \fifo_pop__2\,
      O => \fifo_sample_count[2]_i_1_n_0\
    );
\fifo_sample_count[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFEFEFEFEFEFE"
    )
        port map (
      I0 => \fifo_sample_count_reg_n_0_[1]\,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => fifo_valid,
      I4 => fifo_valid_reg,
      I5 => cache_valid,
      O => \fifo_pop__2\
    );
\fifo_sample_count_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[0]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[0]\
    );
\fifo_sample_count_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[1]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[1]\
    );
\fifo_sample_count_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \fifo_sample_count[2]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[2]\
    );
fifo_valid_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEAAAAA"
    )
        port map (
      I0 => \fifo_pop__2\,
      I1 => fifo_valid,
      I2 => cache_valid,
      I3 => fifo_valid_reg_0,
      I4 => fifo_valid_reg,
      O => Q_next_1
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_SimpleDualPortRAM_generic
     port map (
      ADDRA(1 downto 0) => rd_addr(1 downto 0),
      ADDRD(1 downto 0) => wr_addr(1 downto 0),
      AXI4_Stream_Slave_TDATA(15 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 0),
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      D(13 downto 0) => D(13 downto 0),
      IPCORE_CLK => IPCORE_CLK,
      \Out_1_reg[13]\(13 downto 0) => w_d2(13 downto 0),
      Q(13 downto 0) => Q(13 downto 0),
      cache_valid => cache_valid,
      data_int(13 downto 0) => data_int(13 downto 0),
      \data_int_reg[13]_0\(13 downto 0) => \data_int_reg[13]\(13 downto 0),
      \data_int_reg[1]_0\ => \fifo_sample_count_reg_n_0_[0]\,
      \data_int_reg[1]_1\ => \fifo_sample_count_reg_n_0_[2]\,
      \data_int_reg[1]_2\ => \fifo_sample_count_reg_n_0_[1]\,
      w_d1 => w_d1
    );
w_d1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => fifo_valid_reg,
      I2 => fifo_valid,
      I3 => \fifo_sample_count_reg_n_0_[2]\,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      I5 => \fifo_sample_count_reg_n_0_[1]\,
      O => w_mux1
    );
w_d1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => w_mux1,
      Q => w_d1
    );
\w_d2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(0),
      Q => w_d2(0)
    );
\w_d2_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(10),
      Q => w_d2(10)
    );
\w_d2_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(11),
      Q => w_d2(11)
    );
\w_d2_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(12),
      Q => w_d2(12)
    );
\w_d2_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(13),
      Q => w_d2(13)
    );
\w_d2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(1),
      Q => w_d2(1)
    );
\w_d2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(2),
      Q => w_d2(2)
    );
\w_d2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(3),
      Q => w_d2(3)
    );
\w_d2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(4),
      Q => w_d2(4)
    );
\w_d2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(5),
      Q => w_d2(5)
    );
\w_d2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(6),
      Q => w_d2(6)
    );
\w_d2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(7),
      Q => w_d2(7)
    );
\w_d2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(8),
      Q => w_d2(8)
    );
\w_d2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => reset,
      D => data_int(9),
      Q => w_d2(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT is
  port (
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[21]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[30]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    tlast_counter_out_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    tlast_size_value : in STD_LOGIC_VECTOR ( 30 downto 0 );
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    auto_tlast : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT is
  signal \^axi4_stream_master_tlast\ : STD_LOGIC;
  signal Q_next : STD_LOGIC;
  signal Q_next_1 : STD_LOGIC;
  signal cache_data_reg_n_0 : STD_LOGIC;
  signal cache_valid : STD_LOGIC;
  signal cache_wr_en : STD_LOGIC;
  signal fifo_valid : STD_LOGIC;
  signal out_valid_0 : STD_LOGIC;
  signal \out_valid_i_1__1_n_0\ : STD_LOGIC;
  signal out_wr_en : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_12 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_13 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of Out_1_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of cache_data_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cache_valid_i_1__1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \out_valid_i_1__1\ : label is "soft_lutpair3";
begin
  AXI4_Stream_Master_TLAST <= \^axi4_stream_master_tlast\;
Out_1_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => out_valid_0,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => out_wr_en
    );
Out_1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_13,
      Q => \^axi4_stream_master_tlast\
    );
cache_data_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A600"
    )
        port map (
      I0 => cache_valid,
      I1 => out_valid_0,
      I2 => AXI4_Stream_Master_TREADY,
      I3 => fifo_valid,
      O => cache_wr_en
    );
cache_data_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_12,
      Q => cache_data_reg_n_0
    );
\cache_valid_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F220"
    )
        port map (
      I0 => out_valid_0,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => Q_next
    );
cache_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next,
      Q => cache_valid
    );
fifo_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next_1,
      Q => fifo_valid
    );
\out_valid_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEFE"
    )
        port map (
      I0 => fifo_valid,
      I1 => cache_valid,
      I2 => out_valid_0,
      I3 => AXI4_Stream_Master_TREADY,
      O => \out_valid_i_1__1_n_0\
    );
out_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => \out_valid_i_1__1_n_0\,
      Q => out_valid_0
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic
     port map (
      AXI4_Stream_Master_TLAST => \^axi4_stream_master_tlast\,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      IPCORE_CLK => IPCORE_CLK,
      Out_1_reg => cache_data_reg_n_0,
      Q(0) => Q(0),
      Q_next_1 => Q_next_1,
      S(3 downto 0) => S(3 downto 0),
      auto_ready_dut_enb => auto_ready_dut_enb,
      auto_tlast => auto_tlast,
      cache_data_reg => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_13,
      cache_valid => cache_valid,
      cache_wr_en => cache_wr_en,
      data_int_reg => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_classic_inst_n_12,
      fifo_valid => fifo_valid,
      out_valid => out_valid,
      out_valid_0 => out_valid_0,
      out_wr_en => out_wr_en,
      reset => reset,
      tlast_counter_out_reg(31 downto 0) => tlast_counter_out_reg(31 downto 0),
      \tlast_counter_out_reg[21]\(3 downto 0) => \tlast_counter_out_reg[21]\(3 downto 0),
      \tlast_counter_out_reg[30]\(2 downto 0) => \tlast_counter_out_reg[30]\(2 downto 0),
      tlast_size_value(30 downto 0) => tlast_size_value(30 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data is
  port (
    out_valid_reg_0 : out STD_LOGIC;
    auto_tlast : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    out_valid_reg_1 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 13 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    fifo_valid_reg_0 : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data is
  signal Q_next : STD_LOGIC;
  signal Q_next_1 : STD_LOGIC;
  signal Q_next_2 : STD_LOGIC;
  signal cache_data : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal cache_valid : STD_LOGIC;
  signal cache_wr_en : STD_LOGIC;
  signal data_out_next : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal fifo_data_out : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal fifo_valid : STD_LOGIC;
  signal \^out_valid_reg_0\ : STD_LOGIC;
  signal out_wr_en : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of cache_valid_i_1 : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of out_valid_i_1 : label is "soft_lutpair41";
begin
  out_valid_reg_0 <= \^out_valid_reg_0\;
\Out_1[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => out_wr_en
    );
\Out_1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(0),
      Q => Q(0)
    );
\Out_1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(10),
      Q => Q(10)
    );
\Out_1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(11),
      Q => Q(11)
    );
\Out_1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(12),
      Q => Q(12)
    );
\Out_1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(13),
      Q => Q(13)
    );
\Out_1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(1),
      Q => Q(1)
    );
\Out_1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(2),
      Q => Q(2)
    );
\Out_1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(3),
      Q => Q(3)
    );
\Out_1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(4),
      Q => Q(4)
    );
\Out_1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(5),
      Q => Q(5)
    );
\Out_1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(6),
      Q => Q(6)
    );
\Out_1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(7),
      Q => Q(7)
    );
\Out_1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(8),
      Q => Q(8)
    );
\Out_1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => data_out_next(9),
      Q => Q(9)
    );
\cache_data[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A600"
    )
        port map (
      I0 => cache_valid,
      I1 => \^out_valid_reg_0\,
      I2 => fifo_valid_reg_0,
      I3 => fifo_valid,
      O => cache_wr_en
    );
\cache_data_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(0),
      Q => cache_data(0)
    );
\cache_data_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(10),
      Q => cache_data(10)
    );
\cache_data_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(11),
      Q => cache_data(11)
    );
\cache_data_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(12),
      Q => cache_data(12)
    );
\cache_data_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(13),
      Q => cache_data(13)
    );
\cache_data_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(1),
      Q => cache_data(1)
    );
\cache_data_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(2),
      Q => cache_data(2)
    );
\cache_data_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(3),
      Q => cache_data(3)
    );
\cache_data_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(4),
      Q => cache_data(4)
    );
\cache_data_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(5),
      Q => cache_data(5)
    );
\cache_data_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(6),
      Q => cache_data(6)
    );
\cache_data_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(7),
      Q => cache_data(7)
    );
\cache_data_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(8),
      Q => cache_data(8)
    );
\cache_data_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => fifo_data_out(9),
      Q => cache_data(9)
    );
cache_valid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F220"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => Q_next
    );
cache_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next,
      Q => cache_valid
    );
fifo_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next_1,
      Q => fifo_valid
    );
out_valid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => fifo_valid,
      I1 => cache_valid,
      I2 => fifo_valid_reg_0,
      I3 => \^out_valid_reg_0\,
      O => Q_next_2
    );
out_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next_2,
      Q => \^out_valid_reg_0\
    );
ram_reg_0_3_0_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      I2 => CO(0),
      O => auto_tlast
    );
\tlast_counter_out[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      O => out_valid_reg_1
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_classic
     port map (
      AXI4_Stream_Slave_TDATA(15 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      D(13 downto 0) => data_out_next(13 downto 0),
      IPCORE_CLK => IPCORE_CLK,
      Q(13 downto 0) => cache_data(13 downto 0),
      Q_next_1 => Q_next_1,
      cache_valid => cache_valid,
      \data_int_reg[13]\(13 downto 0) => fifo_data_out(13 downto 0),
      fifo_valid => fifo_valid,
      fifo_valid_reg => \^out_valid_reg_0\,
      fifo_valid_reg_0 => fifo_valid_reg_0,
      reset => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT is
  port (
    out_valid_reg_0 : out STD_LOGIC;
    auto_ready_axi4_stream_master : out STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    p26y_out_add_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT is
  signal Q_next : STD_LOGIC;
  signal Q_next_1 : STD_LOGIC;
  signal Q_next_2 : STD_LOGIC;
  signal \cache_data_reg_n_0_[0]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[10]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[11]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[12]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[13]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[14]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[15]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[1]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[2]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[3]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[4]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[5]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[6]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[7]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[8]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[9]\ : STD_LOGIC;
  signal cache_valid : STD_LOGIC;
  signal cache_wr_en : STD_LOGIC;
  signal fifo_valid : STD_LOGIC;
  signal \^out_valid_reg_0\ : STD_LOGIC;
  signal out_wr_en : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_10 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_11 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_12 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_13 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_14 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_15 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_16 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_17 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_18 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_19 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_2 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_20 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_21 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_22 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_23 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_24 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_25 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_26 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_27 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_28 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_29 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_3 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_30 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_31 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_32 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_33 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_4 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_5 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_6 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_7 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_8 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_9 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cache_valid_i_1__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \out_valid_i_1__0\ : label is "soft_lutpair23";
begin
  out_valid_reg_0 <= \^out_valid_reg_0\;
\Out_1[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => out_wr_en
    );
\Out_1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_17,
      Q => AXI4_Stream_Master_TDATA(0)
    );
\Out_1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_7,
      Q => AXI4_Stream_Master_TDATA(10)
    );
\Out_1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_6,
      Q => AXI4_Stream_Master_TDATA(11)
    );
\Out_1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_5,
      Q => AXI4_Stream_Master_TDATA(12)
    );
\Out_1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_4,
      Q => AXI4_Stream_Master_TDATA(13)
    );
\Out_1_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_3,
      Q => AXI4_Stream_Master_TDATA(14)
    );
\Out_1_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_2,
      Q => AXI4_Stream_Master_TDATA(15)
    );
\Out_1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_16,
      Q => AXI4_Stream_Master_TDATA(1)
    );
\Out_1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_15,
      Q => AXI4_Stream_Master_TDATA(2)
    );
\Out_1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_14,
      Q => AXI4_Stream_Master_TDATA(3)
    );
\Out_1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_13,
      Q => AXI4_Stream_Master_TDATA(4)
    );
\Out_1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_12,
      Q => AXI4_Stream_Master_TDATA(5)
    );
\Out_1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_11,
      Q => AXI4_Stream_Master_TDATA(6)
    );
\Out_1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_10,
      Q => AXI4_Stream_Master_TDATA(7)
    );
\Out_1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_9,
      Q => AXI4_Stream_Master_TDATA(8)
    );
\Out_1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(0),
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_8,
      Q => AXI4_Stream_Master_TDATA(9)
    );
\cache_data[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A600"
    )
        port map (
      I0 => cache_valid,
      I1 => \^out_valid_reg_0\,
      I2 => AXI4_Stream_Master_TREADY,
      I3 => fifo_valid,
      O => cache_wr_en
    );
\cache_data_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_33,
      Q => \cache_data_reg_n_0_[0]\
    );
\cache_data_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_23,
      Q => \cache_data_reg_n_0_[10]\
    );
\cache_data_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_22,
      Q => \cache_data_reg_n_0_[11]\
    );
\cache_data_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_21,
      Q => \cache_data_reg_n_0_[12]\
    );
\cache_data_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_20,
      Q => \cache_data_reg_n_0_[13]\
    );
\cache_data_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_19,
      Q => \cache_data_reg_n_0_[14]\
    );
\cache_data_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_18,
      Q => \cache_data_reg_n_0_[15]\
    );
\cache_data_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_32,
      Q => \cache_data_reg_n_0_[1]\
    );
\cache_data_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_31,
      Q => \cache_data_reg_n_0_[2]\
    );
\cache_data_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_30,
      Q => \cache_data_reg_n_0_[3]\
    );
\cache_data_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_29,
      Q => \cache_data_reg_n_0_[4]\
    );
\cache_data_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_28,
      Q => \cache_data_reg_n_0_[5]\
    );
\cache_data_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_27,
      Q => \cache_data_reg_n_0_[6]\
    );
\cache_data_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_26,
      Q => \cache_data_reg_n_0_[7]\
    );
\cache_data_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_25,
      Q => \cache_data_reg_n_0_[8]\
    );
\cache_data_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => reset,
      D => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_24,
      Q => \cache_data_reg_n_0_[9]\
    );
\cache_valid_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F220"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => Q_next
    );
cache_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next,
      Q => cache_valid
    );
fifo_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next_1,
      Q => fifo_valid
    );
\out_valid_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => fifo_valid,
      I1 => cache_valid,
      I2 => AXI4_Stream_Master_TREADY,
      I3 => \^out_valid_reg_0\,
      O => Q_next_2
    );
out_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => Q_next_2,
      Q => \^out_valid_reg_0\
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic
     port map (
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      D(15) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_2,
      D(14) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_3,
      D(13) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_4,
      D(12) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_5,
      D(11) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_6,
      D(10) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_7,
      D(9) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_8,
      D(8) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_9,
      D(7) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_10,
      D(6) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_11,
      D(5) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_12,
      D(4) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_13,
      D(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_14,
      D(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_15,
      D(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_16,
      D(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_17,
      IPCORE_CLK => IPCORE_CLK,
      Q(15) => \cache_data_reg_n_0_[15]\,
      Q(14) => \cache_data_reg_n_0_[14]\,
      Q(13) => \cache_data_reg_n_0_[13]\,
      Q(12) => \cache_data_reg_n_0_[12]\,
      Q(11) => \cache_data_reg_n_0_[11]\,
      Q(10) => \cache_data_reg_n_0_[10]\,
      Q(9) => \cache_data_reg_n_0_[9]\,
      Q(8) => \cache_data_reg_n_0_[8]\,
      Q(7) => \cache_data_reg_n_0_[7]\,
      Q(6) => \cache_data_reg_n_0_[6]\,
      Q(5) => \cache_data_reg_n_0_[5]\,
      Q(4) => \cache_data_reg_n_0_[4]\,
      Q(3) => \cache_data_reg_n_0_[3]\,
      Q(2) => \cache_data_reg_n_0_[2]\,
      Q(1) => \cache_data_reg_n_0_[1]\,
      Q(0) => \cache_data_reg_n_0_[0]\,
      Q_next_1 => Q_next_1,
      auto_ready_axi4_stream_master => auto_ready_axi4_stream_master,
      auto_ready_dut_enb => auto_ready_dut_enb,
      cache_valid => cache_valid,
      \data_int_reg[15]\(15) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_18,
      \data_int_reg[15]\(14) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_19,
      \data_int_reg[15]\(13) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_20,
      \data_int_reg[15]\(12) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_21,
      \data_int_reg[15]\(11) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_22,
      \data_int_reg[15]\(10) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_23,
      \data_int_reg[15]\(9) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_24,
      \data_int_reg[15]\(8) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_25,
      \data_int_reg[15]\(7) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_26,
      \data_int_reg[15]\(6) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_27,
      \data_int_reg[15]\(5) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_28,
      \data_int_reg[15]\(4) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_29,
      \data_int_reg[15]\(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_30,
      \data_int_reg[15]\(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_31,
      \data_int_reg[15]\(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_32,
      \data_int_reg[15]\(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_classic_inst_n_33,
      fifo_valid => fifo_valid,
      out_valid => out_valid,
      p26y_out_add_temp(13 downto 0) => p26y_out_add_temp(13 downto 0),
      reset => reset,
      w_d1_reg_0 => \^out_valid_reg_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master is
  port (
    out_valid_reg : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    auto_ready_axi4_stream_master : out STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    \tlast_counter_out_reg[31]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    relop_relop1_carry_i_3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    relop_relop1_carry_i_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \relop_relop1_carry__0_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \relop_relop1_carry__0_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \relop_relop1_carry__0_i_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \relop_relop1_carry__1_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \relop_relop1_carry__1_i_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    auto_ready_dut_enb : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    p26y_out_add_temp : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    auto_tlast : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \relop_relop1_carry__0_n_0\ : STD_LOGIC;
  signal \relop_relop1_carry__0_n_1\ : STD_LOGIC;
  signal \relop_relop1_carry__0_n_2\ : STD_LOGIC;
  signal \relop_relop1_carry__0_n_3\ : STD_LOGIC;
  signal \relop_relop1_carry__1_n_2\ : STD_LOGIC;
  signal \relop_relop1_carry__1_n_3\ : STD_LOGIC;
  signal relop_relop1_carry_n_0 : STD_LOGIC;
  signal relop_relop1_carry_n_1 : STD_LOGIC;
  signal relop_relop1_carry_n_2 : STD_LOGIC;
  signal relop_relop1_carry_n_3 : STD_LOGIC;
  signal \tlast_counter_out[0]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_6_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_7_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_5_n_0\ : STD_LOGIC;
  signal tlast_counter_out_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \tlast_counter_out_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal tlast_size_value : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \tlast_size_value_carry__0_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__0_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__0_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__0_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__6_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__6_n_3\ : STD_LOGIC;
  signal tlast_size_value_carry_n_0 : STD_LOGIC;
  signal tlast_size_value_carry_n_1 : STD_LOGIC;
  signal tlast_size_value_carry_n_2 : STD_LOGIC;
  signal tlast_size_value_carry_n_3 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_1 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_10 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_11 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_2 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_3 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_4 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_5 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_6 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_7 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_8 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_9 : STD_LOGIC;
  signal NLW_relop_relop1_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_relop_relop1_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_relop_relop1_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_relop_relop1_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_tlast_size_value_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tlast_size_value_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  CO(0) <= \^co\(0);
relop_relop1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => relop_relop1_carry_n_0,
      CO(2) => relop_relop1_carry_n_1,
      CO(1) => relop_relop1_carry_n_2,
      CO(0) => relop_relop1_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_relop_relop1_carry_O_UNCONNECTED(3 downto 0),
      S(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_1,
      S(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_2,
      S(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_3,
      S(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_4
    );
\relop_relop1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => relop_relop1_carry_n_0,
      CO(3) => \relop_relop1_carry__0_n_0\,
      CO(2) => \relop_relop1_carry__0_n_1\,
      CO(1) => \relop_relop1_carry__0_n_2\,
      CO(0) => \relop_relop1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_relop_relop1_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_5,
      S(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_6,
      S(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_7,
      S(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_8
    );
\relop_relop1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \relop_relop1_carry__0_n_0\,
      CO(3) => \NLW_relop_relop1_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \^co\(0),
      CO(1) => \relop_relop1_carry__1_n_2\,
      CO(0) => \relop_relop1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_relop_relop1_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_9,
      S(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_10,
      S(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_11
    );
\tlast_counter_out[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(0),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_3_n_0\
    );
\tlast_counter_out[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(3),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_4_n_0\
    );
\tlast_counter_out[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(2),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_5_n_0\
    );
\tlast_counter_out[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(1),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_6_n_0\
    );
\tlast_counter_out[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1555"
    )
        port map (
      I0 => tlast_counter_out_reg(0),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_7_n_0\
    );
\tlast_counter_out[12]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(15),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_2_n_0\
    );
\tlast_counter_out[12]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(14),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_3_n_0\
    );
\tlast_counter_out[12]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(13),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_4_n_0\
    );
\tlast_counter_out[12]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(12),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_5_n_0\
    );
\tlast_counter_out[16]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(19),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_2_n_0\
    );
\tlast_counter_out[16]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(18),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_3_n_0\
    );
\tlast_counter_out[16]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(17),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_4_n_0\
    );
\tlast_counter_out[16]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(16),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_5_n_0\
    );
\tlast_counter_out[20]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(23),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_2_n_0\
    );
\tlast_counter_out[20]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(22),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_3_n_0\
    );
\tlast_counter_out[20]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(21),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_4_n_0\
    );
\tlast_counter_out[20]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(20),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_5_n_0\
    );
\tlast_counter_out[24]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(27),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_2_n_0\
    );
\tlast_counter_out[24]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(26),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_3_n_0\
    );
\tlast_counter_out[24]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(25),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_4_n_0\
    );
\tlast_counter_out[24]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(24),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_5_n_0\
    );
\tlast_counter_out[28]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(31),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_2_n_0\
    );
\tlast_counter_out[28]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(30),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_3_n_0\
    );
\tlast_counter_out[28]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(29),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_4_n_0\
    );
\tlast_counter_out[28]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(28),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_5_n_0\
    );
\tlast_counter_out[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(7),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_2_n_0\
    );
\tlast_counter_out[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(6),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_3_n_0\
    );
\tlast_counter_out[4]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(5),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_4_n_0\
    );
\tlast_counter_out[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(4),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_5_n_0\
    );
\tlast_counter_out[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(11),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_2_n_0\
    );
\tlast_counter_out[8]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(10),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_3_n_0\
    );
\tlast_counter_out[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(9),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_4_n_0\
    );
\tlast_counter_out[8]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(8),
      I1 => \^co\(0),
      I2 => auto_ready_dut_enb,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_5_n_0\
    );
\tlast_counter_out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[0]_i_2_n_7\,
      Q => tlast_counter_out_reg(0)
    );
\tlast_counter_out_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tlast_counter_out_reg[0]_i_2_n_0\,
      CO(2) => \tlast_counter_out_reg[0]_i_2_n_1\,
      CO(1) => \tlast_counter_out_reg[0]_i_2_n_2\,
      CO(0) => \tlast_counter_out_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tlast_counter_out[0]_i_3_n_0\,
      O(3) => \tlast_counter_out_reg[0]_i_2_n_4\,
      O(2) => \tlast_counter_out_reg[0]_i_2_n_5\,
      O(1) => \tlast_counter_out_reg[0]_i_2_n_6\,
      O(0) => \tlast_counter_out_reg[0]_i_2_n_7\,
      S(3) => \tlast_counter_out[0]_i_4_n_0\,
      S(2) => \tlast_counter_out[0]_i_5_n_0\,
      S(1) => \tlast_counter_out[0]_i_6_n_0\,
      S(0) => \tlast_counter_out[0]_i_7_n_0\
    );
\tlast_counter_out_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[8]_i_1_n_5\,
      Q => tlast_counter_out_reg(10)
    );
\tlast_counter_out_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[8]_i_1_n_4\,
      Q => tlast_counter_out_reg(11)
    );
\tlast_counter_out_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[12]_i_1_n_7\,
      Q => tlast_counter_out_reg(12)
    );
\tlast_counter_out_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[8]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[12]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[12]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[12]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[12]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[12]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[12]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[12]_i_1_n_7\,
      S(3) => \tlast_counter_out[12]_i_2_n_0\,
      S(2) => \tlast_counter_out[12]_i_3_n_0\,
      S(1) => \tlast_counter_out[12]_i_4_n_0\,
      S(0) => \tlast_counter_out[12]_i_5_n_0\
    );
\tlast_counter_out_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[12]_i_1_n_6\,
      Q => tlast_counter_out_reg(13)
    );
\tlast_counter_out_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[12]_i_1_n_5\,
      Q => tlast_counter_out_reg(14)
    );
\tlast_counter_out_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[12]_i_1_n_4\,
      Q => tlast_counter_out_reg(15)
    );
\tlast_counter_out_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[16]_i_1_n_7\,
      Q => tlast_counter_out_reg(16)
    );
\tlast_counter_out_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[12]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[16]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[16]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[16]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[16]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[16]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[16]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[16]_i_1_n_7\,
      S(3) => \tlast_counter_out[16]_i_2_n_0\,
      S(2) => \tlast_counter_out[16]_i_3_n_0\,
      S(1) => \tlast_counter_out[16]_i_4_n_0\,
      S(0) => \tlast_counter_out[16]_i_5_n_0\
    );
\tlast_counter_out_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[16]_i_1_n_6\,
      Q => tlast_counter_out_reg(17)
    );
\tlast_counter_out_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[16]_i_1_n_5\,
      Q => tlast_counter_out_reg(18)
    );
\tlast_counter_out_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[16]_i_1_n_4\,
      Q => tlast_counter_out_reg(19)
    );
\tlast_counter_out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[0]_i_2_n_6\,
      Q => tlast_counter_out_reg(1)
    );
\tlast_counter_out_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[20]_i_1_n_7\,
      Q => tlast_counter_out_reg(20)
    );
\tlast_counter_out_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[16]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[20]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[20]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[20]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[20]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[20]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[20]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[20]_i_1_n_7\,
      S(3) => \tlast_counter_out[20]_i_2_n_0\,
      S(2) => \tlast_counter_out[20]_i_3_n_0\,
      S(1) => \tlast_counter_out[20]_i_4_n_0\,
      S(0) => \tlast_counter_out[20]_i_5_n_0\
    );
\tlast_counter_out_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[20]_i_1_n_6\,
      Q => tlast_counter_out_reg(21)
    );
\tlast_counter_out_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[20]_i_1_n_5\,
      Q => tlast_counter_out_reg(22)
    );
\tlast_counter_out_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[20]_i_1_n_4\,
      Q => tlast_counter_out_reg(23)
    );
\tlast_counter_out_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[24]_i_1_n_7\,
      Q => tlast_counter_out_reg(24)
    );
\tlast_counter_out_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[20]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[24]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[24]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[24]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[24]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[24]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[24]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[24]_i_1_n_7\,
      S(3) => \tlast_counter_out[24]_i_2_n_0\,
      S(2) => \tlast_counter_out[24]_i_3_n_0\,
      S(1) => \tlast_counter_out[24]_i_4_n_0\,
      S(0) => \tlast_counter_out[24]_i_5_n_0\
    );
\tlast_counter_out_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[24]_i_1_n_6\,
      Q => tlast_counter_out_reg(25)
    );
\tlast_counter_out_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[24]_i_1_n_5\,
      Q => tlast_counter_out_reg(26)
    );
\tlast_counter_out_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[24]_i_1_n_4\,
      Q => tlast_counter_out_reg(27)
    );
\tlast_counter_out_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[28]_i_1_n_7\,
      Q => tlast_counter_out_reg(28)
    );
\tlast_counter_out_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[24]_i_1_n_0\,
      CO(3) => \NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \tlast_counter_out_reg[28]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[28]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[28]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[28]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[28]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[28]_i_1_n_7\,
      S(3) => \tlast_counter_out[28]_i_2_n_0\,
      S(2) => \tlast_counter_out[28]_i_3_n_0\,
      S(1) => \tlast_counter_out[28]_i_4_n_0\,
      S(0) => \tlast_counter_out[28]_i_5_n_0\
    );
\tlast_counter_out_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[28]_i_1_n_6\,
      Q => tlast_counter_out_reg(29)
    );
\tlast_counter_out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[0]_i_2_n_5\,
      Q => tlast_counter_out_reg(2)
    );
\tlast_counter_out_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[28]_i_1_n_5\,
      Q => tlast_counter_out_reg(30)
    );
\tlast_counter_out_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[28]_i_1_n_4\,
      Q => tlast_counter_out_reg(31)
    );
\tlast_counter_out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[0]_i_2_n_4\,
      Q => tlast_counter_out_reg(3)
    );
\tlast_counter_out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[4]_i_1_n_7\,
      Q => tlast_counter_out_reg(4)
    );
\tlast_counter_out_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[0]_i_2_n_0\,
      CO(3) => \tlast_counter_out_reg[4]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[4]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[4]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[4]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[4]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[4]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[4]_i_1_n_7\,
      S(3) => \tlast_counter_out[4]_i_2_n_0\,
      S(2) => \tlast_counter_out[4]_i_3_n_0\,
      S(1) => \tlast_counter_out[4]_i_4_n_0\,
      S(0) => \tlast_counter_out[4]_i_5_n_0\
    );
\tlast_counter_out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[4]_i_1_n_6\,
      Q => tlast_counter_out_reg(5)
    );
\tlast_counter_out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[4]_i_1_n_5\,
      Q => tlast_counter_out_reg(6)
    );
\tlast_counter_out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[4]_i_1_n_4\,
      Q => tlast_counter_out_reg(7)
    );
\tlast_counter_out_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[8]_i_1_n_7\,
      Q => tlast_counter_out_reg(8)
    );
\tlast_counter_out_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[4]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[8]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[8]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[8]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[8]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[8]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[8]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[8]_i_1_n_7\,
      S(3) => \tlast_counter_out[8]_i_2_n_0\,
      S(2) => \tlast_counter_out[8]_i_3_n_0\,
      S(1) => \tlast_counter_out[8]_i_4_n_0\,
      S(0) => \tlast_counter_out[8]_i_5_n_0\
    );
\tlast_counter_out_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => reset,
      D => \tlast_counter_out_reg[8]_i_1_n_6\,
      Q => tlast_counter_out_reg(9)
    );
tlast_size_value_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => tlast_size_value_carry_n_0,
      CO(2) => tlast_size_value_carry_n_1,
      CO(1) => tlast_size_value_carry_n_2,
      CO(0) => tlast_size_value_carry_n_3,
      CYINIT => Q(0),
      DI(3 downto 0) => Q(4 downto 1),
      O(3 downto 0) => tlast_size_value(4 downto 1),
      S(3 downto 0) => S(3 downto 0)
    );
\tlast_size_value_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => tlast_size_value_carry_n_0,
      CO(3) => \tlast_size_value_carry__0_n_0\,
      CO(2) => \tlast_size_value_carry__0_n_1\,
      CO(1) => \tlast_size_value_carry__0_n_2\,
      CO(0) => \tlast_size_value_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(8 downto 5),
      O(3 downto 0) => tlast_size_value(8 downto 5),
      S(3 downto 0) => relop_relop1_carry_i_3(3 downto 0)
    );
\tlast_size_value_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__0_n_0\,
      CO(3) => \tlast_size_value_carry__1_n_0\,
      CO(2) => \tlast_size_value_carry__1_n_1\,
      CO(1) => \tlast_size_value_carry__1_n_2\,
      CO(0) => \tlast_size_value_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(12 downto 9),
      O(3 downto 0) => tlast_size_value(12 downto 9),
      S(3 downto 0) => relop_relop1_carry_i_1(3 downto 0)
    );
\tlast_size_value_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__1_n_0\,
      CO(3) => \tlast_size_value_carry__2_n_0\,
      CO(2) => \tlast_size_value_carry__2_n_1\,
      CO(1) => \tlast_size_value_carry__2_n_2\,
      CO(0) => \tlast_size_value_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(16 downto 13),
      O(3 downto 0) => tlast_size_value(16 downto 13),
      S(3 downto 0) => \relop_relop1_carry__0_i_4\(3 downto 0)
    );
\tlast_size_value_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__2_n_0\,
      CO(3) => \tlast_size_value_carry__3_n_0\,
      CO(2) => \tlast_size_value_carry__3_n_1\,
      CO(1) => \tlast_size_value_carry__3_n_2\,
      CO(0) => \tlast_size_value_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(20 downto 17),
      O(3 downto 0) => tlast_size_value(20 downto 17),
      S(3 downto 0) => \relop_relop1_carry__0_i_3\(3 downto 0)
    );
\tlast_size_value_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__3_n_0\,
      CO(3) => \tlast_size_value_carry__4_n_0\,
      CO(2) => \tlast_size_value_carry__4_n_1\,
      CO(1) => \tlast_size_value_carry__4_n_2\,
      CO(0) => \tlast_size_value_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(24 downto 21),
      O(3 downto 0) => tlast_size_value(24 downto 21),
      S(3 downto 0) => \relop_relop1_carry__0_i_1\(3 downto 0)
    );
\tlast_size_value_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__4_n_0\,
      CO(3) => \tlast_size_value_carry__5_n_0\,
      CO(2) => \tlast_size_value_carry__5_n_1\,
      CO(1) => \tlast_size_value_carry__5_n_2\,
      CO(0) => \tlast_size_value_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(28 downto 25),
      O(3 downto 0) => tlast_size_value(28 downto 25),
      S(3 downto 0) => \relop_relop1_carry__1_i_3\(3 downto 0)
    );
\tlast_size_value_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__5_n_0\,
      CO(3 downto 2) => \NLW_tlast_size_value_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \tlast_size_value_carry__6_n_2\,
      CO(0) => \tlast_size_value_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => Q(30 downto 29),
      O(3) => \NLW_tlast_size_value_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => tlast_size_value(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => \relop_relop1_carry__1_i_2\(2 downto 0)
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT
     port map (
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      IPCORE_CLK => IPCORE_CLK,
      Q(0) => Q(0),
      S(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_1,
      S(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_2,
      S(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_3,
      S(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_4,
      auto_ready_dut_enb => auto_ready_dut_enb,
      auto_tlast => auto_tlast,
      out_valid => out_valid,
      reset => reset,
      tlast_counter_out_reg(31 downto 0) => tlast_counter_out_reg(31 downto 0),
      \tlast_counter_out_reg[21]\(3) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_5,
      \tlast_counter_out_reg[21]\(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_6,
      \tlast_counter_out_reg[21]\(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_7,
      \tlast_counter_out_reg[21]\(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_8,
      \tlast_counter_out_reg[30]\(2) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_9,
      \tlast_counter_out_reg[30]\(1) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_10,
      \tlast_counter_out_reg[30]\(0) => u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst_n_11,
      tlast_size_value(30 downto 0) => tlast_size_value(31 downto 1)
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT
     port map (
      AR(0) => AR(0),
      AXI4_Stream_Master_TDATA(15 downto 0) => AXI4_Stream_Master_TDATA(15 downto 0),
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      IPCORE_CLK => IPCORE_CLK,
      auto_ready_axi4_stream_master => auto_ready_axi4_stream_master,
      auto_ready_dut_enb => auto_ready_dut_enb,
      out_valid => out_valid,
      out_valid_reg_0 => out_valid_reg,
      p26y_out_add_temp(13 downto 0) => p26y_out_add_temp(13 downto 0),
      reset => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave is
  port (
    out_valid : out STD_LOGIC;
    auto_ready_dut_enb : out STD_LOGIC;
    auto_tlast : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    out_valid_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 13 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    reset : in STD_LOGIC;
    auto_ready_axi4_stream_master : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AR : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave is
  signal \^auto_ready_dut_enb\ : STD_LOGIC;
begin
  auto_ready_dut_enb <= \^auto_ready_dut_enb\;
fifo_rd_ack_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => reset,
      D => auto_ready_axi4_stream_master,
      Q => \^auto_ready_dut_enb\
    );
u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_fifo_data
     port map (
      AR(0) => AR(0),
      AXI4_Stream_Slave_TDATA(15 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      CO(0) => CO(0),
      IPCORE_CLK => IPCORE_CLK,
      Q(13 downto 0) => Q(13 downto 0),
      auto_tlast => auto_tlast,
      fifo_valid_reg_0 => \^auto_ready_dut_enb\,
      out_valid_reg_0 => out_valid,
      out_valid_reg_1 => out_valid_reg,
      reset => reset
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore is
  port (
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_RVALID : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_BVALID : out STD_LOGIC;
    AXI4_Lite_WREADY : out STD_LOGIC;
    out_valid_reg : out STD_LOGIC;
    delayed_xout : out STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore is
  signal Out_1 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal auto_ready_axi4_stream_master : STD_LOGIC;
  signal auto_ready_dut_enb : STD_LOGIC;
  signal auto_tlast : STD_LOGIC;
  signal dut_enable : STD_LOGIC;
  signal relop_relop1 : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst_n_4 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_1 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_41 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_42 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_43 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_44 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_45 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_46 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_47 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_48 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_49 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_50 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_51 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_52 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_53 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_54 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_55 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_56 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_57 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_58 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_59 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_60 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_61 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_62 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_63 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_64 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_65 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_66 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_67 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_68 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_7 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_8 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_9 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_15 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_16 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_17 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_18 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_19 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_20 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_21 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_22 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_23 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_24 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_25 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_26 : STD_LOGIC;
  signal u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_27 : STD_LOGIC;
  signal \u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst/out_valid\ : STD_LOGIC;
  signal wr_din0 : STD_LOGIC;
  signal write_reg_h_in1 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal write_reg_h_in2 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal write_reg_h_in3 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal write_reg_h_in4 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal write_reg_packet_size_axi4_stream_master : STD_LOGIC_VECTOR ( 30 downto 0 );
begin
u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master
     port map (
      AR(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_1,
      AXI4_Stream_Master_TDATA(15 downto 0) => AXI4_Stream_Master_TDATA(15 downto 0),
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      CO(0) => relop_relop1,
      IPCORE_CLK => IPCORE_CLK,
      Q(30 downto 0) => write_reg_packet_size_axi4_stream_master(30 downto 0),
      S(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_65,
      S(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_66,
      S(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_67,
      S(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_68,
      auto_ready_axi4_stream_master => auto_ready_axi4_stream_master,
      auto_ready_dut_enb => auto_ready_dut_enb,
      auto_tlast => auto_tlast,
      out_valid => \u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst/out_valid\,
      out_valid_reg => out_valid_reg,
      p26y_out_add_temp(13) => wr_din0,
      p26y_out_add_temp(12) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_15,
      p26y_out_add_temp(11) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_16,
      p26y_out_add_temp(10) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_17,
      p26y_out_add_temp(9) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_18,
      p26y_out_add_temp(8) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_19,
      p26y_out_add_temp(7) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_20,
      p26y_out_add_temp(6) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_21,
      p26y_out_add_temp(5) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_22,
      p26y_out_add_temp(4) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_23,
      p26y_out_add_temp(3) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_24,
      p26y_out_add_temp(2) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_25,
      p26y_out_add_temp(1) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_26,
      p26y_out_add_temp(0) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_27,
      \relop_relop1_carry__0_i_1\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_45,
      \relop_relop1_carry__0_i_1\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_46,
      \relop_relop1_carry__0_i_1\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_47,
      \relop_relop1_carry__0_i_1\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_48,
      \relop_relop1_carry__0_i_3\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_49,
      \relop_relop1_carry__0_i_3\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_50,
      \relop_relop1_carry__0_i_3\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_51,
      \relop_relop1_carry__0_i_3\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_52,
      \relop_relop1_carry__0_i_4\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_53,
      \relop_relop1_carry__0_i_4\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_54,
      \relop_relop1_carry__0_i_4\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_55,
      \relop_relop1_carry__0_i_4\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_56,
      \relop_relop1_carry__1_i_2\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_7,
      \relop_relop1_carry__1_i_2\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_8,
      \relop_relop1_carry__1_i_2\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_9,
      \relop_relop1_carry__1_i_3\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_41,
      \relop_relop1_carry__1_i_3\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_42,
      \relop_relop1_carry__1_i_3\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_43,
      \relop_relop1_carry__1_i_3\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_44,
      relop_relop1_carry_i_1(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_57,
      relop_relop1_carry_i_1(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_58,
      relop_relop1_carry_i_1(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_59,
      relop_relop1_carry_i_1(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_60,
      relop_relop1_carry_i_3(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_61,
      relop_relop1_carry_i_3(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_62,
      relop_relop1_carry_i_3(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_63,
      relop_relop1_carry_i_3(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_64,
      reset => reset,
      \tlast_counter_out_reg[31]_0\ => u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst_n_4
    );
u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave
     port map (
      AR(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69,
      AXI4_Stream_Slave_TDATA(15 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      CO(0) => relop_relop1,
      IPCORE_CLK => IPCORE_CLK,
      Q(13 downto 0) => Out_1(13 downto 0),
      auto_ready_axi4_stream_master => auto_ready_axi4_stream_master,
      auto_ready_dut_enb => auto_ready_dut_enb,
      auto_tlast => auto_tlast,
      out_valid => \u_mlhdlc_sfir_fixpt_ipcore_fifo_data_inst/out_valid\,
      out_valid_reg => u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst_n_4,
      reset => reset
    );
u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_axi_lite
     port map (
      AR(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_1,
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(13 downto 0),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARESETN_0(0) => reset,
      AXI4_Lite_ARESETN_1(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(13 downto 0),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_RDATA(0) => AXI4_Lite_RDATA(0),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_WDATA(31 downto 0) => AXI4_Lite_WDATA(31 downto 0),
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      E(0) => dut_enable,
      \FSM_onehot_axi_lite_wstate_reg[2]\(1) => AXI4_Lite_BVALID,
      \FSM_onehot_axi_lite_wstate_reg[2]\(0) => AXI4_Lite_WREADY,
      FSM_sequential_axi_lite_rstate_reg => AXI4_Lite_RVALID,
      IPCORE_RESETN => IPCORE_RESETN,
      Q(30 downto 0) => write_reg_packet_size_axi4_stream_master(30 downto 0),
      S(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_65,
      S(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_66,
      S(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_67,
      S(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_68,
      auto_ready_dut_enb => auto_ready_dut_enb,
      \write_reg_h_in1_reg[13]\(13 downto 0) => write_reg_h_in1(13 downto 0),
      \write_reg_h_in2_reg[13]\(13 downto 0) => write_reg_h_in2(13 downto 0),
      \write_reg_h_in3_reg[13]\(13 downto 0) => write_reg_h_in3(13 downto 0),
      \write_reg_h_in4_reg[13]\(13 downto 0) => write_reg_h_in4(13 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[12]\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_57,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_58,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_59,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_60,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_53,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_54,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_55,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_56,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_49,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_50,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_51,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_52,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_45,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_46,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_47,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_48,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_41,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_42,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_43,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_44,
      \write_reg_packet_size_axi4_stream_master_reg[31]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_7,
      \write_reg_packet_size_axi4_stream_master_reg[31]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_8,
      \write_reg_packet_size_axi4_stream_master_reg[31]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_9,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(3) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_61,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(2) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_62,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(1) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_63,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_64
    );
u_mlhdlc_sfir_fixpt_ipcore_dut_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore_dut
     port map (
      AR(0) => u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst_n_69,
      D(13 downto 0) => Out_1(13 downto 0),
      E(0) => dut_enable,
      IPCORE_CLK => IPCORE_CLK,
      delayed_xout(13 downto 0) => delayed_xout(13 downto 0),
      p20m1_mul_temp(13 downto 0) => write_reg_h_in1(13 downto 0),
      p21m2_mul_temp(13 downto 0) => write_reg_h_in2(13 downto 0),
      p22m3_mul_temp(13 downto 0) => write_reg_h_in3(13 downto 0),
      p23m4_mul_temp(13 downto 0) => write_reg_h_in4(13 downto 0),
      p26y_out_add_temp(13) => wr_din0,
      p26y_out_add_temp(12) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_15,
      p26y_out_add_temp(11) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_16,
      p26y_out_add_temp(10) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_17,
      p26y_out_add_temp(9) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_18,
      p26y_out_add_temp(8) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_19,
      p26y_out_add_temp(7) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_20,
      p26y_out_add_temp(6) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_21,
      p26y_out_add_temp(5) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_22,
      p26y_out_add_temp(4) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_23,
      p26y_out_add_temp(3) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_24,
      p26y_out_add_temp(2) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_25,
      p26y_out_add_temp(1) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_26,
      p26y_out_add_temp(0) => u_mlhdlc_sfir_fixpt_ipcore_dut_inst_n_27
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    IPCORE_CLK : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARESETN : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Stream_Master_TVALID : out STD_LOGIC;
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    delayed_xout : out STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_WREADY : out STD_LOGIC;
    AXI4_Lite_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_BVALID : out STD_LOGIC;
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_RVALID : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "System_mlhdlc_sfir_fixpt_ip_0_0,mlhdlc_sfir_fixpt_ipcore,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "mlhdlc_sfir_fixpt_ipcore,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^axi4_lite_rdata\ : STD_LOGIC_VECTOR ( 29 to 29 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of AXI4_Lite_ACLK : signal is "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of AXI4_Lite_ACLK : signal is "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI4_Lite_ARESETN : signal is "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST";
  attribute X_INTERFACE_PARAMETER of AXI4_Lite_ARESETN : signal is "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI4_Lite_ARREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY";
  attribute X_INTERFACE_INFO of AXI4_Lite_ARVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID";
  attribute X_INTERFACE_INFO of AXI4_Lite_AWREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY";
  attribute X_INTERFACE_INFO of AXI4_Lite_AWVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID";
  attribute X_INTERFACE_INFO of AXI4_Lite_BREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY";
  attribute X_INTERFACE_INFO of AXI4_Lite_BVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID";
  attribute X_INTERFACE_INFO of AXI4_Lite_RREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY";
  attribute X_INTERFACE_INFO of AXI4_Lite_RVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID";
  attribute X_INTERFACE_PARAMETER of AXI4_Lite_RVALID : signal is "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI4_Lite_WREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY";
  attribute X_INTERFACE_INFO of AXI4_Lite_WVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID";
  attribute X_INTERFACE_INFO of AXI4_Stream_Master_TLAST : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TLAST";
  attribute X_INTERFACE_PARAMETER of AXI4_Stream_Master_TLAST : signal is "XIL_INTERFACENAME AXI4_Stream_Master, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI4_Stream_Master_TREADY : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TREADY";
  attribute X_INTERFACE_INFO of AXI4_Stream_Master_TVALID : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TVALID";
  attribute X_INTERFACE_INFO of AXI4_Stream_Slave_TREADY : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TREADY";
  attribute X_INTERFACE_PARAMETER of AXI4_Stream_Slave_TREADY : signal is "XIL_INTERFACENAME AXI4_Stream_Slave, TDATA_NUM_BYTES 2, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI4_Stream_Slave_TVALID : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TVALID";
  attribute X_INTERFACE_INFO of IPCORE_CLK : signal is "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK";
  attribute X_INTERFACE_PARAMETER of IPCORE_CLK : signal is "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, ASSOCIATED_BUSIF AXI4_Stream_Master:AXI4_Stream_Slave, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of IPCORE_RESETN : signal is "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST";
  attribute X_INTERFACE_PARAMETER of IPCORE_RESETN : signal is "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of AXI4_Lite_ARADDR : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR";
  attribute X_INTERFACE_INFO of AXI4_Lite_AWADDR : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR";
  attribute X_INTERFACE_INFO of AXI4_Lite_BRESP : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP";
  attribute X_INTERFACE_INFO of AXI4_Lite_RDATA : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA";
  attribute X_INTERFACE_INFO of AXI4_Lite_RRESP : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP";
  attribute X_INTERFACE_INFO of AXI4_Lite_WDATA : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA";
  attribute X_INTERFACE_INFO of AXI4_Lite_WSTRB : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB";
  attribute X_INTERFACE_INFO of AXI4_Stream_Master_TDATA : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TDATA";
  attribute X_INTERFACE_INFO of AXI4_Stream_Slave_TDATA : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TDATA";
begin
  AXI4_Lite_BRESP(1) <= \<const0>\;
  AXI4_Lite_BRESP(0) <= \<const0>\;
  AXI4_Lite_RDATA(31) <= \<const0>\;
  AXI4_Lite_RDATA(30) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(29) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(28) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(27) <= \<const0>\;
  AXI4_Lite_RDATA(26) <= \<const0>\;
  AXI4_Lite_RDATA(25) <= \<const0>\;
  AXI4_Lite_RDATA(24) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(23) <= \<const0>\;
  AXI4_Lite_RDATA(22) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(21) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(20) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(19) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(18) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(17) <= \<const0>\;
  AXI4_Lite_RDATA(16) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(15) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(14) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(13) <= \<const0>\;
  AXI4_Lite_RDATA(12) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(11) <= \<const0>\;
  AXI4_Lite_RDATA(10) <= \<const0>\;
  AXI4_Lite_RDATA(9) <= \<const0>\;
  AXI4_Lite_RDATA(8) <= \<const0>\;
  AXI4_Lite_RDATA(7) <= \<const0>\;
  AXI4_Lite_RDATA(6) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(5) <= \<const0>\;
  AXI4_Lite_RDATA(4) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(3) <= \<const0>\;
  AXI4_Lite_RDATA(2) <= \<const0>\;
  AXI4_Lite_RDATA(1) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(0) <= \<const0>\;
  AXI4_Lite_RRESP(1) <= \<const0>\;
  AXI4_Lite_RRESP(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_mlhdlc_sfir_fixpt_ipcore
     port map (
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(15 downto 2),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(15 downto 2),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_BVALID => AXI4_Lite_BVALID,
      AXI4_Lite_RDATA(0) => \^axi4_lite_rdata\(29),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_RVALID => AXI4_Lite_RVALID,
      AXI4_Lite_WDATA(31 downto 0) => AXI4_Lite_WDATA(31 downto 0),
      AXI4_Lite_WREADY => AXI4_Lite_WREADY,
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      AXI4_Stream_Master_TDATA(15 downto 0) => AXI4_Stream_Master_TDATA(15 downto 0),
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      AXI4_Stream_Slave_TDATA(15 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      IPCORE_CLK => IPCORE_CLK,
      IPCORE_RESETN => IPCORE_RESETN,
      delayed_xout(13 downto 0) => delayed_xout(13 downto 0),
      out_valid_reg => AXI4_Stream_Master_TVALID
    );
end STRUCTURE;
