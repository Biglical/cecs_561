// -------------------------------------------------------------
// 
// File Name: C:\Users\biggg\AppData\Local\Temp\mlhdlc_sfir\codegen\mlhdlc_sfir\hdlsrc\mlhdlc_sfir_fixpt_ipcore_axi4_stream_master.v
// Created: 2019-04-07 10:19:07
// 
// Generated by MATLAB 9.5, MATLAB Coder 4.1 and HDL Coder 3.13
// 
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: mlhdlc_sfir_fixpt_ipcore_axi4_stream_master
// Source Path: mlhdlc_sfir_fixpt_ipcore/mlhdlc_sfir_fixpt_ipcore_axi4_stream_master
// Hierarchy Level: 1
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module mlhdlc_sfir_fixpt_ipcore_axi4_stream_master
          (clk,
           reset,
           enb,
           AXI4_Stream_Master_TREADY,
           user_data,
           user_valid,
           write_packet_size_axi4_stream_master,
           AXI4_Stream_Master_TDATA,
           AXI4_Stream_Master_TVALID,
           AXI4_Stream_Master_TLAST,
           auto_ready);


  input   clk;
  input   reset;
  input   enb;
  input   AXI4_Stream_Master_TREADY;  // ufix1
  input   [31:0] user_data;  // ufix32
  input   user_valid;  // ufix1
  input   [31:0] write_packet_size_axi4_stream_master;  // ufix32
  output  [31:0] AXI4_Stream_Master_TDATA;  // ufix32
  output  AXI4_Stream_Master_TVALID;  // ufix1
  output  AXI4_Stream_Master_TLAST;  // ufix1
  output  auto_ready;  // ufix1


  wire fifo_afull_data;  // ufix1
  wire internal_ready;  // ufix1
  reg  internal_ready_delayed;  // ufix1
  wire fifo_push;  // ufix1
  wire fifo_empty_data;  // ufix1
  wire [31:0] const_1;  // ufix32
  wire [31:0] tlast_size_value;  // ufix32
  wire auto_tlast;  // ufix1
  reg [31:0] tlast_counter_out;  // ufix32
  wire relop_relop1;


  assign internal_ready =  ~ fifo_afull_data;



  always @(posedge clk or posedge reset)
    begin : intdelay_process
      if (reset == 1'b1) begin
        internal_ready_delayed <= 1'b0;
      end
      else begin
        if (enb) begin
          internal_ready_delayed <= internal_ready;
        end
      end
    end



  assign fifo_push = internal_ready_delayed & user_valid;



  mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT u_mlhdlc_sfir_fixpt_ipcore_fifo_data_OUT_inst (.clk(clk),
                                                                                        .reset(reset),
                                                                                        .enb(enb),
                                                                                        .In(user_data),  // ufix32
                                                                                        .Push(fifo_push),  // ufix1
                                                                                        .Pop(AXI4_Stream_Master_TREADY),  // ufix1
                                                                                        .Out(AXI4_Stream_Master_TDATA),  // ufix32
                                                                                        .Empty(fifo_empty_data),  // ufix1
                                                                                        .AFull(fifo_afull_data)  // ufix1
                                                                                        );

  assign AXI4_Stream_Master_TVALID =  ~ fifo_empty_data;



  assign const_1 = 32'b00000000000000000000000000000001;



  assign tlast_size_value = write_packet_size_axi4_stream_master - const_1;



  // Free running, Unsigned Counter
  //  initial value   = 0
  //  step value      = 1
  always @(posedge clk or posedge reset)
    begin : tlast_counter_process
      if (reset == 1'b1) begin
        tlast_counter_out <= 32'b00000000000000000000000000000000;
      end
      else begin
        if (enb) begin
          if (auto_tlast == 1'b1) begin
            tlast_counter_out <= 32'b00000000000000000000000000000000;
          end
          else if (fifo_push == 1'b1) begin
            tlast_counter_out <= tlast_counter_out + 32'b00000000000000000000000000000001;
          end
        end
      end
    end



  assign relop_relop1 = tlast_counter_out == tlast_size_value;



  assign auto_tlast = fifo_push & relop_relop1;



  mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT u_mlhdlc_sfir_fixpt_ipcore_fifo_TLAST_OUT_inst (.clk(clk),
                                                                                          .reset(reset),
                                                                                          .enb(enb),
                                                                                          .In(auto_tlast),  // ufix1
                                                                                          .Push(fifo_push),  // ufix1
                                                                                          .Pop(AXI4_Stream_Master_TREADY),  // ufix1
                                                                                          .Out(AXI4_Stream_Master_TLAST)  // ufix1
                                                                                          );

  assign auto_ready = internal_ready;

endmodule  // mlhdlc_sfir_fixpt_ipcore_axi4_stream_master

