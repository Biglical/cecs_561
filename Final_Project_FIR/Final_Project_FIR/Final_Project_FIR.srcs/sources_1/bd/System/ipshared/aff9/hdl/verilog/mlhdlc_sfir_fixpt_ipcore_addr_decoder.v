// -------------------------------------------------------------
// 
// File Name: C:\Users\biggg\AppData\Local\Temp\mlhdlc_sfir\codegen\mlhdlc_sfir\hdlsrc\mlhdlc_sfir_fixpt_ipcore_addr_decoder.v
// Created: 2019-04-07 10:19:07
// 
// Generated by MATLAB 9.5, MATLAB Coder 4.1 and HDL Coder 3.13
// 
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: mlhdlc_sfir_fixpt_ipcore_addr_decoder
// Source Path: mlhdlc_sfir_fixpt_ipcore/mlhdlc_sfir_fixpt_ipcore_axi_lite/mlhdlc_sfir_fixpt_ipcore_addr_decoder
// Hierarchy Level: 2
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module mlhdlc_sfir_fixpt_ipcore_addr_decoder
          (clk,
           reset,
           data_write,
           addr_sel,
           wr_enb,
           rd_enb,
           read_ip_timestamp,
           data_read,
           write_axi_enable,
           write_packet_size_axi4_stream_master,
           write_h_in1,
           write_h_in2,
           write_h_in3,
           write_h_in4);


  input   clk;
  input   reset;
  input   [31:0] data_write;  // ufix32
  input   [13:0] addr_sel;  // ufix14
  input   wr_enb;  // ufix1
  input   rd_enb;  // ufix1
  input   [31:0] read_ip_timestamp;  // ufix32
  output  [31:0] data_read;  // ufix32
  output  write_axi_enable;  // ufix1
  output  [31:0] write_packet_size_axi4_stream_master;  // ufix32
  output  signed [13:0] write_h_in1;  // sfix14_En15
  output  signed [13:0] write_h_in2;  // sfix14_En16
  output  [13:0] write_h_in3;  // ufix14_En16
  output  [13:0] write_h_in4;  // ufix14_En15


  wire enb;
  wire decode_sel_ip_timestamp;  // ufix1
  wire const_1;  // ufix1
  wire [31:0] const_0;  // ufix32
  reg [31:0] read_reg_ip_timestamp;  // ufix32
  wire [31:0] decode_rd_ip_timestamp;  // ufix32
  wire decode_sel_axi_enable;  // ufix1
  wire reg_enb_axi_enable;  // ufix1
  wire data_in_axi_enable;  // ufix1
  reg  write_reg_axi_enable;  // ufix1
  wire decode_sel_packet_size_axi4_stream_master;  // ufix1
  wire reg_enb_packet_size_axi4_stream_master;  // ufix1
  reg [31:0] write_reg_packet_size_axi4_stream_master;  // ufix32
  wire decode_sel_h_in1;  // ufix1
  wire reg_enb_h_in1;  // ufix1
  wire signed [13:0] data_in_h_in1;  // sfix14_En15
  reg signed [13:0] write_reg_h_in1;  // sfix14_En15
  wire decode_sel_h_in2;  // ufix1
  wire reg_enb_h_in2;  // ufix1
  wire signed [13:0] data_in_h_in2;  // sfix14_En16
  reg signed [13:0] write_reg_h_in2;  // sfix14_En16
  wire decode_sel_h_in3;  // ufix1
  wire reg_enb_h_in3;  // ufix1
  wire [13:0] data_in_h_in3;  // ufix14_En16
  reg [13:0] write_reg_h_in3;  // ufix14_En16
  wire decode_sel_h_in4;  // ufix1
  wire reg_enb_h_in4;  // ufix1
  wire [13:0] data_in_h_in4;  // ufix14_En15
  reg [13:0] write_reg_h_in4;  // ufix14_En15


  assign decode_sel_ip_timestamp = addr_sel == 14'b00000000000011;



  assign const_1 = 1'b1;



  assign enb = const_1;

  assign const_0 = 32'b00000000000000000000000000000000;



  always @(posedge clk or posedge reset)
    begin : reg_ip_timestamp_process
      if (reset == 1'b1) begin
        read_reg_ip_timestamp <= 32'b00000000000000000000000000000000;
      end
      else begin
        if (enb) begin
          read_reg_ip_timestamp <= read_ip_timestamp;
        end
      end
    end



  assign decode_rd_ip_timestamp = (decode_sel_ip_timestamp == 1'b0 ? const_0 :
              read_reg_ip_timestamp);



  assign data_read = decode_rd_ip_timestamp;

  assign decode_sel_axi_enable = addr_sel == 14'b00000000000001;



  assign reg_enb_axi_enable = decode_sel_axi_enable & wr_enb;



  assign data_in_axi_enable = data_write[0];



  always @(posedge clk or posedge reset)
    begin : reg_axi_enable_process
      if (reset == 1'b1) begin
        write_reg_axi_enable <= 1'b1;
      end
      else begin
        if (enb && reg_enb_axi_enable) begin
          write_reg_axi_enable <= data_in_axi_enable;
        end
      end
    end



  assign write_axi_enable = write_reg_axi_enable;

  assign decode_sel_packet_size_axi4_stream_master = addr_sel == 14'b00000000000010;



  assign reg_enb_packet_size_axi4_stream_master = decode_sel_packet_size_axi4_stream_master & wr_enb;



  always @(posedge clk or posedge reset)
    begin : reg_packet_size_axi4_stream_master_process
      if (reset == 1'b1) begin
        write_reg_packet_size_axi4_stream_master <= 32'b00000000000000000000010000000000;
      end
      else begin
        if (enb && reg_enb_packet_size_axi4_stream_master) begin
          write_reg_packet_size_axi4_stream_master <= data_write;
        end
      end
    end



  assign write_packet_size_axi4_stream_master = write_reg_packet_size_axi4_stream_master;

  assign decode_sel_h_in1 = addr_sel == 14'b00000001000000;



  assign reg_enb_h_in1 = decode_sel_h_in1 & wr_enb;



  assign data_in_h_in1 = $signed(data_write[13:0]);



  always @(posedge clk or posedge reset)
    begin : reg_h_in1_process
      if (reset == 1'b1) begin
        write_reg_h_in1 <= 14'sb00000000000000;
      end
      else begin
        if (enb && reg_enb_h_in1) begin
          write_reg_h_in1 <= data_in_h_in1;
        end
      end
    end



  assign write_h_in1 = write_reg_h_in1;

  assign decode_sel_h_in2 = addr_sel == 14'b00000001000001;



  assign reg_enb_h_in2 = decode_sel_h_in2 & wr_enb;



  assign data_in_h_in2 = $signed(data_write[13:0]);



  always @(posedge clk or posedge reset)
    begin : reg_h_in2_process
      if (reset == 1'b1) begin
        write_reg_h_in2 <= 14'sb00000000000000;
      end
      else begin
        if (enb && reg_enb_h_in2) begin
          write_reg_h_in2 <= data_in_h_in2;
        end
      end
    end



  assign write_h_in2 = write_reg_h_in2;

  assign decode_sel_h_in3 = addr_sel == 14'b00000001000010;



  assign reg_enb_h_in3 = decode_sel_h_in3 & wr_enb;



  assign data_in_h_in3 = data_write[13:0];



  always @(posedge clk or posedge reset)
    begin : reg_h_in3_process
      if (reset == 1'b1) begin
        write_reg_h_in3 <= 14'b00000000000000;
      end
      else begin
        if (enb && reg_enb_h_in3) begin
          write_reg_h_in3 <= data_in_h_in3;
        end
      end
    end



  assign write_h_in3 = write_reg_h_in3;

  assign decode_sel_h_in4 = addr_sel == 14'b00000001000011;



  assign reg_enb_h_in4 = decode_sel_h_in4 & wr_enb;



  assign data_in_h_in4 = data_write[13:0];



  always @(posedge clk or posedge reset)
    begin : reg_h_in4_process
      if (reset == 1'b1) begin
        write_reg_h_in4 <= 14'b00000000000000;
      end
      else begin
        if (enb && reg_enb_h_in4) begin
          write_reg_h_in4 <= data_in_h_in4;
        end
      end
    end



  assign write_h_in4 = write_reg_h_in4;

endmodule  // mlhdlc_sfir_fixpt_ipcore_addr_decoder

