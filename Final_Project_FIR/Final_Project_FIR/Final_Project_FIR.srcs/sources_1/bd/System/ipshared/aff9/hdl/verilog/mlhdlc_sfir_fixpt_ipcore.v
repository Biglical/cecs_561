// -------------------------------------------------------------
// 
// File Name: C:\Users\biggg\AppData\Local\Temp\mlhdlc_sfir\codegen\mlhdlc_sfir\hdlsrc\mlhdlc_sfir_fixpt_ipcore.v
// Created: 2019-04-07 10:19:08
// 
// Generated by MATLAB 9.5, MATLAB Coder 4.1 and HDL Coder 3.13
// 
// 
// 
// -- -------------------------------------------------------------
// -- Rate and Clocking Details
// -- -------------------------------------------------------------
// Model base rate: -1
// Target subsystem base rate: -1
// 
// -------------------------------------------------------------


// -------------------------------------------------------------
// 
// Module: mlhdlc_sfir_fixpt_ipcore
// Source Path: mlhdlc_sfir_fixpt_ipcore
// Hierarchy Level: 0
// 
// -------------------------------------------------------------

`timescale 1 ns / 1 ns

module mlhdlc_sfir_fixpt_ipcore
          (IPCORE_CLK,
           IPCORE_RESETN,
           AXI4_Stream_Master_TREADY,
           AXI4_Stream_Slave_TDATA,
           AXI4_Stream_Slave_TVALID,
           AXI4_Lite_ACLK,
           AXI4_Lite_ARESETN,
           AXI4_Lite_AWADDR,
           AXI4_Lite_AWVALID,
           AXI4_Lite_WDATA,
           AXI4_Lite_WSTRB,
           AXI4_Lite_WVALID,
           AXI4_Lite_BREADY,
           AXI4_Lite_ARADDR,
           AXI4_Lite_ARVALID,
           AXI4_Lite_RREADY,
           AXI4_Stream_Master_TDATA,
           AXI4_Stream_Master_TVALID,
           AXI4_Stream_Master_TLAST,
           AXI4_Stream_Slave_TREADY,
           delayed_xout,
           AXI4_Lite_AWREADY,
           AXI4_Lite_WREADY,
           AXI4_Lite_BRESP,
           AXI4_Lite_BVALID,
           AXI4_Lite_ARREADY,
           AXI4_Lite_RDATA,
           AXI4_Lite_RRESP,
           AXI4_Lite_RVALID);


  input   IPCORE_CLK;  // ufix1
  input   IPCORE_RESETN;  // ufix1
  input   AXI4_Stream_Master_TREADY;  // ufix1
  input   [31:0] AXI4_Stream_Slave_TDATA;  // ufix32
  input   AXI4_Stream_Slave_TVALID;  // ufix1
  input   AXI4_Lite_ACLK;  // ufix1
  input   AXI4_Lite_ARESETN;  // ufix1
  input   [15:0] AXI4_Lite_AWADDR;  // ufix16
  input   AXI4_Lite_AWVALID;  // ufix1
  input   [31:0] AXI4_Lite_WDATA;  // ufix32
  input   [3:0] AXI4_Lite_WSTRB;  // ufix4
  input   AXI4_Lite_WVALID;  // ufix1
  input   AXI4_Lite_BREADY;  // ufix1
  input   [15:0] AXI4_Lite_ARADDR;  // ufix16
  input   AXI4_Lite_ARVALID;  // ufix1
  input   AXI4_Lite_RREADY;  // ufix1
  output  [31:0] AXI4_Stream_Master_TDATA;  // ufix32
  output  AXI4_Stream_Master_TVALID;  // ufix1
  output  AXI4_Stream_Master_TLAST;  // ufix1
  output  AXI4_Stream_Slave_TREADY;  // ufix1
  output  [31:0] delayed_xout;  // ufix32
  output  AXI4_Lite_AWREADY;  // ufix1
  output  AXI4_Lite_WREADY;  // ufix1
  output  [1:0] AXI4_Lite_BRESP;  // ufix2
  output  AXI4_Lite_BVALID;  // ufix1
  output  AXI4_Lite_ARREADY;  // ufix1
  output  [31:0] AXI4_Lite_RDATA;  // ufix32
  output  [1:0] AXI4_Lite_RRESP;  // ufix2
  output  AXI4_Lite_RVALID;  // ufix1


  wire reset;
  wire enb;
  wire [31:0] ip_timestamp;  // ufix32
  wire reset_cm;  // ufix1
  wire reset_internal;  // ufix1
  wire write_axi_enable;  // ufix1
  wire [31:0] write_packet_size_axi4_stream_master;  // ufix32
  wire signed [13:0] write_h_in1;  // sfix14_En15
  wire signed [13:0] write_h_in2;  // sfix14_En16
  wire [13:0] write_h_in3;  // ufix14_En16
  wire [13:0] write_h_in4;  // ufix14_En15
  wire const_1;  // ufix1
  wire Valid_out_sig;  // ufix1
  wire top_user_valid;  // ufix1
  wire top_user_valid_1;  // ufix1
  wire Valid_in_sig;  // ufix1
  wire auto_ready_axi4_stream_master;  // ufix1
  wire [31:0] top_user_data;  // ufix32
  wire signed [31:0] x_in_sig;  // sfix32_En12
  reg  auto_ready_dut_enb;  // ufix1
  wire dut_enable;  // ufix1
  wire ce_out_sig;  // ufix1
  wire signed [31:0] y_out_sig;  // sfix32_En12
  wire signed [31:0] delayed_xout_sig;  // sfix32_En12
  wire [31:0] top_user_data_1;  // ufix32


  assign ip_timestamp = 32'b01110001011111011101000101101011;



  assign reset_cm =  ~ IPCORE_RESETN;



  assign reset = reset_cm | reset_internal;



  mlhdlc_sfir_fixpt_ipcore_axi_lite u_mlhdlc_sfir_fixpt_ipcore_axi_lite_inst (.reset(reset),
                                                                              .AXI4_Lite_ACLK(AXI4_Lite_ACLK),  // ufix1
                                                                              .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),  // ufix1
                                                                              .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),  // ufix16
                                                                              .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),  // ufix1
                                                                              .AXI4_Lite_WDATA(AXI4_Lite_WDATA),  // ufix32
                                                                              .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),  // ufix4
                                                                              .AXI4_Lite_WVALID(AXI4_Lite_WVALID),  // ufix1
                                                                              .AXI4_Lite_BREADY(AXI4_Lite_BREADY),  // ufix1
                                                                              .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),  // ufix16
                                                                              .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),  // ufix1
                                                                              .AXI4_Lite_RREADY(AXI4_Lite_RREADY),  // ufix1
                                                                              .read_ip_timestamp(ip_timestamp),  // ufix32
                                                                              .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),  // ufix1
                                                                              .AXI4_Lite_WREADY(AXI4_Lite_WREADY),  // ufix1
                                                                              .AXI4_Lite_BRESP(AXI4_Lite_BRESP),  // ufix2
                                                                              .AXI4_Lite_BVALID(AXI4_Lite_BVALID),  // ufix1
                                                                              .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),  // ufix1
                                                                              .AXI4_Lite_RDATA(AXI4_Lite_RDATA),  // ufix32
                                                                              .AXI4_Lite_RRESP(AXI4_Lite_RRESP),  // ufix2
                                                                              .AXI4_Lite_RVALID(AXI4_Lite_RVALID),  // ufix1
                                                                              .write_axi_enable(write_axi_enable),  // ufix1
                                                                              .write_packet_size_axi4_stream_master(write_packet_size_axi4_stream_master),  // ufix32
                                                                              .write_h_in1(write_h_in1),  // sfix14_En15
                                                                              .write_h_in2(write_h_in2),  // sfix14_En16
                                                                              .write_h_in3(write_h_in3),  // ufix14_En16
                                                                              .write_h_in4(write_h_in4),  // ufix14_En15
                                                                              .reset_internal(reset_internal)  // ufix1
                                                                              );

  assign const_1 = 1'b1;



  assign enb = const_1;

  assign top_user_valid = Valid_out_sig;



  assign Valid_in_sig = top_user_valid_1;



  mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_slave_inst (.clk(IPCORE_CLK),  // ufix1
                                                                                                .reset(reset),
                                                                                                .enb(const_1),
                                                                                                .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),  // ufix32
                                                                                                .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),  // ufix1
                                                                                                .auto_ready(auto_ready_axi4_stream_master),  // ufix1
                                                                                                .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),  // ufix1
                                                                                                .user_data(top_user_data),  // ufix32
                                                                                                .user_valid(top_user_valid_1)  // ufix1
                                                                                                );

  assign x_in_sig = top_user_data;



  always @(posedge IPCORE_CLK or posedge reset)
    begin : reg_rsvd_process
      if (reset == 1'b1) begin
        auto_ready_dut_enb <= 1'b0;
      end
      else begin
        if (enb) begin
          auto_ready_dut_enb <= auto_ready_axi4_stream_master;
        end
      end
    end



  assign dut_enable = write_axi_enable & auto_ready_dut_enb;



  mlhdlc_sfir_fixpt_ipcore_dut u_mlhdlc_sfir_fixpt_ipcore_dut_inst (.clk(IPCORE_CLK),  // ufix1
                                                                    .reset(reset),
                                                                    .dut_enable(dut_enable),  // ufix1
                                                                    .x_in(x_in_sig),  // sfix32_En12
                                                                    .h_in1(write_h_in1),  // sfix14_En15
                                                                    .h_in2(write_h_in2),  // sfix14_En16
                                                                    .h_in3(write_h_in3),  // ufix14_En16
                                                                    .h_in4(write_h_in4),  // ufix14_En15
                                                                    .Valid_in(Valid_in_sig),  // ufix1
                                                                    .ce_out(ce_out_sig),  // ufix1
                                                                    .y_out(y_out_sig),  // sfix32_En12
                                                                    .delayed_xout(delayed_xout_sig),  // sfix32_En12
                                                                    .Valid_out(Valid_out_sig)  // ufix1
                                                                    );

  assign top_user_data_1 = y_out_sig;



  mlhdlc_sfir_fixpt_ipcore_axi4_stream_master u_mlhdlc_sfir_fixpt_ipcore_axi4_stream_master_inst (.clk(IPCORE_CLK),  // ufix1
                                                                                                  .reset(reset),
                                                                                                  .enb(const_1),
                                                                                                  .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),  // ufix1
                                                                                                  .user_data(top_user_data_1),  // ufix32
                                                                                                  .user_valid(top_user_valid),  // ufix1
                                                                                                  .write_packet_size_axi4_stream_master(write_packet_size_axi4_stream_master),  // ufix32
                                                                                                  .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),  // ufix32
                                                                                                  .AXI4_Stream_Master_TVALID(AXI4_Stream_Master_TVALID),  // ufix1
                                                                                                  .AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),  // ufix1
                                                                                                  .auto_ready(auto_ready_axi4_stream_master)  // ufix1
                                                                                                  );

  assign delayed_xout = delayed_xout_sig;

endmodule  // mlhdlc_sfir_fixpt_ipcore

