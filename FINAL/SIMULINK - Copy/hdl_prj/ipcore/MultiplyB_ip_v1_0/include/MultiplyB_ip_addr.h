/*
 * File Name:         hdl_prj\ipcore\MultiplyB_ip_v1_0\include\MultiplyB_ip_addr.h
 * Description:       C Header File
 * Created:           2019-05-04 07:01:26
*/

#ifndef MULTIPLYB_IP_H_
#define MULTIPLYB_IP_H_

#define  IPCore_Reset_MultiplyB_ip                           0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_MultiplyB_ip                          0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_PacketSize_AXI4_Stream_Master_MultiplyB_ip   0x8  //Packet size for AXI4-Stream Master interface, the default value is 1024. The TLAST output signal of the AXI4-Stream Master interface is generated based on the packet size.
#define  IPCore_Timestamp_MultiplyB_ip                       0xC  //contains unique IP timestamp (yymmddHHMM): 1905040701

#endif /* MULTIPLYB_IP_H_ */
