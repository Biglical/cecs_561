/*
 * File Name:         hdl_prj\ipcore\MultiplyB_ip_v1_0\include\MultiplyB_ip_addr.h
 * Description:       C Header File
 * Created:           2019-05-04 10:43:57
*/

#ifndef MULTIPLYB_IP_H_
#define MULTIPLYB_IP_H_

#define  IPCore_Reset_MultiplyB_ip       0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_MultiplyB_ip      0x4  //enabled (by default) when bit 0 is 0x1
#define  IPCore_Timestamp_MultiplyB_ip   0x8  //contains unique IP timestamp (yymmddHHMM): 1905041043
#define  In1_Data_MultiplyB_ip           0x100  //data register for Inport In1
#define  Out1_Data_MultiplyB_ip          0x108  //data register for Outport Out1

#endif /* MULTIPLYB_IP_H_ */
