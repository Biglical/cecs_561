// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Sat May  4 10:45:23 2019
// Host        : DESKTOP-MC6SQ8S running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               c:/git/cecs_561/FINAL/HDL/FINAL/FINAL.srcs/sources_1/bd/System/ip/System_MultiplyB_ip_0_0/System_MultiplyB_ip_0_0_sim_netlist.v
// Design      : System_MultiplyB_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "System_MultiplyB_ip_0_0,MultiplyB_ip,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "MultiplyB_ip,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module System_MultiplyB_ip_0_0
   (IPCORE_CLK,
    IPCORE_RESETN,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARESETN,
    AXI4_Lite_AWADDR,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WDATA,
    AXI4_Lite_WSTRB,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_ARADDR,
    AXI4_Lite_ARVALID,
    AXI4_Lite_RREADY,
    AXI4_Lite_AWREADY,
    AXI4_Lite_WREADY,
    AXI4_Lite_BRESP,
    AXI4_Lite_BVALID,
    AXI4_Lite_ARREADY,
    AXI4_Lite_RDATA,
    AXI4_Lite_RRESP,
    AXI4_Lite_RVALID);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input IPCORE_CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST" *) (* x_interface_parameter = "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input IPCORE_RESETN;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input AXI4_Lite_ACLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input AXI4_Lite_ARESETN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [15:0]AXI4_Lite_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID" *) input AXI4_Lite_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA" *) input [31:0]AXI4_Lite_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB" *) input [3:0]AXI4_Lite_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID" *) input AXI4_Lite_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY" *) input AXI4_Lite_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR" *) input [15:0]AXI4_Lite_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID" *) input AXI4_Lite_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY" *) input AXI4_Lite_RREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY" *) output AXI4_Lite_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY" *) output AXI4_Lite_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP" *) output [1:0]AXI4_Lite_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID" *) output AXI4_Lite_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY" *) output AXI4_Lite_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA" *) output [31:0]AXI4_Lite_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP" *) output [1:0]AXI4_Lite_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID" *) output AXI4_Lite_RVALID;

  wire \<const0> ;
  wire AXI4_Lite_ACLK;
  wire [15:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [15:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire AXI4_Lite_BVALID;
  wire [31:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire AXI4_Lite_RVALID;
  wire [31:0]AXI4_Lite_WDATA;
  wire AXI4_Lite_WREADY;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire IPCORE_RESETN;

  assign AXI4_Lite_BRESP[1] = \<const0> ;
  assign AXI4_Lite_BRESP[0] = \<const0> ;
  assign AXI4_Lite_RRESP[1] = \<const0> ;
  assign AXI4_Lite_RRESP[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  System_MultiplyB_ip_0_0_MultiplyB_ip U0
       (.AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR[15:2]),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR[15:2]),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_BVALID(AXI4_Lite_BVALID),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_RVALID(AXI4_Lite_RVALID),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA[30:0]),
        .AXI4_Lite_WREADY(AXI4_Lite_WREADY),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .IPCORE_RESETN(IPCORE_RESETN));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip" *) 
module System_MultiplyB_ip_0_0_MultiplyB_ip
   (AXI4_Lite_RVALID,
    AXI4_Lite_BVALID,
    AXI4_Lite_WREADY,
    AXI4_Lite_RDATA,
    AXI4_Lite_ARREADY,
    AXI4_Lite_AWREADY,
    AXI4_Lite_ARVALID,
    AXI4_Lite_ACLK,
    AXI4_Lite_AWADDR,
    AXI4_Lite_WDATA,
    AXI4_Lite_ARADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN);
  output AXI4_Lite_RVALID;
  output AXI4_Lite_BVALID;
  output AXI4_Lite_WREADY;
  output [31:0]AXI4_Lite_RDATA;
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_AWREADY;
  input AXI4_Lite_ARVALID;
  input AXI4_Lite_ACLK;
  input [13:0]AXI4_Lite_AWADDR;
  input [30:0]AXI4_Lite_WDATA;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_RREADY;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;

  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire AXI4_Lite_BVALID;
  wire [31:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire AXI4_Lite_RVALID;
  wire [30:0]AXI4_Lite_WDATA;
  wire AXI4_Lite_WREADY;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire IPCORE_RESETN;
  wire [31:2]read_Out1;
  wire [30:0]write_reg_In1;

  System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite u_MultiplyB_ip_axi_lite_inst
       (.AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .FSM_sequential_axi_lite_rstate_reg(AXI4_Lite_RVALID),
        .IPCORE_RESETN(IPCORE_RESETN),
        .Q({AXI4_Lite_BVALID,AXI4_Lite_WREADY}),
        .read_Out1(read_Out1),
        .\write_reg_In1_reg[30] (write_reg_In1));
  System_MultiplyB_ip_0_0_MultiplyB_ip_dut u_MultiplyB_ip_dut_inst
       (.read_Out1(read_Out1),
        .\read_reg_Out1_reg[31] (write_reg_In1));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip_addr_decoder" *) 
module System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder
   (read_reg_ip_timestamp,
    \write_reg_In1_reg[30]_0 ,
    \read_reg_Out1_reg[31]_0 ,
    AXI4_Lite_ACLK,
    AR,
    E,
    Q,
    read_Out1);
  output [0:0]read_reg_ip_timestamp;
  output [30:0]\write_reg_In1_reg[30]_0 ;
  output [30:0]\read_reg_Out1_reg[31]_0 ;
  input AXI4_Lite_ACLK;
  input [0:0]AR;
  input [0:0]E;
  input [30:0]Q;
  input [29:0]read_Out1;

  wire [0:0]AR;
  wire AXI4_Lite_ACLK;
  wire [0:0]E;
  wire [30:0]Q;
  wire [29:0]read_Out1;
  wire [30:0]\read_reg_Out1_reg[31]_0 ;
  wire [0:0]read_reg_ip_timestamp;
  wire [30:0]\write_reg_In1_reg[30]_0 ;

  FDCE \read_reg_Out1_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[8]),
        .Q(\read_reg_Out1_reg[31]_0 [9]));
  FDCE \read_reg_Out1_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[9]),
        .Q(\read_reg_Out1_reg[31]_0 [10]));
  FDCE \read_reg_Out1_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[10]),
        .Q(\read_reg_Out1_reg[31]_0 [11]));
  FDCE \read_reg_Out1_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[11]),
        .Q(\read_reg_Out1_reg[31]_0 [12]));
  FDCE \read_reg_Out1_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[12]),
        .Q(\read_reg_Out1_reg[31]_0 [13]));
  FDCE \read_reg_Out1_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[13]),
        .Q(\read_reg_Out1_reg[31]_0 [14]));
  FDCE \read_reg_Out1_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[14]),
        .Q(\read_reg_Out1_reg[31]_0 [15]));
  FDCE \read_reg_Out1_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[15]),
        .Q(\read_reg_Out1_reg[31]_0 [16]));
  FDCE \read_reg_Out1_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[16]),
        .Q(\read_reg_Out1_reg[31]_0 [17]));
  FDCE \read_reg_Out1_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[17]),
        .Q(\read_reg_Out1_reg[31]_0 [18]));
  FDCE \read_reg_Out1_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(\write_reg_In1_reg[30]_0 [0]),
        .Q(\read_reg_Out1_reg[31]_0 [0]));
  FDCE \read_reg_Out1_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[18]),
        .Q(\read_reg_Out1_reg[31]_0 [19]));
  FDCE \read_reg_Out1_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[19]),
        .Q(\read_reg_Out1_reg[31]_0 [20]));
  FDCE \read_reg_Out1_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[20]),
        .Q(\read_reg_Out1_reg[31]_0 [21]));
  FDCE \read_reg_Out1_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[21]),
        .Q(\read_reg_Out1_reg[31]_0 [22]));
  FDCE \read_reg_Out1_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[22]),
        .Q(\read_reg_Out1_reg[31]_0 [23]));
  FDCE \read_reg_Out1_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[23]),
        .Q(\read_reg_Out1_reg[31]_0 [24]));
  FDCE \read_reg_Out1_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[24]),
        .Q(\read_reg_Out1_reg[31]_0 [25]));
  FDCE \read_reg_Out1_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[25]),
        .Q(\read_reg_Out1_reg[31]_0 [26]));
  FDCE \read_reg_Out1_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[26]),
        .Q(\read_reg_Out1_reg[31]_0 [27]));
  FDCE \read_reg_Out1_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[27]),
        .Q(\read_reg_Out1_reg[31]_0 [28]));
  FDCE \read_reg_Out1_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[0]),
        .Q(\read_reg_Out1_reg[31]_0 [1]));
  FDCE \read_reg_Out1_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[28]),
        .Q(\read_reg_Out1_reg[31]_0 [29]));
  FDCE \read_reg_Out1_reg[31] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[29]),
        .Q(\read_reg_Out1_reg[31]_0 [30]));
  FDCE \read_reg_Out1_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[1]),
        .Q(\read_reg_Out1_reg[31]_0 [2]));
  FDCE \read_reg_Out1_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[2]),
        .Q(\read_reg_Out1_reg[31]_0 [3]));
  FDCE \read_reg_Out1_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[3]),
        .Q(\read_reg_Out1_reg[31]_0 [4]));
  FDCE \read_reg_Out1_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[4]),
        .Q(\read_reg_Out1_reg[31]_0 [5]));
  FDCE \read_reg_Out1_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[5]),
        .Q(\read_reg_Out1_reg[31]_0 [6]));
  FDCE \read_reg_Out1_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[6]),
        .Q(\read_reg_Out1_reg[31]_0 [7]));
  FDCE \read_reg_Out1_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(read_Out1[7]),
        .Q(\read_reg_Out1_reg[31]_0 [8]));
  FDCE \read_reg_ip_timestamp_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(AR),
        .D(1'b1),
        .Q(read_reg_ip_timestamp));
  FDCE \write_reg_In1_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[0]),
        .Q(\write_reg_In1_reg[30]_0 [0]));
  FDCE \write_reg_In1_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[10]),
        .Q(\write_reg_In1_reg[30]_0 [10]));
  FDCE \write_reg_In1_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[11]),
        .Q(\write_reg_In1_reg[30]_0 [11]));
  FDCE \write_reg_In1_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[12]),
        .Q(\write_reg_In1_reg[30]_0 [12]));
  FDCE \write_reg_In1_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[13]),
        .Q(\write_reg_In1_reg[30]_0 [13]));
  FDCE \write_reg_In1_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[14]),
        .Q(\write_reg_In1_reg[30]_0 [14]));
  FDCE \write_reg_In1_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[15]),
        .Q(\write_reg_In1_reg[30]_0 [15]));
  FDCE \write_reg_In1_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[16]),
        .Q(\write_reg_In1_reg[30]_0 [16]));
  FDCE \write_reg_In1_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[17]),
        .Q(\write_reg_In1_reg[30]_0 [17]));
  FDCE \write_reg_In1_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[18]),
        .Q(\write_reg_In1_reg[30]_0 [18]));
  FDCE \write_reg_In1_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[19]),
        .Q(\write_reg_In1_reg[30]_0 [19]));
  FDCE \write_reg_In1_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[1]),
        .Q(\write_reg_In1_reg[30]_0 [1]));
  FDCE \write_reg_In1_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[20]),
        .Q(\write_reg_In1_reg[30]_0 [20]));
  FDCE \write_reg_In1_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[21]),
        .Q(\write_reg_In1_reg[30]_0 [21]));
  FDCE \write_reg_In1_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[22]),
        .Q(\write_reg_In1_reg[30]_0 [22]));
  FDCE \write_reg_In1_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[23]),
        .Q(\write_reg_In1_reg[30]_0 [23]));
  FDCE \write_reg_In1_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[24]),
        .Q(\write_reg_In1_reg[30]_0 [24]));
  FDCE \write_reg_In1_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[25]),
        .Q(\write_reg_In1_reg[30]_0 [25]));
  FDCE \write_reg_In1_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[26]),
        .Q(\write_reg_In1_reg[30]_0 [26]));
  FDCE \write_reg_In1_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[27]),
        .Q(\write_reg_In1_reg[30]_0 [27]));
  FDCE \write_reg_In1_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[28]),
        .Q(\write_reg_In1_reg[30]_0 [28]));
  FDCE \write_reg_In1_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[29]),
        .Q(\write_reg_In1_reg[30]_0 [29]));
  FDCE \write_reg_In1_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[2]),
        .Q(\write_reg_In1_reg[30]_0 [2]));
  FDCE \write_reg_In1_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[30]),
        .Q(\write_reg_In1_reg[30]_0 [30]));
  FDCE \write_reg_In1_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[3]),
        .Q(\write_reg_In1_reg[30]_0 [3]));
  FDCE \write_reg_In1_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[4]),
        .Q(\write_reg_In1_reg[30]_0 [4]));
  FDCE \write_reg_In1_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[5]),
        .Q(\write_reg_In1_reg[30]_0 [5]));
  FDCE \write_reg_In1_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[6]),
        .Q(\write_reg_In1_reg[30]_0 [6]));
  FDCE \write_reg_In1_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[7]),
        .Q(\write_reg_In1_reg[30]_0 [7]));
  FDCE \write_reg_In1_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[8]),
        .Q(\write_reg_In1_reg[30]_0 [8]));
  FDCE \write_reg_In1_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR),
        .D(Q[9]),
        .Q(\write_reg_In1_reg[30]_0 [9]));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip_axi_lite" *) 
module System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite
   (FSM_sequential_axi_lite_rstate_reg,
    Q,
    \write_reg_In1_reg[30] ,
    AXI4_Lite_RDATA,
    AXI4_Lite_ARREADY,
    AXI4_Lite_AWREADY,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARVALID,
    AXI4_Lite_AWADDR,
    AXI4_Lite_WDATA,
    read_Out1,
    AXI4_Lite_ARADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN);
  output FSM_sequential_axi_lite_rstate_reg;
  output [1:0]Q;
  output [30:0]\write_reg_In1_reg[30] ;
  output [31:0]AXI4_Lite_RDATA;
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_AWREADY;
  input AXI4_Lite_ACLK;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_AWADDR;
  input [30:0]AXI4_Lite_WDATA;
  input [29:0]read_Out1;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_RREADY;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;

  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire [31:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire [30:0]AXI4_Lite_WDATA;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire FSM_sequential_axi_lite_rstate_reg;
  wire IPCORE_RESETN;
  wire [1:0]Q;
  wire [29:0]read_Out1;
  wire [31:1]read_reg_Out1;
  wire [30:30]read_reg_ip_timestamp;
  wire reg_enb_In1;
  wire reset;
  wire [0:0]top_data_write;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_1;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_10;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_11;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_12;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_13;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_14;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_15;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_16;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_17;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_18;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_19;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_2;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_20;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_21;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_22;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_23;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_24;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_25;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_26;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_27;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_28;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_29;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_3;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_30;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_4;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_5;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_6;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_7;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_8;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_9;
  wire [30:0]\write_reg_In1_reg[30] ;

  System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder u_MultiplyB_ip_addr_decoder_inst
       (.AR(reset),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .E(reg_enb_In1),
        .Q({u_MultiplyB_ip_axi_lite_module_inst_n_1,u_MultiplyB_ip_axi_lite_module_inst_n_2,u_MultiplyB_ip_axi_lite_module_inst_n_3,u_MultiplyB_ip_axi_lite_module_inst_n_4,u_MultiplyB_ip_axi_lite_module_inst_n_5,u_MultiplyB_ip_axi_lite_module_inst_n_6,u_MultiplyB_ip_axi_lite_module_inst_n_7,u_MultiplyB_ip_axi_lite_module_inst_n_8,u_MultiplyB_ip_axi_lite_module_inst_n_9,u_MultiplyB_ip_axi_lite_module_inst_n_10,u_MultiplyB_ip_axi_lite_module_inst_n_11,u_MultiplyB_ip_axi_lite_module_inst_n_12,u_MultiplyB_ip_axi_lite_module_inst_n_13,u_MultiplyB_ip_axi_lite_module_inst_n_14,u_MultiplyB_ip_axi_lite_module_inst_n_15,u_MultiplyB_ip_axi_lite_module_inst_n_16,u_MultiplyB_ip_axi_lite_module_inst_n_17,u_MultiplyB_ip_axi_lite_module_inst_n_18,u_MultiplyB_ip_axi_lite_module_inst_n_19,u_MultiplyB_ip_axi_lite_module_inst_n_20,u_MultiplyB_ip_axi_lite_module_inst_n_21,u_MultiplyB_ip_axi_lite_module_inst_n_22,u_MultiplyB_ip_axi_lite_module_inst_n_23,u_MultiplyB_ip_axi_lite_module_inst_n_24,u_MultiplyB_ip_axi_lite_module_inst_n_25,u_MultiplyB_ip_axi_lite_module_inst_n_26,u_MultiplyB_ip_axi_lite_module_inst_n_27,u_MultiplyB_ip_axi_lite_module_inst_n_28,u_MultiplyB_ip_axi_lite_module_inst_n_29,u_MultiplyB_ip_axi_lite_module_inst_n_30,top_data_write}),
        .read_Out1(read_Out1),
        .\read_reg_Out1_reg[31]_0 (read_reg_Out1),
        .read_reg_ip_timestamp(read_reg_ip_timestamp),
        .\write_reg_In1_reg[30]_0 (\write_reg_In1_reg[30] ));
  System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module u_MultiplyB_ip_axi_lite_module_inst
       (.AR(reset),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .\AXI4_Lite_RDATA_tmp_reg[31]_0 (read_reg_Out1),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .E(reg_enb_In1),
        .\FSM_onehot_axi_lite_wstate_reg[2]_0 (Q),
        .FSM_sequential_axi_lite_rstate_reg_0(FSM_sequential_axi_lite_rstate_reg),
        .IPCORE_RESETN(IPCORE_RESETN),
        .Q({u_MultiplyB_ip_axi_lite_module_inst_n_1,u_MultiplyB_ip_axi_lite_module_inst_n_2,u_MultiplyB_ip_axi_lite_module_inst_n_3,u_MultiplyB_ip_axi_lite_module_inst_n_4,u_MultiplyB_ip_axi_lite_module_inst_n_5,u_MultiplyB_ip_axi_lite_module_inst_n_6,u_MultiplyB_ip_axi_lite_module_inst_n_7,u_MultiplyB_ip_axi_lite_module_inst_n_8,u_MultiplyB_ip_axi_lite_module_inst_n_9,u_MultiplyB_ip_axi_lite_module_inst_n_10,u_MultiplyB_ip_axi_lite_module_inst_n_11,u_MultiplyB_ip_axi_lite_module_inst_n_12,u_MultiplyB_ip_axi_lite_module_inst_n_13,u_MultiplyB_ip_axi_lite_module_inst_n_14,u_MultiplyB_ip_axi_lite_module_inst_n_15,u_MultiplyB_ip_axi_lite_module_inst_n_16,u_MultiplyB_ip_axi_lite_module_inst_n_17,u_MultiplyB_ip_axi_lite_module_inst_n_18,u_MultiplyB_ip_axi_lite_module_inst_n_19,u_MultiplyB_ip_axi_lite_module_inst_n_20,u_MultiplyB_ip_axi_lite_module_inst_n_21,u_MultiplyB_ip_axi_lite_module_inst_n_22,u_MultiplyB_ip_axi_lite_module_inst_n_23,u_MultiplyB_ip_axi_lite_module_inst_n_24,u_MultiplyB_ip_axi_lite_module_inst_n_25,u_MultiplyB_ip_axi_lite_module_inst_n_26,u_MultiplyB_ip_axi_lite_module_inst_n_27,u_MultiplyB_ip_axi_lite_module_inst_n_28,u_MultiplyB_ip_axi_lite_module_inst_n_29,u_MultiplyB_ip_axi_lite_module_inst_n_30,top_data_write}),
        .read_reg_ip_timestamp(read_reg_ip_timestamp));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip_axi_lite_module" *) 
module System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module
   (FSM_sequential_axi_lite_rstate_reg_0,
    Q,
    \FSM_onehot_axi_lite_wstate_reg[2]_0 ,
    AXI4_Lite_RDATA,
    E,
    AXI4_Lite_ARREADY,
    AXI4_Lite_AWREADY,
    AR,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARVALID,
    AXI4_Lite_AWADDR,
    AXI4_Lite_WDATA,
    read_reg_ip_timestamp,
    \AXI4_Lite_RDATA_tmp_reg[31]_0 ,
    AXI4_Lite_ARADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN);
  output FSM_sequential_axi_lite_rstate_reg_0;
  output [30:0]Q;
  output [1:0]\FSM_onehot_axi_lite_wstate_reg[2]_0 ;
  output [31:0]AXI4_Lite_RDATA;
  output [0:0]E;
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_AWREADY;
  output [0:0]AR;
  input AXI4_Lite_ACLK;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_AWADDR;
  input [30:0]AXI4_Lite_WDATA;
  input [0:0]read_reg_ip_timestamp;
  input [30:0]\AXI4_Lite_RDATA_tmp_reg[31]_0 ;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_RREADY;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;

  wire [0:0]AR;
  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire [31:0]AXI4_Lite_RDATA;
  wire \AXI4_Lite_RDATA_tmp[31]_i_10_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_7_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_8_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[31]_i_9_n_0 ;
  wire [30:0]\AXI4_Lite_RDATA_tmp_reg[31]_0 ;
  wire AXI4_Lite_RREADY;
  wire [30:0]AXI4_Lite_WDATA;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [0:0]E;
  wire [1:0]\FSM_onehot_axi_lite_wstate_reg[2]_0 ;
  wire \FSM_onehot_axi_lite_wstate_reg_n_0_[0] ;
  wire FSM_sequential_axi_lite_rstate_reg_0;
  wire IPCORE_RESETN;
  wire [30:0]Q;
  wire aw_transfer;
  wire axi_lite_rstate_next;
  wire [2:0]axi_lite_wstate_next;
  wire [31:0]data_read;
  wire [0:0]read_reg_ip_timestamp;
  wire reset;
  wire [13:0]sel0;
  wire soft_reset;
  wire soft_reset_i_2_n_0;
  wire soft_reset_i_3_n_0;
  wire strobe_sw;
  wire top_rd_enb;
  wire top_wr_enb;
  wire w_transfer;
  wire w_transfer_and_wstrb;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h04)) 
    AXI4_Lite_ARREADY_INST_0
       (.I0(FSM_sequential_axi_lite_rstate_reg_0),
        .I1(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I2(AXI4_Lite_AWVALID),
        .O(AXI4_Lite_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    AXI4_Lite_AWREADY_INST_0
       (.I0(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .O(AXI4_Lite_AWREADY));
  LUT5 #(
    .INIT(32'h00000800)) 
    \AXI4_Lite_RDATA_tmp[0]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I3(read_reg_ip_timestamp),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .O(data_read[0]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[10]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [9]),
        .O(data_read[10]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[11]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [10]),
        .O(data_read[11]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[12]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [11]),
        .O(data_read[12]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[13]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [12]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[13]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[14]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [13]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[14]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[15]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [14]),
        .O(data_read[15]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[16]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [15]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[16]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[17]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [16]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[17]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[18]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [17]),
        .O(data_read[18]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[19]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [18]),
        .O(data_read[19]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[1]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [0]),
        .O(data_read[1]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[20]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [19]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[20]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[21]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [20]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[21]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[22]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [21]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[22]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[23]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [22]),
        .O(data_read[23]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[24]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [23]),
        .O(data_read[24]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[25]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [24]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[25]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[26]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [25]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[26]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[27]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [26]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[27]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[28]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [27]),
        .O(data_read[28]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[29]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [28]),
        .O(data_read[29]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[2]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [1]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[2]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[30]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [29]),
        .O(data_read[30]));
  LUT4 #(
    .INIT(16'h0020)) 
    \AXI4_Lite_RDATA_tmp[31]_i_1 
       (.I0(AXI4_Lite_ARVALID),
        .I1(AXI4_Lite_AWVALID),
        .I2(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I3(FSM_sequential_axi_lite_rstate_reg_0),
        .O(top_rd_enb));
  LUT5 #(
    .INIT(32'h00000004)) 
    \AXI4_Lite_RDATA_tmp[31]_i_10 
       (.I0(AXI4_Lite_ARADDR[2]),
        .I1(AXI4_Lite_ARVALID),
        .I2(AXI4_Lite_ARADDR[12]),
        .I3(AXI4_Lite_ARADDR[13]),
        .I4(AXI4_Lite_ARADDR[3]),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_10_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[31]_i_2 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [30]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[31]));
  LUT6 #(
    .INIT(64'h0001FFFF00010001)) 
    \AXI4_Lite_RDATA_tmp[31]_i_3 
       (.I0(\AXI4_Lite_RDATA_tmp[31]_i_7_n_0 ),
        .I1(AXI4_Lite_ARADDR[5]),
        .I2(AXI4_Lite_ARADDR[4]),
        .I3(AXI4_Lite_ARADDR[0]),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_8_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp[31]_i_9_n_0 ),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h00000000FFFFFFFE)) 
    \AXI4_Lite_RDATA_tmp[31]_i_4 
       (.I0(AXI4_Lite_ARVALID),
        .I1(sel0[12]),
        .I2(sel0[3]),
        .I3(sel0[13]),
        .I4(sel0[2]),
        .I5(\AXI4_Lite_RDATA_tmp[31]_i_10_n_0 ),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \AXI4_Lite_RDATA_tmp[31]_i_5 
       (.I0(AXI4_Lite_ARADDR[1]),
        .I1(AXI4_Lite_ARVALID),
        .I2(sel0[1]),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \AXI4_Lite_RDATA_tmp[31]_i_6 
       (.I0(AXI4_Lite_ARADDR[6]),
        .I1(AXI4_Lite_ARVALID),
        .I2(sel0[6]),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    \AXI4_Lite_RDATA_tmp[31]_i_7 
       (.I0(AXI4_Lite_ARADDR[9]),
        .I1(AXI4_Lite_ARADDR[8]),
        .I2(AXI4_Lite_ARADDR[11]),
        .I3(AXI4_Lite_ARVALID),
        .I4(AXI4_Lite_ARADDR[7]),
        .I5(AXI4_Lite_ARADDR[10]),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \AXI4_Lite_RDATA_tmp[31]_i_8 
       (.I0(sel0[11]),
        .I1(sel0[10]),
        .I2(sel0[9]),
        .I3(sel0[8]),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \AXI4_Lite_RDATA_tmp[31]_i_9 
       (.I0(sel0[5]),
        .I1(sel0[4]),
        .I2(sel0[0]),
        .I3(sel0[7]),
        .I4(AXI4_Lite_ARVALID),
        .O(\AXI4_Lite_RDATA_tmp[31]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[3]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [2]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[3]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[4]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [3]),
        .O(data_read[4]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[5]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [4]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[5]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[6]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [5]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[6]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[7]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [6]),
        .O(data_read[7]));
  LUT5 #(
    .INIT(32'h08000000)) 
    \AXI4_Lite_RDATA_tmp[8]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[31]_0 [7]),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .O(data_read[8]));
  LUT6 #(
    .INIT(64'h00E0000000200000)) 
    \AXI4_Lite_RDATA_tmp[9]_i_1 
       (.I0(read_reg_ip_timestamp),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I5(\AXI4_Lite_RDATA_tmp_reg[31]_0 [8]),
        .O(data_read[9]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[0]),
        .Q(AXI4_Lite_RDATA[0]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[10]),
        .Q(AXI4_Lite_RDATA[10]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[11]),
        .Q(AXI4_Lite_RDATA[11]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[12]),
        .Q(AXI4_Lite_RDATA[12]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[13]),
        .Q(AXI4_Lite_RDATA[13]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[14]),
        .Q(AXI4_Lite_RDATA[14]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[15]),
        .Q(AXI4_Lite_RDATA[15]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[16]),
        .Q(AXI4_Lite_RDATA[16]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[17]),
        .Q(AXI4_Lite_RDATA[17]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[18]),
        .Q(AXI4_Lite_RDATA[18]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[19]),
        .Q(AXI4_Lite_RDATA[19]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[1]),
        .Q(AXI4_Lite_RDATA[1]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[20]),
        .Q(AXI4_Lite_RDATA[20]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[21]),
        .Q(AXI4_Lite_RDATA[21]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[22]),
        .Q(AXI4_Lite_RDATA[22]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[23]),
        .Q(AXI4_Lite_RDATA[23]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[24]),
        .Q(AXI4_Lite_RDATA[24]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[25]),
        .Q(AXI4_Lite_RDATA[25]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[26]),
        .Q(AXI4_Lite_RDATA[26]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[27]),
        .Q(AXI4_Lite_RDATA[27]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[28]),
        .Q(AXI4_Lite_RDATA[28]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[29]),
        .Q(AXI4_Lite_RDATA[29]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[2]),
        .Q(AXI4_Lite_RDATA[2]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[30]),
        .Q(AXI4_Lite_RDATA[30]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[31] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[31]),
        .Q(AXI4_Lite_RDATA[31]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[3]),
        .Q(AXI4_Lite_RDATA[3]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[4]),
        .Q(AXI4_Lite_RDATA[4]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[5]),
        .Q(AXI4_Lite_RDATA[5]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[6]),
        .Q(AXI4_Lite_RDATA[6]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[7]),
        .Q(AXI4_Lite_RDATA[7]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[8]),
        .Q(AXI4_Lite_RDATA[8]));
  FDCE \AXI4_Lite_RDATA_tmp_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(top_rd_enb),
        .CLR(reset),
        .D(data_read[9]),
        .Q(AXI4_Lite_RDATA[9]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFF8F8888)) 
    \FSM_onehot_axi_lite_wstate[0]_i_1 
       (.I0(\FSM_onehot_axi_lite_wstate_reg[2]_0 [1]),
        .I1(AXI4_Lite_BREADY),
        .I2(AXI4_Lite_AWVALID),
        .I3(FSM_sequential_axi_lite_rstate_reg_0),
        .I4(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .O(axi_lite_wstate_next[0]));
  LUT5 #(
    .INIT(32'h20FF2020)) 
    \FSM_onehot_axi_lite_wstate[1]_i_1 
       (.I0(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .I2(AXI4_Lite_AWVALID),
        .I3(AXI4_Lite_WVALID),
        .I4(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .O(axi_lite_wstate_next[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_axi_lite_wstate[1]_i_2 
       (.I0(AXI4_Lite_ARESETN),
        .O(reset));
  LUT4 #(
    .INIT(16'h8F88)) 
    \FSM_onehot_axi_lite_wstate[2]_i_1 
       (.I0(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .I1(AXI4_Lite_WVALID),
        .I2(AXI4_Lite_BREADY),
        .I3(\FSM_onehot_axi_lite_wstate_reg[2]_0 [1]),
        .O(axi_lite_wstate_next[2]));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_axi_lite_wstate_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .D(axi_lite_wstate_next[0]),
        .PRE(reset),
        .Q(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_axi_lite_wstate_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_wstate_next[1]),
        .Q(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_axi_lite_wstate_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_wstate_next[2]),
        .Q(\FSM_onehot_axi_lite_wstate_reg[2]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h44744444)) 
    FSM_sequential_axi_lite_rstate_i_1
       (.I0(AXI4_Lite_RREADY),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .I2(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I3(AXI4_Lite_AWVALID),
        .I4(AXI4_Lite_ARVALID),
        .O(axi_lite_rstate_next));
  (* FSM_ENCODED_STATES = "iSTATE:0,iSTATE0:1" *) 
  FDCE FSM_sequential_axi_lite_rstate_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_rstate_next),
        .Q(FSM_sequential_axi_lite_rstate_reg_0));
  LUT3 #(
    .INIT(8'hDF)) 
    \read_reg_ip_timestamp[30]_i_1 
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AR));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    soft_reset_i_1
       (.I0(soft_reset_i_2_n_0),
        .I1(soft_reset_i_3_n_0),
        .I2(sel0[11]),
        .I3(sel0[10]),
        .I4(sel0[9]),
        .I5(sel0[8]),
        .O(strobe_sw));
  LUT6 #(
    .INIT(64'h0000000000000100)) 
    soft_reset_i_2
       (.I0(sel0[5]),
        .I1(sel0[4]),
        .I2(sel0[12]),
        .I3(Q[0]),
        .I4(sel0[0]),
        .I5(sel0[7]),
        .O(soft_reset_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    soft_reset_i_3
       (.I0(sel0[2]),
        .I1(sel0[13]),
        .I2(sel0[3]),
        .I3(sel0[1]),
        .I4(sel0[6]),
        .I5(top_wr_enb),
        .O(soft_reset_i_3_n_0));
  FDCE soft_reset_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(strobe_sw),
        .Q(soft_reset));
  LUT3 #(
    .INIT(8'h20)) 
    \waddr[15]_i_1 
       (.I0(AXI4_Lite_AWVALID),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .I2(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .O(aw_transfer));
  FDCE \waddr_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[8]),
        .Q(sel0[8]));
  FDCE \waddr_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[9]),
        .Q(sel0[9]));
  FDCE \waddr_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[10]),
        .Q(sel0[10]));
  FDCE \waddr_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[11]),
        .Q(sel0[11]));
  FDCE \waddr_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[12]),
        .Q(sel0[12]));
  FDCE \waddr_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[13]),
        .Q(sel0[13]));
  FDCE \waddr_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[0]),
        .Q(sel0[0]));
  FDCE \waddr_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[1]),
        .Q(sel0[1]));
  FDCE \waddr_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[2]),
        .Q(sel0[2]));
  FDCE \waddr_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[3]),
        .Q(sel0[3]));
  FDCE \waddr_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[4]),
        .Q(sel0[4]));
  FDCE \waddr_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[5]),
        .Q(sel0[5]));
  FDCE \waddr_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[6]),
        .Q(sel0[6]));
  FDCE \waddr_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[7]),
        .Q(sel0[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \wdata[30]_i_1 
       (.I0(AXI4_Lite_WVALID),
        .I1(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .O(w_transfer));
  FDCE \wdata_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[0]),
        .Q(Q[0]));
  FDCE \wdata_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[10]),
        .Q(Q[10]));
  FDCE \wdata_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[11]),
        .Q(Q[11]));
  FDCE \wdata_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[12]),
        .Q(Q[12]));
  FDCE \wdata_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[13]),
        .Q(Q[13]));
  FDCE \wdata_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[14]),
        .Q(Q[14]));
  FDCE \wdata_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[15]),
        .Q(Q[15]));
  FDCE \wdata_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[16]),
        .Q(Q[16]));
  FDCE \wdata_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[17]),
        .Q(Q[17]));
  FDCE \wdata_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[18]),
        .Q(Q[18]));
  FDCE \wdata_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[19]),
        .Q(Q[19]));
  FDCE \wdata_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[1]),
        .Q(Q[1]));
  FDCE \wdata_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[20]),
        .Q(Q[20]));
  FDCE \wdata_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[21]),
        .Q(Q[21]));
  FDCE \wdata_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[22]),
        .Q(Q[22]));
  FDCE \wdata_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[23]),
        .Q(Q[23]));
  FDCE \wdata_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[24]),
        .Q(Q[24]));
  FDCE \wdata_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[25]),
        .Q(Q[25]));
  FDCE \wdata_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[26]),
        .Q(Q[26]));
  FDCE \wdata_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[27]),
        .Q(Q[27]));
  FDCE \wdata_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[28]),
        .Q(Q[28]));
  FDCE \wdata_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[29]),
        .Q(Q[29]));
  FDCE \wdata_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[2]),
        .Q(Q[2]));
  FDCE \wdata_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[30]),
        .Q(Q[30]));
  FDCE \wdata_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[3]),
        .Q(Q[3]));
  FDCE \wdata_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[4]),
        .Q(Q[4]));
  FDCE \wdata_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[5]),
        .Q(Q[5]));
  FDCE \wdata_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[6]),
        .Q(Q[6]));
  FDCE \wdata_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[7]),
        .Q(Q[7]));
  FDCE \wdata_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[8]),
        .Q(Q[8]));
  FDCE \wdata_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[9]),
        .Q(Q[9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    wr_enb_1_i_1
       (.I0(\FSM_onehot_axi_lite_wstate_reg[2]_0 [0]),
        .I1(AXI4_Lite_WVALID),
        .I2(AXI4_Lite_WSTRB[2]),
        .I3(AXI4_Lite_WSTRB[1]),
        .I4(AXI4_Lite_WSTRB[0]),
        .I5(AXI4_Lite_WSTRB[3]),
        .O(w_transfer_and_wstrb));
  FDCE wr_enb_1_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(w_transfer_and_wstrb),
        .Q(top_wr_enb));
  LUT5 #(
    .INIT(32'h00000800)) 
    \write_reg_In1[30]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp[31]_i_3_n_0 ),
        .I1(\AXI4_Lite_RDATA_tmp[31]_i_6_n_0 ),
        .I2(\AXI4_Lite_RDATA_tmp[31]_i_5_n_0 ),
        .I3(top_wr_enb),
        .I4(\AXI4_Lite_RDATA_tmp[31]_i_4_n_0 ),
        .O(E));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip_dut" *) 
module System_MultiplyB_ip_0_0_MultiplyB_ip_dut
   (read_Out1,
    \read_reg_Out1_reg[31] );
  output [29:0]read_Out1;
  input [30:0]\read_reg_Out1_reg[31] ;

  wire [29:0]read_Out1;
  wire [30:0]\read_reg_Out1_reg[31] ;

  System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10 u_MultiplyB_ip_src_MultiplyBy10
       (.read_Out1(read_Out1),
        .\read_reg_Out1_reg[31] (\read_reg_Out1_reg[31] ));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip_src_MultiplyBy10" *) 
module System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10
   (read_Out1,
    \read_reg_Out1_reg[31] );
  output [29:0]read_Out1;
  input [30:0]\read_reg_Out1_reg[31] ;

  wire [29:0]read_Out1;
  wire \read_reg_Out1[13]_i_2_n_0 ;
  wire \read_reg_Out1[13]_i_3_n_0 ;
  wire \read_reg_Out1[13]_i_4_n_0 ;
  wire \read_reg_Out1[13]_i_5_n_0 ;
  wire \read_reg_Out1[17]_i_2_n_0 ;
  wire \read_reg_Out1[17]_i_3_n_0 ;
  wire \read_reg_Out1[17]_i_4_n_0 ;
  wire \read_reg_Out1[17]_i_5_n_0 ;
  wire \read_reg_Out1[21]_i_2_n_0 ;
  wire \read_reg_Out1[21]_i_3_n_0 ;
  wire \read_reg_Out1[21]_i_4_n_0 ;
  wire \read_reg_Out1[21]_i_5_n_0 ;
  wire \read_reg_Out1[25]_i_2_n_0 ;
  wire \read_reg_Out1[25]_i_3_n_0 ;
  wire \read_reg_Out1[25]_i_4_n_0 ;
  wire \read_reg_Out1[25]_i_5_n_0 ;
  wire \read_reg_Out1[29]_i_2_n_0 ;
  wire \read_reg_Out1[29]_i_3_n_0 ;
  wire \read_reg_Out1[29]_i_4_n_0 ;
  wire \read_reg_Out1[29]_i_5_n_0 ;
  wire \read_reg_Out1[31]_i_2_n_0 ;
  wire \read_reg_Out1[31]_i_3_n_0 ;
  wire \read_reg_Out1[5]_i_2_n_0 ;
  wire \read_reg_Out1[5]_i_3_n_0 ;
  wire \read_reg_Out1[5]_i_4_n_0 ;
  wire \read_reg_Out1[9]_i_2_n_0 ;
  wire \read_reg_Out1[9]_i_3_n_0 ;
  wire \read_reg_Out1[9]_i_4_n_0 ;
  wire \read_reg_Out1[9]_i_5_n_0 ;
  wire \read_reg_Out1_reg[13]_i_1_n_0 ;
  wire \read_reg_Out1_reg[13]_i_1_n_1 ;
  wire \read_reg_Out1_reg[13]_i_1_n_2 ;
  wire \read_reg_Out1_reg[13]_i_1_n_3 ;
  wire \read_reg_Out1_reg[17]_i_1_n_0 ;
  wire \read_reg_Out1_reg[17]_i_1_n_1 ;
  wire \read_reg_Out1_reg[17]_i_1_n_2 ;
  wire \read_reg_Out1_reg[17]_i_1_n_3 ;
  wire \read_reg_Out1_reg[21]_i_1_n_0 ;
  wire \read_reg_Out1_reg[21]_i_1_n_1 ;
  wire \read_reg_Out1_reg[21]_i_1_n_2 ;
  wire \read_reg_Out1_reg[21]_i_1_n_3 ;
  wire \read_reg_Out1_reg[25]_i_1_n_0 ;
  wire \read_reg_Out1_reg[25]_i_1_n_1 ;
  wire \read_reg_Out1_reg[25]_i_1_n_2 ;
  wire \read_reg_Out1_reg[25]_i_1_n_3 ;
  wire \read_reg_Out1_reg[29]_i_1_n_0 ;
  wire \read_reg_Out1_reg[29]_i_1_n_1 ;
  wire \read_reg_Out1_reg[29]_i_1_n_2 ;
  wire \read_reg_Out1_reg[29]_i_1_n_3 ;
  wire [30:0]\read_reg_Out1_reg[31] ;
  wire \read_reg_Out1_reg[31]_i_1_n_3 ;
  wire \read_reg_Out1_reg[5]_i_1_n_0 ;
  wire \read_reg_Out1_reg[5]_i_1_n_1 ;
  wire \read_reg_Out1_reg[5]_i_1_n_2 ;
  wire \read_reg_Out1_reg[5]_i_1_n_3 ;
  wire \read_reg_Out1_reg[9]_i_1_n_0 ;
  wire \read_reg_Out1_reg[9]_i_1_n_1 ;
  wire \read_reg_Out1_reg[9]_i_1_n_2 ;
  wire \read_reg_Out1_reg[9]_i_1_n_3 ;
  wire [3:1]\NLW_read_reg_Out1_reg[31]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_read_reg_Out1_reg[31]_i_1_O_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[13]_i_2 
       (.I0(\read_reg_Out1_reg[31] [12]),
        .I1(\read_reg_Out1_reg[31] [10]),
        .O(\read_reg_Out1[13]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[13]_i_3 
       (.I0(\read_reg_Out1_reg[31] [11]),
        .I1(\read_reg_Out1_reg[31] [9]),
        .O(\read_reg_Out1[13]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[13]_i_4 
       (.I0(\read_reg_Out1_reg[31] [10]),
        .I1(\read_reg_Out1_reg[31] [8]),
        .O(\read_reg_Out1[13]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[13]_i_5 
       (.I0(\read_reg_Out1_reg[31] [9]),
        .I1(\read_reg_Out1_reg[31] [7]),
        .O(\read_reg_Out1[13]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[17]_i_2 
       (.I0(\read_reg_Out1_reg[31] [16]),
        .I1(\read_reg_Out1_reg[31] [14]),
        .O(\read_reg_Out1[17]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[17]_i_3 
       (.I0(\read_reg_Out1_reg[31] [15]),
        .I1(\read_reg_Out1_reg[31] [13]),
        .O(\read_reg_Out1[17]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[17]_i_4 
       (.I0(\read_reg_Out1_reg[31] [14]),
        .I1(\read_reg_Out1_reg[31] [12]),
        .O(\read_reg_Out1[17]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[17]_i_5 
       (.I0(\read_reg_Out1_reg[31] [13]),
        .I1(\read_reg_Out1_reg[31] [11]),
        .O(\read_reg_Out1[17]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[21]_i_2 
       (.I0(\read_reg_Out1_reg[31] [20]),
        .I1(\read_reg_Out1_reg[31] [18]),
        .O(\read_reg_Out1[21]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[21]_i_3 
       (.I0(\read_reg_Out1_reg[31] [19]),
        .I1(\read_reg_Out1_reg[31] [17]),
        .O(\read_reg_Out1[21]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[21]_i_4 
       (.I0(\read_reg_Out1_reg[31] [18]),
        .I1(\read_reg_Out1_reg[31] [16]),
        .O(\read_reg_Out1[21]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[21]_i_5 
       (.I0(\read_reg_Out1_reg[31] [17]),
        .I1(\read_reg_Out1_reg[31] [15]),
        .O(\read_reg_Out1[21]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[25]_i_2 
       (.I0(\read_reg_Out1_reg[31] [24]),
        .I1(\read_reg_Out1_reg[31] [22]),
        .O(\read_reg_Out1[25]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[25]_i_3 
       (.I0(\read_reg_Out1_reg[31] [23]),
        .I1(\read_reg_Out1_reg[31] [21]),
        .O(\read_reg_Out1[25]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[25]_i_4 
       (.I0(\read_reg_Out1_reg[31] [22]),
        .I1(\read_reg_Out1_reg[31] [20]),
        .O(\read_reg_Out1[25]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[25]_i_5 
       (.I0(\read_reg_Out1_reg[31] [21]),
        .I1(\read_reg_Out1_reg[31] [19]),
        .O(\read_reg_Out1[25]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[29]_i_2 
       (.I0(\read_reg_Out1_reg[31] [28]),
        .I1(\read_reg_Out1_reg[31] [26]),
        .O(\read_reg_Out1[29]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[29]_i_3 
       (.I0(\read_reg_Out1_reg[31] [27]),
        .I1(\read_reg_Out1_reg[31] [25]),
        .O(\read_reg_Out1[29]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[29]_i_4 
       (.I0(\read_reg_Out1_reg[31] [26]),
        .I1(\read_reg_Out1_reg[31] [24]),
        .O(\read_reg_Out1[29]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[29]_i_5 
       (.I0(\read_reg_Out1_reg[31] [25]),
        .I1(\read_reg_Out1_reg[31] [23]),
        .O(\read_reg_Out1[29]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[31]_i_2 
       (.I0(\read_reg_Out1_reg[31] [30]),
        .I1(\read_reg_Out1_reg[31] [28]),
        .O(\read_reg_Out1[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[31]_i_3 
       (.I0(\read_reg_Out1_reg[31] [29]),
        .I1(\read_reg_Out1_reg[31] [27]),
        .O(\read_reg_Out1[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[5]_i_2 
       (.I0(\read_reg_Out1_reg[31] [4]),
        .I1(\read_reg_Out1_reg[31] [2]),
        .O(\read_reg_Out1[5]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[5]_i_3 
       (.I0(\read_reg_Out1_reg[31] [3]),
        .I1(\read_reg_Out1_reg[31] [1]),
        .O(\read_reg_Out1[5]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[5]_i_4 
       (.I0(\read_reg_Out1_reg[31] [2]),
        .I1(\read_reg_Out1_reg[31] [0]),
        .O(\read_reg_Out1[5]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[9]_i_2 
       (.I0(\read_reg_Out1_reg[31] [8]),
        .I1(\read_reg_Out1_reg[31] [6]),
        .O(\read_reg_Out1[9]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[9]_i_3 
       (.I0(\read_reg_Out1_reg[31] [7]),
        .I1(\read_reg_Out1_reg[31] [5]),
        .O(\read_reg_Out1[9]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[9]_i_4 
       (.I0(\read_reg_Out1_reg[31] [6]),
        .I1(\read_reg_Out1_reg[31] [4]),
        .O(\read_reg_Out1[9]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \read_reg_Out1[9]_i_5 
       (.I0(\read_reg_Out1_reg[31] [5]),
        .I1(\read_reg_Out1_reg[31] [3]),
        .O(\read_reg_Out1[9]_i_5_n_0 ));
  CARRY4 \read_reg_Out1_reg[13]_i_1 
       (.CI(\read_reg_Out1_reg[9]_i_1_n_0 ),
        .CO({\read_reg_Out1_reg[13]_i_1_n_0 ,\read_reg_Out1_reg[13]_i_1_n_1 ,\read_reg_Out1_reg[13]_i_1_n_2 ,\read_reg_Out1_reg[13]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\read_reg_Out1_reg[31] [12:9]),
        .O(read_Out1[11:8]),
        .S({\read_reg_Out1[13]_i_2_n_0 ,\read_reg_Out1[13]_i_3_n_0 ,\read_reg_Out1[13]_i_4_n_0 ,\read_reg_Out1[13]_i_5_n_0 }));
  CARRY4 \read_reg_Out1_reg[17]_i_1 
       (.CI(\read_reg_Out1_reg[13]_i_1_n_0 ),
        .CO({\read_reg_Out1_reg[17]_i_1_n_0 ,\read_reg_Out1_reg[17]_i_1_n_1 ,\read_reg_Out1_reg[17]_i_1_n_2 ,\read_reg_Out1_reg[17]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\read_reg_Out1_reg[31] [16:13]),
        .O(read_Out1[15:12]),
        .S({\read_reg_Out1[17]_i_2_n_0 ,\read_reg_Out1[17]_i_3_n_0 ,\read_reg_Out1[17]_i_4_n_0 ,\read_reg_Out1[17]_i_5_n_0 }));
  CARRY4 \read_reg_Out1_reg[21]_i_1 
       (.CI(\read_reg_Out1_reg[17]_i_1_n_0 ),
        .CO({\read_reg_Out1_reg[21]_i_1_n_0 ,\read_reg_Out1_reg[21]_i_1_n_1 ,\read_reg_Out1_reg[21]_i_1_n_2 ,\read_reg_Out1_reg[21]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\read_reg_Out1_reg[31] [20:17]),
        .O(read_Out1[19:16]),
        .S({\read_reg_Out1[21]_i_2_n_0 ,\read_reg_Out1[21]_i_3_n_0 ,\read_reg_Out1[21]_i_4_n_0 ,\read_reg_Out1[21]_i_5_n_0 }));
  CARRY4 \read_reg_Out1_reg[25]_i_1 
       (.CI(\read_reg_Out1_reg[21]_i_1_n_0 ),
        .CO({\read_reg_Out1_reg[25]_i_1_n_0 ,\read_reg_Out1_reg[25]_i_1_n_1 ,\read_reg_Out1_reg[25]_i_1_n_2 ,\read_reg_Out1_reg[25]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\read_reg_Out1_reg[31] [24:21]),
        .O(read_Out1[23:20]),
        .S({\read_reg_Out1[25]_i_2_n_0 ,\read_reg_Out1[25]_i_3_n_0 ,\read_reg_Out1[25]_i_4_n_0 ,\read_reg_Out1[25]_i_5_n_0 }));
  CARRY4 \read_reg_Out1_reg[29]_i_1 
       (.CI(\read_reg_Out1_reg[25]_i_1_n_0 ),
        .CO({\read_reg_Out1_reg[29]_i_1_n_0 ,\read_reg_Out1_reg[29]_i_1_n_1 ,\read_reg_Out1_reg[29]_i_1_n_2 ,\read_reg_Out1_reg[29]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\read_reg_Out1_reg[31] [28:25]),
        .O(read_Out1[27:24]),
        .S({\read_reg_Out1[29]_i_2_n_0 ,\read_reg_Out1[29]_i_3_n_0 ,\read_reg_Out1[29]_i_4_n_0 ,\read_reg_Out1[29]_i_5_n_0 }));
  CARRY4 \read_reg_Out1_reg[31]_i_1 
       (.CI(\read_reg_Out1_reg[29]_i_1_n_0 ),
        .CO({\NLW_read_reg_Out1_reg[31]_i_1_CO_UNCONNECTED [3:1],\read_reg_Out1_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\read_reg_Out1_reg[31] [29]}),
        .O({\NLW_read_reg_Out1_reg[31]_i_1_O_UNCONNECTED [3:2],read_Out1[29:28]}),
        .S({1'b0,1'b0,\read_reg_Out1[31]_i_2_n_0 ,\read_reg_Out1[31]_i_3_n_0 }));
  CARRY4 \read_reg_Out1_reg[5]_i_1 
       (.CI(1'b0),
        .CO({\read_reg_Out1_reg[5]_i_1_n_0 ,\read_reg_Out1_reg[5]_i_1_n_1 ,\read_reg_Out1_reg[5]_i_1_n_2 ,\read_reg_Out1_reg[5]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\read_reg_Out1_reg[31] [4:2],1'b0}),
        .O(read_Out1[3:0]),
        .S({\read_reg_Out1[5]_i_2_n_0 ,\read_reg_Out1[5]_i_3_n_0 ,\read_reg_Out1[5]_i_4_n_0 ,\read_reg_Out1_reg[31] [1]}));
  CARRY4 \read_reg_Out1_reg[9]_i_1 
       (.CI(\read_reg_Out1_reg[5]_i_1_n_0 ),
        .CO({\read_reg_Out1_reg[9]_i_1_n_0 ,\read_reg_Out1_reg[9]_i_1_n_1 ,\read_reg_Out1_reg[9]_i_1_n_2 ,\read_reg_Out1_reg[9]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(\read_reg_Out1_reg[31] [8:5]),
        .O(read_Out1[7:4]),
        .S({\read_reg_Out1[9]_i_2_n_0 ,\read_reg_Out1[9]_i_3_n_0 ,\read_reg_Out1[9]_i_4_n_0 ,\read_reg_Out1[9]_i_5_n_0 }));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
