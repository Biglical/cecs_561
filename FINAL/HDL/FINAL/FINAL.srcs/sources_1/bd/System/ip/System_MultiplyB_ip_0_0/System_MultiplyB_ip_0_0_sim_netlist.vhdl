-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Sat May  4 10:45:23 2019
-- Host        : DESKTOP-MC6SQ8S running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim
--               c:/git/cecs_561/FINAL/HDL/FINAL/FINAL.srcs/sources_1/bd/System/ip/System_MultiplyB_ip_0_0/System_MultiplyB_ip_0_0_sim_netlist.vhdl
-- Design      : System_MultiplyB_ip_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder is
  port (
    read_reg_ip_timestamp : out STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_In1_reg[30]_0\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \read_reg_Out1_reg[31]_0\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI4_Lite_ACLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    read_Out1 : in STD_LOGIC_VECTOR ( 29 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder : entity is "MultiplyB_ip_addr_decoder";
end System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder;

architecture STRUCTURE of System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder is
  signal \^write_reg_in1_reg[30]_0\ : STD_LOGIC_VECTOR ( 30 downto 0 );
begin
  \write_reg_In1_reg[30]_0\(30 downto 0) <= \^write_reg_in1_reg[30]_0\(30 downto 0);
\read_reg_Out1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(8),
      Q => \read_reg_Out1_reg[31]_0\(9)
    );
\read_reg_Out1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(9),
      Q => \read_reg_Out1_reg[31]_0\(10)
    );
\read_reg_Out1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(10),
      Q => \read_reg_Out1_reg[31]_0\(11)
    );
\read_reg_Out1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(11),
      Q => \read_reg_Out1_reg[31]_0\(12)
    );
\read_reg_Out1_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(12),
      Q => \read_reg_Out1_reg[31]_0\(13)
    );
\read_reg_Out1_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(13),
      Q => \read_reg_Out1_reg[31]_0\(14)
    );
\read_reg_Out1_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(14),
      Q => \read_reg_Out1_reg[31]_0\(15)
    );
\read_reg_Out1_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(15),
      Q => \read_reg_Out1_reg[31]_0\(16)
    );
\read_reg_Out1_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(16),
      Q => \read_reg_Out1_reg[31]_0\(17)
    );
\read_reg_Out1_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(17),
      Q => \read_reg_Out1_reg[31]_0\(18)
    );
\read_reg_Out1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => \^write_reg_in1_reg[30]_0\(0),
      Q => \read_reg_Out1_reg[31]_0\(0)
    );
\read_reg_Out1_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(18),
      Q => \read_reg_Out1_reg[31]_0\(19)
    );
\read_reg_Out1_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(19),
      Q => \read_reg_Out1_reg[31]_0\(20)
    );
\read_reg_Out1_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(20),
      Q => \read_reg_Out1_reg[31]_0\(21)
    );
\read_reg_Out1_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(21),
      Q => \read_reg_Out1_reg[31]_0\(22)
    );
\read_reg_Out1_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(22),
      Q => \read_reg_Out1_reg[31]_0\(23)
    );
\read_reg_Out1_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(23),
      Q => \read_reg_Out1_reg[31]_0\(24)
    );
\read_reg_Out1_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(24),
      Q => \read_reg_Out1_reg[31]_0\(25)
    );
\read_reg_Out1_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(25),
      Q => \read_reg_Out1_reg[31]_0\(26)
    );
\read_reg_Out1_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(26),
      Q => \read_reg_Out1_reg[31]_0\(27)
    );
\read_reg_Out1_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(27),
      Q => \read_reg_Out1_reg[31]_0\(28)
    );
\read_reg_Out1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(0),
      Q => \read_reg_Out1_reg[31]_0\(1)
    );
\read_reg_Out1_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(28),
      Q => \read_reg_Out1_reg[31]_0\(29)
    );
\read_reg_Out1_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(29),
      Q => \read_reg_Out1_reg[31]_0\(30)
    );
\read_reg_Out1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(1),
      Q => \read_reg_Out1_reg[31]_0\(2)
    );
\read_reg_Out1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(2),
      Q => \read_reg_Out1_reg[31]_0\(3)
    );
\read_reg_Out1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(3),
      Q => \read_reg_Out1_reg[31]_0\(4)
    );
\read_reg_Out1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(4),
      Q => \read_reg_Out1_reg[31]_0\(5)
    );
\read_reg_Out1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(5),
      Q => \read_reg_Out1_reg[31]_0\(6)
    );
\read_reg_Out1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(6),
      Q => \read_reg_Out1_reg[31]_0\(7)
    );
\read_reg_Out1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => read_Out1(7),
      Q => \read_reg_Out1_reg[31]_0\(8)
    );
\read_reg_ip_timestamp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => AR(0),
      D => '1',
      Q => read_reg_ip_timestamp(0)
    );
\write_reg_In1_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(0),
      Q => \^write_reg_in1_reg[30]_0\(0)
    );
\write_reg_In1_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(10),
      Q => \^write_reg_in1_reg[30]_0\(10)
    );
\write_reg_In1_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(11),
      Q => \^write_reg_in1_reg[30]_0\(11)
    );
\write_reg_In1_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(12),
      Q => \^write_reg_in1_reg[30]_0\(12)
    );
\write_reg_In1_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(13),
      Q => \^write_reg_in1_reg[30]_0\(13)
    );
\write_reg_In1_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(14),
      Q => \^write_reg_in1_reg[30]_0\(14)
    );
\write_reg_In1_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(15),
      Q => \^write_reg_in1_reg[30]_0\(15)
    );
\write_reg_In1_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(16),
      Q => \^write_reg_in1_reg[30]_0\(16)
    );
\write_reg_In1_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(17),
      Q => \^write_reg_in1_reg[30]_0\(17)
    );
\write_reg_In1_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(18),
      Q => \^write_reg_in1_reg[30]_0\(18)
    );
\write_reg_In1_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(19),
      Q => \^write_reg_in1_reg[30]_0\(19)
    );
\write_reg_In1_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(1),
      Q => \^write_reg_in1_reg[30]_0\(1)
    );
\write_reg_In1_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(20),
      Q => \^write_reg_in1_reg[30]_0\(20)
    );
\write_reg_In1_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(21),
      Q => \^write_reg_in1_reg[30]_0\(21)
    );
\write_reg_In1_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(22),
      Q => \^write_reg_in1_reg[30]_0\(22)
    );
\write_reg_In1_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(23),
      Q => \^write_reg_in1_reg[30]_0\(23)
    );
\write_reg_In1_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(24),
      Q => \^write_reg_in1_reg[30]_0\(24)
    );
\write_reg_In1_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(25),
      Q => \^write_reg_in1_reg[30]_0\(25)
    );
\write_reg_In1_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(26),
      Q => \^write_reg_in1_reg[30]_0\(26)
    );
\write_reg_In1_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(27),
      Q => \^write_reg_in1_reg[30]_0\(27)
    );
\write_reg_In1_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(28),
      Q => \^write_reg_in1_reg[30]_0\(28)
    );
\write_reg_In1_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(29),
      Q => \^write_reg_in1_reg[30]_0\(29)
    );
\write_reg_In1_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(2),
      Q => \^write_reg_in1_reg[30]_0\(2)
    );
\write_reg_In1_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(30),
      Q => \^write_reg_in1_reg[30]_0\(30)
    );
\write_reg_In1_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(3),
      Q => \^write_reg_in1_reg[30]_0\(3)
    );
\write_reg_In1_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(4),
      Q => \^write_reg_in1_reg[30]_0\(4)
    );
\write_reg_In1_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(5),
      Q => \^write_reg_in1_reg[30]_0\(5)
    );
\write_reg_In1_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(6),
      Q => \^write_reg_in1_reg[30]_0\(6)
    );
\write_reg_In1_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(7),
      Q => \^write_reg_in1_reg[30]_0\(7)
    );
\write_reg_In1_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(8),
      Q => \^write_reg_in1_reg[30]_0\(8)
    );
\write_reg_In1_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => Q(9),
      Q => \^write_reg_in1_reg[30]_0\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module is
  port (
    FSM_sequential_axi_lite_rstate_reg_0 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \FSM_onehot_axi_lite_wstate_reg[2]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 30 downto 0 );
    read_reg_ip_timestamp : in STD_LOGIC_VECTOR ( 0 to 0 );
    \AXI4_Lite_RDATA_tmp_reg[31]_0\ : in STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module : entity is "MultiplyB_ip_axi_lite_module";
end System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module;

architecture STRUCTURE of System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module is
  signal \AXI4_Lite_RDATA_tmp[31]_i_10_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_7_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_8_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[31]_i_9_n_0\ : STD_LOGIC;
  signal \^fsm_onehot_axi_lite_wstate_reg[2]_0\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\ : STD_LOGIC;
  signal \^fsm_sequential_axi_lite_rstate_reg_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal aw_transfer : STD_LOGIC;
  signal axi_lite_rstate_next : STD_LOGIC;
  signal axi_lite_wstate_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal data_read : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal reset : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal soft_reset : STD_LOGIC;
  signal soft_reset_i_2_n_0 : STD_LOGIC;
  signal soft_reset_i_3_n_0 : STD_LOGIC;
  signal strobe_sw : STD_LOGIC;
  signal top_rd_enb : STD_LOGIC;
  signal top_wr_enb : STD_LOGIC;
  signal w_transfer : STD_LOGIC;
  signal w_transfer_and_wstrb : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI4_Lite_ARREADY_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of AXI4_Lite_AWREADY_INST_0 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_axi_lite_wstate[0]_i_1\ : label is "soft_lutpair1";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[0]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[1]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[2]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute SOFT_HLUTNM of FSM_sequential_axi_lite_rstate_i_1 : label is "soft_lutpair0";
  attribute FSM_ENCODED_STATES of FSM_sequential_axi_lite_rstate_reg : label is "iSTATE:0,iSTATE0:1";
begin
  \FSM_onehot_axi_lite_wstate_reg[2]_0\(1 downto 0) <= \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1 downto 0);
  FSM_sequential_axi_lite_rstate_reg_0 <= \^fsm_sequential_axi_lite_rstate_reg_0\;
  Q(30 downto 0) <= \^q\(30 downto 0);
AXI4_Lite_ARREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I1 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I2 => AXI4_Lite_AWVALID,
      O => AXI4_Lite_ARREADY
    );
AXI4_Lite_AWREADY_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => AXI4_Lite_AWREADY
    );
\AXI4_Lite_RDATA_tmp[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I3 => read_reg_ip_timestamp(0),
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      O => data_read(0)
    );
\AXI4_Lite_RDATA_tmp[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(9),
      O => data_read(10)
    );
\AXI4_Lite_RDATA_tmp[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(10),
      O => data_read(11)
    );
\AXI4_Lite_RDATA_tmp[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(11),
      O => data_read(12)
    );
\AXI4_Lite_RDATA_tmp[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(12),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(13)
    );
\AXI4_Lite_RDATA_tmp[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(13),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(14)
    );
\AXI4_Lite_RDATA_tmp[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(14),
      O => data_read(15)
    );
\AXI4_Lite_RDATA_tmp[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(15),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(16)
    );
\AXI4_Lite_RDATA_tmp[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(16),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(17)
    );
\AXI4_Lite_RDATA_tmp[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(17),
      O => data_read(18)
    );
\AXI4_Lite_RDATA_tmp[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(18),
      O => data_read(19)
    );
\AXI4_Lite_RDATA_tmp[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(0),
      O => data_read(1)
    );
\AXI4_Lite_RDATA_tmp[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(19),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(20)
    );
\AXI4_Lite_RDATA_tmp[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(20),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(21)
    );
\AXI4_Lite_RDATA_tmp[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(21),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(22)
    );
\AXI4_Lite_RDATA_tmp[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(22),
      O => data_read(23)
    );
\AXI4_Lite_RDATA_tmp[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(23),
      O => data_read(24)
    );
\AXI4_Lite_RDATA_tmp[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(24),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(25)
    );
\AXI4_Lite_RDATA_tmp[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(25),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(26)
    );
\AXI4_Lite_RDATA_tmp[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(26),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(27)
    );
\AXI4_Lite_RDATA_tmp[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(27),
      O => data_read(28)
    );
\AXI4_Lite_RDATA_tmp[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(28),
      O => data_read(29)
    );
\AXI4_Lite_RDATA_tmp[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(1),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(2)
    );
\AXI4_Lite_RDATA_tmp[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(29),
      O => data_read(30)
    );
\AXI4_Lite_RDATA_tmp[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => AXI4_Lite_ARVALID,
      I1 => AXI4_Lite_AWVALID,
      I2 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I3 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => top_rd_enb
    );
\AXI4_Lite_RDATA_tmp[31]_i_10\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000004"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(2),
      I1 => AXI4_Lite_ARVALID,
      I2 => AXI4_Lite_ARADDR(12),
      I3 => AXI4_Lite_ARADDR(13),
      I4 => AXI4_Lite_ARADDR(3),
      O => \AXI4_Lite_RDATA_tmp[31]_i_10_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(30),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(31)
    );
\AXI4_Lite_RDATA_tmp[31]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001FFFF00010001"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp[31]_i_7_n_0\,
      I1 => AXI4_Lite_ARADDR(5),
      I2 => AXI4_Lite_ARADDR(4),
      I3 => AXI4_Lite_ARADDR(0),
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_8_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp[31]_i_9_n_0\,
      O => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000FFFFFFFE"
    )
        port map (
      I0 => AXI4_Lite_ARVALID,
      I1 => sel0(12),
      I2 => sel0(3),
      I3 => sel0(13),
      I4 => sel0(2),
      I5 => \AXI4_Lite_RDATA_tmp[31]_i_10_n_0\,
      O => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(1),
      I1 => AXI4_Lite_ARVALID,
      I2 => sel0(1),
      O => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(6),
      I1 => AXI4_Lite_ARVALID,
      I2 => sel0(6),
      O => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFEFF"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(9),
      I1 => AXI4_Lite_ARADDR(8),
      I2 => AXI4_Lite_ARADDR(11),
      I3 => AXI4_Lite_ARVALID,
      I4 => AXI4_Lite_ARADDR(7),
      I5 => AXI4_Lite_ARADDR(10),
      O => \AXI4_Lite_RDATA_tmp[31]_i_7_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => sel0(11),
      I1 => sel0(10),
      I2 => sel0(9),
      I3 => sel0(8),
      O => \AXI4_Lite_RDATA_tmp[31]_i_8_n_0\
    );
\AXI4_Lite_RDATA_tmp[31]_i_9\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => sel0(5),
      I1 => sel0(4),
      I2 => sel0(0),
      I3 => sel0(7),
      I4 => AXI4_Lite_ARVALID,
      O => \AXI4_Lite_RDATA_tmp[31]_i_9_n_0\
    );
\AXI4_Lite_RDATA_tmp[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(2),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(3)
    );
\AXI4_Lite_RDATA_tmp[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(3),
      O => data_read(4)
    );
\AXI4_Lite_RDATA_tmp[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(4),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(5)
    );
\AXI4_Lite_RDATA_tmp[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(5),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(6)
    );
\AXI4_Lite_RDATA_tmp[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(6),
      O => data_read(7)
    );
\AXI4_Lite_RDATA_tmp[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"08000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(7),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      O => data_read(8)
    );
\AXI4_Lite_RDATA_tmp[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00E0000000200000"
    )
        port map (
      I0 => read_reg_ip_timestamp(0),
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I5 => \AXI4_Lite_RDATA_tmp_reg[31]_0\(8),
      O => data_read(9)
    );
\AXI4_Lite_RDATA_tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(0),
      Q => AXI4_Lite_RDATA(0)
    );
\AXI4_Lite_RDATA_tmp_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(10),
      Q => AXI4_Lite_RDATA(10)
    );
\AXI4_Lite_RDATA_tmp_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(11),
      Q => AXI4_Lite_RDATA(11)
    );
\AXI4_Lite_RDATA_tmp_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(12),
      Q => AXI4_Lite_RDATA(12)
    );
\AXI4_Lite_RDATA_tmp_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(13),
      Q => AXI4_Lite_RDATA(13)
    );
\AXI4_Lite_RDATA_tmp_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(14),
      Q => AXI4_Lite_RDATA(14)
    );
\AXI4_Lite_RDATA_tmp_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(15),
      Q => AXI4_Lite_RDATA(15)
    );
\AXI4_Lite_RDATA_tmp_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(16),
      Q => AXI4_Lite_RDATA(16)
    );
\AXI4_Lite_RDATA_tmp_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(17),
      Q => AXI4_Lite_RDATA(17)
    );
\AXI4_Lite_RDATA_tmp_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(18),
      Q => AXI4_Lite_RDATA(18)
    );
\AXI4_Lite_RDATA_tmp_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(19),
      Q => AXI4_Lite_RDATA(19)
    );
\AXI4_Lite_RDATA_tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(1),
      Q => AXI4_Lite_RDATA(1)
    );
\AXI4_Lite_RDATA_tmp_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(20),
      Q => AXI4_Lite_RDATA(20)
    );
\AXI4_Lite_RDATA_tmp_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(21),
      Q => AXI4_Lite_RDATA(21)
    );
\AXI4_Lite_RDATA_tmp_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(22),
      Q => AXI4_Lite_RDATA(22)
    );
\AXI4_Lite_RDATA_tmp_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(23),
      Q => AXI4_Lite_RDATA(23)
    );
\AXI4_Lite_RDATA_tmp_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(24),
      Q => AXI4_Lite_RDATA(24)
    );
\AXI4_Lite_RDATA_tmp_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(25),
      Q => AXI4_Lite_RDATA(25)
    );
\AXI4_Lite_RDATA_tmp_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(26),
      Q => AXI4_Lite_RDATA(26)
    );
\AXI4_Lite_RDATA_tmp_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(27),
      Q => AXI4_Lite_RDATA(27)
    );
\AXI4_Lite_RDATA_tmp_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(28),
      Q => AXI4_Lite_RDATA(28)
    );
\AXI4_Lite_RDATA_tmp_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(29),
      Q => AXI4_Lite_RDATA(29)
    );
\AXI4_Lite_RDATA_tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(2),
      Q => AXI4_Lite_RDATA(2)
    );
\AXI4_Lite_RDATA_tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(30),
      Q => AXI4_Lite_RDATA(30)
    );
\AXI4_Lite_RDATA_tmp_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(31),
      Q => AXI4_Lite_RDATA(31)
    );
\AXI4_Lite_RDATA_tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(3),
      Q => AXI4_Lite_RDATA(3)
    );
\AXI4_Lite_RDATA_tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(4),
      Q => AXI4_Lite_RDATA(4)
    );
\AXI4_Lite_RDATA_tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(5),
      Q => AXI4_Lite_RDATA(5)
    );
\AXI4_Lite_RDATA_tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(6),
      Q => AXI4_Lite_RDATA(6)
    );
\AXI4_Lite_RDATA_tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(7),
      Q => AXI4_Lite_RDATA(7)
    );
\AXI4_Lite_RDATA_tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(8),
      Q => AXI4_Lite_RDATA(8)
    );
\AXI4_Lite_RDATA_tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => top_rd_enb,
      CLR => reset,
      D => data_read(9),
      Q => AXI4_Lite_RDATA(9)
    );
\FSM_onehot_axi_lite_wstate[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF8F8888"
    )
        port map (
      I0 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1),
      I1 => AXI4_Lite_BREADY,
      I2 => AXI4_Lite_AWVALID,
      I3 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I4 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      O => axi_lite_wstate_next(0)
    );
\FSM_onehot_axi_lite_wstate[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"20FF2020"
    )
        port map (
      I0 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I2 => AXI4_Lite_AWVALID,
      I3 => AXI4_Lite_WVALID,
      I4 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      O => axi_lite_wstate_next(1)
    );
\FSM_onehot_axi_lite_wstate[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      O => reset
    );
\FSM_onehot_axi_lite_wstate[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      I1 => AXI4_Lite_WVALID,
      I2 => AXI4_Lite_BREADY,
      I3 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1),
      O => axi_lite_wstate_next(2)
    );
\FSM_onehot_axi_lite_wstate_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      D => axi_lite_wstate_next(0),
      PRE => reset,
      Q => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\
    );
\FSM_onehot_axi_lite_wstate_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_wstate_next(1),
      Q => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0)
    );
\FSM_onehot_axi_lite_wstate_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_wstate_next(2),
      Q => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(1)
    );
FSM_sequential_axi_lite_rstate_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"44744444"
    )
        port map (
      I0 => AXI4_Lite_RREADY,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I2 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I3 => AXI4_Lite_AWVALID,
      I4 => AXI4_Lite_ARVALID,
      O => axi_lite_rstate_next
    );
FSM_sequential_axi_lite_rstate_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_rstate_next,
      Q => \^fsm_sequential_axi_lite_rstate_reg_0\
    );
\read_reg_ip_timestamp[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AR(0)
    );
soft_reset_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000002"
    )
        port map (
      I0 => soft_reset_i_2_n_0,
      I1 => soft_reset_i_3_n_0,
      I2 => sel0(11),
      I3 => sel0(10),
      I4 => sel0(9),
      I5 => sel0(8),
      O => strobe_sw
    );
soft_reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000100"
    )
        port map (
      I0 => sel0(5),
      I1 => sel0(4),
      I2 => sel0(12),
      I3 => \^q\(0),
      I4 => sel0(0),
      I5 => sel0(7),
      O => soft_reset_i_2_n_0
    );
soft_reset_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(13),
      I2 => sel0(3),
      I3 => sel0(1),
      I4 => sel0(6),
      I5 => top_wr_enb,
      O => soft_reset_i_3_n_0
    );
soft_reset_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => strobe_sw,
      Q => soft_reset
    );
\waddr[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => AXI4_Lite_AWVALID,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I2 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      O => aw_transfer
    );
\waddr_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(8),
      Q => sel0(8)
    );
\waddr_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(9),
      Q => sel0(9)
    );
\waddr_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(10),
      Q => sel0(10)
    );
\waddr_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(11),
      Q => sel0(11)
    );
\waddr_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(12),
      Q => sel0(12)
    );
\waddr_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(13),
      Q => sel0(13)
    );
\waddr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(0),
      Q => sel0(0)
    );
\waddr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(1),
      Q => sel0(1)
    );
\waddr_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(2),
      Q => sel0(2)
    );
\waddr_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(3),
      Q => sel0(3)
    );
\waddr_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(4),
      Q => sel0(4)
    );
\waddr_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(5),
      Q => sel0(5)
    );
\waddr_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(6),
      Q => sel0(6)
    );
\waddr_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(7),
      Q => sel0(7)
    );
\wdata[30]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => AXI4_Lite_WVALID,
      I1 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      O => w_transfer
    );
\wdata_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(0),
      Q => \^q\(0)
    );
\wdata_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(10),
      Q => \^q\(10)
    );
\wdata_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(11),
      Q => \^q\(11)
    );
\wdata_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(12),
      Q => \^q\(12)
    );
\wdata_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(13),
      Q => \^q\(13)
    );
\wdata_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(14),
      Q => \^q\(14)
    );
\wdata_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(15),
      Q => \^q\(15)
    );
\wdata_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(16),
      Q => \^q\(16)
    );
\wdata_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(17),
      Q => \^q\(17)
    );
\wdata_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(18),
      Q => \^q\(18)
    );
\wdata_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(19),
      Q => \^q\(19)
    );
\wdata_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(1),
      Q => \^q\(1)
    );
\wdata_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(20),
      Q => \^q\(20)
    );
\wdata_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(21),
      Q => \^q\(21)
    );
\wdata_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(22),
      Q => \^q\(22)
    );
\wdata_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(23),
      Q => \^q\(23)
    );
\wdata_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(24),
      Q => \^q\(24)
    );
\wdata_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(25),
      Q => \^q\(25)
    );
\wdata_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(26),
      Q => \^q\(26)
    );
\wdata_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(27),
      Q => \^q\(27)
    );
\wdata_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(28),
      Q => \^q\(28)
    );
\wdata_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(29),
      Q => \^q\(29)
    );
\wdata_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(2),
      Q => \^q\(2)
    );
\wdata_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(30),
      Q => \^q\(30)
    );
\wdata_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(3),
      Q => \^q\(3)
    );
\wdata_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(4),
      Q => \^q\(4)
    );
\wdata_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(5),
      Q => \^q\(5)
    );
\wdata_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(6),
      Q => \^q\(6)
    );
\wdata_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(7),
      Q => \^q\(7)
    );
\wdata_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(8),
      Q => \^q\(8)
    );
\wdata_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(9),
      Q => \^q\(9)
    );
wr_enb_1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \^fsm_onehot_axi_lite_wstate_reg[2]_0\(0),
      I1 => AXI4_Lite_WVALID,
      I2 => AXI4_Lite_WSTRB(2),
      I3 => AXI4_Lite_WSTRB(1),
      I4 => AXI4_Lite_WSTRB(0),
      I5 => AXI4_Lite_WSTRB(3),
      O => w_transfer_and_wstrb
    );
wr_enb_1_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => w_transfer_and_wstrb,
      Q => top_wr_enb
    );
\write_reg_In1[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000800"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp[31]_i_3_n_0\,
      I1 => \AXI4_Lite_RDATA_tmp[31]_i_6_n_0\,
      I2 => \AXI4_Lite_RDATA_tmp[31]_i_5_n_0\,
      I3 => top_wr_enb,
      I4 => \AXI4_Lite_RDATA_tmp[31]_i_4_n_0\,
      O => E(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10 is
  port (
    read_Out1 : out STD_LOGIC_VECTOR ( 29 downto 0 );
    \read_reg_Out1_reg[31]\ : in STD_LOGIC_VECTOR ( 30 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10 : entity is "MultiplyB_ip_src_MultiplyBy10";
end System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10;

architecture STRUCTURE of System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10 is
  signal \read_reg_Out1[13]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[13]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[13]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[13]_i_5_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[17]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[17]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[17]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[17]_i_5_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[21]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[21]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[21]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[21]_i_5_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[25]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[25]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[25]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[25]_i_5_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[29]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[29]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[29]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[29]_i_5_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[31]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[31]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[5]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[5]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[5]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[9]_i_2_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[9]_i_3_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[9]_i_4_n_0\ : STD_LOGIC;
  signal \read_reg_Out1[9]_i_5_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[13]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[13]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[13]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[13]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[17]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[17]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[17]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[17]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[21]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[21]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[21]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[21]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[25]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[25]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[25]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[25]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[29]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[29]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[29]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[29]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[5]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[5]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[5]_i_1_n_3\ : STD_LOGIC;
  signal \read_reg_Out1_reg[9]_i_1_n_0\ : STD_LOGIC;
  signal \read_reg_Out1_reg[9]_i_1_n_1\ : STD_LOGIC;
  signal \read_reg_Out1_reg[9]_i_1_n_2\ : STD_LOGIC;
  signal \read_reg_Out1_reg[9]_i_1_n_3\ : STD_LOGIC;
  signal \NLW_read_reg_Out1_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_read_reg_Out1_reg[31]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
begin
\read_reg_Out1[13]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(12),
      I1 => \read_reg_Out1_reg[31]\(10),
      O => \read_reg_Out1[13]_i_2_n_0\
    );
\read_reg_Out1[13]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(11),
      I1 => \read_reg_Out1_reg[31]\(9),
      O => \read_reg_Out1[13]_i_3_n_0\
    );
\read_reg_Out1[13]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(10),
      I1 => \read_reg_Out1_reg[31]\(8),
      O => \read_reg_Out1[13]_i_4_n_0\
    );
\read_reg_Out1[13]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(9),
      I1 => \read_reg_Out1_reg[31]\(7),
      O => \read_reg_Out1[13]_i_5_n_0\
    );
\read_reg_Out1[17]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(16),
      I1 => \read_reg_Out1_reg[31]\(14),
      O => \read_reg_Out1[17]_i_2_n_0\
    );
\read_reg_Out1[17]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(15),
      I1 => \read_reg_Out1_reg[31]\(13),
      O => \read_reg_Out1[17]_i_3_n_0\
    );
\read_reg_Out1[17]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(14),
      I1 => \read_reg_Out1_reg[31]\(12),
      O => \read_reg_Out1[17]_i_4_n_0\
    );
\read_reg_Out1[17]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(13),
      I1 => \read_reg_Out1_reg[31]\(11),
      O => \read_reg_Out1[17]_i_5_n_0\
    );
\read_reg_Out1[21]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(20),
      I1 => \read_reg_Out1_reg[31]\(18),
      O => \read_reg_Out1[21]_i_2_n_0\
    );
\read_reg_Out1[21]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(19),
      I1 => \read_reg_Out1_reg[31]\(17),
      O => \read_reg_Out1[21]_i_3_n_0\
    );
\read_reg_Out1[21]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(18),
      I1 => \read_reg_Out1_reg[31]\(16),
      O => \read_reg_Out1[21]_i_4_n_0\
    );
\read_reg_Out1[21]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(17),
      I1 => \read_reg_Out1_reg[31]\(15),
      O => \read_reg_Out1[21]_i_5_n_0\
    );
\read_reg_Out1[25]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(24),
      I1 => \read_reg_Out1_reg[31]\(22),
      O => \read_reg_Out1[25]_i_2_n_0\
    );
\read_reg_Out1[25]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(23),
      I1 => \read_reg_Out1_reg[31]\(21),
      O => \read_reg_Out1[25]_i_3_n_0\
    );
\read_reg_Out1[25]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(22),
      I1 => \read_reg_Out1_reg[31]\(20),
      O => \read_reg_Out1[25]_i_4_n_0\
    );
\read_reg_Out1[25]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(21),
      I1 => \read_reg_Out1_reg[31]\(19),
      O => \read_reg_Out1[25]_i_5_n_0\
    );
\read_reg_Out1[29]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(28),
      I1 => \read_reg_Out1_reg[31]\(26),
      O => \read_reg_Out1[29]_i_2_n_0\
    );
\read_reg_Out1[29]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(27),
      I1 => \read_reg_Out1_reg[31]\(25),
      O => \read_reg_Out1[29]_i_3_n_0\
    );
\read_reg_Out1[29]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(26),
      I1 => \read_reg_Out1_reg[31]\(24),
      O => \read_reg_Out1[29]_i_4_n_0\
    );
\read_reg_Out1[29]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(25),
      I1 => \read_reg_Out1_reg[31]\(23),
      O => \read_reg_Out1[29]_i_5_n_0\
    );
\read_reg_Out1[31]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(30),
      I1 => \read_reg_Out1_reg[31]\(28),
      O => \read_reg_Out1[31]_i_2_n_0\
    );
\read_reg_Out1[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(29),
      I1 => \read_reg_Out1_reg[31]\(27),
      O => \read_reg_Out1[31]_i_3_n_0\
    );
\read_reg_Out1[5]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(4),
      I1 => \read_reg_Out1_reg[31]\(2),
      O => \read_reg_Out1[5]_i_2_n_0\
    );
\read_reg_Out1[5]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(3),
      I1 => \read_reg_Out1_reg[31]\(1),
      O => \read_reg_Out1[5]_i_3_n_0\
    );
\read_reg_Out1[5]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(2),
      I1 => \read_reg_Out1_reg[31]\(0),
      O => \read_reg_Out1[5]_i_4_n_0\
    );
\read_reg_Out1[9]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(8),
      I1 => \read_reg_Out1_reg[31]\(6),
      O => \read_reg_Out1[9]_i_2_n_0\
    );
\read_reg_Out1[9]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(7),
      I1 => \read_reg_Out1_reg[31]\(5),
      O => \read_reg_Out1[9]_i_3_n_0\
    );
\read_reg_Out1[9]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(6),
      I1 => \read_reg_Out1_reg[31]\(4),
      O => \read_reg_Out1[9]_i_4_n_0\
    );
\read_reg_Out1[9]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \read_reg_Out1_reg[31]\(5),
      I1 => \read_reg_Out1_reg[31]\(3),
      O => \read_reg_Out1[9]_i_5_n_0\
    );
\read_reg_Out1_reg[13]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[9]_i_1_n_0\,
      CO(3) => \read_reg_Out1_reg[13]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[13]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[13]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[13]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \read_reg_Out1_reg[31]\(12 downto 9),
      O(3 downto 0) => read_Out1(11 downto 8),
      S(3) => \read_reg_Out1[13]_i_2_n_0\,
      S(2) => \read_reg_Out1[13]_i_3_n_0\,
      S(1) => \read_reg_Out1[13]_i_4_n_0\,
      S(0) => \read_reg_Out1[13]_i_5_n_0\
    );
\read_reg_Out1_reg[17]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[13]_i_1_n_0\,
      CO(3) => \read_reg_Out1_reg[17]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[17]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[17]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[17]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \read_reg_Out1_reg[31]\(16 downto 13),
      O(3 downto 0) => read_Out1(15 downto 12),
      S(3) => \read_reg_Out1[17]_i_2_n_0\,
      S(2) => \read_reg_Out1[17]_i_3_n_0\,
      S(1) => \read_reg_Out1[17]_i_4_n_0\,
      S(0) => \read_reg_Out1[17]_i_5_n_0\
    );
\read_reg_Out1_reg[21]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[17]_i_1_n_0\,
      CO(3) => \read_reg_Out1_reg[21]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[21]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[21]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[21]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \read_reg_Out1_reg[31]\(20 downto 17),
      O(3 downto 0) => read_Out1(19 downto 16),
      S(3) => \read_reg_Out1[21]_i_2_n_0\,
      S(2) => \read_reg_Out1[21]_i_3_n_0\,
      S(1) => \read_reg_Out1[21]_i_4_n_0\,
      S(0) => \read_reg_Out1[21]_i_5_n_0\
    );
\read_reg_Out1_reg[25]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[21]_i_1_n_0\,
      CO(3) => \read_reg_Out1_reg[25]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[25]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[25]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[25]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \read_reg_Out1_reg[31]\(24 downto 21),
      O(3 downto 0) => read_Out1(23 downto 20),
      S(3) => \read_reg_Out1[25]_i_2_n_0\,
      S(2) => \read_reg_Out1[25]_i_3_n_0\,
      S(1) => \read_reg_Out1[25]_i_4_n_0\,
      S(0) => \read_reg_Out1[25]_i_5_n_0\
    );
\read_reg_Out1_reg[29]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[25]_i_1_n_0\,
      CO(3) => \read_reg_Out1_reg[29]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[29]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[29]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[29]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \read_reg_Out1_reg[31]\(28 downto 25),
      O(3 downto 0) => read_Out1(27 downto 24),
      S(3) => \read_reg_Out1[29]_i_2_n_0\,
      S(2) => \read_reg_Out1[29]_i_3_n_0\,
      S(1) => \read_reg_Out1[29]_i_4_n_0\,
      S(0) => \read_reg_Out1[29]_i_5_n_0\
    );
\read_reg_Out1_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[29]_i_1_n_0\,
      CO(3 downto 1) => \NLW_read_reg_Out1_reg[31]_i_1_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \read_reg_Out1_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \read_reg_Out1_reg[31]\(29),
      O(3 downto 2) => \NLW_read_reg_Out1_reg[31]_i_1_O_UNCONNECTED\(3 downto 2),
      O(1 downto 0) => read_Out1(29 downto 28),
      S(3 downto 2) => B"00",
      S(1) => \read_reg_Out1[31]_i_2_n_0\,
      S(0) => \read_reg_Out1[31]_i_3_n_0\
    );
\read_reg_Out1_reg[5]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \read_reg_Out1_reg[5]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[5]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[5]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[5]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => \read_reg_Out1_reg[31]\(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => read_Out1(3 downto 0),
      S(3) => \read_reg_Out1[5]_i_2_n_0\,
      S(2) => \read_reg_Out1[5]_i_3_n_0\,
      S(1) => \read_reg_Out1[5]_i_4_n_0\,
      S(0) => \read_reg_Out1_reg[31]\(1)
    );
\read_reg_Out1_reg[9]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \read_reg_Out1_reg[5]_i_1_n_0\,
      CO(3) => \read_reg_Out1_reg[9]_i_1_n_0\,
      CO(2) => \read_reg_Out1_reg[9]_i_1_n_1\,
      CO(1) => \read_reg_Out1_reg[9]_i_1_n_2\,
      CO(0) => \read_reg_Out1_reg[9]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => \read_reg_Out1_reg[31]\(8 downto 5),
      O(3 downto 0) => read_Out1(7 downto 4),
      S(3) => \read_reg_Out1[9]_i_2_n_0\,
      S(2) => \read_reg_Out1[9]_i_3_n_0\,
      S(1) => \read_reg_Out1[9]_i_4_n_0\,
      S(0) => \read_reg_Out1[9]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite is
  port (
    FSM_sequential_axi_lite_rstate_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \write_reg_In1_reg[30]\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 30 downto 0 );
    read_Out1 : in STD_LOGIC_VECTOR ( 29 downto 0 );
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite : entity is "MultiplyB_ip_axi_lite";
end System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite;

architecture STRUCTURE of System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite is
  signal read_reg_Out1 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal read_reg_ip_timestamp : STD_LOGIC_VECTOR ( 30 to 30 );
  signal reg_enb_In1 : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal top_data_write : STD_LOGIC_VECTOR ( 0 to 0 );
  signal u_MultiplyB_ip_axi_lite_module_inst_n_1 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_10 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_11 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_12 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_13 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_14 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_15 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_16 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_17 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_18 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_19 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_2 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_20 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_21 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_22 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_23 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_24 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_25 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_26 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_27 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_28 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_29 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_3 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_30 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_4 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_5 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_6 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_7 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_8 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_9 : STD_LOGIC;
begin
u_MultiplyB_ip_addr_decoder_inst: entity work.System_MultiplyB_ip_0_0_MultiplyB_ip_addr_decoder
     port map (
      AR(0) => reset,
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      E(0) => reg_enb_In1,
      Q(30) => u_MultiplyB_ip_axi_lite_module_inst_n_1,
      Q(29) => u_MultiplyB_ip_axi_lite_module_inst_n_2,
      Q(28) => u_MultiplyB_ip_axi_lite_module_inst_n_3,
      Q(27) => u_MultiplyB_ip_axi_lite_module_inst_n_4,
      Q(26) => u_MultiplyB_ip_axi_lite_module_inst_n_5,
      Q(25) => u_MultiplyB_ip_axi_lite_module_inst_n_6,
      Q(24) => u_MultiplyB_ip_axi_lite_module_inst_n_7,
      Q(23) => u_MultiplyB_ip_axi_lite_module_inst_n_8,
      Q(22) => u_MultiplyB_ip_axi_lite_module_inst_n_9,
      Q(21) => u_MultiplyB_ip_axi_lite_module_inst_n_10,
      Q(20) => u_MultiplyB_ip_axi_lite_module_inst_n_11,
      Q(19) => u_MultiplyB_ip_axi_lite_module_inst_n_12,
      Q(18) => u_MultiplyB_ip_axi_lite_module_inst_n_13,
      Q(17) => u_MultiplyB_ip_axi_lite_module_inst_n_14,
      Q(16) => u_MultiplyB_ip_axi_lite_module_inst_n_15,
      Q(15) => u_MultiplyB_ip_axi_lite_module_inst_n_16,
      Q(14) => u_MultiplyB_ip_axi_lite_module_inst_n_17,
      Q(13) => u_MultiplyB_ip_axi_lite_module_inst_n_18,
      Q(12) => u_MultiplyB_ip_axi_lite_module_inst_n_19,
      Q(11) => u_MultiplyB_ip_axi_lite_module_inst_n_20,
      Q(10) => u_MultiplyB_ip_axi_lite_module_inst_n_21,
      Q(9) => u_MultiplyB_ip_axi_lite_module_inst_n_22,
      Q(8) => u_MultiplyB_ip_axi_lite_module_inst_n_23,
      Q(7) => u_MultiplyB_ip_axi_lite_module_inst_n_24,
      Q(6) => u_MultiplyB_ip_axi_lite_module_inst_n_25,
      Q(5) => u_MultiplyB_ip_axi_lite_module_inst_n_26,
      Q(4) => u_MultiplyB_ip_axi_lite_module_inst_n_27,
      Q(3) => u_MultiplyB_ip_axi_lite_module_inst_n_28,
      Q(2) => u_MultiplyB_ip_axi_lite_module_inst_n_29,
      Q(1) => u_MultiplyB_ip_axi_lite_module_inst_n_30,
      Q(0) => top_data_write(0),
      read_Out1(29 downto 0) => read_Out1(29 downto 0),
      \read_reg_Out1_reg[31]_0\(30 downto 0) => read_reg_Out1(31 downto 1),
      read_reg_ip_timestamp(0) => read_reg_ip_timestamp(30),
      \write_reg_In1_reg[30]_0\(30 downto 0) => \write_reg_In1_reg[30]\(30 downto 0)
    );
u_MultiplyB_ip_axi_lite_module_inst: entity work.System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite_module
     port map (
      AR(0) => reset,
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(13 downto 0),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(13 downto 0),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_RDATA(31 downto 0) => AXI4_Lite_RDATA(31 downto 0),
      \AXI4_Lite_RDATA_tmp_reg[31]_0\(30 downto 0) => read_reg_Out1(31 downto 1),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_WDATA(30 downto 0) => AXI4_Lite_WDATA(30 downto 0),
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      E(0) => reg_enb_In1,
      \FSM_onehot_axi_lite_wstate_reg[2]_0\(1 downto 0) => Q(1 downto 0),
      FSM_sequential_axi_lite_rstate_reg_0 => FSM_sequential_axi_lite_rstate_reg,
      IPCORE_RESETN => IPCORE_RESETN,
      Q(30) => u_MultiplyB_ip_axi_lite_module_inst_n_1,
      Q(29) => u_MultiplyB_ip_axi_lite_module_inst_n_2,
      Q(28) => u_MultiplyB_ip_axi_lite_module_inst_n_3,
      Q(27) => u_MultiplyB_ip_axi_lite_module_inst_n_4,
      Q(26) => u_MultiplyB_ip_axi_lite_module_inst_n_5,
      Q(25) => u_MultiplyB_ip_axi_lite_module_inst_n_6,
      Q(24) => u_MultiplyB_ip_axi_lite_module_inst_n_7,
      Q(23) => u_MultiplyB_ip_axi_lite_module_inst_n_8,
      Q(22) => u_MultiplyB_ip_axi_lite_module_inst_n_9,
      Q(21) => u_MultiplyB_ip_axi_lite_module_inst_n_10,
      Q(20) => u_MultiplyB_ip_axi_lite_module_inst_n_11,
      Q(19) => u_MultiplyB_ip_axi_lite_module_inst_n_12,
      Q(18) => u_MultiplyB_ip_axi_lite_module_inst_n_13,
      Q(17) => u_MultiplyB_ip_axi_lite_module_inst_n_14,
      Q(16) => u_MultiplyB_ip_axi_lite_module_inst_n_15,
      Q(15) => u_MultiplyB_ip_axi_lite_module_inst_n_16,
      Q(14) => u_MultiplyB_ip_axi_lite_module_inst_n_17,
      Q(13) => u_MultiplyB_ip_axi_lite_module_inst_n_18,
      Q(12) => u_MultiplyB_ip_axi_lite_module_inst_n_19,
      Q(11) => u_MultiplyB_ip_axi_lite_module_inst_n_20,
      Q(10) => u_MultiplyB_ip_axi_lite_module_inst_n_21,
      Q(9) => u_MultiplyB_ip_axi_lite_module_inst_n_22,
      Q(8) => u_MultiplyB_ip_axi_lite_module_inst_n_23,
      Q(7) => u_MultiplyB_ip_axi_lite_module_inst_n_24,
      Q(6) => u_MultiplyB_ip_axi_lite_module_inst_n_25,
      Q(5) => u_MultiplyB_ip_axi_lite_module_inst_n_26,
      Q(4) => u_MultiplyB_ip_axi_lite_module_inst_n_27,
      Q(3) => u_MultiplyB_ip_axi_lite_module_inst_n_28,
      Q(2) => u_MultiplyB_ip_axi_lite_module_inst_n_29,
      Q(1) => u_MultiplyB_ip_axi_lite_module_inst_n_30,
      Q(0) => top_data_write(0),
      read_reg_ip_timestamp(0) => read_reg_ip_timestamp(30)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0_MultiplyB_ip_dut is
  port (
    read_Out1 : out STD_LOGIC_VECTOR ( 29 downto 0 );
    \read_reg_Out1_reg[31]\ : in STD_LOGIC_VECTOR ( 30 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of System_MultiplyB_ip_0_0_MultiplyB_ip_dut : entity is "MultiplyB_ip_dut";
end System_MultiplyB_ip_0_0_MultiplyB_ip_dut;

architecture STRUCTURE of System_MultiplyB_ip_0_0_MultiplyB_ip_dut is
begin
u_MultiplyB_ip_src_MultiplyBy10: entity work.System_MultiplyB_ip_0_0_MultiplyB_ip_src_MultiplyBy10
     port map (
      read_Out1(29 downto 0) => read_Out1(29 downto 0),
      \read_reg_Out1_reg[31]\(30 downto 0) => \read_reg_Out1_reg[31]\(30 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0_MultiplyB_ip is
  port (
    AXI4_Lite_RVALID : out STD_LOGIC;
    AXI4_Lite_BVALID : out STD_LOGIC;
    AXI4_Lite_WREADY : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of System_MultiplyB_ip_0_0_MultiplyB_ip : entity is "MultiplyB_ip";
end System_MultiplyB_ip_0_0_MultiplyB_ip;

architecture STRUCTURE of System_MultiplyB_ip_0_0_MultiplyB_ip is
  signal read_Out1 : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal write_reg_In1 : STD_LOGIC_VECTOR ( 30 downto 0 );
begin
u_MultiplyB_ip_axi_lite_inst: entity work.System_MultiplyB_ip_0_0_MultiplyB_ip_axi_lite
     port map (
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(13 downto 0),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(13 downto 0),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_RDATA(31 downto 0) => AXI4_Lite_RDATA(31 downto 0),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_WDATA(30 downto 0) => AXI4_Lite_WDATA(30 downto 0),
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      FSM_sequential_axi_lite_rstate_reg => AXI4_Lite_RVALID,
      IPCORE_RESETN => IPCORE_RESETN,
      Q(1) => AXI4_Lite_BVALID,
      Q(0) => AXI4_Lite_WREADY,
      read_Out1(29 downto 0) => read_Out1(31 downto 2),
      \write_reg_In1_reg[30]\(30 downto 0) => write_reg_In1(30 downto 0)
    );
u_MultiplyB_ip_dut_inst: entity work.System_MultiplyB_ip_0_0_MultiplyB_ip_dut
     port map (
      read_Out1(29 downto 0) => read_Out1(31 downto 2),
      \read_reg_Out1_reg[31]\(30 downto 0) => write_reg_In1(30 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity System_MultiplyB_ip_0_0 is
  port (
    IPCORE_CLK : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARESETN : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_WREADY : out STD_LOGIC;
    AXI4_Lite_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_BVALID : out STD_LOGIC;
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_RVALID : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of System_MultiplyB_ip_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of System_MultiplyB_ip_0_0 : entity is "System_MultiplyB_ip_0_0,MultiplyB_ip,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of System_MultiplyB_ip_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of System_MultiplyB_ip_0_0 : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of System_MultiplyB_ip_0_0 : entity is "MultiplyB_ip,Vivado 2018.3";
end System_MultiplyB_ip_0_0;

architecture STRUCTURE of System_MultiplyB_ip_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of AXI4_Lite_ACLK : signal is "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of AXI4_Lite_ACLK : signal is "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_ARESETN : signal is "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST";
  attribute x_interface_parameter of AXI4_Lite_ARESETN : signal is "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_ARREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY";
  attribute x_interface_info of AXI4_Lite_ARVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID";
  attribute x_interface_info of AXI4_Lite_AWREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY";
  attribute x_interface_info of AXI4_Lite_AWVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID";
  attribute x_interface_info of AXI4_Lite_BREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY";
  attribute x_interface_info of AXI4_Lite_BVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID";
  attribute x_interface_info of AXI4_Lite_RREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY";
  attribute x_interface_info of AXI4_Lite_RVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID";
  attribute x_interface_info of AXI4_Lite_WREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY";
  attribute x_interface_info of AXI4_Lite_WVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID";
  attribute x_interface_info of IPCORE_CLK : signal is "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK";
  attribute x_interface_parameter of IPCORE_CLK : signal is "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of IPCORE_RESETN : signal is "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST";
  attribute x_interface_parameter of IPCORE_RESETN : signal is "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_ARADDR : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR";
  attribute x_interface_info of AXI4_Lite_AWADDR : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR";
  attribute x_interface_parameter of AXI4_Lite_AWADDR : signal is "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_BRESP : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP";
  attribute x_interface_info of AXI4_Lite_RDATA : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA";
  attribute x_interface_info of AXI4_Lite_RRESP : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP";
  attribute x_interface_info of AXI4_Lite_WDATA : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA";
  attribute x_interface_info of AXI4_Lite_WSTRB : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB";
begin
  AXI4_Lite_BRESP(1) <= \<const0>\;
  AXI4_Lite_BRESP(0) <= \<const0>\;
  AXI4_Lite_RRESP(1) <= \<const0>\;
  AXI4_Lite_RRESP(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.System_MultiplyB_ip_0_0_MultiplyB_ip
     port map (
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(15 downto 2),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(15 downto 2),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_BVALID => AXI4_Lite_BVALID,
      AXI4_Lite_RDATA(31 downto 0) => AXI4_Lite_RDATA(31 downto 0),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_RVALID => AXI4_Lite_RVALID,
      AXI4_Lite_WDATA(30 downto 0) => AXI4_Lite_WDATA(30 downto 0),
      AXI4_Lite_WREADY => AXI4_Lite_WREADY,
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      IPCORE_RESETN => IPCORE_RESETN
    );
end STRUCTURE;
