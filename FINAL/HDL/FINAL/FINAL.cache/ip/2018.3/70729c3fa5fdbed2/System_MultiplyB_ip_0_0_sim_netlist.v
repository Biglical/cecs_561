// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
// Date        : Sat May  4 07:26:36 2019
// Host        : DESKTOP-MC6SQ8S running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ System_MultiplyB_ip_0_0_sim_netlist.v
// Design      : System_MultiplyB_ip_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip
   (AXI4_Lite_RVALID,
    AXI4_Lite_BVALID,
    AXI4_Lite_WREADY,
    out_valid_reg,
    AXI4_Stream_Master_TDATA,
    AXI4_Lite_ARREADY,
    AXI4_Lite_AWREADY,
    AXI4_Stream_Slave_TREADY,
    AXI4_Lite_RDATA,
    AXI4_Stream_Master_TLAST,
    AXI4_Stream_Master_TREADY,
    AXI4_Stream_Slave_TVALID,
    AXI4_Lite_ACLK,
    AXI4_Lite_AWADDR,
    AXI4_Lite_WDATA,
    IPCORE_CLK,
    AXI4_Stream_Slave_TDATA,
    AXI4_Lite_ARVALID,
    AXI4_Lite_ARADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_WSTRB,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN);
  output AXI4_Lite_RVALID;
  output AXI4_Lite_BVALID;
  output AXI4_Lite_WREADY;
  output out_valid_reg;
  output [31:0]AXI4_Stream_Master_TDATA;
  output AXI4_Lite_ARREADY;
  output AXI4_Lite_AWREADY;
  output AXI4_Stream_Slave_TREADY;
  output [0:0]AXI4_Lite_RDATA;
  output AXI4_Stream_Master_TLAST;
  input AXI4_Stream_Master_TREADY;
  input AXI4_Stream_Slave_TVALID;
  input AXI4_Lite_ACLK;
  input [13:0]AXI4_Lite_AWADDR;
  input [31:0]AXI4_Lite_WDATA;
  input IPCORE_CLK;
  input [31:0]AXI4_Stream_Slave_TDATA;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_RREADY;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;

  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire AXI4_Lite_BVALID;
  wire [0:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire AXI4_Lite_RVALID;
  wire [31:0]AXI4_Lite_WDATA;
  wire AXI4_Lite_WREADY;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [31:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire [31:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire IPCORE_CLK;
  wire IPCORE_RESETN;
  wire In_rsvd;
  wire [30:0]Out_tmp;
  wire internal_ready;
  wire internal_ready_delayed;
  wire out_valid_reg;
  wire relop_relop1;
  wire reset;
  wire u_MultiplyB_ip_axi4_stream_slave_inst_n_4;
  wire u_MultiplyB_ip_axi_lite_inst_n_3;
  wire u_MultiplyB_ip_axi_lite_inst_n_37;
  wire u_MultiplyB_ip_axi_lite_inst_n_38;
  wire u_MultiplyB_ip_axi_lite_inst_n_39;
  wire u_MultiplyB_ip_axi_lite_inst_n_4;
  wire u_MultiplyB_ip_axi_lite_inst_n_40;
  wire u_MultiplyB_ip_axi_lite_inst_n_41;
  wire u_MultiplyB_ip_axi_lite_inst_n_42;
  wire u_MultiplyB_ip_axi_lite_inst_n_43;
  wire u_MultiplyB_ip_axi_lite_inst_n_44;
  wire u_MultiplyB_ip_axi_lite_inst_n_45;
  wire u_MultiplyB_ip_axi_lite_inst_n_46;
  wire u_MultiplyB_ip_axi_lite_inst_n_47;
  wire u_MultiplyB_ip_axi_lite_inst_n_48;
  wire u_MultiplyB_ip_axi_lite_inst_n_49;
  wire u_MultiplyB_ip_axi_lite_inst_n_5;
  wire u_MultiplyB_ip_axi_lite_inst_n_50;
  wire u_MultiplyB_ip_axi_lite_inst_n_51;
  wire u_MultiplyB_ip_axi_lite_inst_n_52;
  wire u_MultiplyB_ip_axi_lite_inst_n_53;
  wire u_MultiplyB_ip_axi_lite_inst_n_54;
  wire u_MultiplyB_ip_axi_lite_inst_n_55;
  wire u_MultiplyB_ip_axi_lite_inst_n_56;
  wire u_MultiplyB_ip_axi_lite_inst_n_57;
  wire u_MultiplyB_ip_axi_lite_inst_n_58;
  wire u_MultiplyB_ip_axi_lite_inst_n_59;
  wire u_MultiplyB_ip_axi_lite_inst_n_60;
  wire u_MultiplyB_ip_axi_lite_inst_n_61;
  wire u_MultiplyB_ip_axi_lite_inst_n_62;
  wire u_MultiplyB_ip_axi_lite_inst_n_63;
  wire u_MultiplyB_ip_axi_lite_inst_n_64;
  wire u_MultiplyB_ip_axi_lite_inst_n_65;
  wire u_MultiplyB_ip_axi_lite_inst_n_66;
  wire u_MultiplyB_ip_axi_lite_inst_n_67;
  wire \u_MultiplyB_ip_fifo_data_inst/out_valid ;
  wire [31:2]wr_din;
  wire [30:0]write_reg_packet_size_axi4_stream_master;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_master u_MultiplyB_ip_axi4_stream_master_inst
       (.AR(u_MultiplyB_ip_axi_lite_inst_n_67),
        .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),
        .AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .CO(relop_relop1),
        .IPCORE_CLK(IPCORE_CLK),
        .In_rsvd(In_rsvd),
        .\Out_tmp_reg[0] (reset),
        .Q(write_reg_packet_size_axi4_stream_master),
        .S({u_MultiplyB_ip_axi_lite_inst_n_61,u_MultiplyB_ip_axi_lite_inst_n_62,u_MultiplyB_ip_axi_lite_inst_n_63,u_MultiplyB_ip_axi_lite_inst_n_64}),
        .auto_tlast0_carry__0_i_1({u_MultiplyB_ip_axi_lite_inst_n_41,u_MultiplyB_ip_axi_lite_inst_n_42,u_MultiplyB_ip_axi_lite_inst_n_43,u_MultiplyB_ip_axi_lite_inst_n_44}),
        .auto_tlast0_carry__0_i_3({u_MultiplyB_ip_axi_lite_inst_n_45,u_MultiplyB_ip_axi_lite_inst_n_46,u_MultiplyB_ip_axi_lite_inst_n_47,u_MultiplyB_ip_axi_lite_inst_n_48}),
        .auto_tlast0_carry__0_i_4({u_MultiplyB_ip_axi_lite_inst_n_49,u_MultiplyB_ip_axi_lite_inst_n_50,u_MultiplyB_ip_axi_lite_inst_n_51,u_MultiplyB_ip_axi_lite_inst_n_52}),
        .auto_tlast0_carry__1_i_2({u_MultiplyB_ip_axi_lite_inst_n_3,u_MultiplyB_ip_axi_lite_inst_n_4,u_MultiplyB_ip_axi_lite_inst_n_5}),
        .auto_tlast0_carry__1_i_3({u_MultiplyB_ip_axi_lite_inst_n_37,u_MultiplyB_ip_axi_lite_inst_n_38,u_MultiplyB_ip_axi_lite_inst_n_39,u_MultiplyB_ip_axi_lite_inst_n_40}),
        .auto_tlast0_carry_i_1({u_MultiplyB_ip_axi_lite_inst_n_53,u_MultiplyB_ip_axi_lite_inst_n_54,u_MultiplyB_ip_axi_lite_inst_n_55,u_MultiplyB_ip_axi_lite_inst_n_56}),
        .auto_tlast0_carry_i_3({u_MultiplyB_ip_axi_lite_inst_n_57,u_MultiplyB_ip_axi_lite_inst_n_58,u_MultiplyB_ip_axi_lite_inst_n_59,u_MultiplyB_ip_axi_lite_inst_n_60}),
        .\fifo_back_indx_reg[0] (u_MultiplyB_ip_axi_lite_inst_n_65),
        .\fifo_sample_count_reg[2] (u_MultiplyB_ip_axi_lite_inst_n_66),
        .internal_ready(internal_ready),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(\u_MultiplyB_ip_fifo_data_inst/out_valid ),
        .out_valid_reg(out_valid_reg),
        .\tlast_counter_out_reg[31]_0 (u_MultiplyB_ip_axi4_stream_slave_inst_n_4),
        .wr_din({wr_din,Out_tmp[0]}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_slave u_MultiplyB_ip_axi4_stream_slave_inst
       (.AR({u_MultiplyB_ip_axi_lite_inst_n_65,u_MultiplyB_ip_axi_lite_inst_n_66}),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .CO(relop_relop1),
        .IPCORE_CLK(IPCORE_CLK),
        .In_rsvd(In_rsvd),
        .Q(Out_tmp),
        .internal_ready(internal_ready),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(\u_MultiplyB_ip_fifo_data_inst/out_valid ),
        .out_valid_reg(u_MultiplyB_ip_axi4_stream_slave_inst_n_4));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite u_MultiplyB_ip_axi_lite_inst
       (.AR({u_MultiplyB_ip_axi_lite_inst_n_65,u_MultiplyB_ip_axi_lite_inst_n_66}),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARESETN_0(reset),
        .AXI4_Lite_ARESETN_1(u_MultiplyB_ip_axi_lite_inst_n_67),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .\FSM_onehot_axi_lite_wstate_reg[2] ({AXI4_Lite_BVALID,AXI4_Lite_WREADY}),
        .FSM_sequential_axi_lite_rstate_reg(AXI4_Lite_RVALID),
        .IPCORE_RESETN(IPCORE_RESETN),
        .Q(write_reg_packet_size_axi4_stream_master),
        .S({u_MultiplyB_ip_axi_lite_inst_n_61,u_MultiplyB_ip_axi_lite_inst_n_62,u_MultiplyB_ip_axi_lite_inst_n_63,u_MultiplyB_ip_axi_lite_inst_n_64}),
        .\write_reg_packet_size_axi4_stream_master_reg[12] ({u_MultiplyB_ip_axi_lite_inst_n_53,u_MultiplyB_ip_axi_lite_inst_n_54,u_MultiplyB_ip_axi_lite_inst_n_55,u_MultiplyB_ip_axi_lite_inst_n_56}),
        .\write_reg_packet_size_axi4_stream_master_reg[16] ({u_MultiplyB_ip_axi_lite_inst_n_49,u_MultiplyB_ip_axi_lite_inst_n_50,u_MultiplyB_ip_axi_lite_inst_n_51,u_MultiplyB_ip_axi_lite_inst_n_52}),
        .\write_reg_packet_size_axi4_stream_master_reg[20] ({u_MultiplyB_ip_axi_lite_inst_n_45,u_MultiplyB_ip_axi_lite_inst_n_46,u_MultiplyB_ip_axi_lite_inst_n_47,u_MultiplyB_ip_axi_lite_inst_n_48}),
        .\write_reg_packet_size_axi4_stream_master_reg[24] ({u_MultiplyB_ip_axi_lite_inst_n_41,u_MultiplyB_ip_axi_lite_inst_n_42,u_MultiplyB_ip_axi_lite_inst_n_43,u_MultiplyB_ip_axi_lite_inst_n_44}),
        .\write_reg_packet_size_axi4_stream_master_reg[28] ({u_MultiplyB_ip_axi_lite_inst_n_37,u_MultiplyB_ip_axi_lite_inst_n_38,u_MultiplyB_ip_axi_lite_inst_n_39,u_MultiplyB_ip_axi_lite_inst_n_40}),
        .\write_reg_packet_size_axi4_stream_master_reg[31] ({u_MultiplyB_ip_axi_lite_inst_n_3,u_MultiplyB_ip_axi_lite_inst_n_4,u_MultiplyB_ip_axi_lite_inst_n_5}),
        .\write_reg_packet_size_axi4_stream_master_reg[8] ({u_MultiplyB_ip_axi_lite_inst_n_57,u_MultiplyB_ip_axi_lite_inst_n_58,u_MultiplyB_ip_axi_lite_inst_n_59,u_MultiplyB_ip_axi_lite_inst_n_60}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_dut u_MultiplyB_ip_dut_inst
       (.Q(Out_tmp),
        .wr_din(wr_din));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic
   (D,
    data_int,
    \data_int_reg[30]_0 ,
    Q,
    \Out_tmp_reg[30] ,
    w_d1,
    cache_valid,
    AXI4_Stream_Slave_TVALID,
    \data_int_reg[1]_0 ,
    \data_int_reg[1]_1 ,
    \data_int_reg[1]_2 ,
    IPCORE_CLK,
    AXI4_Stream_Slave_TDATA,
    ADDRA,
    ADDRD);
  output [30:0]D;
  output [30:0]data_int;
  output [30:0]\data_int_reg[30]_0 ;
  input [30:0]Q;
  input [30:0]\Out_tmp_reg[30] ;
  input w_d1;
  input cache_valid;
  input AXI4_Stream_Slave_TVALID;
  input \data_int_reg[1]_0 ;
  input \data_int_reg[1]_1 ;
  input \data_int_reg[1]_2 ;
  input IPCORE_CLK;
  input [31:0]AXI4_Stream_Slave_TDATA;
  input [1:0]ADDRA;
  input [1:0]ADDRD;

  wire [1:0]ADDRA;
  wire [1:0]ADDRD;
  wire [31:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TVALID;
  wire [30:0]D;
  wire IPCORE_CLK;
  wire [30:0]\Out_tmp_reg[30] ;
  wire [30:0]Q;
  wire cache_valid;
  wire [30:0]data_int;
  wire \data_int_reg[1]_0 ;
  wire \data_int_reg[1]_1 ;
  wire \data_int_reg[1]_2 ;
  wire [30:0]\data_int_reg[30]_0 ;
  wire [30:0]p_1_out;
  wire ram_reg_0_3_30_31_n_0;
  wire w_d1;
  wire wr_en;
  wire [1:0]NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[0]_i_1 
       (.I0(Q[0]),
        .I1(data_int[0]),
        .I2(\Out_tmp_reg[30] [0]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[10]_i_1 
       (.I0(Q[10]),
        .I1(data_int[10]),
        .I2(\Out_tmp_reg[30] [10]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[11]_i_1 
       (.I0(Q[11]),
        .I1(data_int[11]),
        .I2(\Out_tmp_reg[30] [11]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[12]_i_1 
       (.I0(Q[12]),
        .I1(data_int[12]),
        .I2(\Out_tmp_reg[30] [12]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[13]_i_1 
       (.I0(Q[13]),
        .I1(data_int[13]),
        .I2(\Out_tmp_reg[30] [13]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[14]_i_1 
       (.I0(Q[14]),
        .I1(data_int[14]),
        .I2(\Out_tmp_reg[30] [14]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[15]_i_1 
       (.I0(Q[15]),
        .I1(data_int[15]),
        .I2(\Out_tmp_reg[30] [15]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[16]_i_1 
       (.I0(Q[16]),
        .I1(data_int[16]),
        .I2(\Out_tmp_reg[30] [16]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[17]_i_1 
       (.I0(Q[17]),
        .I1(data_int[17]),
        .I2(\Out_tmp_reg[30] [17]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[18]_i_1 
       (.I0(Q[18]),
        .I1(data_int[18]),
        .I2(\Out_tmp_reg[30] [18]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[19]_i_1 
       (.I0(Q[19]),
        .I1(data_int[19]),
        .I2(\Out_tmp_reg[30] [19]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[1]_i_1 
       (.I0(Q[1]),
        .I1(data_int[1]),
        .I2(\Out_tmp_reg[30] [1]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[20]_i_1 
       (.I0(Q[20]),
        .I1(data_int[20]),
        .I2(\Out_tmp_reg[30] [20]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[21]_i_1 
       (.I0(Q[21]),
        .I1(data_int[21]),
        .I2(\Out_tmp_reg[30] [21]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[22]_i_1 
       (.I0(Q[22]),
        .I1(data_int[22]),
        .I2(\Out_tmp_reg[30] [22]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[23]_i_1 
       (.I0(Q[23]),
        .I1(data_int[23]),
        .I2(\Out_tmp_reg[30] [23]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[24]_i_1 
       (.I0(Q[24]),
        .I1(data_int[24]),
        .I2(\Out_tmp_reg[30] [24]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[25]_i_1 
       (.I0(Q[25]),
        .I1(data_int[25]),
        .I2(\Out_tmp_reg[30] [25]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[25]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[26]_i_1 
       (.I0(Q[26]),
        .I1(data_int[26]),
        .I2(\Out_tmp_reg[30] [26]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[26]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[27]_i_1 
       (.I0(Q[27]),
        .I1(data_int[27]),
        .I2(\Out_tmp_reg[30] [27]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[27]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[28]_i_1 
       (.I0(Q[28]),
        .I1(data_int[28]),
        .I2(\Out_tmp_reg[30] [28]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[28]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[29]_i_1 
       (.I0(Q[29]),
        .I1(data_int[29]),
        .I2(\Out_tmp_reg[30] [29]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[29]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[2]_i_1 
       (.I0(Q[2]),
        .I1(data_int[2]),
        .I2(\Out_tmp_reg[30] [2]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[30]_i_2 
       (.I0(Q[30]),
        .I1(data_int[30]),
        .I2(\Out_tmp_reg[30] [30]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[30]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[3]_i_1 
       (.I0(Q[3]),
        .I1(data_int[3]),
        .I2(\Out_tmp_reg[30] [3]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[4]_i_1 
       (.I0(Q[4]),
        .I1(data_int[4]),
        .I2(\Out_tmp_reg[30] [4]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[5]_i_1 
       (.I0(Q[5]),
        .I1(data_int[5]),
        .I2(\Out_tmp_reg[30] [5]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[6]_i_1 
       (.I0(Q[6]),
        .I1(data_int[6]),
        .I2(\Out_tmp_reg[30] [6]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[7]_i_1 
       (.I0(Q[7]),
        .I1(data_int[7]),
        .I2(\Out_tmp_reg[30] [7]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[8]_i_1 
       (.I0(Q[8]),
        .I1(data_int[8]),
        .I2(\Out_tmp_reg[30] [8]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[9]_i_1 
       (.I0(Q[9]),
        .I1(data_int[9]),
        .I2(\Out_tmp_reg[30] [9]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[0]_i_1 
       (.I0(data_int[0]),
        .I1(\Out_tmp_reg[30] [0]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair60" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[10]_i_1 
       (.I0(data_int[10]),
        .I1(\Out_tmp_reg[30] [10]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [10]));
  (* SOFT_HLUTNM = "soft_lutpair62" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[11]_i_1 
       (.I0(data_int[11]),
        .I1(\Out_tmp_reg[30] [11]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [11]));
  (* SOFT_HLUTNM = "soft_lutpair64" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[12]_i_1 
       (.I0(data_int[12]),
        .I1(\Out_tmp_reg[30] [12]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [12]));
  (* SOFT_HLUTNM = "soft_lutpair66" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[13]_i_1 
       (.I0(data_int[13]),
        .I1(\Out_tmp_reg[30] [13]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [13]));
  (* SOFT_HLUTNM = "soft_lutpair47" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[14]_i_1 
       (.I0(data_int[14]),
        .I1(\Out_tmp_reg[30] [14]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [14]));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[15]_i_1 
       (.I0(data_int[15]),
        .I1(\Out_tmp_reg[30] [15]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [15]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[16]_i_1 
       (.I0(data_int[16]),
        .I1(\Out_tmp_reg[30] [16]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [16]));
  (* SOFT_HLUTNM = "soft_lutpair59" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[17]_i_1 
       (.I0(data_int[17]),
        .I1(\Out_tmp_reg[30] [17]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [17]));
  (* SOFT_HLUTNM = "soft_lutpair63" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[18]_i_1 
       (.I0(data_int[18]),
        .I1(\Out_tmp_reg[30] [18]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [18]));
  (* SOFT_HLUTNM = "soft_lutpair65" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[19]_i_1 
       (.I0(data_int[19]),
        .I1(\Out_tmp_reg[30] [19]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [19]));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[1]_i_1 
       (.I0(data_int[1]),
        .I1(\Out_tmp_reg[30] [1]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair67" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[20]_i_1 
       (.I0(data_int[20]),
        .I1(\Out_tmp_reg[30] [20]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [20]));
  (* SOFT_HLUTNM = "soft_lutpair70" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[21]_i_1 
       (.I0(data_int[21]),
        .I1(\Out_tmp_reg[30] [21]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [21]));
  (* SOFT_HLUTNM = "soft_lutpair69" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[22]_i_1 
       (.I0(data_int[22]),
        .I1(\Out_tmp_reg[30] [22]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [22]));
  (* SOFT_HLUTNM = "soft_lutpair68" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[23]_i_1 
       (.I0(data_int[23]),
        .I1(\Out_tmp_reg[30] [23]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [23]));
  (* SOFT_HLUTNM = "soft_lutpair61" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[24]_i_1 
       (.I0(data_int[24]),
        .I1(\Out_tmp_reg[30] [24]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [24]));
  (* SOFT_HLUTNM = "soft_lutpair49" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[25]_i_1 
       (.I0(data_int[25]),
        .I1(\Out_tmp_reg[30] [25]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [25]));
  (* SOFT_HLUTNM = "soft_lutpair51" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[26]_i_1 
       (.I0(data_int[26]),
        .I1(\Out_tmp_reg[30] [26]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [26]));
  (* SOFT_HLUTNM = "soft_lutpair53" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[27]_i_1 
       (.I0(data_int[27]),
        .I1(\Out_tmp_reg[30] [27]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [27]));
  (* SOFT_HLUTNM = "soft_lutpair55" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[28]_i_1 
       (.I0(data_int[28]),
        .I1(\Out_tmp_reg[30] [28]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [28]));
  (* SOFT_HLUTNM = "soft_lutpair57" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[29]_i_1 
       (.I0(data_int[29]),
        .I1(\Out_tmp_reg[30] [29]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [29]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[2]_i_1 
       (.I0(data_int[2]),
        .I1(\Out_tmp_reg[30] [2]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[30]_i_2 
       (.I0(data_int[30]),
        .I1(\Out_tmp_reg[30] [30]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [30]));
  (* SOFT_HLUTNM = "soft_lutpair46" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[3]_i_1 
       (.I0(data_int[3]),
        .I1(\Out_tmp_reg[30] [3]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair48" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[4]_i_1 
       (.I0(data_int[4]),
        .I1(\Out_tmp_reg[30] [4]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair50" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[5]_i_1 
       (.I0(data_int[5]),
        .I1(\Out_tmp_reg[30] [5]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair52" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[6]_i_1 
       (.I0(data_int[6]),
        .I1(\Out_tmp_reg[30] [6]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair54" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[7]_i_1 
       (.I0(data_int[7]),
        .I1(\Out_tmp_reg[30] [7]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair56" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[8]_i_1 
       (.I0(data_int[8]),
        .I1(\Out_tmp_reg[30] [8]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair58" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[9]_i_1 
       (.I0(data_int[9]),
        .I1(\Out_tmp_reg[30] [9]),
        .I2(w_d1),
        .O(\data_int_reg[30]_0 [9]));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[0]),
        .Q(data_int[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[10] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[10]),
        .Q(data_int[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[11] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[11]),
        .Q(data_int[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[12] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[12]),
        .Q(data_int[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[13] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[13]),
        .Q(data_int[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[14] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[14]),
        .Q(data_int[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[15] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[15]),
        .Q(data_int[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[16] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[16]),
        .Q(data_int[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[17] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[17]),
        .Q(data_int[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[18] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[18]),
        .Q(data_int[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[19] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[19]),
        .Q(data_int[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[1]),
        .Q(data_int[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[20] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[20]),
        .Q(data_int[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[21] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[21]),
        .Q(data_int[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[22] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[22]),
        .Q(data_int[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[23] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[23]),
        .Q(data_int[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[24] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[24]),
        .Q(data_int[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[25] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[25]),
        .Q(data_int[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[26] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[26]),
        .Q(data_int[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[27] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[27]),
        .Q(data_int[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[28] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[28]),
        .Q(data_int[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[29] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[29]),
        .Q(data_int[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[2]),
        .Q(data_int[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[30] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[30]),
        .Q(data_int[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[3] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[3]),
        .Q(data_int[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[4] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[4]),
        .Q(data_int[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[5] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[5]),
        .Q(data_int[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[6] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[6]),
        .Q(data_int[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[7] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[7]),
        .Q(data_int[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[8] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[8]),
        .Q(data_int[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[9] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out[9]),
        .Q(data_int[9]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_0_5
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[1:0]),
        .DIB(AXI4_Stream_Slave_TDATA[3:2]),
        .DIC(AXI4_Stream_Slave_TDATA[5:4]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out[1:0]),
        .DOB(p_1_out[3:2]),
        .DOC(p_1_out[5:4]),
        .DOD(NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  LUT4 #(
    .INIT(16'hAAA2)) 
    ram_reg_0_3_0_5_i_1
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\data_int_reg[1]_0 ),
        .I2(\data_int_reg[1]_1 ),
        .I3(\data_int_reg[1]_2 ),
        .O(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_12_17
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[13:12]),
        .DIB(AXI4_Stream_Slave_TDATA[15:14]),
        .DIC(AXI4_Stream_Slave_TDATA[17:16]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out[13:12]),
        .DOB(p_1_out[15:14]),
        .DOC(p_1_out[17:16]),
        .DOD(NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_18_23
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[19:18]),
        .DIB(AXI4_Stream_Slave_TDATA[21:20]),
        .DIC(AXI4_Stream_Slave_TDATA[23:22]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out[19:18]),
        .DOB(p_1_out[21:20]),
        .DOC(p_1_out[23:22]),
        .DOD(NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_24_29
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[25:24]),
        .DIB(AXI4_Stream_Slave_TDATA[27:26]),
        .DIC(AXI4_Stream_Slave_TDATA[29:28]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out[25:24]),
        .DOB(p_1_out[27:26]),
        .DOC(p_1_out[29:28]),
        .DOD(NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_30_31
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[31:30]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA({ram_reg_0_3_30_31_n_0,p_1_out[30]}),
        .DOB(NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_6_11
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(AXI4_Stream_Slave_TDATA[7:6]),
        .DIB(AXI4_Stream_Slave_TDATA[9:8]),
        .DIC(AXI4_Stream_Slave_TDATA[11:10]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out[7:6]),
        .DOB(p_1_out[9:8]),
        .DOC(p_1_out[11:10]),
        .DOD(NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
endmodule

(* ORIG_REF_NAME = "MultiplyB_ip_SimpleDualPortRAM_generic" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0
   (D,
    data_int,
    \data_int_reg[31]_0 ,
    wr_en,
    Q,
    \Out_tmp_reg[31] ,
    w_d1,
    cache_valid,
    internal_ready_delayed,
    out_valid,
    Num,
    IPCORE_CLK,
    wr_din,
    ADDRA,
    ADDRD);
  output [31:0]D;
  output [31:0]data_int;
  output [31:0]\data_int_reg[31]_0 ;
  output wr_en;
  input [31:0]Q;
  input [31:0]\Out_tmp_reg[31] ;
  input w_d1;
  input cache_valid;
  input internal_ready_delayed;
  input out_valid;
  input [2:0]Num;
  input IPCORE_CLK;
  input [30:0]wr_din;
  input [1:0]ADDRA;
  input [1:0]ADDRD;

  wire [1:0]ADDRA;
  wire [1:0]ADDRD;
  wire [31:0]D;
  wire IPCORE_CLK;
  wire [2:0]Num;
  wire [31:0]\Out_tmp_reg[31] ;
  wire [31:0]Q;
  wire cache_valid;
  wire [31:0]data_int;
  wire [31:0]\data_int_reg[31]_0 ;
  wire internal_ready_delayed;
  wire out_valid;
  wire [31:0]p_1_out__0;
  wire w_d1;
  wire [30:0]wr_din;
  wire wr_en;
  wire [1:0]NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED;
  wire [1:0]NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[0]_i_1__0 
       (.I0(Q[0]),
        .I1(data_int[0]),
        .I2(\Out_tmp_reg[31] [0]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[10]_i_1__0 
       (.I0(Q[10]),
        .I1(data_int[10]),
        .I2(\Out_tmp_reg[31] [10]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[10]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[11]_i_1__0 
       (.I0(Q[11]),
        .I1(data_int[11]),
        .I2(\Out_tmp_reg[31] [11]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[11]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[12]_i_1__0 
       (.I0(Q[12]),
        .I1(data_int[12]),
        .I2(\Out_tmp_reg[31] [12]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[12]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[13]_i_1__0 
       (.I0(Q[13]),
        .I1(data_int[13]),
        .I2(\Out_tmp_reg[31] [13]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[13]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[14]_i_1__0 
       (.I0(Q[14]),
        .I1(data_int[14]),
        .I2(\Out_tmp_reg[31] [14]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[14]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[15]_i_1__0 
       (.I0(Q[15]),
        .I1(data_int[15]),
        .I2(\Out_tmp_reg[31] [15]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[15]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[16]_i_1__0 
       (.I0(Q[16]),
        .I1(data_int[16]),
        .I2(\Out_tmp_reg[31] [16]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[16]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[17]_i_1__0 
       (.I0(Q[17]),
        .I1(data_int[17]),
        .I2(\Out_tmp_reg[31] [17]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[17]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[18]_i_1__0 
       (.I0(Q[18]),
        .I1(data_int[18]),
        .I2(\Out_tmp_reg[31] [18]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[18]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[19]_i_1__0 
       (.I0(Q[19]),
        .I1(data_int[19]),
        .I2(\Out_tmp_reg[31] [19]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[19]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[1]_i_1__0 
       (.I0(Q[1]),
        .I1(data_int[1]),
        .I2(\Out_tmp_reg[31] [1]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[20]_i_1__0 
       (.I0(Q[20]),
        .I1(data_int[20]),
        .I2(\Out_tmp_reg[31] [20]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[20]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[21]_i_1__0 
       (.I0(Q[21]),
        .I1(data_int[21]),
        .I2(\Out_tmp_reg[31] [21]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[21]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[22]_i_1__0 
       (.I0(Q[22]),
        .I1(data_int[22]),
        .I2(\Out_tmp_reg[31] [22]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[22]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[23]_i_1__0 
       (.I0(Q[23]),
        .I1(data_int[23]),
        .I2(\Out_tmp_reg[31] [23]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[23]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[24]_i_1__0 
       (.I0(Q[24]),
        .I1(data_int[24]),
        .I2(\Out_tmp_reg[31] [24]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[25]_i_1__0 
       (.I0(Q[25]),
        .I1(data_int[25]),
        .I2(\Out_tmp_reg[31] [25]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[25]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[26]_i_1__0 
       (.I0(Q[26]),
        .I1(data_int[26]),
        .I2(\Out_tmp_reg[31] [26]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[26]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[27]_i_1__0 
       (.I0(Q[27]),
        .I1(data_int[27]),
        .I2(\Out_tmp_reg[31] [27]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[27]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[28]_i_1__0 
       (.I0(Q[28]),
        .I1(data_int[28]),
        .I2(\Out_tmp_reg[31] [28]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[28]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[29]_i_1__0 
       (.I0(Q[29]),
        .I1(data_int[29]),
        .I2(\Out_tmp_reg[31] [29]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[29]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[2]_i_1__0 
       (.I0(Q[2]),
        .I1(data_int[2]),
        .I2(\Out_tmp_reg[31] [2]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[30]_i_1__0 
       (.I0(Q[30]),
        .I1(data_int[30]),
        .I2(\Out_tmp_reg[31] [30]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[30]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[31]_i_2 
       (.I0(Q[31]),
        .I1(data_int[31]),
        .I2(\Out_tmp_reg[31] [31]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[31]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[3]_i_1__0 
       (.I0(Q[3]),
        .I1(data_int[3]),
        .I2(\Out_tmp_reg[31] [3]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[4]_i_1__0 
       (.I0(Q[4]),
        .I1(data_int[4]),
        .I2(\Out_tmp_reg[31] [4]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[5]_i_1__0 
       (.I0(Q[5]),
        .I1(data_int[5]),
        .I2(\Out_tmp_reg[31] [5]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[5]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[6]_i_1__0 
       (.I0(Q[6]),
        .I1(data_int[6]),
        .I2(\Out_tmp_reg[31] [6]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[6]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[7]_i_1__0 
       (.I0(Q[7]),
        .I1(data_int[7]),
        .I2(\Out_tmp_reg[31] [7]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[7]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[8]_i_1__0 
       (.I0(Q[8]),
        .I1(data_int[8]),
        .I2(\Out_tmp_reg[31] [8]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[8]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hAAAACCF0)) 
    \Out_tmp[9]_i_1__0 
       (.I0(Q[9]),
        .I1(data_int[9]),
        .I2(\Out_tmp_reg[31] [9]),
        .I3(w_d1),
        .I4(cache_valid),
        .O(D[9]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[0]_i_1__0 
       (.I0(data_int[0]),
        .I1(\Out_tmp_reg[31] [0]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [0]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[10]_i_1__0 
       (.I0(data_int[10]),
        .I1(\Out_tmp_reg[31] [10]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [10]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[11]_i_1__0 
       (.I0(data_int[11]),
        .I1(\Out_tmp_reg[31] [11]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [11]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[12]_i_1__0 
       (.I0(data_int[12]),
        .I1(\Out_tmp_reg[31] [12]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [12]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[13]_i_1__0 
       (.I0(data_int[13]),
        .I1(\Out_tmp_reg[31] [13]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [13]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[14]_i_1__0 
       (.I0(data_int[14]),
        .I1(\Out_tmp_reg[31] [14]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [14]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[15]_i_1__0 
       (.I0(data_int[15]),
        .I1(\Out_tmp_reg[31] [15]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [15]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[16]_i_1__0 
       (.I0(data_int[16]),
        .I1(\Out_tmp_reg[31] [16]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [16]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[17]_i_1__0 
       (.I0(data_int[17]),
        .I1(\Out_tmp_reg[31] [17]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [17]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[18]_i_1__0 
       (.I0(data_int[18]),
        .I1(\Out_tmp_reg[31] [18]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [18]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[19]_i_1__0 
       (.I0(data_int[19]),
        .I1(\Out_tmp_reg[31] [19]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [19]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[1]_i_1__0 
       (.I0(data_int[1]),
        .I1(\Out_tmp_reg[31] [1]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [1]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[20]_i_1__0 
       (.I0(data_int[20]),
        .I1(\Out_tmp_reg[31] [20]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [20]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[21]_i_1__0 
       (.I0(data_int[21]),
        .I1(\Out_tmp_reg[31] [21]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [21]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[22]_i_1__0 
       (.I0(data_int[22]),
        .I1(\Out_tmp_reg[31] [22]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [22]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[23]_i_1__0 
       (.I0(data_int[23]),
        .I1(\Out_tmp_reg[31] [23]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [23]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[24]_i_1__0 
       (.I0(data_int[24]),
        .I1(\Out_tmp_reg[31] [24]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [24]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[25]_i_1__0 
       (.I0(data_int[25]),
        .I1(\Out_tmp_reg[31] [25]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [25]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[26]_i_1__0 
       (.I0(data_int[26]),
        .I1(\Out_tmp_reg[31] [26]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [26]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[27]_i_1__0 
       (.I0(data_int[27]),
        .I1(\Out_tmp_reg[31] [27]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [27]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[28]_i_1__0 
       (.I0(data_int[28]),
        .I1(\Out_tmp_reg[31] [28]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [28]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[29]_i_1__0 
       (.I0(data_int[29]),
        .I1(\Out_tmp_reg[31] [29]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [29]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[2]_i_1__0 
       (.I0(data_int[2]),
        .I1(\Out_tmp_reg[31] [2]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [2]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[30]_i_1__0 
       (.I0(data_int[30]),
        .I1(\Out_tmp_reg[31] [30]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [30]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[31]_i_2 
       (.I0(data_int[31]),
        .I1(\Out_tmp_reg[31] [31]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [31]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[3]_i_1__0 
       (.I0(data_int[3]),
        .I1(\Out_tmp_reg[31] [3]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [3]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[4]_i_1__0 
       (.I0(data_int[4]),
        .I1(\Out_tmp_reg[31] [4]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[5]_i_1__0 
       (.I0(data_int[5]),
        .I1(\Out_tmp_reg[31] [5]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [5]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[6]_i_1__0 
       (.I0(data_int[6]),
        .I1(\Out_tmp_reg[31] [6]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [6]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[7]_i_1__0 
       (.I0(data_int[7]),
        .I1(\Out_tmp_reg[31] [7]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [7]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[8]_i_1__0 
       (.I0(data_int[8]),
        .I1(\Out_tmp_reg[31] [8]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [8]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'hAC)) 
    \cache_data[9]_i_1__0 
       (.I0(data_int[9]),
        .I1(\Out_tmp_reg[31] [9]),
        .I2(w_d1),
        .O(\data_int_reg[31]_0 [9]));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[0]),
        .Q(data_int[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[10] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[10]),
        .Q(data_int[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[11] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[11]),
        .Q(data_int[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[12] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[12]),
        .Q(data_int[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[13] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[13]),
        .Q(data_int[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[14] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[14]),
        .Q(data_int[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[15] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[15]),
        .Q(data_int[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[16] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[16]),
        .Q(data_int[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[17] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[17]),
        .Q(data_int[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[18] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[18]),
        .Q(data_int[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[19] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[19]),
        .Q(data_int[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[1]),
        .Q(data_int[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[20] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[20]),
        .Q(data_int[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[21] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[21]),
        .Q(data_int[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[22] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[22]),
        .Q(data_int[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[23] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[23]),
        .Q(data_int[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[24] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[24]),
        .Q(data_int[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[25] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[25]),
        .Q(data_int[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[26] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[26]),
        .Q(data_int[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[27] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[27]),
        .Q(data_int[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[28] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[28]),
        .Q(data_int[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[29] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[29]),
        .Q(data_int[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[2]),
        .Q(data_int[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[30] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[30]),
        .Q(data_int[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[31] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[31]),
        .Q(data_int[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[3] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[3]),
        .Q(data_int[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[4] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[4]),
        .Q(data_int[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[5] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[5]),
        .Q(data_int[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[6] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[6]),
        .Q(data_int[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[7] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[7]),
        .Q(data_int[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[8] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[8]),
        .Q(data_int[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \data_int_reg[9] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__0[9]),
        .Q(data_int[9]),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "5" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_0_5
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA({wr_din[0],1'b0}),
        .DIB(wr_din[2:1]),
        .DIC(wr_din[4:3]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out__0[1:0]),
        .DOB(p_1_out__0[3:2]),
        .DOC(p_1_out__0[5:4]),
        .DOD(NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  LUT5 #(
    .INIT(32'h88888088)) 
    ram_reg_0_3_0_5_i_1__0
       (.I0(internal_ready_delayed),
        .I1(out_valid),
        .I2(Num[0]),
        .I3(Num[2]),
        .I4(Num[1]),
        .O(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "12" *) 
  (* ram_slice_end = "17" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_12_17
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(wr_din[12:11]),
        .DIB(wr_din[14:13]),
        .DIC(wr_din[16:15]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out__0[13:12]),
        .DOB(p_1_out__0[15:14]),
        .DOC(p_1_out__0[17:16]),
        .DOD(NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "18" *) 
  (* ram_slice_end = "23" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_18_23
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(wr_din[18:17]),
        .DIB(wr_din[20:19]),
        .DIC(wr_din[22:21]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out__0[19:18]),
        .DOB(p_1_out__0[21:20]),
        .DOC(p_1_out__0[23:22]),
        .DOD(NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "24" *) 
  (* ram_slice_end = "29" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_24_29
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(wr_din[24:23]),
        .DIB(wr_din[26:25]),
        .DIC(wr_din[28:27]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out__0[25:24]),
        .DOB(p_1_out__0[27:26]),
        .DOC(p_1_out__0[29:28]),
        .DOD(NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "30" *) 
  (* ram_slice_end = "31" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_30_31
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(wr_din[30:29]),
        .DIB({1'b0,1'b0}),
        .DIC({1'b0,1'b0}),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out__0[31:30]),
        .DOB(NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED[1:0]),
        .DOC(NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED[1:0]),
        .DOD(NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "6" *) 
  (* ram_slice_end = "11" *) 
  RAM32M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    ram_reg_0_3_6_11
       (.ADDRA({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRB({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRC({1'b0,1'b0,1'b0,ADDRA}),
        .ADDRD({1'b0,1'b0,1'b0,ADDRD}),
        .DIA(wr_din[6:5]),
        .DIB(wr_din[8:7]),
        .DIC(wr_din[10:9]),
        .DID({1'b0,1'b0}),
        .DOA(p_1_out__0[7:6]),
        .DOB(p_1_out__0[9:8]),
        .DOC(p_1_out__0[11:10]),
        .DOD(NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED[1:0]),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_singlebit
   (S,
    \tlast_counter_out_reg[21] ,
    \tlast_counter_out_reg[30] ,
    wr_en,
    data_int_reg_0,
    cache_data_reg,
    fifo_data_out,
    tlast_counter_out_reg,
    Q,
    tlast_size_value,
    internal_ready_delayed,
    out_valid,
    data_int_reg_1,
    data_int_reg_2,
    data_int_reg_3,
    w_d1,
    w_d2,
    cache_wr_en,
    Out_rsvd_reg,
    cache_valid,
    out_wr_en,
    AXI4_Stream_Master_TLAST,
    IPCORE_CLK,
    In_rsvd,
    wr_addr,
    rd_addr);
  output [3:0]S;
  output [3:0]\tlast_counter_out_reg[21] ;
  output [2:0]\tlast_counter_out_reg[30] ;
  output wr_en;
  output data_int_reg_0;
  output cache_data_reg;
  output fifo_data_out;
  input [31:0]tlast_counter_out_reg;
  input [0:0]Q;
  input [30:0]tlast_size_value;
  input internal_ready_delayed;
  input out_valid;
  input data_int_reg_1;
  input data_int_reg_2;
  input data_int_reg_3;
  input w_d1;
  input w_d2;
  input cache_wr_en;
  input Out_rsvd_reg;
  input cache_valid;
  input out_wr_en;
  input AXI4_Stream_Master_TLAST;
  input IPCORE_CLK;
  input In_rsvd;
  input [1:0]wr_addr;
  input [1:0]rd_addr;

  wire AXI4_Stream_Master_TLAST;
  wire IPCORE_CLK;
  wire In_rsvd;
  wire Out_rsvd_reg;
  wire [0:0]Q;
  wire [3:0]S;
  wire cache_data_reg;
  wire cache_valid;
  wire cache_wr_en;
  wire data_int_reg_0;
  wire data_int_reg_1;
  wire data_int_reg_2;
  wire data_int_reg_3;
  wire fifo_data_out;
  wire internal_ready_delayed;
  wire out_valid;
  wire out_wr_en;
  wire p_1_out__1;
  wire [1:0]rd_addr;
  wire [31:0]tlast_counter_out_reg;
  wire [3:0]\tlast_counter_out_reg[21] ;
  wire [2:0]\tlast_counter_out_reg[30] ;
  wire [30:0]tlast_size_value;
  wire w_d1;
  wire w_d2;
  wire w_waddr_1;
  wire [1:0]wr_addr;
  wire wr_en;
  wire NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED;

  LUT5 #(
    .INIT(32'hACFFAC00)) 
    Out_rsvd_i_1
       (.I0(Out_rsvd_reg),
        .I1(fifo_data_out),
        .I2(cache_valid),
        .I3(out_wr_en),
        .I4(AXI4_Stream_Master_TLAST),
        .O(cache_data_reg));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry__0_i_1
       (.I0(tlast_counter_out_reg[21]),
        .I1(tlast_size_value[20]),
        .I2(tlast_size_value[22]),
        .I3(tlast_counter_out_reg[23]),
        .I4(tlast_size_value[21]),
        .I5(tlast_counter_out_reg[22]),
        .O(\tlast_counter_out_reg[21] [3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry__0_i_2
       (.I0(tlast_counter_out_reg[18]),
        .I1(tlast_size_value[17]),
        .I2(tlast_size_value[19]),
        .I3(tlast_counter_out_reg[20]),
        .I4(tlast_size_value[18]),
        .I5(tlast_counter_out_reg[19]),
        .O(\tlast_counter_out_reg[21] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry__0_i_3
       (.I0(tlast_counter_out_reg[15]),
        .I1(tlast_size_value[14]),
        .I2(tlast_size_value[16]),
        .I3(tlast_counter_out_reg[17]),
        .I4(tlast_size_value[15]),
        .I5(tlast_counter_out_reg[16]),
        .O(\tlast_counter_out_reg[21] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry__0_i_4
       (.I0(tlast_counter_out_reg[12]),
        .I1(tlast_size_value[11]),
        .I2(tlast_size_value[13]),
        .I3(tlast_counter_out_reg[14]),
        .I4(tlast_size_value[12]),
        .I5(tlast_counter_out_reg[13]),
        .O(\tlast_counter_out_reg[21] [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    auto_tlast0_carry__1_i_1
       (.I0(tlast_counter_out_reg[30]),
        .I1(tlast_size_value[29]),
        .I2(tlast_counter_out_reg[31]),
        .I3(tlast_size_value[30]),
        .O(\tlast_counter_out_reg[30] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry__1_i_2
       (.I0(tlast_counter_out_reg[27]),
        .I1(tlast_size_value[26]),
        .I2(tlast_size_value[28]),
        .I3(tlast_counter_out_reg[29]),
        .I4(tlast_size_value[27]),
        .I5(tlast_counter_out_reg[28]),
        .O(\tlast_counter_out_reg[30] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry__1_i_3
       (.I0(tlast_counter_out_reg[24]),
        .I1(tlast_size_value[23]),
        .I2(tlast_size_value[25]),
        .I3(tlast_counter_out_reg[26]),
        .I4(tlast_size_value[24]),
        .I5(tlast_counter_out_reg[25]),
        .O(\tlast_counter_out_reg[30] [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry_i_1
       (.I0(tlast_counter_out_reg[9]),
        .I1(tlast_size_value[8]),
        .I2(tlast_size_value[10]),
        .I3(tlast_counter_out_reg[11]),
        .I4(tlast_size_value[9]),
        .I5(tlast_counter_out_reg[10]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry_i_2
       (.I0(tlast_counter_out_reg[6]),
        .I1(tlast_size_value[5]),
        .I2(tlast_size_value[7]),
        .I3(tlast_counter_out_reg[8]),
        .I4(tlast_size_value[6]),
        .I5(tlast_counter_out_reg[7]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    auto_tlast0_carry_i_3
       (.I0(tlast_counter_out_reg[3]),
        .I1(tlast_size_value[2]),
        .I2(tlast_size_value[4]),
        .I3(tlast_counter_out_reg[5]),
        .I4(tlast_size_value[3]),
        .I5(tlast_counter_out_reg[4]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    auto_tlast0_carry_i_4
       (.I0(tlast_counter_out_reg[0]),
        .I1(Q),
        .I2(tlast_size_value[1]),
        .I3(tlast_counter_out_reg[2]),
        .I4(tlast_size_value[0]),
        .I5(tlast_counter_out_reg[1]),
        .O(S[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hB8FFB800)) 
    cache_data_i_1
       (.I0(w_waddr_1),
        .I1(w_d1),
        .I2(w_d2),
        .I3(cache_wr_en),
        .I4(Out_rsvd_reg),
        .O(data_int_reg_0));
  FDRE #(
    .INIT(1'b0)) 
    data_int_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .D(p_1_out__1),
        .Q(w_waddr_1),
        .R(1'b0));
  (* XILINX_LEGACY_PRIM = "RAM16X1D" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "3" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "0" *) 
  RAM32X1D #(
    .INIT(32'h00000000)) 
    ram_reg_0_3_0_0
       (.A0(wr_addr[0]),
        .A1(wr_addr[1]),
        .A2(1'b0),
        .A3(1'b0),
        .A4(1'b0),
        .D(In_rsvd),
        .DPO(p_1_out__1),
        .DPRA0(rd_addr[0]),
        .DPRA1(rd_addr[1]),
        .DPRA2(1'b0),
        .DPRA3(1'b0),
        .DPRA4(1'b0),
        .SPO(NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED),
        .WCLK(IPCORE_CLK),
        .WE(wr_en));
  LUT5 #(
    .INIT(32'h88888808)) 
    ram_reg_0_3_0_0_i_2
       (.I0(internal_ready_delayed),
        .I1(out_valid),
        .I2(data_int_reg_1),
        .I3(data_int_reg_2),
        .I4(data_int_reg_3),
        .O(wr_en));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    w_d2_i_1
       (.I0(w_waddr_1),
        .I1(w_d1),
        .I2(w_d2),
        .O(fifo_data_out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_addr_decoder
   (read_reg_ip_timestamp,
    \write_reg_packet_size_axi4_stream_master_reg[31]_0 ,
    Q,
    \write_reg_packet_size_axi4_stream_master_reg[28]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[24]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[20]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[16]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[12]_0 ,
    \write_reg_packet_size_axi4_stream_master_reg[8]_0 ,
    S,
    \AXI4_Lite_ARADDR[2] ,
    AXI4_Lite_ACLK,
    \read_reg_ip_timestamp_reg[30]_0 ,
    \AXI4_Lite_RDATA_tmp_reg[30] ,
    AXI4_Lite_ARADDR,
    AXI4_Lite_ARVALID,
    \AXI4_Lite_RDATA_tmp_reg[30]_0 ,
    E,
    \write_reg_packet_size_axi4_stream_master_reg[31]_1 ,
    AR);
  output [0:0]read_reg_ip_timestamp;
  output [2:0]\write_reg_packet_size_axi4_stream_master_reg[31]_0 ;
  output [30:0]Q;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[28]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[24]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[20]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[16]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[12]_0 ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[8]_0 ;
  output [3:0]S;
  output \AXI4_Lite_ARADDR[2] ;
  input AXI4_Lite_ACLK;
  input [0:0]\read_reg_ip_timestamp_reg[30]_0 ;
  input \AXI4_Lite_RDATA_tmp_reg[30] ;
  input [1:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_ARVALID;
  input \AXI4_Lite_RDATA_tmp_reg[30]_0 ;
  input [0:0]E;
  input [31:0]\write_reg_packet_size_axi4_stream_master_reg[31]_1 ;
  input [1:0]AR;

  wire [1:0]AR;
  wire AXI4_Lite_ACLK;
  wire [1:0]AXI4_Lite_ARADDR;
  wire \AXI4_Lite_ARADDR[2] ;
  wire AXI4_Lite_ARVALID;
  wire \AXI4_Lite_RDATA_tmp_reg[30] ;
  wire \AXI4_Lite_RDATA_tmp_reg[30]_0 ;
  wire [0:0]E;
  wire [30:0]Q;
  wire [3:0]S;
  wire [0:0]read_reg_ip_timestamp;
  wire [0:0]\read_reg_ip_timestamp_reg[30]_0 ;
  wire [31:31]write_reg_packet_size_axi4_stream_master;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[12]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[16]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[20]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[24]_0 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[28]_0 ;
  wire [2:0]\write_reg_packet_size_axi4_stream_master_reg[31]_0 ;
  wire [31:0]\write_reg_packet_size_axi4_stream_master_reg[31]_1 ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[8]_0 ;

  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \AXI4_Lite_RDATA_tmp[30]_i_2 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[30] ),
        .I1(AXI4_Lite_ARADDR[0]),
        .I2(read_reg_ip_timestamp),
        .I3(AXI4_Lite_ARVALID),
        .I4(AXI4_Lite_ARADDR[1]),
        .I5(\AXI4_Lite_RDATA_tmp_reg[30]_0 ),
        .O(\AXI4_Lite_ARADDR[2] ));
  FDCE \read_reg_ip_timestamp_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(\read_reg_ip_timestamp_reg[30]_0 ),
        .D(1'b1),
        .Q(read_reg_ip_timestamp));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_1
       (.I0(Q[8]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_2
       (.I0(Q[7]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_3
       (.I0(Q[6]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__0_i_4
       (.I0(Q[5]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[8]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_1
       (.I0(Q[12]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_2
       (.I0(Q[11]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_3
       (.I0(Q[10]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__1_i_4
       (.I0(Q[9]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[12]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_1
       (.I0(Q[16]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_2
       (.I0(Q[15]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_3
       (.I0(Q[14]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__2_i_4
       (.I0(Q[13]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[16]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_1
       (.I0(Q[20]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_2
       (.I0(Q[19]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_3
       (.I0(Q[18]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__3_i_4
       (.I0(Q[17]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[20]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_1
       (.I0(Q[24]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_2
       (.I0(Q[23]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_3
       (.I0(Q[22]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__4_i_4
       (.I0(Q[21]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[24]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_1
       (.I0(Q[28]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_2
       (.I0(Q[27]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_3
       (.I0(Q[26]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__5_i_4
       (.I0(Q[25]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[28]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__6_i_1
       (.I0(write_reg_packet_size_axi4_stream_master),
        .O(\write_reg_packet_size_axi4_stream_master_reg[31]_0 [2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__6_i_2
       (.I0(Q[30]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[31]_0 [1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry__6_i_3
       (.I0(Q[29]),
        .O(\write_reg_packet_size_axi4_stream_master_reg[31]_0 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_1
       (.I0(Q[4]),
        .O(S[3]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_2
       (.I0(Q[3]),
        .O(S[2]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_3
       (.I0(Q[2]),
        .O(S[1]));
  LUT1 #(
    .INIT(2'h1)) 
    tlast_size_value_carry_i_4
       (.I0(Q[1]),
        .O(S[0]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[0]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [0]),
        .Q(Q[0]));
  FDPE \write_reg_packet_size_axi4_stream_master_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [10]),
        .PRE(AR[1]),
        .Q(Q[10]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [11]),
        .Q(Q[11]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [12]),
        .Q(Q[12]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [13]),
        .Q(Q[13]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [14]),
        .Q(Q[14]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [15]),
        .Q(Q[15]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [16]),
        .Q(Q[16]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [17]),
        .Q(Q[17]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [18]),
        .Q(Q[18]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [19]),
        .Q(Q[19]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[0]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [1]),
        .Q(Q[1]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [20]),
        .Q(Q[20]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [21]),
        .Q(Q[21]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [22]),
        .Q(Q[22]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [23]),
        .Q(Q[23]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [24]),
        .Q(Q[24]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [25]),
        .Q(Q[25]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [26]),
        .Q(Q[26]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [27]),
        .Q(Q[27]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [28]),
        .Q(Q[28]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [29]),
        .Q(Q[29]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[0]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [2]),
        .Q(Q[2]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [30]),
        .Q(Q[30]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[31] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [31]),
        .Q(write_reg_packet_size_axi4_stream_master));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[0]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [3]),
        .Q(Q[3]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[0]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [4]),
        .Q(Q[4]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [5]),
        .Q(Q[5]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [6]),
        .Q(Q[6]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [7]),
        .Q(Q[7]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [8]),
        .Q(Q[8]));
  FDCE \write_reg_packet_size_axi4_stream_master_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(E),
        .CLR(AR[1]),
        .D(\write_reg_packet_size_axi4_stream_master_reg[31]_1 [9]),
        .Q(Q[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_master
   (out_valid_reg,
    CO,
    AXI4_Stream_Master_TLAST,
    internal_ready,
    AXI4_Stream_Master_TDATA,
    IPCORE_CLK,
    AR,
    \fifo_sample_count_reg[2] ,
    \tlast_counter_out_reg[31]_0 ,
    Q,
    S,
    auto_tlast0_carry_i_3,
    auto_tlast0_carry_i_1,
    auto_tlast0_carry__0_i_4,
    auto_tlast0_carry__0_i_3,
    auto_tlast0_carry__0_i_1,
    auto_tlast0_carry__1_i_3,
    auto_tlast0_carry__1_i_2,
    internal_ready_delayed,
    out_valid,
    AXI4_Stream_Master_TREADY,
    wr_din,
    \Out_tmp_reg[0] ,
    In_rsvd,
    \fifo_back_indx_reg[0] );
  output out_valid_reg;
  output [0:0]CO;
  output AXI4_Stream_Master_TLAST;
  output internal_ready;
  output [31:0]AXI4_Stream_Master_TDATA;
  input IPCORE_CLK;
  input [0:0]AR;
  input \fifo_sample_count_reg[2] ;
  input \tlast_counter_out_reg[31]_0 ;
  input [30:0]Q;
  input [3:0]S;
  input [3:0]auto_tlast0_carry_i_3;
  input [3:0]auto_tlast0_carry_i_1;
  input [3:0]auto_tlast0_carry__0_i_4;
  input [3:0]auto_tlast0_carry__0_i_3;
  input [3:0]auto_tlast0_carry__0_i_1;
  input [3:0]auto_tlast0_carry__1_i_3;
  input [2:0]auto_tlast0_carry__1_i_2;
  input internal_ready_delayed;
  input out_valid;
  input AXI4_Stream_Master_TREADY;
  input [30:0]wr_din;
  input [0:0]\Out_tmp_reg[0] ;
  input In_rsvd;
  input [0:0]\fifo_back_indx_reg[0] ;

  wire [0:0]AR;
  wire [31:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire [0:0]CO;
  wire IPCORE_CLK;
  wire In_rsvd;
  wire [0:0]\Out_tmp_reg[0] ;
  wire [30:0]Q;
  wire [3:0]S;
  wire [3:0]auto_tlast0_carry__0_i_1;
  wire [3:0]auto_tlast0_carry__0_i_3;
  wire [3:0]auto_tlast0_carry__0_i_4;
  wire auto_tlast0_carry__0_n_0;
  wire auto_tlast0_carry__0_n_1;
  wire auto_tlast0_carry__0_n_2;
  wire auto_tlast0_carry__0_n_3;
  wire [2:0]auto_tlast0_carry__1_i_2;
  wire [3:0]auto_tlast0_carry__1_i_3;
  wire auto_tlast0_carry__1_n_2;
  wire auto_tlast0_carry__1_n_3;
  wire [3:0]auto_tlast0_carry_i_1;
  wire [3:0]auto_tlast0_carry_i_3;
  wire auto_tlast0_carry_n_0;
  wire auto_tlast0_carry_n_1;
  wire auto_tlast0_carry_n_2;
  wire auto_tlast0_carry_n_3;
  wire [0:0]\fifo_back_indx_reg[0] ;
  wire \fifo_sample_count_reg[2] ;
  wire internal_ready;
  wire internal_ready_delayed;
  wire out_valid;
  wire out_valid_reg;
  wire \tlast_counter_out[0]_i_3_n_0 ;
  wire \tlast_counter_out[0]_i_4_n_0 ;
  wire \tlast_counter_out[0]_i_5_n_0 ;
  wire \tlast_counter_out[0]_i_6_n_0 ;
  wire \tlast_counter_out[0]_i_7_n_0 ;
  wire \tlast_counter_out[12]_i_2_n_0 ;
  wire \tlast_counter_out[12]_i_3_n_0 ;
  wire \tlast_counter_out[12]_i_4_n_0 ;
  wire \tlast_counter_out[12]_i_5_n_0 ;
  wire \tlast_counter_out[16]_i_2_n_0 ;
  wire \tlast_counter_out[16]_i_3_n_0 ;
  wire \tlast_counter_out[16]_i_4_n_0 ;
  wire \tlast_counter_out[16]_i_5_n_0 ;
  wire \tlast_counter_out[20]_i_2_n_0 ;
  wire \tlast_counter_out[20]_i_3_n_0 ;
  wire \tlast_counter_out[20]_i_4_n_0 ;
  wire \tlast_counter_out[20]_i_5_n_0 ;
  wire \tlast_counter_out[24]_i_2_n_0 ;
  wire \tlast_counter_out[24]_i_3_n_0 ;
  wire \tlast_counter_out[24]_i_4_n_0 ;
  wire \tlast_counter_out[24]_i_5_n_0 ;
  wire \tlast_counter_out[28]_i_2_n_0 ;
  wire \tlast_counter_out[28]_i_3_n_0 ;
  wire \tlast_counter_out[28]_i_4_n_0 ;
  wire \tlast_counter_out[28]_i_5_n_0 ;
  wire \tlast_counter_out[4]_i_2_n_0 ;
  wire \tlast_counter_out[4]_i_3_n_0 ;
  wire \tlast_counter_out[4]_i_4_n_0 ;
  wire \tlast_counter_out[4]_i_5_n_0 ;
  wire \tlast_counter_out[8]_i_2_n_0 ;
  wire \tlast_counter_out[8]_i_3_n_0 ;
  wire \tlast_counter_out[8]_i_4_n_0 ;
  wire \tlast_counter_out[8]_i_5_n_0 ;
  wire [31:0]tlast_counter_out_reg;
  wire \tlast_counter_out_reg[0]_i_2_n_0 ;
  wire \tlast_counter_out_reg[0]_i_2_n_1 ;
  wire \tlast_counter_out_reg[0]_i_2_n_2 ;
  wire \tlast_counter_out_reg[0]_i_2_n_3 ;
  wire \tlast_counter_out_reg[0]_i_2_n_4 ;
  wire \tlast_counter_out_reg[0]_i_2_n_5 ;
  wire \tlast_counter_out_reg[0]_i_2_n_6 ;
  wire \tlast_counter_out_reg[0]_i_2_n_7 ;
  wire \tlast_counter_out_reg[12]_i_1_n_0 ;
  wire \tlast_counter_out_reg[12]_i_1_n_1 ;
  wire \tlast_counter_out_reg[12]_i_1_n_2 ;
  wire \tlast_counter_out_reg[12]_i_1_n_3 ;
  wire \tlast_counter_out_reg[12]_i_1_n_4 ;
  wire \tlast_counter_out_reg[12]_i_1_n_5 ;
  wire \tlast_counter_out_reg[12]_i_1_n_6 ;
  wire \tlast_counter_out_reg[12]_i_1_n_7 ;
  wire \tlast_counter_out_reg[16]_i_1_n_0 ;
  wire \tlast_counter_out_reg[16]_i_1_n_1 ;
  wire \tlast_counter_out_reg[16]_i_1_n_2 ;
  wire \tlast_counter_out_reg[16]_i_1_n_3 ;
  wire \tlast_counter_out_reg[16]_i_1_n_4 ;
  wire \tlast_counter_out_reg[16]_i_1_n_5 ;
  wire \tlast_counter_out_reg[16]_i_1_n_6 ;
  wire \tlast_counter_out_reg[16]_i_1_n_7 ;
  wire \tlast_counter_out_reg[20]_i_1_n_0 ;
  wire \tlast_counter_out_reg[20]_i_1_n_1 ;
  wire \tlast_counter_out_reg[20]_i_1_n_2 ;
  wire \tlast_counter_out_reg[20]_i_1_n_3 ;
  wire \tlast_counter_out_reg[20]_i_1_n_4 ;
  wire \tlast_counter_out_reg[20]_i_1_n_5 ;
  wire \tlast_counter_out_reg[20]_i_1_n_6 ;
  wire \tlast_counter_out_reg[20]_i_1_n_7 ;
  wire \tlast_counter_out_reg[24]_i_1_n_0 ;
  wire \tlast_counter_out_reg[24]_i_1_n_1 ;
  wire \tlast_counter_out_reg[24]_i_1_n_2 ;
  wire \tlast_counter_out_reg[24]_i_1_n_3 ;
  wire \tlast_counter_out_reg[24]_i_1_n_4 ;
  wire \tlast_counter_out_reg[24]_i_1_n_5 ;
  wire \tlast_counter_out_reg[24]_i_1_n_6 ;
  wire \tlast_counter_out_reg[24]_i_1_n_7 ;
  wire \tlast_counter_out_reg[28]_i_1_n_1 ;
  wire \tlast_counter_out_reg[28]_i_1_n_2 ;
  wire \tlast_counter_out_reg[28]_i_1_n_3 ;
  wire \tlast_counter_out_reg[28]_i_1_n_4 ;
  wire \tlast_counter_out_reg[28]_i_1_n_5 ;
  wire \tlast_counter_out_reg[28]_i_1_n_6 ;
  wire \tlast_counter_out_reg[28]_i_1_n_7 ;
  wire \tlast_counter_out_reg[31]_0 ;
  wire \tlast_counter_out_reg[4]_i_1_n_0 ;
  wire \tlast_counter_out_reg[4]_i_1_n_1 ;
  wire \tlast_counter_out_reg[4]_i_1_n_2 ;
  wire \tlast_counter_out_reg[4]_i_1_n_3 ;
  wire \tlast_counter_out_reg[4]_i_1_n_4 ;
  wire \tlast_counter_out_reg[4]_i_1_n_5 ;
  wire \tlast_counter_out_reg[4]_i_1_n_6 ;
  wire \tlast_counter_out_reg[4]_i_1_n_7 ;
  wire \tlast_counter_out_reg[8]_i_1_n_0 ;
  wire \tlast_counter_out_reg[8]_i_1_n_1 ;
  wire \tlast_counter_out_reg[8]_i_1_n_2 ;
  wire \tlast_counter_out_reg[8]_i_1_n_3 ;
  wire \tlast_counter_out_reg[8]_i_1_n_4 ;
  wire \tlast_counter_out_reg[8]_i_1_n_5 ;
  wire \tlast_counter_out_reg[8]_i_1_n_6 ;
  wire \tlast_counter_out_reg[8]_i_1_n_7 ;
  wire [31:1]tlast_size_value;
  wire tlast_size_value_carry__0_n_0;
  wire tlast_size_value_carry__0_n_1;
  wire tlast_size_value_carry__0_n_2;
  wire tlast_size_value_carry__0_n_3;
  wire tlast_size_value_carry__1_n_0;
  wire tlast_size_value_carry__1_n_1;
  wire tlast_size_value_carry__1_n_2;
  wire tlast_size_value_carry__1_n_3;
  wire tlast_size_value_carry__2_n_0;
  wire tlast_size_value_carry__2_n_1;
  wire tlast_size_value_carry__2_n_2;
  wire tlast_size_value_carry__2_n_3;
  wire tlast_size_value_carry__3_n_0;
  wire tlast_size_value_carry__3_n_1;
  wire tlast_size_value_carry__3_n_2;
  wire tlast_size_value_carry__3_n_3;
  wire tlast_size_value_carry__4_n_0;
  wire tlast_size_value_carry__4_n_1;
  wire tlast_size_value_carry__4_n_2;
  wire tlast_size_value_carry__4_n_3;
  wire tlast_size_value_carry__5_n_0;
  wire tlast_size_value_carry__5_n_1;
  wire tlast_size_value_carry__5_n_2;
  wire tlast_size_value_carry__5_n_3;
  wire tlast_size_value_carry__6_n_2;
  wire tlast_size_value_carry__6_n_3;
  wire tlast_size_value_carry_n_0;
  wire tlast_size_value_carry_n_1;
  wire tlast_size_value_carry_n_2;
  wire tlast_size_value_carry_n_3;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_1;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_10;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_11;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_2;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_3;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_4;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_5;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_6;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_7;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_8;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_9;
  wire [30:0]wr_din;
  wire [3:0]NLW_auto_tlast0_carry_O_UNCONNECTED;
  wire [3:0]NLW_auto_tlast0_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_auto_tlast0_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_auto_tlast0_carry__1_O_UNCONNECTED;
  wire [3:3]\NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]NLW_tlast_size_value_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_tlast_size_value_carry__6_O_UNCONNECTED;

  CARRY4 auto_tlast0_carry
       (.CI(1'b0),
        .CO({auto_tlast0_carry_n_0,auto_tlast0_carry_n_1,auto_tlast0_carry_n_2,auto_tlast0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_auto_tlast0_carry_O_UNCONNECTED[3:0]),
        .S({u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_1,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_2,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_3,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_4}));
  CARRY4 auto_tlast0_carry__0
       (.CI(auto_tlast0_carry_n_0),
        .CO({auto_tlast0_carry__0_n_0,auto_tlast0_carry__0_n_1,auto_tlast0_carry__0_n_2,auto_tlast0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_auto_tlast0_carry__0_O_UNCONNECTED[3:0]),
        .S({u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_5,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_6,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_7,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_8}));
  CARRY4 auto_tlast0_carry__1
       (.CI(auto_tlast0_carry__0_n_0),
        .CO({NLW_auto_tlast0_carry__1_CO_UNCONNECTED[3],CO,auto_tlast0_carry__1_n_2,auto_tlast0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_auto_tlast0_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_9,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_10,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_11}));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_3 
       (.I0(tlast_counter_out_reg[0]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_4 
       (.I0(tlast_counter_out_reg[3]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_5 
       (.I0(tlast_counter_out_reg[2]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[0]_i_6 
       (.I0(tlast_counter_out_reg[1]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'h1555)) 
    \tlast_counter_out[0]_i_7 
       (.I0(tlast_counter_out_reg[0]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[0]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_2 
       (.I0(tlast_counter_out_reg[15]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_3 
       (.I0(tlast_counter_out_reg[14]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_4 
       (.I0(tlast_counter_out_reg[13]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[12]_i_5 
       (.I0(tlast_counter_out_reg[12]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[12]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_2 
       (.I0(tlast_counter_out_reg[19]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_3 
       (.I0(tlast_counter_out_reg[18]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_4 
       (.I0(tlast_counter_out_reg[17]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[16]_i_5 
       (.I0(tlast_counter_out_reg[16]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[16]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_2 
       (.I0(tlast_counter_out_reg[23]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_3 
       (.I0(tlast_counter_out_reg[22]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_4 
       (.I0(tlast_counter_out_reg[21]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[20]_i_5 
       (.I0(tlast_counter_out_reg[20]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[20]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_2 
       (.I0(tlast_counter_out_reg[27]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_3 
       (.I0(tlast_counter_out_reg[26]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_4 
       (.I0(tlast_counter_out_reg[25]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[24]_i_5 
       (.I0(tlast_counter_out_reg[24]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[24]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_2 
       (.I0(tlast_counter_out_reg[31]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_3 
       (.I0(tlast_counter_out_reg[30]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_4 
       (.I0(tlast_counter_out_reg[29]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[28]_i_5 
       (.I0(tlast_counter_out_reg[28]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[28]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_2 
       (.I0(tlast_counter_out_reg[7]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_3 
       (.I0(tlast_counter_out_reg[6]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_4 
       (.I0(tlast_counter_out_reg[5]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[4]_i_5 
       (.I0(tlast_counter_out_reg[4]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[4]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_2 
       (.I0(tlast_counter_out_reg[11]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_3 
       (.I0(tlast_counter_out_reg[10]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_4 
       (.I0(tlast_counter_out_reg[9]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h2AAA)) 
    \tlast_counter_out[8]_i_5 
       (.I0(tlast_counter_out_reg[8]),
        .I1(CO),
        .I2(internal_ready_delayed),
        .I3(out_valid),
        .O(\tlast_counter_out[8]_i_5_n_0 ));
  FDCE \tlast_counter_out_reg[0] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[0]_i_2_n_7 ),
        .Q(tlast_counter_out_reg[0]));
  CARRY4 \tlast_counter_out_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\tlast_counter_out_reg[0]_i_2_n_0 ,\tlast_counter_out_reg[0]_i_2_n_1 ,\tlast_counter_out_reg[0]_i_2_n_2 ,\tlast_counter_out_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tlast_counter_out[0]_i_3_n_0 }),
        .O({\tlast_counter_out_reg[0]_i_2_n_4 ,\tlast_counter_out_reg[0]_i_2_n_5 ,\tlast_counter_out_reg[0]_i_2_n_6 ,\tlast_counter_out_reg[0]_i_2_n_7 }),
        .S({\tlast_counter_out[0]_i_4_n_0 ,\tlast_counter_out[0]_i_5_n_0 ,\tlast_counter_out[0]_i_6_n_0 ,\tlast_counter_out[0]_i_7_n_0 }));
  FDCE \tlast_counter_out_reg[10] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[8]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[10]));
  FDCE \tlast_counter_out_reg[11] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[8]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[11]));
  FDCE \tlast_counter_out_reg[12] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[12]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[12]));
  CARRY4 \tlast_counter_out_reg[12]_i_1 
       (.CI(\tlast_counter_out_reg[8]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[12]_i_1_n_0 ,\tlast_counter_out_reg[12]_i_1_n_1 ,\tlast_counter_out_reg[12]_i_1_n_2 ,\tlast_counter_out_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[12]_i_1_n_4 ,\tlast_counter_out_reg[12]_i_1_n_5 ,\tlast_counter_out_reg[12]_i_1_n_6 ,\tlast_counter_out_reg[12]_i_1_n_7 }),
        .S({\tlast_counter_out[12]_i_2_n_0 ,\tlast_counter_out[12]_i_3_n_0 ,\tlast_counter_out[12]_i_4_n_0 ,\tlast_counter_out[12]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[13] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[12]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[13]));
  FDCE \tlast_counter_out_reg[14] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[12]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[14]));
  FDCE \tlast_counter_out_reg[15] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[12]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[15]));
  FDCE \tlast_counter_out_reg[16] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[16]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[16]));
  CARRY4 \tlast_counter_out_reg[16]_i_1 
       (.CI(\tlast_counter_out_reg[12]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[16]_i_1_n_0 ,\tlast_counter_out_reg[16]_i_1_n_1 ,\tlast_counter_out_reg[16]_i_1_n_2 ,\tlast_counter_out_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[16]_i_1_n_4 ,\tlast_counter_out_reg[16]_i_1_n_5 ,\tlast_counter_out_reg[16]_i_1_n_6 ,\tlast_counter_out_reg[16]_i_1_n_7 }),
        .S({\tlast_counter_out[16]_i_2_n_0 ,\tlast_counter_out[16]_i_3_n_0 ,\tlast_counter_out[16]_i_4_n_0 ,\tlast_counter_out[16]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[17] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[16]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[17]));
  FDCE \tlast_counter_out_reg[18] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[16]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[18]));
  FDCE \tlast_counter_out_reg[19] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[16]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[19]));
  FDCE \tlast_counter_out_reg[1] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[0]_i_2_n_6 ),
        .Q(tlast_counter_out_reg[1]));
  FDCE \tlast_counter_out_reg[20] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[20]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[20]));
  CARRY4 \tlast_counter_out_reg[20]_i_1 
       (.CI(\tlast_counter_out_reg[16]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[20]_i_1_n_0 ,\tlast_counter_out_reg[20]_i_1_n_1 ,\tlast_counter_out_reg[20]_i_1_n_2 ,\tlast_counter_out_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[20]_i_1_n_4 ,\tlast_counter_out_reg[20]_i_1_n_5 ,\tlast_counter_out_reg[20]_i_1_n_6 ,\tlast_counter_out_reg[20]_i_1_n_7 }),
        .S({\tlast_counter_out[20]_i_2_n_0 ,\tlast_counter_out[20]_i_3_n_0 ,\tlast_counter_out[20]_i_4_n_0 ,\tlast_counter_out[20]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[21] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[20]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[21]));
  FDCE \tlast_counter_out_reg[22] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[20]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[22]));
  FDCE \tlast_counter_out_reg[23] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[20]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[23]));
  FDCE \tlast_counter_out_reg[24] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[24]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[24]));
  CARRY4 \tlast_counter_out_reg[24]_i_1 
       (.CI(\tlast_counter_out_reg[20]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[24]_i_1_n_0 ,\tlast_counter_out_reg[24]_i_1_n_1 ,\tlast_counter_out_reg[24]_i_1_n_2 ,\tlast_counter_out_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[24]_i_1_n_4 ,\tlast_counter_out_reg[24]_i_1_n_5 ,\tlast_counter_out_reg[24]_i_1_n_6 ,\tlast_counter_out_reg[24]_i_1_n_7 }),
        .S({\tlast_counter_out[24]_i_2_n_0 ,\tlast_counter_out[24]_i_3_n_0 ,\tlast_counter_out[24]_i_4_n_0 ,\tlast_counter_out[24]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[25] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[24]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[25]));
  FDCE \tlast_counter_out_reg[26] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[24]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[26]));
  FDCE \tlast_counter_out_reg[27] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[24]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[27]));
  FDCE \tlast_counter_out_reg[28] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[28]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[28]));
  CARRY4 \tlast_counter_out_reg[28]_i_1 
       (.CI(\tlast_counter_out_reg[24]_i_1_n_0 ),
        .CO({\NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED [3],\tlast_counter_out_reg[28]_i_1_n_1 ,\tlast_counter_out_reg[28]_i_1_n_2 ,\tlast_counter_out_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[28]_i_1_n_4 ,\tlast_counter_out_reg[28]_i_1_n_5 ,\tlast_counter_out_reg[28]_i_1_n_6 ,\tlast_counter_out_reg[28]_i_1_n_7 }),
        .S({\tlast_counter_out[28]_i_2_n_0 ,\tlast_counter_out[28]_i_3_n_0 ,\tlast_counter_out[28]_i_4_n_0 ,\tlast_counter_out[28]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[29] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[28]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[29]));
  FDCE \tlast_counter_out_reg[2] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[0]_i_2_n_5 ),
        .Q(tlast_counter_out_reg[2]));
  FDCE \tlast_counter_out_reg[30] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[28]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[30]));
  FDCE \tlast_counter_out_reg[31] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(AR),
        .D(\tlast_counter_out_reg[28]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[31]));
  FDCE \tlast_counter_out_reg[3] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[0]_i_2_n_4 ),
        .Q(tlast_counter_out_reg[3]));
  FDCE \tlast_counter_out_reg[4] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[4]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[4]));
  CARRY4 \tlast_counter_out_reg[4]_i_1 
       (.CI(\tlast_counter_out_reg[0]_i_2_n_0 ),
        .CO({\tlast_counter_out_reg[4]_i_1_n_0 ,\tlast_counter_out_reg[4]_i_1_n_1 ,\tlast_counter_out_reg[4]_i_1_n_2 ,\tlast_counter_out_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[4]_i_1_n_4 ,\tlast_counter_out_reg[4]_i_1_n_5 ,\tlast_counter_out_reg[4]_i_1_n_6 ,\tlast_counter_out_reg[4]_i_1_n_7 }),
        .S({\tlast_counter_out[4]_i_2_n_0 ,\tlast_counter_out[4]_i_3_n_0 ,\tlast_counter_out[4]_i_4_n_0 ,\tlast_counter_out[4]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[5] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[4]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[5]));
  FDCE \tlast_counter_out_reg[6] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[4]_i_1_n_5 ),
        .Q(tlast_counter_out_reg[6]));
  FDCE \tlast_counter_out_reg[7] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[4]_i_1_n_4 ),
        .Q(tlast_counter_out_reg[7]));
  FDCE \tlast_counter_out_reg[8] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[8]_i_1_n_7 ),
        .Q(tlast_counter_out_reg[8]));
  CARRY4 \tlast_counter_out_reg[8]_i_1 
       (.CI(\tlast_counter_out_reg[4]_i_1_n_0 ),
        .CO({\tlast_counter_out_reg[8]_i_1_n_0 ,\tlast_counter_out_reg[8]_i_1_n_1 ,\tlast_counter_out_reg[8]_i_1_n_2 ,\tlast_counter_out_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tlast_counter_out_reg[8]_i_1_n_4 ,\tlast_counter_out_reg[8]_i_1_n_5 ,\tlast_counter_out_reg[8]_i_1_n_6 ,\tlast_counter_out_reg[8]_i_1_n_7 }),
        .S({\tlast_counter_out[8]_i_2_n_0 ,\tlast_counter_out[8]_i_3_n_0 ,\tlast_counter_out[8]_i_4_n_0 ,\tlast_counter_out[8]_i_5_n_0 }));
  FDCE \tlast_counter_out_reg[9] 
       (.C(IPCORE_CLK),
        .CE(\tlast_counter_out_reg[31]_0 ),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(\tlast_counter_out_reg[8]_i_1_n_6 ),
        .Q(tlast_counter_out_reg[9]));
  CARRY4 tlast_size_value_carry
       (.CI(1'b0),
        .CO({tlast_size_value_carry_n_0,tlast_size_value_carry_n_1,tlast_size_value_carry_n_2,tlast_size_value_carry_n_3}),
        .CYINIT(Q[0]),
        .DI(Q[4:1]),
        .O(tlast_size_value[4:1]),
        .S(S));
  CARRY4 tlast_size_value_carry__0
       (.CI(tlast_size_value_carry_n_0),
        .CO({tlast_size_value_carry__0_n_0,tlast_size_value_carry__0_n_1,tlast_size_value_carry__0_n_2,tlast_size_value_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O(tlast_size_value[8:5]),
        .S(auto_tlast0_carry_i_3));
  CARRY4 tlast_size_value_carry__1
       (.CI(tlast_size_value_carry__0_n_0),
        .CO({tlast_size_value_carry__1_n_0,tlast_size_value_carry__1_n_1,tlast_size_value_carry__1_n_2,tlast_size_value_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O(tlast_size_value[12:9]),
        .S(auto_tlast0_carry_i_1));
  CARRY4 tlast_size_value_carry__2
       (.CI(tlast_size_value_carry__1_n_0),
        .CO({tlast_size_value_carry__2_n_0,tlast_size_value_carry__2_n_1,tlast_size_value_carry__2_n_2,tlast_size_value_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[16:13]),
        .O(tlast_size_value[16:13]),
        .S(auto_tlast0_carry__0_i_4));
  CARRY4 tlast_size_value_carry__3
       (.CI(tlast_size_value_carry__2_n_0),
        .CO({tlast_size_value_carry__3_n_0,tlast_size_value_carry__3_n_1,tlast_size_value_carry__3_n_2,tlast_size_value_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(Q[20:17]),
        .O(tlast_size_value[20:17]),
        .S(auto_tlast0_carry__0_i_3));
  CARRY4 tlast_size_value_carry__4
       (.CI(tlast_size_value_carry__3_n_0),
        .CO({tlast_size_value_carry__4_n_0,tlast_size_value_carry__4_n_1,tlast_size_value_carry__4_n_2,tlast_size_value_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(Q[24:21]),
        .O(tlast_size_value[24:21]),
        .S(auto_tlast0_carry__0_i_1));
  CARRY4 tlast_size_value_carry__5
       (.CI(tlast_size_value_carry__4_n_0),
        .CO({tlast_size_value_carry__5_n_0,tlast_size_value_carry__5_n_1,tlast_size_value_carry__5_n_2,tlast_size_value_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(Q[28:25]),
        .O(tlast_size_value[28:25]),
        .S(auto_tlast0_carry__1_i_3));
  CARRY4 tlast_size_value_carry__6
       (.CI(tlast_size_value_carry__5_n_0),
        .CO({NLW_tlast_size_value_carry__6_CO_UNCONNECTED[3:2],tlast_size_value_carry__6_n_2,tlast_size_value_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,Q[30:29]}),
        .O({NLW_tlast_size_value_carry__6_O_UNCONNECTED[3],tlast_size_value[31:29]}),
        .S({1'b0,auto_tlast0_carry__1_i_2}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT u_MultiplyB_ip_fifo_TLAST_OUT_inst
       (.AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .IPCORE_CLK(IPCORE_CLK),
        .In_rsvd(In_rsvd),
        .Q(Q[0]),
        .S({u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_1,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_2,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_3,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_4}),
        .\fifo_back_indx_reg[0] (\fifo_back_indx_reg[0] ),
        .\fifo_sample_count_reg[2] (\fifo_sample_count_reg[2] ),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(out_valid),
        .tlast_counter_out_reg(tlast_counter_out_reg),
        .\tlast_counter_out_reg[21] ({u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_5,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_6,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_7,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_8}),
        .\tlast_counter_out_reg[30] ({u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_9,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_10,u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_11}),
        .tlast_size_value(tlast_size_value));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT u_MultiplyB_ip_fifo_data_OUT_inst
       (.AR(AR),
        .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .IPCORE_CLK(IPCORE_CLK),
        .\Out_tmp_reg[0]_0 (\Out_tmp_reg[0] ),
        .\fifo_back_indx_reg[0] (\fifo_back_indx_reg[0] ),
        .\fifo_sample_count_reg[2] (\fifo_sample_count_reg[2] ),
        .internal_ready(internal_ready),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(out_valid),
        .out_valid_reg_0(out_valid_reg),
        .wr_din(wr_din));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_slave
   (out_valid,
    internal_ready_delayed,
    In_rsvd,
    AXI4_Stream_Slave_TREADY,
    out_valid_reg,
    Q,
    IPCORE_CLK,
    AR,
    internal_ready,
    AXI4_Stream_Slave_TVALID,
    CO,
    AXI4_Stream_Slave_TDATA);
  output out_valid;
  output internal_ready_delayed;
  output In_rsvd;
  output AXI4_Stream_Slave_TREADY;
  output out_valid_reg;
  output [30:0]Q;
  input IPCORE_CLK;
  input [1:0]AR;
  input internal_ready;
  input AXI4_Stream_Slave_TVALID;
  input [0:0]CO;
  input [31:0]AXI4_Stream_Slave_TDATA;

  wire [1:0]AR;
  wire [31:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire [0:0]CO;
  wire IPCORE_CLK;
  wire In_rsvd;
  wire [30:0]Q;
  wire internal_ready;
  wire internal_ready_delayed;
  wire out_valid;
  wire out_valid_reg;

  FDCE fifo_rd_ack_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[0]),
        .D(internal_ready),
        .Q(internal_ready_delayed));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data u_MultiplyB_ip_fifo_data_inst
       (.AR(AR),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .CO(CO),
        .IPCORE_CLK(IPCORE_CLK),
        .In_rsvd(In_rsvd),
        .Q(Q),
        .fifo_valid_reg_0(internal_ready_delayed),
        .out_valid_reg_0(out_valid),
        .out_valid_reg_1(out_valid_reg));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite
   (FSM_sequential_axi_lite_rstate_reg,
    AXI4_Lite_ARESETN_0,
    AXI4_Lite_RDATA,
    \write_reg_packet_size_axi4_stream_master_reg[31] ,
    Q,
    \write_reg_packet_size_axi4_stream_master_reg[28] ,
    \write_reg_packet_size_axi4_stream_master_reg[24] ,
    \write_reg_packet_size_axi4_stream_master_reg[20] ,
    \write_reg_packet_size_axi4_stream_master_reg[16] ,
    \write_reg_packet_size_axi4_stream_master_reg[12] ,
    \write_reg_packet_size_axi4_stream_master_reg[8] ,
    S,
    AR,
    AXI4_Lite_ARESETN_1,
    AXI4_Lite_AWREADY,
    \FSM_onehot_axi_lite_wstate_reg[2] ,
    AXI4_Lite_ARREADY,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN,
    AXI4_Lite_AWVALID,
    AXI4_Lite_ARVALID,
    AXI4_Lite_AWADDR,
    AXI4_Lite_WDATA,
    AXI4_Lite_ARADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_WSTRB);
  output FSM_sequential_axi_lite_rstate_reg;
  output [0:0]AXI4_Lite_ARESETN_0;
  output [0:0]AXI4_Lite_RDATA;
  output [2:0]\write_reg_packet_size_axi4_stream_master_reg[31] ;
  output [30:0]Q;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[28] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[24] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[20] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[16] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[12] ;
  output [3:0]\write_reg_packet_size_axi4_stream_master_reg[8] ;
  output [3:0]S;
  output [1:0]AR;
  output [0:0]AXI4_Lite_ARESETN_1;
  output AXI4_Lite_AWREADY;
  output [1:0]\FSM_onehot_axi_lite_wstate_reg[2] ;
  output AXI4_Lite_ARREADY;
  input AXI4_Lite_ACLK;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_AWADDR;
  input [31:0]AXI4_Lite_WDATA;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_RREADY;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input [3:0]AXI4_Lite_WSTRB;

  wire [1:0]AR;
  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire [0:0]AXI4_Lite_ARESETN_0;
  wire [0:0]AXI4_Lite_ARESETN_1;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire [0:0]AXI4_Lite_RDATA;
  wire AXI4_Lite_RREADY;
  wire [31:0]AXI4_Lite_WDATA;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [1:0]\FSM_onehot_axi_lite_wstate_reg[2] ;
  wire FSM_sequential_axi_lite_rstate_reg;
  wire IPCORE_RESETN;
  wire [30:0]Q;
  wire [3:0]S;
  wire [30:30]read_reg_ip_timestamp;
  wire reg_enb_packet_size_axi4_stream_master;
  wire [0:0]top_data_write;
  wire u_MultiplyB_ip_addr_decoder_inst_n_63;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_13;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_14;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_15;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_16;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_17;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_18;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_19;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_20;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_21;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_22;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_23;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_24;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_25;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_26;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_27;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_28;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_29;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_30;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_31;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_32;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_33;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_34;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_35;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_36;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_37;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_38;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_39;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_40;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_41;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_42;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_43;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_8;
  wire u_MultiplyB_ip_axi_lite_module_inst_n_9;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[12] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[16] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[20] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[24] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[28] ;
  wire [2:0]\write_reg_packet_size_axi4_stream_master_reg[31] ;
  wire [3:0]\write_reg_packet_size_axi4_stream_master_reg[8] ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_addr_decoder u_MultiplyB_ip_addr_decoder_inst
       (.AR(AR),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR[1:0]),
        .\AXI4_Lite_ARADDR[2] (u_MultiplyB_ip_addr_decoder_inst_n_63),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .\AXI4_Lite_RDATA_tmp_reg[30] (u_MultiplyB_ip_axi_lite_module_inst_n_9),
        .\AXI4_Lite_RDATA_tmp_reg[30]_0 (u_MultiplyB_ip_axi_lite_module_inst_n_8),
        .E(reg_enb_packet_size_axi4_stream_master),
        .Q(Q),
        .S(S),
        .read_reg_ip_timestamp(read_reg_ip_timestamp),
        .\read_reg_ip_timestamp_reg[30]_0 (AXI4_Lite_ARESETN_0),
        .\write_reg_packet_size_axi4_stream_master_reg[12]_0 (\write_reg_packet_size_axi4_stream_master_reg[12] ),
        .\write_reg_packet_size_axi4_stream_master_reg[16]_0 (\write_reg_packet_size_axi4_stream_master_reg[16] ),
        .\write_reg_packet_size_axi4_stream_master_reg[20]_0 (\write_reg_packet_size_axi4_stream_master_reg[20] ),
        .\write_reg_packet_size_axi4_stream_master_reg[24]_0 (\write_reg_packet_size_axi4_stream_master_reg[24] ),
        .\write_reg_packet_size_axi4_stream_master_reg[28]_0 (\write_reg_packet_size_axi4_stream_master_reg[28] ),
        .\write_reg_packet_size_axi4_stream_master_reg[31]_0 (\write_reg_packet_size_axi4_stream_master_reg[31] ),
        .\write_reg_packet_size_axi4_stream_master_reg[31]_1 ({u_MultiplyB_ip_axi_lite_module_inst_n_13,u_MultiplyB_ip_axi_lite_module_inst_n_14,u_MultiplyB_ip_axi_lite_module_inst_n_15,u_MultiplyB_ip_axi_lite_module_inst_n_16,u_MultiplyB_ip_axi_lite_module_inst_n_17,u_MultiplyB_ip_axi_lite_module_inst_n_18,u_MultiplyB_ip_axi_lite_module_inst_n_19,u_MultiplyB_ip_axi_lite_module_inst_n_20,u_MultiplyB_ip_axi_lite_module_inst_n_21,u_MultiplyB_ip_axi_lite_module_inst_n_22,u_MultiplyB_ip_axi_lite_module_inst_n_23,u_MultiplyB_ip_axi_lite_module_inst_n_24,u_MultiplyB_ip_axi_lite_module_inst_n_25,u_MultiplyB_ip_axi_lite_module_inst_n_26,u_MultiplyB_ip_axi_lite_module_inst_n_27,u_MultiplyB_ip_axi_lite_module_inst_n_28,u_MultiplyB_ip_axi_lite_module_inst_n_29,u_MultiplyB_ip_axi_lite_module_inst_n_30,u_MultiplyB_ip_axi_lite_module_inst_n_31,u_MultiplyB_ip_axi_lite_module_inst_n_32,u_MultiplyB_ip_axi_lite_module_inst_n_33,u_MultiplyB_ip_axi_lite_module_inst_n_34,u_MultiplyB_ip_axi_lite_module_inst_n_35,u_MultiplyB_ip_axi_lite_module_inst_n_36,u_MultiplyB_ip_axi_lite_module_inst_n_37,u_MultiplyB_ip_axi_lite_module_inst_n_38,u_MultiplyB_ip_axi_lite_module_inst_n_39,u_MultiplyB_ip_axi_lite_module_inst_n_40,u_MultiplyB_ip_axi_lite_module_inst_n_41,u_MultiplyB_ip_axi_lite_module_inst_n_42,u_MultiplyB_ip_axi_lite_module_inst_n_43,top_data_write}),
        .\write_reg_packet_size_axi4_stream_master_reg[8]_0 (\write_reg_packet_size_axi4_stream_master_reg[8] ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite_module u_MultiplyB_ip_axi_lite_module_inst
       (.AR(AR),
        .AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR),
        .AXI4_Lite_ARADDR_12_sp_1(u_MultiplyB_ip_axi_lite_module_inst_n_9),
        .AXI4_Lite_ARADDR_6_sp_1(u_MultiplyB_ip_axi_lite_module_inst_n_8),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARESETN_0(AXI4_Lite_ARESETN_0),
        .AXI4_Lite_ARESETN_1(AXI4_Lite_ARESETN_1),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_RDATA(AXI4_Lite_RDATA),
        .\AXI4_Lite_RDATA_tmp_reg[30]_0 (u_MultiplyB_ip_addr_decoder_inst_n_63),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .E(reg_enb_packet_size_axi4_stream_master),
        .FSM_sequential_axi_lite_rstate_reg_0(FSM_sequential_axi_lite_rstate_reg),
        .IPCORE_RESETN(IPCORE_RESETN),
        .Q(\FSM_onehot_axi_lite_wstate_reg[2] ),
        .read_reg_ip_timestamp(read_reg_ip_timestamp),
        .\wdata_reg[31]_0 ({u_MultiplyB_ip_axi_lite_module_inst_n_13,u_MultiplyB_ip_axi_lite_module_inst_n_14,u_MultiplyB_ip_axi_lite_module_inst_n_15,u_MultiplyB_ip_axi_lite_module_inst_n_16,u_MultiplyB_ip_axi_lite_module_inst_n_17,u_MultiplyB_ip_axi_lite_module_inst_n_18,u_MultiplyB_ip_axi_lite_module_inst_n_19,u_MultiplyB_ip_axi_lite_module_inst_n_20,u_MultiplyB_ip_axi_lite_module_inst_n_21,u_MultiplyB_ip_axi_lite_module_inst_n_22,u_MultiplyB_ip_axi_lite_module_inst_n_23,u_MultiplyB_ip_axi_lite_module_inst_n_24,u_MultiplyB_ip_axi_lite_module_inst_n_25,u_MultiplyB_ip_axi_lite_module_inst_n_26,u_MultiplyB_ip_axi_lite_module_inst_n_27,u_MultiplyB_ip_axi_lite_module_inst_n_28,u_MultiplyB_ip_axi_lite_module_inst_n_29,u_MultiplyB_ip_axi_lite_module_inst_n_30,u_MultiplyB_ip_axi_lite_module_inst_n_31,u_MultiplyB_ip_axi_lite_module_inst_n_32,u_MultiplyB_ip_axi_lite_module_inst_n_33,u_MultiplyB_ip_axi_lite_module_inst_n_34,u_MultiplyB_ip_axi_lite_module_inst_n_35,u_MultiplyB_ip_axi_lite_module_inst_n_36,u_MultiplyB_ip_axi_lite_module_inst_n_37,u_MultiplyB_ip_axi_lite_module_inst_n_38,u_MultiplyB_ip_axi_lite_module_inst_n_39,u_MultiplyB_ip_axi_lite_module_inst_n_40,u_MultiplyB_ip_axi_lite_module_inst_n_41,u_MultiplyB_ip_axi_lite_module_inst_n_42,u_MultiplyB_ip_axi_lite_module_inst_n_43,top_data_write}));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite_module
   (FSM_sequential_axi_lite_rstate_reg_0,
    AXI4_Lite_RDATA,
    AXI4_Lite_ARESETN_0,
    AR,
    AXI4_Lite_ARESETN_1,
    AXI4_Lite_AWREADY,
    E,
    AXI4_Lite_ARADDR_6_sp_1,
    AXI4_Lite_ARADDR_12_sp_1,
    AXI4_Lite_ARREADY,
    Q,
    \wdata_reg[31]_0 ,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARESETN,
    IPCORE_RESETN,
    \AXI4_Lite_RDATA_tmp_reg[30]_0 ,
    AXI4_Lite_AWVALID,
    AXI4_Lite_ARVALID,
    AXI4_Lite_ARADDR,
    AXI4_Lite_RREADY,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_WSTRB,
    read_reg_ip_timestamp,
    AXI4_Lite_AWADDR,
    AXI4_Lite_WDATA);
  output FSM_sequential_axi_lite_rstate_reg_0;
  output [0:0]AXI4_Lite_RDATA;
  output [0:0]AXI4_Lite_ARESETN_0;
  output [1:0]AR;
  output [0:0]AXI4_Lite_ARESETN_1;
  output AXI4_Lite_AWREADY;
  output [0:0]E;
  output AXI4_Lite_ARADDR_6_sp_1;
  output AXI4_Lite_ARADDR_12_sp_1;
  output AXI4_Lite_ARREADY;
  output [1:0]Q;
  output [31:0]\wdata_reg[31]_0 ;
  input AXI4_Lite_ACLK;
  input AXI4_Lite_ARESETN;
  input IPCORE_RESETN;
  input \AXI4_Lite_RDATA_tmp_reg[30]_0 ;
  input AXI4_Lite_AWVALID;
  input AXI4_Lite_ARVALID;
  input [13:0]AXI4_Lite_ARADDR;
  input AXI4_Lite_RREADY;
  input AXI4_Lite_WVALID;
  input AXI4_Lite_BREADY;
  input [3:0]AXI4_Lite_WSTRB;
  input [0:0]read_reg_ip_timestamp;
  input [13:0]AXI4_Lite_AWADDR;
  input [31:0]AXI4_Lite_WDATA;

  wire [1:0]AR;
  wire AXI4_Lite_ACLK;
  wire [13:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARADDR_12_sn_1;
  wire AXI4_Lite_ARADDR_6_sn_1;
  wire AXI4_Lite_ARESETN;
  wire [0:0]AXI4_Lite_ARESETN_0;
  wire [0:0]AXI4_Lite_ARESETN_1;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [13:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire [0:0]AXI4_Lite_RDATA;
  wire \AXI4_Lite_RDATA_tmp[30]_i_1_n_0 ;
  wire \AXI4_Lite_RDATA_tmp[30]_i_3_n_0 ;
  wire \AXI4_Lite_RDATA_tmp_reg[30]_0 ;
  wire AXI4_Lite_RREADY;
  wire [31:0]AXI4_Lite_WDATA;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [0:0]E;
  wire \FSM_onehot_axi_lite_wstate_reg_n_0_[0] ;
  wire FSM_sequential_axi_lite_rstate_reg_0;
  wire IPCORE_RESETN;
  wire [1:0]Q;
  wire aw_transfer;
  wire axi_lite_rstate_next;
  wire [2:0]axi_lite_wstate_next;
  wire [0:0]read_reg_ip_timestamp;
  wire reset;
  wire [13:0]sel0;
  wire soft_reset;
  wire soft_reset_i_2_n_0;
  wire soft_reset_i_3_n_0;
  wire strobe_sw;
  wire top_wr_enb;
  wire w_transfer;
  wire w_transfer_and_wstrb;
  wire [31:0]\wdata_reg[31]_0 ;
  wire \write_reg_packet_size_axi4_stream_master[31]_i_3_n_0 ;
  wire \write_reg_packet_size_axi4_stream_master[31]_i_5_n_0 ;
  wire \write_reg_packet_size_axi4_stream_master[31]_i_6_n_0 ;
  wire \write_reg_packet_size_axi4_stream_master[31]_i_7_n_0 ;

  assign AXI4_Lite_ARADDR_12_sp_1 = AXI4_Lite_ARADDR_12_sn_1;
  assign AXI4_Lite_ARADDR_6_sp_1 = AXI4_Lite_ARADDR_6_sn_1;
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT3 #(
    .INIT(8'h04)) 
    AXI4_Lite_ARREADY_INST_0
       (.I0(FSM_sequential_axi_lite_rstate_reg_0),
        .I1(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I2(AXI4_Lite_AWVALID),
        .O(AXI4_Lite_ARREADY));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT2 #(
    .INIT(4'h2)) 
    AXI4_Lite_AWREADY_INST_0
       (.I0(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I1(FSM_sequential_axi_lite_rstate_reg_0),
        .O(AXI4_Lite_AWREADY));
  LUT6 #(
    .INIT(64'hFEFFFFFF0E000000)) 
    \AXI4_Lite_RDATA_tmp[30]_i_1 
       (.I0(\AXI4_Lite_RDATA_tmp_reg[30]_0 ),
        .I1(\AXI4_Lite_RDATA_tmp[30]_i_3_n_0 ),
        .I2(AXI4_Lite_AWVALID),
        .I3(AXI4_Lite_ARVALID),
        .I4(AXI4_Lite_AWREADY),
        .I5(AXI4_Lite_RDATA),
        .O(\AXI4_Lite_RDATA_tmp[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0080000000000000)) 
    \AXI4_Lite_RDATA_tmp[30]_i_3 
       (.I0(\write_reg_packet_size_axi4_stream_master[31]_i_7_n_0 ),
        .I1(sel0[0]),
        .I2(read_reg_ip_timestamp),
        .I3(AXI4_Lite_ARVALID),
        .I4(sel0[1]),
        .I5(\write_reg_packet_size_axi4_stream_master[31]_i_5_n_0 ),
        .O(\AXI4_Lite_RDATA_tmp[30]_i_3_n_0 ));
  FDCE \AXI4_Lite_RDATA_tmp_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(\AXI4_Lite_RDATA_tmp[30]_i_1_n_0 ),
        .Q(AXI4_Lite_RDATA));
  LUT5 #(
    .INIT(32'hFFC0D5C0)) 
    \FSM_onehot_axi_lite_wstate[0]_i_1 
       (.I0(AXI4_Lite_AWVALID),
        .I1(AXI4_Lite_BREADY),
        .I2(Q[1]),
        .I3(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I4(FSM_sequential_axi_lite_rstate_reg_0),
        .O(axi_lite_wstate_next[0]));
  (* SOFT_HLUTNM = "soft_lutpair76" *) 
  LUT5 #(
    .INIT(32'h4444F444)) 
    \FSM_onehot_axi_lite_wstate[1]_i_1 
       (.I0(AXI4_Lite_WVALID),
        .I1(Q[0]),
        .I2(AXI4_Lite_AWVALID),
        .I3(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I4(FSM_sequential_axi_lite_rstate_reg_0),
        .O(axi_lite_wstate_next[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_axi_lite_wstate[1]_i_2 
       (.I0(AXI4_Lite_ARESETN),
        .O(reset));
  LUT4 #(
    .INIT(16'h8F88)) 
    \FSM_onehot_axi_lite_wstate[2]_i_1 
       (.I0(Q[0]),
        .I1(AXI4_Lite_WVALID),
        .I2(AXI4_Lite_BREADY),
        .I3(Q[1]),
        .O(axi_lite_wstate_next[2]));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDPE #(
    .INIT(1'b1)) 
    \FSM_onehot_axi_lite_wstate_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .D(axi_lite_wstate_next[0]),
        .PRE(reset),
        .Q(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_axi_lite_wstate_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_wstate_next[1]),
        .Q(Q[0]));
  (* FSM_ENCODED_STATES = "iSTATE:010,iSTATE0:100,iSTATE1:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_onehot_axi_lite_wstate_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_wstate_next[2]),
        .Q(Q[1]));
  (* SOFT_HLUTNM = "soft_lutpair75" *) 
  LUT5 #(
    .INIT(32'h50505C50)) 
    FSM_sequential_axi_lite_rstate_i_1
       (.I0(AXI4_Lite_RREADY),
        .I1(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I2(FSM_sequential_axi_lite_rstate_reg_0),
        .I3(AXI4_Lite_ARVALID),
        .I4(AXI4_Lite_AWVALID),
        .O(axi_lite_rstate_next));
  (* FSM_ENCODED_STATES = "iSTATE:0,iSTATE0:1" *) 
  FDCE FSM_sequential_axi_lite_rstate_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(axi_lite_rstate_next),
        .Q(FSM_sequential_axi_lite_rstate_reg_0));
  LUT3 #(
    .INIT(8'hDF)) 
    Out_rsvd_i_2
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AR[0]));
  LUT3 #(
    .INIT(8'hDF)) 
    \Out_tmp[31]_i_3 
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AXI4_Lite_ARESETN_0));
  LUT3 #(
    .INIT(8'hDF)) 
    \fifo_sample_count[2]_i_2__1 
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AR[1]));
  LUT3 #(
    .INIT(8'hDF)) 
    out_valid_i_2
       (.I0(AXI4_Lite_ARESETN),
        .I1(soft_reset),
        .I2(IPCORE_RESETN),
        .O(AXI4_Lite_ARESETN_1));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    soft_reset_i_1
       (.I0(soft_reset_i_2_n_0),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[3]),
        .I4(sel0[2]),
        .I5(soft_reset_i_3_n_0),
        .O(strobe_sw));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    soft_reset_i_2
       (.I0(sel0[12]),
        .I1(sel0[13]),
        .I2(sel0[10]),
        .I3(sel0[11]),
        .I4(top_wr_enb),
        .I5(\wdata_reg[31]_0 [0]),
        .O(soft_reset_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    soft_reset_i_3
       (.I0(sel0[6]),
        .I1(sel0[7]),
        .I2(sel0[4]),
        .I3(sel0[5]),
        .I4(sel0[9]),
        .I5(sel0[8]),
        .O(soft_reset_i_3_n_0));
  FDCE soft_reset_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(strobe_sw),
        .Q(soft_reset));
  LUT3 #(
    .INIT(8'h40)) 
    \waddr[15]_i_1 
       (.I0(FSM_sequential_axi_lite_rstate_reg_0),
        .I1(\FSM_onehot_axi_lite_wstate_reg_n_0_[0] ),
        .I2(AXI4_Lite_AWVALID),
        .O(aw_transfer));
  FDCE \waddr_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[8]),
        .Q(sel0[8]));
  FDCE \waddr_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[9]),
        .Q(sel0[9]));
  FDCE \waddr_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[10]),
        .Q(sel0[10]));
  FDCE \waddr_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[11]),
        .Q(sel0[11]));
  FDCE \waddr_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[12]),
        .Q(sel0[12]));
  FDCE \waddr_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[13]),
        .Q(sel0[13]));
  FDCE \waddr_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[0]),
        .Q(sel0[0]));
  FDCE \waddr_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[1]),
        .Q(sel0[1]));
  FDCE \waddr_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[2]),
        .Q(sel0[2]));
  FDCE \waddr_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[3]),
        .Q(sel0[3]));
  FDCE \waddr_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[4]),
        .Q(sel0[4]));
  FDCE \waddr_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[5]),
        .Q(sel0[5]));
  FDCE \waddr_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[6]),
        .Q(sel0[6]));
  FDCE \waddr_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(aw_transfer),
        .CLR(reset),
        .D(AXI4_Lite_AWADDR[7]),
        .Q(sel0[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \wdata[31]_i_1 
       (.I0(AXI4_Lite_WVALID),
        .I1(Q[0]),
        .O(w_transfer));
  FDCE \wdata_reg[0] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[0]),
        .Q(\wdata_reg[31]_0 [0]));
  FDCE \wdata_reg[10] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[10]),
        .Q(\wdata_reg[31]_0 [10]));
  FDCE \wdata_reg[11] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[11]),
        .Q(\wdata_reg[31]_0 [11]));
  FDCE \wdata_reg[12] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[12]),
        .Q(\wdata_reg[31]_0 [12]));
  FDCE \wdata_reg[13] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[13]),
        .Q(\wdata_reg[31]_0 [13]));
  FDCE \wdata_reg[14] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[14]),
        .Q(\wdata_reg[31]_0 [14]));
  FDCE \wdata_reg[15] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[15]),
        .Q(\wdata_reg[31]_0 [15]));
  FDCE \wdata_reg[16] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[16]),
        .Q(\wdata_reg[31]_0 [16]));
  FDCE \wdata_reg[17] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[17]),
        .Q(\wdata_reg[31]_0 [17]));
  FDCE \wdata_reg[18] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[18]),
        .Q(\wdata_reg[31]_0 [18]));
  FDCE \wdata_reg[19] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[19]),
        .Q(\wdata_reg[31]_0 [19]));
  FDCE \wdata_reg[1] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[1]),
        .Q(\wdata_reg[31]_0 [1]));
  FDCE \wdata_reg[20] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[20]),
        .Q(\wdata_reg[31]_0 [20]));
  FDCE \wdata_reg[21] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[21]),
        .Q(\wdata_reg[31]_0 [21]));
  FDCE \wdata_reg[22] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[22]),
        .Q(\wdata_reg[31]_0 [22]));
  FDCE \wdata_reg[23] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[23]),
        .Q(\wdata_reg[31]_0 [23]));
  FDCE \wdata_reg[24] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[24]),
        .Q(\wdata_reg[31]_0 [24]));
  FDCE \wdata_reg[25] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[25]),
        .Q(\wdata_reg[31]_0 [25]));
  FDCE \wdata_reg[26] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[26]),
        .Q(\wdata_reg[31]_0 [26]));
  FDCE \wdata_reg[27] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[27]),
        .Q(\wdata_reg[31]_0 [27]));
  FDCE \wdata_reg[28] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[28]),
        .Q(\wdata_reg[31]_0 [28]));
  FDCE \wdata_reg[29] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[29]),
        .Q(\wdata_reg[31]_0 [29]));
  FDCE \wdata_reg[2] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[2]),
        .Q(\wdata_reg[31]_0 [2]));
  FDCE \wdata_reg[30] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[30]),
        .Q(\wdata_reg[31]_0 [30]));
  FDCE \wdata_reg[31] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[31]),
        .Q(\wdata_reg[31]_0 [31]));
  FDCE \wdata_reg[3] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[3]),
        .Q(\wdata_reg[31]_0 [3]));
  FDCE \wdata_reg[4] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[4]),
        .Q(\wdata_reg[31]_0 [4]));
  FDCE \wdata_reg[5] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[5]),
        .Q(\wdata_reg[31]_0 [5]));
  FDCE \wdata_reg[6] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[6]),
        .Q(\wdata_reg[31]_0 [6]));
  FDCE \wdata_reg[7] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[7]),
        .Q(\wdata_reg[31]_0 [7]));
  FDCE \wdata_reg[8] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[8]),
        .Q(\wdata_reg[31]_0 [8]));
  FDCE \wdata_reg[9] 
       (.C(AXI4_Lite_ACLK),
        .CE(w_transfer),
        .CLR(reset),
        .D(AXI4_Lite_WDATA[9]),
        .Q(\wdata_reg[31]_0 [9]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    wr_enb_1_i_1
       (.I0(AXI4_Lite_WSTRB[2]),
        .I1(AXI4_Lite_WSTRB[3]),
        .I2(AXI4_Lite_WSTRB[0]),
        .I3(AXI4_Lite_WSTRB[1]),
        .I4(Q[0]),
        .I5(AXI4_Lite_WVALID),
        .O(w_transfer_and_wstrb));
  FDCE wr_enb_1_reg
       (.C(AXI4_Lite_ACLK),
        .CE(1'b1),
        .CLR(reset),
        .D(w_transfer_and_wstrb),
        .Q(top_wr_enb));
  LUT6 #(
    .INIT(64'hFF80808080808080)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_1 
       (.I0(AXI4_Lite_ARADDR_6_sn_1),
        .I1(\write_reg_packet_size_axi4_stream_master[31]_i_3_n_0 ),
        .I2(AXI4_Lite_ARADDR_12_sn_1),
        .I3(\write_reg_packet_size_axi4_stream_master[31]_i_5_n_0 ),
        .I4(\write_reg_packet_size_axi4_stream_master[31]_i_6_n_0 ),
        .I5(\write_reg_packet_size_axi4_stream_master[31]_i_7_n_0 ),
        .O(E));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_2 
       (.I0(AXI4_Lite_ARADDR[4]),
        .I1(AXI4_Lite_ARADDR[5]),
        .I2(AXI4_Lite_ARADDR[2]),
        .I3(AXI4_Lite_ARADDR[3]),
        .I4(AXI4_Lite_ARADDR[7]),
        .I5(AXI4_Lite_ARADDR[6]),
        .O(AXI4_Lite_ARADDR_6_sn_1));
  LUT4 #(
    .INIT(16'h4000)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_3 
       (.I0(AXI4_Lite_ARADDR[0]),
        .I1(AXI4_Lite_ARADDR[1]),
        .I2(top_wr_enb),
        .I3(AXI4_Lite_ARVALID),
        .O(\write_reg_packet_size_axi4_stream_master[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_4 
       (.I0(AXI4_Lite_ARADDR[10]),
        .I1(AXI4_Lite_ARADDR[11]),
        .I2(AXI4_Lite_ARADDR[8]),
        .I3(AXI4_Lite_ARADDR[9]),
        .I4(AXI4_Lite_ARADDR[13]),
        .I5(AXI4_Lite_ARADDR[12]),
        .O(AXI4_Lite_ARADDR_12_sn_1));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_5 
       (.I0(sel0[4]),
        .I1(sel0[5]),
        .I2(sel0[2]),
        .I3(sel0[3]),
        .I4(sel0[7]),
        .I5(sel0[6]),
        .O(\write_reg_packet_size_axi4_stream_master[31]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_6 
       (.I0(sel0[0]),
        .I1(sel0[1]),
        .I2(AXI4_Lite_ARVALID),
        .I3(top_wr_enb),
        .O(\write_reg_packet_size_axi4_stream_master[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \write_reg_packet_size_axi4_stream_master[31]_i_7 
       (.I0(sel0[10]),
        .I1(sel0[11]),
        .I2(sel0[8]),
        .I3(sel0[9]),
        .I4(sel0[13]),
        .I5(sel0[12]),
        .O(\write_reg_packet_size_axi4_stream_master[31]_i_7_n_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_dut
   (wr_din,
    Q);
  output [29:0]wr_din;
  input [30:0]Q;

  wire [30:0]Q;
  wire [29:0]wr_din;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_src_MultiplyBy10 u_MultiplyB_ip_src_MultiplyBy10
       (.Q(Q),
        .wr_din(wr_din));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT
   (AXI4_Stream_Master_TLAST,
    S,
    \tlast_counter_out_reg[21] ,
    \tlast_counter_out_reg[30] ,
    IPCORE_CLK,
    \fifo_sample_count_reg[2] ,
    AXI4_Stream_Master_TREADY,
    tlast_counter_out_reg,
    Q,
    tlast_size_value,
    internal_ready_delayed,
    out_valid,
    In_rsvd,
    \fifo_back_indx_reg[0] );
  output AXI4_Stream_Master_TLAST;
  output [3:0]S;
  output [3:0]\tlast_counter_out_reg[21] ;
  output [2:0]\tlast_counter_out_reg[30] ;
  input IPCORE_CLK;
  input \fifo_sample_count_reg[2] ;
  input AXI4_Stream_Master_TREADY;
  input [31:0]tlast_counter_out_reg;
  input [0:0]Q;
  input [30:0]tlast_size_value;
  input internal_ready_delayed;
  input out_valid;
  input In_rsvd;
  input [0:0]\fifo_back_indx_reg[0] ;

  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire IPCORE_CLK;
  wire In_rsvd;
  wire [0:0]Q;
  wire Q_next;
  wire Q_next_1;
  wire [3:0]S;
  wire cache_data_reg_n_0;
  wire cache_valid;
  wire cache_wr_en;
  wire [0:0]\fifo_back_indx_reg[0] ;
  wire \fifo_sample_count_reg[2] ;
  wire fifo_valid;
  wire internal_ready_delayed;
  wire out_valid;
  wire out_valid_0;
  wire out_valid_i_1__1_n_0;
  wire out_wr_en;
  wire [31:0]tlast_counter_out_reg;
  wire [3:0]\tlast_counter_out_reg[21] ;
  wire [2:0]\tlast_counter_out_reg[30] ;
  wire [30:0]tlast_size_value;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_12;
  wire u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_13;

  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hDDD0)) 
    Out_rsvd_i_3
       (.I0(out_valid_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(out_wr_en));
  FDCE Out_rsvd_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_13),
        .Q(AXI4_Stream_Master_TLAST));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hA600)) 
    cache_data_i_2
       (.I0(cache_valid),
        .I1(out_valid_0),
        .I2(AXI4_Stream_Master_TREADY),
        .I3(fifo_valid),
        .O(cache_wr_en));
  FDCE cache_data_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_12),
        .Q(cache_data_reg_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hF220)) 
    cache_valid_i_1__1
       (.I0(out_valid_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(Q_next));
  FDCE cache_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(Q_next),
        .Q(cache_valid));
  FDCE fifo_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(Q_next_1),
        .Q(fifo_valid));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hEEFE)) 
    out_valid_i_1__1
       (.I0(fifo_valid),
        .I1(cache_valid),
        .I2(out_valid_0),
        .I3(AXI4_Stream_Master_TREADY),
        .O(out_valid_i_1__1_n_0));
  FDCE out_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2] ),
        .D(out_valid_i_1__1_n_0),
        .Q(out_valid_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT_classic u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst
       (.AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .IPCORE_CLK(IPCORE_CLK),
        .In_rsvd(In_rsvd),
        .Out_rsvd_reg(cache_data_reg_n_0),
        .Q(Q),
        .Q_next_1(Q_next_1),
        .S(S),
        .cache_data_reg(u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_13),
        .cache_valid(cache_valid),
        .cache_wr_en(cache_wr_en),
        .data_int_reg(u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_12),
        .\fifo_back_indx_reg[0]_0 (\fifo_back_indx_reg[0] ),
        .\fifo_sample_count_reg[2]_0 (\fifo_sample_count_reg[2] ),
        .fifo_valid(fifo_valid),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(out_valid),
        .out_valid_0(out_valid_0),
        .out_wr_en(out_wr_en),
        .tlast_counter_out_reg(tlast_counter_out_reg),
        .\tlast_counter_out_reg[21] (\tlast_counter_out_reg[21] ),
        .\tlast_counter_out_reg[30] (\tlast_counter_out_reg[30] ),
        .tlast_size_value(tlast_size_value));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT_classic
   (Q_next_1,
    S,
    \tlast_counter_out_reg[21] ,
    \tlast_counter_out_reg[30] ,
    data_int_reg,
    cache_data_reg,
    IPCORE_CLK,
    \fifo_sample_count_reg[2]_0 ,
    cache_valid,
    out_valid_0,
    fifo_valid,
    AXI4_Stream_Master_TREADY,
    tlast_counter_out_reg,
    Q,
    tlast_size_value,
    internal_ready_delayed,
    out_valid,
    cache_wr_en,
    Out_rsvd_reg,
    out_wr_en,
    AXI4_Stream_Master_TLAST,
    In_rsvd,
    \fifo_back_indx_reg[0]_0 );
  output Q_next_1;
  output [3:0]S;
  output [3:0]\tlast_counter_out_reg[21] ;
  output [2:0]\tlast_counter_out_reg[30] ;
  output data_int_reg;
  output cache_data_reg;
  input IPCORE_CLK;
  input \fifo_sample_count_reg[2]_0 ;
  input cache_valid;
  input out_valid_0;
  input fifo_valid;
  input AXI4_Stream_Master_TREADY;
  input [31:0]tlast_counter_out_reg;
  input [0:0]Q;
  input [30:0]tlast_size_value;
  input internal_ready_delayed;
  input out_valid;
  input cache_wr_en;
  input Out_rsvd_reg;
  input out_wr_en;
  input AXI4_Stream_Master_TLAST;
  input In_rsvd;
  input [0:0]\fifo_back_indx_reg[0]_0 ;

  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire IPCORE_CLK;
  wire In_rsvd;
  wire Out_rsvd_reg;
  wire [0:0]Q;
  wire Q_next_1;
  wire [3:0]S;
  wire cache_data_reg;
  wire cache_valid;
  wire cache_wr_en;
  wire data_int_reg;
  wire \fifo_back_indx[0]_i_1_n_0 ;
  wire \fifo_back_indx[1]_i_1_n_0 ;
  wire [0:0]\fifo_back_indx_reg[0]_0 ;
  wire \fifo_back_indx_reg_n_0_[0] ;
  wire \fifo_back_indx_reg_n_0_[1] ;
  wire fifo_data_out;
  wire \fifo_front_indx[0]_i_1_n_0 ;
  wire \fifo_front_indx[1]_i_1_n_0 ;
  wire \fifo_front_indx_reg_n_0_[0] ;
  wire \fifo_front_indx_reg_n_0_[1] ;
  wire \fifo_sample_count[0]_i_1_n_0 ;
  wire \fifo_sample_count[1]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_2_n_0 ;
  wire \fifo_sample_count_reg[2]_0 ;
  wire \fifo_sample_count_reg_n_0_[0] ;
  wire \fifo_sample_count_reg_n_0_[1] ;
  wire \fifo_sample_count_reg_n_0_[2] ;
  wire fifo_valid;
  wire internal_ready_delayed;
  wire out_valid;
  wire out_valid_0;
  wire out_wr_en;
  wire [31:0]tlast_counter_out_reg;
  wire [3:0]\tlast_counter_out_reg[21] ;
  wire [2:0]\tlast_counter_out_reg[30] ;
  wire [30:0]tlast_size_value;
  wire w_d1;
  wire w_d1_i_1__1_n_0;
  wire w_d2;
  wire wr_en;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_back_indx[0]_i_1 
       (.I0(wr_en),
        .I1(\fifo_back_indx_reg_n_0_[0] ),
        .O(\fifo_back_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_back_indx[1]_i_1 
       (.I0(\fifo_back_indx_reg_n_0_[0] ),
        .I1(wr_en),
        .I2(\fifo_back_indx_reg_n_0_[1] ),
        .O(\fifo_back_indx[1]_i_1_n_0 ));
  FDCE \fifo_back_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_back_indx[0]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[0] ));
  FDCE \fifo_back_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_back_indx[1]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_front_indx[0]_i_1 
       (.I0(w_d1_i_1__1_n_0),
        .I1(\fifo_front_indx_reg_n_0_[0] ),
        .O(\fifo_front_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_front_indx[1]_i_1 
       (.I0(\fifo_front_indx_reg_n_0_[0] ),
        .I1(w_d1_i_1__1_n_0),
        .I2(\fifo_front_indx_reg_n_0_[1] ),
        .O(\fifo_front_indx[1]_i_1_n_0 ));
  FDCE \fifo_front_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_front_indx[0]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[0] ));
  FDCE \fifo_front_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_front_indx[1]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[1] ));
  LUT6 #(
    .INIT(64'hAA55555567A8A8A8)) 
    \fifo_sample_count[0]_i_1 
       (.I0(\fifo_sample_count[2]_i_2_n_0 ),
        .I1(\fifo_sample_count_reg_n_0_[1] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(out_valid),
        .I4(internal_ready_delayed),
        .I5(\fifo_sample_count_reg_n_0_[0] ),
        .O(\fifo_sample_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF8800F077FF8800)) 
    \fifo_sample_count[1]_i_1 
       (.I0(internal_ready_delayed),
        .I1(out_valid),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(\fifo_sample_count_reg_n_0_[0] ),
        .I4(\fifo_sample_count_reg_n_0_[1] ),
        .I5(\fifo_sample_count[2]_i_2_n_0 ),
        .O(\fifo_sample_count[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF0F0F00078F0F0F0)) 
    \fifo_sample_count[2]_i_1 
       (.I0(internal_ready_delayed),
        .I1(out_valid),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .I3(\fifo_sample_count_reg_n_0_[0] ),
        .I4(\fifo_sample_count_reg_n_0_[1] ),
        .I5(\fifo_sample_count[2]_i_2_n_0 ),
        .O(\fifo_sample_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FEFEFEFEFEFEFE)) 
    \fifo_sample_count[2]_i_2 
       (.I0(\fifo_sample_count_reg_n_0_[2] ),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[1] ),
        .I3(fifo_valid),
        .I4(out_valid_0),
        .I5(cache_valid),
        .O(\fifo_sample_count[2]_i_2_n_0 ));
  FDCE \fifo_sample_count_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(\fifo_sample_count[0]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[0] ));
  FDCE \fifo_sample_count_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(\fifo_sample_count[1]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[1] ));
  FDCE \fifo_sample_count_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(\fifo_sample_count[2]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[2] ));
  LUT5 #(
    .INIT(32'hAAEAAAAA)) 
    fifo_valid_i_1__1
       (.I0(\fifo_sample_count[2]_i_2_n_0 ),
        .I1(fifo_valid),
        .I2(cache_valid),
        .I3(AXI4_Stream_Master_TREADY),
        .I4(out_valid_0),
        .O(Q_next_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_singlebit u_MultiplyB_ip_fifo_TLAST_OUT_classic_ram_singlebit
       (.AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .IPCORE_CLK(IPCORE_CLK),
        .In_rsvd(In_rsvd),
        .Out_rsvd_reg(Out_rsvd_reg),
        .Q(Q),
        .S(S),
        .cache_data_reg(cache_data_reg),
        .cache_valid(cache_valid),
        .cache_wr_en(cache_wr_en),
        .data_int_reg_0(data_int_reg),
        .data_int_reg_1(\fifo_sample_count_reg_n_0_[2] ),
        .data_int_reg_2(\fifo_sample_count_reg_n_0_[0] ),
        .data_int_reg_3(\fifo_sample_count_reg_n_0_[1] ),
        .fifo_data_out(fifo_data_out),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(out_valid),
        .out_wr_en(out_wr_en),
        .rd_addr({\fifo_front_indx_reg_n_0_[1] ,\fifo_front_indx_reg_n_0_[0] }),
        .tlast_counter_out_reg(tlast_counter_out_reg),
        .\tlast_counter_out_reg[21] (\tlast_counter_out_reg[21] ),
        .\tlast_counter_out_reg[30] (\tlast_counter_out_reg[30] ),
        .tlast_size_value(tlast_size_value),
        .w_d1(w_d1),
        .w_d2(w_d2),
        .wr_addr({\fifo_back_indx_reg_n_0_[1] ,\fifo_back_indx_reg_n_0_[0] }),
        .wr_en(wr_en));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    w_d1_i_1__1
       (.I0(cache_valid),
        .I1(out_valid_0),
        .I2(fifo_valid),
        .I3(\fifo_sample_count_reg_n_0_[2] ),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .I5(\fifo_sample_count_reg_n_0_[1] ),
        .O(w_d1_i_1__1_n_0));
  FDCE w_d1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(w_d1_i_1__1_n_0),
        .Q(w_d1));
  FDCE w_d2_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(fifo_data_out),
        .Q(w_d2));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data
   (out_valid_reg_0,
    In_rsvd,
    AXI4_Stream_Slave_TREADY,
    out_valid_reg_1,
    Q,
    IPCORE_CLK,
    AR,
    AXI4_Stream_Slave_TVALID,
    fifo_valid_reg_0,
    CO,
    AXI4_Stream_Slave_TDATA);
  output out_valid_reg_0;
  output In_rsvd;
  output AXI4_Stream_Slave_TREADY;
  output out_valid_reg_1;
  output [30:0]Q;
  input IPCORE_CLK;
  input [1:0]AR;
  input AXI4_Stream_Slave_TVALID;
  input fifo_valid_reg_0;
  input [0:0]CO;
  input [31:0]AXI4_Stream_Slave_TDATA;

  wire [1:0]AR;
  wire [31:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire [0:0]CO;
  wire IPCORE_CLK;
  wire In_rsvd;
  wire [30:0]Q;
  wire Q_next;
  wire Q_next_1;
  wire Q_next_2;
  wire [30:0]cache_data;
  wire cache_valid;
  wire cache_wr_en;
  wire [30:0]data_out_next;
  wire [30:0]fifo_data_out_unsigned;
  wire fifo_valid;
  wire fifo_valid_reg_0;
  wire out_valid_reg_0;
  wire out_valid_reg_1;
  wire out_wr_en;

  LUT4 #(
    .INIT(16'hDDD0)) 
    \Out_tmp[30]_i_1 
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(out_wr_en));
  FDCE \Out_tmp_reg[0] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[0]),
        .Q(Q[0]));
  FDCE \Out_tmp_reg[10] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[10]),
        .Q(Q[10]));
  FDCE \Out_tmp_reg[11] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[11]),
        .Q(Q[11]));
  FDCE \Out_tmp_reg[12] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[12]),
        .Q(Q[12]));
  FDCE \Out_tmp_reg[13] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[13]),
        .Q(Q[13]));
  FDCE \Out_tmp_reg[14] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[14]),
        .Q(Q[14]));
  FDCE \Out_tmp_reg[15] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[15]),
        .Q(Q[15]));
  FDCE \Out_tmp_reg[16] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[16]),
        .Q(Q[16]));
  FDCE \Out_tmp_reg[17] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[17]),
        .Q(Q[17]));
  FDCE \Out_tmp_reg[18] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[18]),
        .Q(Q[18]));
  FDCE \Out_tmp_reg[19] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[19]),
        .Q(Q[19]));
  FDCE \Out_tmp_reg[1] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[1]),
        .Q(Q[1]));
  FDCE \Out_tmp_reg[20] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[20]),
        .Q(Q[20]));
  FDCE \Out_tmp_reg[21] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[21]),
        .Q(Q[21]));
  FDCE \Out_tmp_reg[22] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[22]),
        .Q(Q[22]));
  FDCE \Out_tmp_reg[23] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[23]),
        .Q(Q[23]));
  FDCE \Out_tmp_reg[24] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[24]),
        .Q(Q[24]));
  FDCE \Out_tmp_reg[25] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[25]),
        .Q(Q[25]));
  FDCE \Out_tmp_reg[26] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[26]),
        .Q(Q[26]));
  FDCE \Out_tmp_reg[27] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[27]),
        .Q(Q[27]));
  FDCE \Out_tmp_reg[28] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[28]),
        .Q(Q[28]));
  FDCE \Out_tmp_reg[29] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[29]),
        .Q(Q[29]));
  FDCE \Out_tmp_reg[2] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[2]),
        .Q(Q[2]));
  FDCE \Out_tmp_reg[30] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[30]),
        .Q(Q[30]));
  FDCE \Out_tmp_reg[3] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[3]),
        .Q(Q[3]));
  FDCE \Out_tmp_reg[4] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[4]),
        .Q(Q[4]));
  FDCE \Out_tmp_reg[5] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[5]),
        .Q(Q[5]));
  FDCE \Out_tmp_reg[6] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[6]),
        .Q(Q[6]));
  FDCE \Out_tmp_reg[7] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[7]),
        .Q(Q[7]));
  FDCE \Out_tmp_reg[8] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[8]),
        .Q(Q[8]));
  FDCE \Out_tmp_reg[9] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(AR[1]),
        .D(data_out_next[9]),
        .Q(Q[9]));
  LUT4 #(
    .INIT(16'hA600)) 
    \cache_data[30]_i_1 
       (.I0(cache_valid),
        .I1(out_valid_reg_0),
        .I2(fifo_valid_reg_0),
        .I3(fifo_valid),
        .O(cache_wr_en));
  FDCE \cache_data_reg[0] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[0]),
        .Q(cache_data[0]));
  FDCE \cache_data_reg[10] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[10]),
        .Q(cache_data[10]));
  FDCE \cache_data_reg[11] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[11]),
        .Q(cache_data[11]));
  FDCE \cache_data_reg[12] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[12]),
        .Q(cache_data[12]));
  FDCE \cache_data_reg[13] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[13]),
        .Q(cache_data[13]));
  FDCE \cache_data_reg[14] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[14]),
        .Q(cache_data[14]));
  FDCE \cache_data_reg[15] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[15]),
        .Q(cache_data[15]));
  FDCE \cache_data_reg[16] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[16]),
        .Q(cache_data[16]));
  FDCE \cache_data_reg[17] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[17]),
        .Q(cache_data[17]));
  FDCE \cache_data_reg[18] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[18]),
        .Q(cache_data[18]));
  FDCE \cache_data_reg[19] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[19]),
        .Q(cache_data[19]));
  FDCE \cache_data_reg[1] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[1]),
        .Q(cache_data[1]));
  FDCE \cache_data_reg[20] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[20]),
        .Q(cache_data[20]));
  FDCE \cache_data_reg[21] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[21]),
        .Q(cache_data[21]));
  FDCE \cache_data_reg[22] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[22]),
        .Q(cache_data[22]));
  FDCE \cache_data_reg[23] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[23]),
        .Q(cache_data[23]));
  FDCE \cache_data_reg[24] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[24]),
        .Q(cache_data[24]));
  FDCE \cache_data_reg[25] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[25]),
        .Q(cache_data[25]));
  FDCE \cache_data_reg[26] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[26]),
        .Q(cache_data[26]));
  FDCE \cache_data_reg[27] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[1]),
        .D(fifo_data_out_unsigned[27]),
        .Q(cache_data[27]));
  FDCE \cache_data_reg[28] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[1]),
        .D(fifo_data_out_unsigned[28]),
        .Q(cache_data[28]));
  FDCE \cache_data_reg[29] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[1]),
        .D(fifo_data_out_unsigned[29]),
        .Q(cache_data[29]));
  FDCE \cache_data_reg[2] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[2]),
        .Q(cache_data[2]));
  FDCE \cache_data_reg[30] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[1]),
        .D(fifo_data_out_unsigned[30]),
        .Q(cache_data[30]));
  FDCE \cache_data_reg[3] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[3]),
        .Q(cache_data[3]));
  FDCE \cache_data_reg[4] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[4]),
        .Q(cache_data[4]));
  FDCE \cache_data_reg[5] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[5]),
        .Q(cache_data[5]));
  FDCE \cache_data_reg[6] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[6]),
        .Q(cache_data[6]));
  FDCE \cache_data_reg[7] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[7]),
        .Q(cache_data[7]));
  FDCE \cache_data_reg[8] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[8]),
        .Q(cache_data[8]));
  FDCE \cache_data_reg[9] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR[0]),
        .D(fifo_data_out_unsigned[9]),
        .Q(cache_data[9]));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'hF220)) 
    cache_valid_i_1
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(Q_next));
  FDCE cache_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(Q_next),
        .Q(cache_valid));
  FDCE fifo_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(Q_next_1),
        .Q(fifo_valid));
  (* SOFT_HLUTNM = "soft_lutpair74" *) 
  LUT4 #(
    .INIT(16'hEFEE)) 
    out_valid_i_1
       (.I0(fifo_valid),
        .I1(cache_valid),
        .I2(fifo_valid_reg_0),
        .I3(out_valid_reg_0),
        .O(Q_next_2));
  FDCE out_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(Q_next_2),
        .Q(out_valid_reg_0));
  LUT3 #(
    .INIT(8'h80)) 
    ram_reg_0_3_0_0_i_1
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .I2(CO),
        .O(In_rsvd));
  LUT2 #(
    .INIT(4'h8)) 
    \tlast_counter_out[0]_i_1 
       (.I0(out_valid_reg_0),
        .I1(fifo_valid_reg_0),
        .O(out_valid_reg_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_classic u_MultiplyB_ip_fifo_data_classic_inst
       (.AR(AR),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .D(data_out_next),
        .IPCORE_CLK(IPCORE_CLK),
        .Q(cache_data),
        .Q_next_1(Q_next_1),
        .cache_valid(cache_valid),
        .\data_int_reg[30] (fifo_data_out_unsigned),
        .fifo_valid(fifo_valid),
        .fifo_valid_reg(out_valid_reg_0),
        .fifo_valid_reg_0(fifo_valid_reg_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT
   (out_valid_reg_0,
    internal_ready,
    AXI4_Stream_Master_TDATA,
    IPCORE_CLK,
    AR,
    AXI4_Stream_Master_TREADY,
    internal_ready_delayed,
    out_valid,
    \fifo_sample_count_reg[2] ,
    wr_din,
    \Out_tmp_reg[0]_0 ,
    \fifo_back_indx_reg[0] );
  output out_valid_reg_0;
  output internal_ready;
  output [31:0]AXI4_Stream_Master_TDATA;
  input IPCORE_CLK;
  input [0:0]AR;
  input AXI4_Stream_Master_TREADY;
  input internal_ready_delayed;
  input out_valid;
  input \fifo_sample_count_reg[2] ;
  input [30:0]wr_din;
  input [0:0]\Out_tmp_reg[0]_0 ;
  input [0:0]\fifo_back_indx_reg[0] ;

  wire [0:0]AR;
  wire [31:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TREADY;
  wire IPCORE_CLK;
  wire [0:0]\Out_tmp_reg[0]_0 ;
  wire Q_next;
  wire Q_next_1;
  wire Q_next_2;
  wire \cache_data_reg_n_0_[0] ;
  wire \cache_data_reg_n_0_[10] ;
  wire \cache_data_reg_n_0_[11] ;
  wire \cache_data_reg_n_0_[12] ;
  wire \cache_data_reg_n_0_[13] ;
  wire \cache_data_reg_n_0_[14] ;
  wire \cache_data_reg_n_0_[15] ;
  wire \cache_data_reg_n_0_[16] ;
  wire \cache_data_reg_n_0_[17] ;
  wire \cache_data_reg_n_0_[18] ;
  wire \cache_data_reg_n_0_[19] ;
  wire \cache_data_reg_n_0_[1] ;
  wire \cache_data_reg_n_0_[20] ;
  wire \cache_data_reg_n_0_[21] ;
  wire \cache_data_reg_n_0_[22] ;
  wire \cache_data_reg_n_0_[23] ;
  wire \cache_data_reg_n_0_[24] ;
  wire \cache_data_reg_n_0_[25] ;
  wire \cache_data_reg_n_0_[26] ;
  wire \cache_data_reg_n_0_[27] ;
  wire \cache_data_reg_n_0_[28] ;
  wire \cache_data_reg_n_0_[29] ;
  wire \cache_data_reg_n_0_[2] ;
  wire \cache_data_reg_n_0_[30] ;
  wire \cache_data_reg_n_0_[31] ;
  wire \cache_data_reg_n_0_[3] ;
  wire \cache_data_reg_n_0_[4] ;
  wire \cache_data_reg_n_0_[5] ;
  wire \cache_data_reg_n_0_[6] ;
  wire \cache_data_reg_n_0_[7] ;
  wire \cache_data_reg_n_0_[8] ;
  wire \cache_data_reg_n_0_[9] ;
  wire cache_valid;
  wire cache_wr_en;
  wire [0:0]\fifo_back_indx_reg[0] ;
  wire \fifo_sample_count_reg[2] ;
  wire fifo_valid;
  wire internal_ready;
  wire internal_ready_delayed;
  wire out_valid;
  wire out_valid_reg_0;
  wire out_wr_en;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_10;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_11;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_12;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_13;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_14;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_15;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_16;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_17;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_18;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_19;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_2;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_20;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_21;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_22;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_23;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_24;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_25;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_26;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_27;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_28;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_29;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_3;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_30;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_31;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_32;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_33;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_34;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_35;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_36;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_37;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_38;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_39;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_4;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_40;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_41;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_42;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_43;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_44;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_45;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_46;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_47;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_48;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_49;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_5;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_50;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_51;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_52;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_53;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_54;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_55;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_56;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_57;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_58;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_59;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_6;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_60;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_61;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_62;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_63;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_64;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_65;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_7;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_8;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_9;
  wire [30:0]wr_din;

  LUT4 #(
    .INIT(16'hDDD0)) 
    \Out_tmp[31]_i_1 
       (.I0(out_valid_reg_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(out_wr_en));
  FDCE \Out_tmp_reg[0] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_33),
        .Q(AXI4_Stream_Master_TDATA[0]));
  FDCE \Out_tmp_reg[10] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_23),
        .Q(AXI4_Stream_Master_TDATA[10]));
  FDCE \Out_tmp_reg[11] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_22),
        .Q(AXI4_Stream_Master_TDATA[11]));
  FDCE \Out_tmp_reg[12] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_21),
        .Q(AXI4_Stream_Master_TDATA[12]));
  FDCE \Out_tmp_reg[13] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_20),
        .Q(AXI4_Stream_Master_TDATA[13]));
  FDCE \Out_tmp_reg[14] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_19),
        .Q(AXI4_Stream_Master_TDATA[14]));
  FDCE \Out_tmp_reg[15] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_18),
        .Q(AXI4_Stream_Master_TDATA[15]));
  FDCE \Out_tmp_reg[16] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_17),
        .Q(AXI4_Stream_Master_TDATA[16]));
  FDCE \Out_tmp_reg[17] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_16),
        .Q(AXI4_Stream_Master_TDATA[17]));
  FDCE \Out_tmp_reg[18] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_15),
        .Q(AXI4_Stream_Master_TDATA[18]));
  FDCE \Out_tmp_reg[19] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_14),
        .Q(AXI4_Stream_Master_TDATA[19]));
  FDCE \Out_tmp_reg[1] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_32),
        .Q(AXI4_Stream_Master_TDATA[1]));
  FDCE \Out_tmp_reg[20] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_13),
        .Q(AXI4_Stream_Master_TDATA[20]));
  FDCE \Out_tmp_reg[21] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_12),
        .Q(AXI4_Stream_Master_TDATA[21]));
  FDCE \Out_tmp_reg[22] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_11),
        .Q(AXI4_Stream_Master_TDATA[22]));
  FDCE \Out_tmp_reg[23] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_10),
        .Q(AXI4_Stream_Master_TDATA[23]));
  FDCE \Out_tmp_reg[24] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_9),
        .Q(AXI4_Stream_Master_TDATA[24]));
  FDCE \Out_tmp_reg[25] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_8),
        .Q(AXI4_Stream_Master_TDATA[25]));
  FDCE \Out_tmp_reg[26] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_7),
        .Q(AXI4_Stream_Master_TDATA[26]));
  FDCE \Out_tmp_reg[27] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_6),
        .Q(AXI4_Stream_Master_TDATA[27]));
  FDCE \Out_tmp_reg[28] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_5),
        .Q(AXI4_Stream_Master_TDATA[28]));
  FDCE \Out_tmp_reg[29] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_4),
        .Q(AXI4_Stream_Master_TDATA[29]));
  FDCE \Out_tmp_reg[2] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_31),
        .Q(AXI4_Stream_Master_TDATA[2]));
  FDCE \Out_tmp_reg[30] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_3),
        .Q(AXI4_Stream_Master_TDATA[30]));
  FDCE \Out_tmp_reg[31] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_2),
        .Q(AXI4_Stream_Master_TDATA[31]));
  FDCE \Out_tmp_reg[3] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_30),
        .Q(AXI4_Stream_Master_TDATA[3]));
  FDCE \Out_tmp_reg[4] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_29),
        .Q(AXI4_Stream_Master_TDATA[4]));
  FDCE \Out_tmp_reg[5] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_28),
        .Q(AXI4_Stream_Master_TDATA[5]));
  FDCE \Out_tmp_reg[6] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_27),
        .Q(AXI4_Stream_Master_TDATA[6]));
  FDCE \Out_tmp_reg[7] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_26),
        .Q(AXI4_Stream_Master_TDATA[7]));
  FDCE \Out_tmp_reg[8] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_25),
        .Q(AXI4_Stream_Master_TDATA[8]));
  FDCE \Out_tmp_reg[9] 
       (.C(IPCORE_CLK),
        .CE(out_wr_en),
        .CLR(\Out_tmp_reg[0]_0 ),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_24),
        .Q(AXI4_Stream_Master_TDATA[9]));
  LUT4 #(
    .INIT(16'hA600)) 
    \cache_data[31]_i_1 
       (.I0(cache_valid),
        .I1(out_valid_reg_0),
        .I2(AXI4_Stream_Master_TREADY),
        .I3(fifo_valid),
        .O(cache_wr_en));
  FDCE \cache_data_reg[0] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_65),
        .Q(\cache_data_reg_n_0_[0] ));
  FDCE \cache_data_reg[10] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_55),
        .Q(\cache_data_reg_n_0_[10] ));
  FDCE \cache_data_reg[11] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_54),
        .Q(\cache_data_reg_n_0_[11] ));
  FDCE \cache_data_reg[12] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_53),
        .Q(\cache_data_reg_n_0_[12] ));
  FDCE \cache_data_reg[13] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_52),
        .Q(\cache_data_reg_n_0_[13] ));
  FDCE \cache_data_reg[14] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_51),
        .Q(\cache_data_reg_n_0_[14] ));
  FDCE \cache_data_reg[15] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_50),
        .Q(\cache_data_reg_n_0_[15] ));
  FDCE \cache_data_reg[16] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_49),
        .Q(\cache_data_reg_n_0_[16] ));
  FDCE \cache_data_reg[17] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_48),
        .Q(\cache_data_reg_n_0_[17] ));
  FDCE \cache_data_reg[18] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_47),
        .Q(\cache_data_reg_n_0_[18] ));
  FDCE \cache_data_reg[19] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_46),
        .Q(\cache_data_reg_n_0_[19] ));
  FDCE \cache_data_reg[1] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_64),
        .Q(\cache_data_reg_n_0_[1] ));
  FDCE \cache_data_reg[20] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_45),
        .Q(\cache_data_reg_n_0_[20] ));
  FDCE \cache_data_reg[21] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_44),
        .Q(\cache_data_reg_n_0_[21] ));
  FDCE \cache_data_reg[22] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_43),
        .Q(\cache_data_reg_n_0_[22] ));
  FDCE \cache_data_reg[23] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_42),
        .Q(\cache_data_reg_n_0_[23] ));
  FDCE \cache_data_reg[24] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_41),
        .Q(\cache_data_reg_n_0_[24] ));
  FDCE \cache_data_reg[25] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_40),
        .Q(\cache_data_reg_n_0_[25] ));
  FDCE \cache_data_reg[26] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_39),
        .Q(\cache_data_reg_n_0_[26] ));
  FDCE \cache_data_reg[27] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_38),
        .Q(\cache_data_reg_n_0_[27] ));
  FDCE \cache_data_reg[28] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_37),
        .Q(\cache_data_reg_n_0_[28] ));
  FDCE \cache_data_reg[29] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_36),
        .Q(\cache_data_reg_n_0_[29] ));
  FDCE \cache_data_reg[2] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_63),
        .Q(\cache_data_reg_n_0_[2] ));
  FDCE \cache_data_reg[30] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_35),
        .Q(\cache_data_reg_n_0_[30] ));
  FDCE \cache_data_reg[31] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_34),
        .Q(\cache_data_reg_n_0_[31] ));
  FDCE \cache_data_reg[3] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_62),
        .Q(\cache_data_reg_n_0_[3] ));
  FDCE \cache_data_reg[4] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_61),
        .Q(\cache_data_reg_n_0_[4] ));
  FDCE \cache_data_reg[5] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_60),
        .Q(\cache_data_reg_n_0_[5] ));
  FDCE \cache_data_reg[6] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_59),
        .Q(\cache_data_reg_n_0_[6] ));
  FDCE \cache_data_reg[7] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_58),
        .Q(\cache_data_reg_n_0_[7] ));
  FDCE \cache_data_reg[8] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_57),
        .Q(\cache_data_reg_n_0_[8] ));
  FDCE \cache_data_reg[9] 
       (.C(IPCORE_CLK),
        .CE(cache_wr_en),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_56),
        .Q(\cache_data_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'hF220)) 
    cache_valid_i_1__0
       (.I0(out_valid_reg_0),
        .I1(AXI4_Stream_Master_TREADY),
        .I2(cache_valid),
        .I3(fifo_valid),
        .O(Q_next));
  FDCE cache_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(Q_next),
        .Q(cache_valid));
  FDCE fifo_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(Q_next_1),
        .Q(fifo_valid));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT4 #(
    .INIT(16'hEFEE)) 
    out_valid_i_1__0
       (.I0(fifo_valid),
        .I1(cache_valid),
        .I2(AXI4_Stream_Master_TREADY),
        .I3(out_valid_reg_0),
        .O(Q_next_2));
  FDCE out_valid_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(Q_next_2),
        .Q(out_valid_reg_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT_classic u_MultiplyB_ip_fifo_data_OUT_classic_inst
       (.AR(AR),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .D({u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_2,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_3,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_4,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_5,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_6,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_7,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_8,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_9,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_10,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_11,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_12,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_13,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_14,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_15,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_16,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_17,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_18,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_19,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_20,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_21,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_22,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_23,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_24,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_25,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_26,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_27,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_28,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_29,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_30,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_31,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_32,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_33}),
        .IPCORE_CLK(IPCORE_CLK),
        .Q({\cache_data_reg_n_0_[31] ,\cache_data_reg_n_0_[30] ,\cache_data_reg_n_0_[29] ,\cache_data_reg_n_0_[28] ,\cache_data_reg_n_0_[27] ,\cache_data_reg_n_0_[26] ,\cache_data_reg_n_0_[25] ,\cache_data_reg_n_0_[24] ,\cache_data_reg_n_0_[23] ,\cache_data_reg_n_0_[22] ,\cache_data_reg_n_0_[21] ,\cache_data_reg_n_0_[20] ,\cache_data_reg_n_0_[19] ,\cache_data_reg_n_0_[18] ,\cache_data_reg_n_0_[17] ,\cache_data_reg_n_0_[16] ,\cache_data_reg_n_0_[15] ,\cache_data_reg_n_0_[14] ,\cache_data_reg_n_0_[13] ,\cache_data_reg_n_0_[12] ,\cache_data_reg_n_0_[11] ,\cache_data_reg_n_0_[10] ,\cache_data_reg_n_0_[9] ,\cache_data_reg_n_0_[8] ,\cache_data_reg_n_0_[7] ,\cache_data_reg_n_0_[6] ,\cache_data_reg_n_0_[5] ,\cache_data_reg_n_0_[4] ,\cache_data_reg_n_0_[3] ,\cache_data_reg_n_0_[2] ,\cache_data_reg_n_0_[1] ,\cache_data_reg_n_0_[0] }),
        .Q_next_1(Q_next_1),
        .cache_valid(cache_valid),
        .\data_int_reg[31] ({u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_34,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_35,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_36,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_37,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_38,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_39,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_40,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_41,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_42,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_43,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_44,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_45,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_46,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_47,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_48,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_49,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_50,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_51,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_52,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_53,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_54,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_55,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_56,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_57,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_58,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_59,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_60,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_61,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_62,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_63,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_64,u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_65}),
        .\fifo_back_indx_reg[0]_0 (\fifo_back_indx_reg[0] ),
        .\fifo_sample_count_reg[2]_0 (\fifo_sample_count_reg[2] ),
        .fifo_valid(fifo_valid),
        .internal_ready(internal_ready),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(out_valid),
        .w_d1_reg_0(out_valid_reg_0),
        .wr_din(wr_din));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT_classic
   (Q_next_1,
    internal_ready,
    D,
    \data_int_reg[31] ,
    IPCORE_CLK,
    AR,
    cache_valid,
    w_d1_reg_0,
    fifo_valid,
    AXI4_Stream_Master_TREADY,
    Q,
    internal_ready_delayed,
    out_valid,
    \fifo_sample_count_reg[2]_0 ,
    wr_din,
    \fifo_back_indx_reg[0]_0 );
  output Q_next_1;
  output internal_ready;
  output [31:0]D;
  output [31:0]\data_int_reg[31] ;
  input IPCORE_CLK;
  input [0:0]AR;
  input cache_valid;
  input w_d1_reg_0;
  input fifo_valid;
  input AXI4_Stream_Master_TREADY;
  input [31:0]Q;
  input internal_ready_delayed;
  input out_valid;
  input \fifo_sample_count_reg[2]_0 ;
  input [30:0]wr_din;
  input [0:0]\fifo_back_indx_reg[0]_0 ;

  wire [0:0]AR;
  wire AXI4_Stream_Master_TREADY;
  wire [31:0]D;
  wire IPCORE_CLK;
  wire [2:0]Num;
  wire [31:0]Q;
  wire Q_next_1;
  wire cache_valid;
  wire [31:0]\data_int_reg[31] ;
  wire \fifo_back_indx[0]_i_1_n_0 ;
  wire \fifo_back_indx[1]_i_1_n_0 ;
  wire [0:0]\fifo_back_indx_reg[0]_0 ;
  wire \fifo_back_indx_reg_n_0_[0] ;
  wire \fifo_back_indx_reg_n_0_[1] ;
  wire \fifo_front_indx[0]_i_1_n_0 ;
  wire \fifo_front_indx[1]_i_1_n_0 ;
  wire \fifo_front_indx_reg_n_0_[0] ;
  wire \fifo_front_indx_reg_n_0_[1] ;
  wire fifo_read_enable;
  wire \fifo_sample_count[0]_i_1_n_0 ;
  wire \fifo_sample_count[1]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_2__0_n_0 ;
  wire \fifo_sample_count_reg[2]_0 ;
  wire fifo_valid;
  wire internal_ready;
  wire internal_ready_delayed;
  wire out_valid;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_32;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_33;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_34;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_35;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_36;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_37;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_38;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_39;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_40;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_41;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_42;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_43;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_44;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_45;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_46;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_47;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_48;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_49;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_50;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_51;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_52;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_53;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_54;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_55;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_56;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_57;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_58;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_59;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_60;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_61;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_62;
  wire u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_63;
  wire w_d1;
  wire w_d1_reg_0;
  wire \w_d2_reg_n_0_[0] ;
  wire \w_d2_reg_n_0_[10] ;
  wire \w_d2_reg_n_0_[11] ;
  wire \w_d2_reg_n_0_[12] ;
  wire \w_d2_reg_n_0_[13] ;
  wire \w_d2_reg_n_0_[14] ;
  wire \w_d2_reg_n_0_[15] ;
  wire \w_d2_reg_n_0_[16] ;
  wire \w_d2_reg_n_0_[17] ;
  wire \w_d2_reg_n_0_[18] ;
  wire \w_d2_reg_n_0_[19] ;
  wire \w_d2_reg_n_0_[1] ;
  wire \w_d2_reg_n_0_[20] ;
  wire \w_d2_reg_n_0_[21] ;
  wire \w_d2_reg_n_0_[22] ;
  wire \w_d2_reg_n_0_[23] ;
  wire \w_d2_reg_n_0_[24] ;
  wire \w_d2_reg_n_0_[25] ;
  wire \w_d2_reg_n_0_[26] ;
  wire \w_d2_reg_n_0_[27] ;
  wire \w_d2_reg_n_0_[28] ;
  wire \w_d2_reg_n_0_[29] ;
  wire \w_d2_reg_n_0_[2] ;
  wire \w_d2_reg_n_0_[30] ;
  wire \w_d2_reg_n_0_[31] ;
  wire \w_d2_reg_n_0_[3] ;
  wire \w_d2_reg_n_0_[4] ;
  wire \w_d2_reg_n_0_[5] ;
  wire \w_d2_reg_n_0_[6] ;
  wire \w_d2_reg_n_0_[7] ;
  wire \w_d2_reg_n_0_[8] ;
  wire \w_d2_reg_n_0_[9] ;
  wire w_mux1;
  wire [30:0]wr_din;
  wire wr_en;

  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_back_indx[0]_i_1 
       (.I0(wr_en),
        .I1(\fifo_back_indx_reg_n_0_[0] ),
        .O(\fifo_back_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_back_indx[1]_i_1 
       (.I0(\fifo_back_indx_reg_n_0_[0] ),
        .I1(wr_en),
        .I2(\fifo_back_indx_reg_n_0_[1] ),
        .O(\fifo_back_indx[1]_i_1_n_0 ));
  FDCE \fifo_back_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_back_indx[0]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[0] ));
  FDCE \fifo_back_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_back_indx[1]_i_1_n_0 ),
        .Q(\fifo_back_indx_reg_n_0_[1] ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_front_indx[0]_i_1 
       (.I0(fifo_read_enable),
        .I1(\fifo_front_indx_reg_n_0_[0] ),
        .O(\fifo_front_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_front_indx[1]_i_1 
       (.I0(\fifo_front_indx_reg_n_0_[0] ),
        .I1(fifo_read_enable),
        .I2(\fifo_front_indx_reg_n_0_[1] ),
        .O(\fifo_front_indx[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    \fifo_front_indx[1]_i_2 
       (.I0(cache_valid),
        .I1(w_d1_reg_0),
        .I2(fifo_valid),
        .I3(Num[1]),
        .I4(Num[0]),
        .I5(Num[2]),
        .O(fifo_read_enable));
  FDCE \fifo_front_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_front_indx[0]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[0] ));
  FDCE \fifo_front_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_back_indx_reg[0]_0 ),
        .D(\fifo_front_indx[1]_i_1_n_0 ),
        .Q(\fifo_front_indx_reg_n_0_[1] ));
  LUT3 #(
    .INIT(8'h15)) 
    fifo_rd_ack_i_1
       (.I0(Num[2]),
        .I1(Num[0]),
        .I2(Num[1]),
        .O(internal_ready));
  LUT6 #(
    .INIT(64'hAA55555567A8A8A8)) 
    \fifo_sample_count[0]_i_1 
       (.I0(\fifo_sample_count[2]_i_2__0_n_0 ),
        .I1(Num[1]),
        .I2(Num[2]),
        .I3(out_valid),
        .I4(internal_ready_delayed),
        .I5(Num[0]),
        .O(\fifo_sample_count[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF8F80F007F7F8080)) 
    \fifo_sample_count[1]_i_1 
       (.I0(internal_ready_delayed),
        .I1(out_valid),
        .I2(Num[0]),
        .I3(Num[2]),
        .I4(Num[1]),
        .I5(\fifo_sample_count[2]_i_2__0_n_0 ),
        .O(\fifo_sample_count[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF00F0007F80FF00)) 
    \fifo_sample_count[2]_i_1 
       (.I0(internal_ready_delayed),
        .I1(out_valid),
        .I2(Num[0]),
        .I3(Num[2]),
        .I4(Num[1]),
        .I5(\fifo_sample_count[2]_i_2__0_n_0 ),
        .O(\fifo_sample_count[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FEFEFEFEFEFEFE)) 
    \fifo_sample_count[2]_i_2__0 
       (.I0(Num[1]),
        .I1(Num[0]),
        .I2(Num[2]),
        .I3(fifo_valid),
        .I4(w_d1_reg_0),
        .I5(cache_valid),
        .O(\fifo_sample_count[2]_i_2__0_n_0 ));
  FDCE \fifo_sample_count_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(\fifo_sample_count[0]_i_1_n_0 ),
        .Q(Num[0]));
  FDCE \fifo_sample_count_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(\fifo_sample_count[1]_i_1_n_0 ),
        .Q(Num[1]));
  FDCE \fifo_sample_count_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(\fifo_sample_count_reg[2]_0 ),
        .D(\fifo_sample_count[2]_i_1_n_0 ),
        .Q(Num[2]));
  LUT5 #(
    .INIT(32'hAAEAAAAA)) 
    fifo_valid_i_1__0
       (.I0(\fifo_sample_count[2]_i_2__0_n_0 ),
        .I1(fifo_valid),
        .I2(cache_valid),
        .I3(AXI4_Stream_Master_TREADY),
        .I4(w_d1_reg_0),
        .O(Q_next_1));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0 u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic
       (.ADDRA({\fifo_front_indx_reg_n_0_[1] ,\fifo_front_indx_reg_n_0_[0] }),
        .ADDRD({\fifo_back_indx_reg_n_0_[1] ,\fifo_back_indx_reg_n_0_[0] }),
        .D(D),
        .IPCORE_CLK(IPCORE_CLK),
        .Num(Num),
        .\Out_tmp_reg[31] ({\w_d2_reg_n_0_[31] ,\w_d2_reg_n_0_[30] ,\w_d2_reg_n_0_[29] ,\w_d2_reg_n_0_[28] ,\w_d2_reg_n_0_[27] ,\w_d2_reg_n_0_[26] ,\w_d2_reg_n_0_[25] ,\w_d2_reg_n_0_[24] ,\w_d2_reg_n_0_[23] ,\w_d2_reg_n_0_[22] ,\w_d2_reg_n_0_[21] ,\w_d2_reg_n_0_[20] ,\w_d2_reg_n_0_[19] ,\w_d2_reg_n_0_[18] ,\w_d2_reg_n_0_[17] ,\w_d2_reg_n_0_[16] ,\w_d2_reg_n_0_[15] ,\w_d2_reg_n_0_[14] ,\w_d2_reg_n_0_[13] ,\w_d2_reg_n_0_[12] ,\w_d2_reg_n_0_[11] ,\w_d2_reg_n_0_[10] ,\w_d2_reg_n_0_[9] ,\w_d2_reg_n_0_[8] ,\w_d2_reg_n_0_[7] ,\w_d2_reg_n_0_[6] ,\w_d2_reg_n_0_[5] ,\w_d2_reg_n_0_[4] ,\w_d2_reg_n_0_[3] ,\w_d2_reg_n_0_[2] ,\w_d2_reg_n_0_[1] ,\w_d2_reg_n_0_[0] }),
        .Q(Q),
        .cache_valid(cache_valid),
        .data_int({u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_32,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_33,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_34,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_35,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_36,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_37,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_38,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_39,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_40,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_41,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_42,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_43,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_44,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_45,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_46,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_47,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_48,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_49,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_50,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_51,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_52,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_53,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_54,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_55,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_56,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_57,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_58,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_59,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_60,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_61,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_62,u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_63}),
        .\data_int_reg[31]_0 (\data_int_reg[31] ),
        .internal_ready_delayed(internal_ready_delayed),
        .out_valid(out_valid),
        .w_d1(w_d1),
        .wr_din(wr_din),
        .wr_en(wr_en));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    w_d1_i_1
       (.I0(cache_valid),
        .I1(w_d1_reg_0),
        .I2(fifo_valid),
        .I3(Num[2]),
        .I4(Num[0]),
        .I5(Num[1]),
        .O(w_mux1));
  FDCE w_d1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR),
        .D(w_mux1),
        .Q(w_d1));
  FDCE \w_d2_reg[0] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_63),
        .Q(\w_d2_reg_n_0_[0] ));
  FDCE \w_d2_reg[10] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_53),
        .Q(\w_d2_reg_n_0_[10] ));
  FDCE \w_d2_reg[11] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_52),
        .Q(\w_d2_reg_n_0_[11] ));
  FDCE \w_d2_reg[12] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_51),
        .Q(\w_d2_reg_n_0_[12] ));
  FDCE \w_d2_reg[13] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_50),
        .Q(\w_d2_reg_n_0_[13] ));
  FDCE \w_d2_reg[14] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_49),
        .Q(\w_d2_reg_n_0_[14] ));
  FDCE \w_d2_reg[15] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_48),
        .Q(\w_d2_reg_n_0_[15] ));
  FDCE \w_d2_reg[16] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_47),
        .Q(\w_d2_reg_n_0_[16] ));
  FDCE \w_d2_reg[17] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_46),
        .Q(\w_d2_reg_n_0_[17] ));
  FDCE \w_d2_reg[18] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_45),
        .Q(\w_d2_reg_n_0_[18] ));
  FDCE \w_d2_reg[19] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_44),
        .Q(\w_d2_reg_n_0_[19] ));
  FDCE \w_d2_reg[1] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_62),
        .Q(\w_d2_reg_n_0_[1] ));
  FDCE \w_d2_reg[20] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_43),
        .Q(\w_d2_reg_n_0_[20] ));
  FDCE \w_d2_reg[21] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_42),
        .Q(\w_d2_reg_n_0_[21] ));
  FDCE \w_d2_reg[22] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_41),
        .Q(\w_d2_reg_n_0_[22] ));
  FDCE \w_d2_reg[23] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_40),
        .Q(\w_d2_reg_n_0_[23] ));
  FDCE \w_d2_reg[24] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_39),
        .Q(\w_d2_reg_n_0_[24] ));
  FDCE \w_d2_reg[25] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_38),
        .Q(\w_d2_reg_n_0_[25] ));
  FDCE \w_d2_reg[26] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_37),
        .Q(\w_d2_reg_n_0_[26] ));
  FDCE \w_d2_reg[27] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_36),
        .Q(\w_d2_reg_n_0_[27] ));
  FDCE \w_d2_reg[28] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_35),
        .Q(\w_d2_reg_n_0_[28] ));
  FDCE \w_d2_reg[29] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_34),
        .Q(\w_d2_reg_n_0_[29] ));
  FDCE \w_d2_reg[2] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_61),
        .Q(\w_d2_reg_n_0_[2] ));
  FDCE \w_d2_reg[30] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_33),
        .Q(\w_d2_reg_n_0_[30] ));
  FDCE \w_d2_reg[31] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_32),
        .Q(\w_d2_reg_n_0_[31] ));
  FDCE \w_d2_reg[3] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_60),
        .Q(\w_d2_reg_n_0_[3] ));
  FDCE \w_d2_reg[4] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_59),
        .Q(\w_d2_reg_n_0_[4] ));
  FDCE \w_d2_reg[5] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_58),
        .Q(\w_d2_reg_n_0_[5] ));
  FDCE \w_d2_reg[6] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_57),
        .Q(\w_d2_reg_n_0_[6] ));
  FDCE \w_d2_reg[7] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_56),
        .Q(\w_d2_reg_n_0_[7] ));
  FDCE \w_d2_reg[8] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_55),
        .Q(\w_d2_reg_n_0_[8] ));
  FDCE \w_d2_reg[9] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR),
        .D(u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_54),
        .Q(\w_d2_reg_n_0_[9] ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_classic
   (Q_next_1,
    AXI4_Stream_Slave_TREADY,
    D,
    \data_int_reg[30] ,
    IPCORE_CLK,
    AR,
    cache_valid,
    fifo_valid_reg,
    fifo_valid,
    AXI4_Stream_Slave_TVALID,
    fifo_valid_reg_0,
    Q,
    AXI4_Stream_Slave_TDATA);
  output Q_next_1;
  output AXI4_Stream_Slave_TREADY;
  output [30:0]D;
  output [30:0]\data_int_reg[30] ;
  input IPCORE_CLK;
  input [1:0]AR;
  input cache_valid;
  input fifo_valid_reg;
  input fifo_valid;
  input AXI4_Stream_Slave_TVALID;
  input fifo_valid_reg_0;
  input [30:0]Q;
  input [31:0]AXI4_Stream_Slave_TDATA;

  wire [1:0]AR;
  wire [31:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire [30:0]D;
  wire IPCORE_CLK;
  wire [30:0]Q;
  wire Q_next_1;
  wire cache_valid;
  wire [30:0]data_int;
  wire [30:0]\data_int_reg[30] ;
  wire \fifo_back_indx[0]_i_1_n_0 ;
  wire \fifo_back_indx[1]_i_1_n_0 ;
  wire \fifo_front_indx[0]_i_1_n_0 ;
  wire \fifo_front_indx[1]_i_1_n_0 ;
  wire fifo_read_enable;
  wire \fifo_sample_count[0]_i_1_n_0 ;
  wire \fifo_sample_count[1]_i_1_n_0 ;
  wire \fifo_sample_count[2]_i_1_n_0 ;
  wire \fifo_sample_count_reg_n_0_[0] ;
  wire \fifo_sample_count_reg_n_0_[1] ;
  wire \fifo_sample_count_reg_n_0_[2] ;
  wire fifo_valid;
  wire fifo_valid_i_2_n_0;
  wire fifo_valid_reg;
  wire fifo_valid_reg_0;
  wire [1:0]rd_addr;
  wire w_d1;
  wire [30:0]w_d2;
  wire [1:0]wr_addr;

  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    AXI4_Stream_Slave_TREADY_INST_0
       (.I0(\fifo_sample_count_reg_n_0_[1] ),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[2] ),
        .O(AXI4_Stream_Slave_TREADY));
  (* SOFT_HLUTNM = "soft_lutpair71" *) 
  LUT5 #(
    .INIT(32'h555DAAA2)) 
    \fifo_back_indx[0]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[2] ),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(wr_addr[0]),
        .O(\fifo_back_indx[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h555DFFFFAAA20000)) 
    \fifo_back_indx[1]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[2] ),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(wr_addr[0]),
        .I5(wr_addr[1]),
        .O(\fifo_back_indx[1]_i_1_n_0 ));
  FDCE \fifo_back_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_back_indx[0]_i_1_n_0 ),
        .Q(wr_addr[0]));
  FDCE \fifo_back_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_back_indx[1]_i_1_n_0 ),
        .Q(wr_addr[1]));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \fifo_front_indx[0]_i_1 
       (.I0(fifo_read_enable),
        .I1(rd_addr[0]),
        .O(\fifo_front_indx[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair73" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \fifo_front_indx[1]_i_1 
       (.I0(rd_addr[0]),
        .I1(fifo_read_enable),
        .I2(rd_addr[1]),
        .O(\fifo_front_indx[1]_i_1_n_0 ));
  FDCE \fifo_front_indx_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_front_indx[0]_i_1_n_0 ),
        .Q(rd_addr[0]));
  FDCE \fifo_front_indx_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_front_indx[1]_i_1_n_0 ),
        .Q(rd_addr[1]));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'hA5AD5A52)) 
    \fifo_sample_count[0]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[2] ),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(fifo_read_enable),
        .O(\fifo_sample_count[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair72" *) 
  LUT5 #(
    .INIT(32'hFA0D5FA0)) 
    \fifo_sample_count[1]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[2] ),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(fifo_read_enable),
        .O(\fifo_sample_count[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hCCC16CCC)) 
    \fifo_sample_count[2]_i_1 
       (.I0(AXI4_Stream_Slave_TVALID),
        .I1(\fifo_sample_count_reg_n_0_[2] ),
        .I2(\fifo_sample_count_reg_n_0_[0] ),
        .I3(\fifo_sample_count_reg_n_0_[1] ),
        .I4(fifo_read_enable),
        .O(\fifo_sample_count[2]_i_1_n_0 ));
  FDCE \fifo_sample_count_reg[0] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_sample_count[0]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[0] ));
  FDCE \fifo_sample_count_reg[1] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_sample_count[1]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[1] ));
  FDCE \fifo_sample_count_reg[2] 
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(\fifo_sample_count[2]_i_1_n_0 ),
        .Q(\fifo_sample_count_reg_n_0_[2] ));
  LUT5 #(
    .INIT(32'hAAEAAAAA)) 
    fifo_valid_i_1
       (.I0(fifo_valid_i_2_n_0),
        .I1(fifo_valid),
        .I2(cache_valid),
        .I3(fifo_valid_reg_0),
        .I4(fifo_valid_reg),
        .O(Q_next_1));
  LUT6 #(
    .INIT(64'h00FEFEFEFEFEFEFE)) 
    fifo_valid_i_2
       (.I0(\fifo_sample_count_reg_n_0_[2] ),
        .I1(\fifo_sample_count_reg_n_0_[0] ),
        .I2(\fifo_sample_count_reg_n_0_[1] ),
        .I3(fifo_valid),
        .I4(fifo_valid_reg),
        .I5(cache_valid),
        .O(fifo_valid_i_2_n_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic u_MultiplyB_ip_fifo_data_classic_ram
       (.ADDRA(rd_addr),
        .ADDRD(wr_addr),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .D(D),
        .IPCORE_CLK(IPCORE_CLK),
        .\Out_tmp_reg[30] (w_d2),
        .Q(Q),
        .cache_valid(cache_valid),
        .data_int(data_int),
        .\data_int_reg[1]_0 (\fifo_sample_count_reg_n_0_[2] ),
        .\data_int_reg[1]_1 (\fifo_sample_count_reg_n_0_[0] ),
        .\data_int_reg[1]_2 (\fifo_sample_count_reg_n_0_[1] ),
        .\data_int_reg[30]_0 (\data_int_reg[30] ),
        .w_d1(w_d1));
  LUT6 #(
    .INIT(64'h7F7F7F7F7F7F7F00)) 
    w_d1_i_1__0
       (.I0(cache_valid),
        .I1(fifo_valid_reg),
        .I2(fifo_valid),
        .I3(\fifo_sample_count_reg_n_0_[2] ),
        .I4(\fifo_sample_count_reg_n_0_[0] ),
        .I5(\fifo_sample_count_reg_n_0_[1] ),
        .O(fifo_read_enable));
  FDCE w_d1_reg
       (.C(IPCORE_CLK),
        .CE(1'b1),
        .CLR(AR[1]),
        .D(fifo_read_enable),
        .Q(w_d1));
  FDCE \w_d2_reg[0] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[0]),
        .Q(w_d2[0]));
  FDCE \w_d2_reg[10] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[10]),
        .Q(w_d2[10]));
  FDCE \w_d2_reg[11] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[11]),
        .Q(w_d2[11]));
  FDCE \w_d2_reg[12] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[12]),
        .Q(w_d2[12]));
  FDCE \w_d2_reg[13] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[13]),
        .Q(w_d2[13]));
  FDCE \w_d2_reg[14] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[14]),
        .Q(w_d2[14]));
  FDCE \w_d2_reg[15] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[15]),
        .Q(w_d2[15]));
  FDCE \w_d2_reg[16] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[16]),
        .Q(w_d2[16]));
  FDCE \w_d2_reg[17] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[17]),
        .Q(w_d2[17]));
  FDCE \w_d2_reg[18] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[18]),
        .Q(w_d2[18]));
  FDCE \w_d2_reg[19] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[19]),
        .Q(w_d2[19]));
  FDCE \w_d2_reg[1] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[1]),
        .Q(w_d2[1]));
  FDCE \w_d2_reg[20] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[20]),
        .Q(w_d2[20]));
  FDCE \w_d2_reg[21] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[21]),
        .Q(w_d2[21]));
  FDCE \w_d2_reg[22] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[22]),
        .Q(w_d2[22]));
  FDCE \w_d2_reg[23] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[23]),
        .Q(w_d2[23]));
  FDCE \w_d2_reg[24] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[24]),
        .Q(w_d2[24]));
  FDCE \w_d2_reg[25] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[25]),
        .Q(w_d2[25]));
  FDCE \w_d2_reg[26] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[1]),
        .D(data_int[26]),
        .Q(w_d2[26]));
  FDCE \w_d2_reg[27] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[1]),
        .D(data_int[27]),
        .Q(w_d2[27]));
  FDCE \w_d2_reg[28] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[1]),
        .D(data_int[28]),
        .Q(w_d2[28]));
  FDCE \w_d2_reg[29] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[1]),
        .D(data_int[29]),
        .Q(w_d2[29]));
  FDCE \w_d2_reg[2] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[2]),
        .Q(w_d2[2]));
  FDCE \w_d2_reg[30] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[1]),
        .D(data_int[30]),
        .Q(w_d2[30]));
  FDCE \w_d2_reg[3] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[3]),
        .Q(w_d2[3]));
  FDCE \w_d2_reg[4] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[4]),
        .Q(w_d2[4]));
  FDCE \w_d2_reg[5] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[5]),
        .Q(w_d2[5]));
  FDCE \w_d2_reg[6] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[6]),
        .Q(w_d2[6]));
  FDCE \w_d2_reg[7] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[7]),
        .Q(w_d2[7]));
  FDCE \w_d2_reg[8] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[8]),
        .Q(w_d2[8]));
  FDCE \w_d2_reg[9] 
       (.C(IPCORE_CLK),
        .CE(w_d1),
        .CLR(AR[0]),
        .D(data_int[9]),
        .Q(w_d2[9]));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_src_MultiplyBy10
   (wr_din,
    Q);
  output [29:0]wr_din;
  input [30:0]Q;

  wire [30:0]Q;
  wire ram_reg_0_3_0_5_i_2_n_0;
  wire ram_reg_0_3_0_5_i_2_n_1;
  wire ram_reg_0_3_0_5_i_2_n_2;
  wire ram_reg_0_3_0_5_i_2_n_3;
  wire ram_reg_0_3_0_5_i_3_n_0;
  wire ram_reg_0_3_0_5_i_4_n_0;
  wire ram_reg_0_3_0_5_i_5_n_0;
  wire ram_reg_0_3_12_17_i_1_n_0;
  wire ram_reg_0_3_12_17_i_1_n_1;
  wire ram_reg_0_3_12_17_i_1_n_2;
  wire ram_reg_0_3_12_17_i_1_n_3;
  wire ram_reg_0_3_12_17_i_2_n_0;
  wire ram_reg_0_3_12_17_i_3_n_0;
  wire ram_reg_0_3_12_17_i_4_n_0;
  wire ram_reg_0_3_12_17_i_5_n_0;
  wire ram_reg_0_3_18_23_i_10_n_0;
  wire ram_reg_0_3_18_23_i_1_n_0;
  wire ram_reg_0_3_18_23_i_1_n_1;
  wire ram_reg_0_3_18_23_i_1_n_2;
  wire ram_reg_0_3_18_23_i_1_n_3;
  wire ram_reg_0_3_18_23_i_2_n_0;
  wire ram_reg_0_3_18_23_i_2_n_1;
  wire ram_reg_0_3_18_23_i_2_n_2;
  wire ram_reg_0_3_18_23_i_2_n_3;
  wire ram_reg_0_3_18_23_i_3_n_0;
  wire ram_reg_0_3_18_23_i_4_n_0;
  wire ram_reg_0_3_18_23_i_5_n_0;
  wire ram_reg_0_3_18_23_i_6_n_0;
  wire ram_reg_0_3_18_23_i_7_n_0;
  wire ram_reg_0_3_18_23_i_8_n_0;
  wire ram_reg_0_3_18_23_i_9_n_0;
  wire ram_reg_0_3_24_29_i_1_n_0;
  wire ram_reg_0_3_24_29_i_1_n_1;
  wire ram_reg_0_3_24_29_i_1_n_2;
  wire ram_reg_0_3_24_29_i_1_n_3;
  wire ram_reg_0_3_24_29_i_2_n_0;
  wire ram_reg_0_3_24_29_i_3_n_0;
  wire ram_reg_0_3_24_29_i_4_n_0;
  wire ram_reg_0_3_24_29_i_5_n_0;
  wire ram_reg_0_3_30_31_i_1_n_3;
  wire ram_reg_0_3_30_31_i_2_n_0;
  wire ram_reg_0_3_30_31_i_3_n_0;
  wire ram_reg_0_3_6_11_i_10_n_0;
  wire ram_reg_0_3_6_11_i_1_n_0;
  wire ram_reg_0_3_6_11_i_1_n_1;
  wire ram_reg_0_3_6_11_i_1_n_2;
  wire ram_reg_0_3_6_11_i_1_n_3;
  wire ram_reg_0_3_6_11_i_2_n_0;
  wire ram_reg_0_3_6_11_i_2_n_1;
  wire ram_reg_0_3_6_11_i_2_n_2;
  wire ram_reg_0_3_6_11_i_2_n_3;
  wire ram_reg_0_3_6_11_i_3_n_0;
  wire ram_reg_0_3_6_11_i_4_n_0;
  wire ram_reg_0_3_6_11_i_5_n_0;
  wire ram_reg_0_3_6_11_i_6_n_0;
  wire ram_reg_0_3_6_11_i_7_n_0;
  wire ram_reg_0_3_6_11_i_8_n_0;
  wire ram_reg_0_3_6_11_i_9_n_0;
  wire [29:0]wr_din;
  wire [3:1]NLW_ram_reg_0_3_30_31_i_1_CO_UNCONNECTED;
  wire [3:2]NLW_ram_reg_0_3_30_31_i_1_O_UNCONNECTED;

  CARRY4 ram_reg_0_3_0_5_i_2
       (.CI(1'b0),
        .CO({ram_reg_0_3_0_5_i_2_n_0,ram_reg_0_3_0_5_i_2_n_1,ram_reg_0_3_0_5_i_2_n_2,ram_reg_0_3_0_5_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({Q[4:2],1'b0}),
        .O(wr_din[3:0]),
        .S({ram_reg_0_3_0_5_i_3_n_0,ram_reg_0_3_0_5_i_4_n_0,ram_reg_0_3_0_5_i_5_n_0,Q[1]}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_0_5_i_3
       (.I0(Q[4]),
        .I1(Q[2]),
        .O(ram_reg_0_3_0_5_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_0_5_i_4
       (.I0(Q[3]),
        .I1(Q[1]),
        .O(ram_reg_0_3_0_5_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_0_5_i_5
       (.I0(Q[2]),
        .I1(Q[0]),
        .O(ram_reg_0_3_0_5_i_5_n_0));
  CARRY4 ram_reg_0_3_12_17_i_1
       (.CI(ram_reg_0_3_6_11_i_2_n_0),
        .CO({ram_reg_0_3_12_17_i_1_n_0,ram_reg_0_3_12_17_i_1_n_1,ram_reg_0_3_12_17_i_1_n_2,ram_reg_0_3_12_17_i_1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[16:13]),
        .O(wr_din[15:12]),
        .S({ram_reg_0_3_12_17_i_2_n_0,ram_reg_0_3_12_17_i_3_n_0,ram_reg_0_3_12_17_i_4_n_0,ram_reg_0_3_12_17_i_5_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_12_17_i_2
       (.I0(Q[16]),
        .I1(Q[14]),
        .O(ram_reg_0_3_12_17_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_12_17_i_3
       (.I0(Q[15]),
        .I1(Q[13]),
        .O(ram_reg_0_3_12_17_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_12_17_i_4
       (.I0(Q[14]),
        .I1(Q[12]),
        .O(ram_reg_0_3_12_17_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_12_17_i_5
       (.I0(Q[13]),
        .I1(Q[11]),
        .O(ram_reg_0_3_12_17_i_5_n_0));
  CARRY4 ram_reg_0_3_18_23_i_1
       (.CI(ram_reg_0_3_12_17_i_1_n_0),
        .CO({ram_reg_0_3_18_23_i_1_n_0,ram_reg_0_3_18_23_i_1_n_1,ram_reg_0_3_18_23_i_1_n_2,ram_reg_0_3_18_23_i_1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[20:17]),
        .O(wr_din[19:16]),
        .S({ram_reg_0_3_18_23_i_3_n_0,ram_reg_0_3_18_23_i_4_n_0,ram_reg_0_3_18_23_i_5_n_0,ram_reg_0_3_18_23_i_6_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_10
       (.I0(Q[21]),
        .I1(Q[19]),
        .O(ram_reg_0_3_18_23_i_10_n_0));
  CARRY4 ram_reg_0_3_18_23_i_2
       (.CI(ram_reg_0_3_18_23_i_1_n_0),
        .CO({ram_reg_0_3_18_23_i_2_n_0,ram_reg_0_3_18_23_i_2_n_1,ram_reg_0_3_18_23_i_2_n_2,ram_reg_0_3_18_23_i_2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[24:21]),
        .O(wr_din[23:20]),
        .S({ram_reg_0_3_18_23_i_7_n_0,ram_reg_0_3_18_23_i_8_n_0,ram_reg_0_3_18_23_i_9_n_0,ram_reg_0_3_18_23_i_10_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_3
       (.I0(Q[20]),
        .I1(Q[18]),
        .O(ram_reg_0_3_18_23_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_4
       (.I0(Q[19]),
        .I1(Q[17]),
        .O(ram_reg_0_3_18_23_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_5
       (.I0(Q[18]),
        .I1(Q[16]),
        .O(ram_reg_0_3_18_23_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_6
       (.I0(Q[17]),
        .I1(Q[15]),
        .O(ram_reg_0_3_18_23_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_7
       (.I0(Q[24]),
        .I1(Q[22]),
        .O(ram_reg_0_3_18_23_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_8
       (.I0(Q[23]),
        .I1(Q[21]),
        .O(ram_reg_0_3_18_23_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_18_23_i_9
       (.I0(Q[22]),
        .I1(Q[20]),
        .O(ram_reg_0_3_18_23_i_9_n_0));
  CARRY4 ram_reg_0_3_24_29_i_1
       (.CI(ram_reg_0_3_18_23_i_2_n_0),
        .CO({ram_reg_0_3_24_29_i_1_n_0,ram_reg_0_3_24_29_i_1_n_1,ram_reg_0_3_24_29_i_1_n_2,ram_reg_0_3_24_29_i_1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[28:25]),
        .O(wr_din[27:24]),
        .S({ram_reg_0_3_24_29_i_2_n_0,ram_reg_0_3_24_29_i_3_n_0,ram_reg_0_3_24_29_i_4_n_0,ram_reg_0_3_24_29_i_5_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_24_29_i_2
       (.I0(Q[28]),
        .I1(Q[26]),
        .O(ram_reg_0_3_24_29_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_24_29_i_3
       (.I0(Q[27]),
        .I1(Q[25]),
        .O(ram_reg_0_3_24_29_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_24_29_i_4
       (.I0(Q[26]),
        .I1(Q[24]),
        .O(ram_reg_0_3_24_29_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_24_29_i_5
       (.I0(Q[25]),
        .I1(Q[23]),
        .O(ram_reg_0_3_24_29_i_5_n_0));
  CARRY4 ram_reg_0_3_30_31_i_1
       (.CI(ram_reg_0_3_24_29_i_1_n_0),
        .CO({NLW_ram_reg_0_3_30_31_i_1_CO_UNCONNECTED[3:1],ram_reg_0_3_30_31_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Q[29]}),
        .O({NLW_ram_reg_0_3_30_31_i_1_O_UNCONNECTED[3:2],wr_din[29:28]}),
        .S({1'b0,1'b0,ram_reg_0_3_30_31_i_2_n_0,ram_reg_0_3_30_31_i_3_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_30_31_i_2
       (.I0(Q[30]),
        .I1(Q[28]),
        .O(ram_reg_0_3_30_31_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_30_31_i_3
       (.I0(Q[29]),
        .I1(Q[27]),
        .O(ram_reg_0_3_30_31_i_3_n_0));
  CARRY4 ram_reg_0_3_6_11_i_1
       (.CI(ram_reg_0_3_0_5_i_2_n_0),
        .CO({ram_reg_0_3_6_11_i_1_n_0,ram_reg_0_3_6_11_i_1_n_1,ram_reg_0_3_6_11_i_1_n_2,ram_reg_0_3_6_11_i_1_n_3}),
        .CYINIT(1'b0),
        .DI(Q[8:5]),
        .O(wr_din[7:4]),
        .S({ram_reg_0_3_6_11_i_3_n_0,ram_reg_0_3_6_11_i_4_n_0,ram_reg_0_3_6_11_i_5_n_0,ram_reg_0_3_6_11_i_6_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_10
       (.I0(Q[9]),
        .I1(Q[7]),
        .O(ram_reg_0_3_6_11_i_10_n_0));
  CARRY4 ram_reg_0_3_6_11_i_2
       (.CI(ram_reg_0_3_6_11_i_1_n_0),
        .CO({ram_reg_0_3_6_11_i_2_n_0,ram_reg_0_3_6_11_i_2_n_1,ram_reg_0_3_6_11_i_2_n_2,ram_reg_0_3_6_11_i_2_n_3}),
        .CYINIT(1'b0),
        .DI(Q[12:9]),
        .O(wr_din[11:8]),
        .S({ram_reg_0_3_6_11_i_7_n_0,ram_reg_0_3_6_11_i_8_n_0,ram_reg_0_3_6_11_i_9_n_0,ram_reg_0_3_6_11_i_10_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_3
       (.I0(Q[8]),
        .I1(Q[6]),
        .O(ram_reg_0_3_6_11_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_4
       (.I0(Q[7]),
        .I1(Q[5]),
        .O(ram_reg_0_3_6_11_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_5
       (.I0(Q[6]),
        .I1(Q[4]),
        .O(ram_reg_0_3_6_11_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_6
       (.I0(Q[5]),
        .I1(Q[3]),
        .O(ram_reg_0_3_6_11_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_7
       (.I0(Q[12]),
        .I1(Q[10]),
        .O(ram_reg_0_3_6_11_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_8
       (.I0(Q[11]),
        .I1(Q[9]),
        .O(ram_reg_0_3_6_11_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ram_reg_0_3_6_11_i_9
       (.I0(Q[10]),
        .I1(Q[8]),
        .O(ram_reg_0_3_6_11_i_9_n_0));
endmodule

(* CHECK_LICENSE_TYPE = "System_MultiplyB_ip_0_0,MultiplyB_ip,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "package_project" *) 
(* x_core_info = "MultiplyB_ip,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (IPCORE_CLK,
    IPCORE_RESETN,
    AXI4_Stream_Master_TREADY,
    AXI4_Stream_Slave_TDATA,
    AXI4_Stream_Slave_TVALID,
    AXI4_Lite_ACLK,
    AXI4_Lite_ARESETN,
    AXI4_Lite_AWADDR,
    AXI4_Lite_AWVALID,
    AXI4_Lite_WDATA,
    AXI4_Lite_WSTRB,
    AXI4_Lite_WVALID,
    AXI4_Lite_BREADY,
    AXI4_Lite_ARADDR,
    AXI4_Lite_ARVALID,
    AXI4_Lite_RREADY,
    AXI4_Stream_Master_TDATA,
    AXI4_Stream_Master_TVALID,
    AXI4_Stream_Master_TLAST,
    AXI4_Stream_Slave_TREADY,
    AXI4_Lite_AWREADY,
    AXI4_Lite_WREADY,
    AXI4_Lite_BRESP,
    AXI4_Lite_BVALID,
    AXI4_Lite_ARREADY,
    AXI4_Lite_RDATA,
    AXI4_Lite_RRESP,
    AXI4_Lite_RVALID);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, ASSOCIATED_BUSIF AXI4_Stream_Master:AXI4_Stream_Slave, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input IPCORE_CLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST" *) (* x_interface_parameter = "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input IPCORE_RESETN;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TREADY" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Stream_Master, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input AXI4_Stream_Master_TREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TDATA" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Stream_Slave, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input [31:0]AXI4_Stream_Slave_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TVALID" *) input AXI4_Stream_Slave_TVALID;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input AXI4_Lite_ACLK;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input AXI4_Lite_ARESETN;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [15:0]AXI4_Lite_AWADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID" *) input AXI4_Lite_AWVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA" *) input [31:0]AXI4_Lite_WDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB" *) input [3:0]AXI4_Lite_WSTRB;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID" *) input AXI4_Lite_WVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY" *) input AXI4_Lite_BREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR" *) input [15:0]AXI4_Lite_ARADDR;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID" *) input AXI4_Lite_ARVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY" *) input AXI4_Lite_RREADY;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TDATA" *) output [31:0]AXI4_Stream_Master_TDATA;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TVALID" *) output AXI4_Stream_Master_TVALID;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TLAST" *) output AXI4_Stream_Master_TLAST;
  (* x_interface_info = "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TREADY" *) output AXI4_Stream_Slave_TREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY" *) output AXI4_Lite_AWREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY" *) output AXI4_Lite_WREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP" *) output [1:0]AXI4_Lite_BRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID" *) output AXI4_Lite_BVALID;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY" *) output AXI4_Lite_ARREADY;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA" *) output [31:0]AXI4_Lite_RDATA;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP" *) output [1:0]AXI4_Lite_RRESP;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID" *) output AXI4_Lite_RVALID;

  wire \<const0> ;
  wire AXI4_Lite_ACLK;
  wire [15:0]AXI4_Lite_ARADDR;
  wire AXI4_Lite_ARESETN;
  wire AXI4_Lite_ARREADY;
  wire AXI4_Lite_ARVALID;
  wire [15:0]AXI4_Lite_AWADDR;
  wire AXI4_Lite_AWREADY;
  wire AXI4_Lite_AWVALID;
  wire AXI4_Lite_BREADY;
  wire AXI4_Lite_BVALID;
  wire [29:29]\^AXI4_Lite_RDATA ;
  wire AXI4_Lite_RREADY;
  wire AXI4_Lite_RVALID;
  wire [31:0]AXI4_Lite_WDATA;
  wire AXI4_Lite_WREADY;
  wire [3:0]AXI4_Lite_WSTRB;
  wire AXI4_Lite_WVALID;
  wire [31:0]AXI4_Stream_Master_TDATA;
  wire AXI4_Stream_Master_TLAST;
  wire AXI4_Stream_Master_TREADY;
  wire AXI4_Stream_Master_TVALID;
  wire [31:0]AXI4_Stream_Slave_TDATA;
  wire AXI4_Stream_Slave_TREADY;
  wire AXI4_Stream_Slave_TVALID;
  wire IPCORE_CLK;
  wire IPCORE_RESETN;

  assign AXI4_Lite_BRESP[1] = \<const0> ;
  assign AXI4_Lite_BRESP[0] = \<const0> ;
  assign AXI4_Lite_RDATA[31] = \<const0> ;
  assign AXI4_Lite_RDATA[30] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[29] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[28] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[27] = \<const0> ;
  assign AXI4_Lite_RDATA[26] = \<const0> ;
  assign AXI4_Lite_RDATA[25] = \<const0> ;
  assign AXI4_Lite_RDATA[24] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[23] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[22] = \<const0> ;
  assign AXI4_Lite_RDATA[21] = \<const0> ;
  assign AXI4_Lite_RDATA[20] = \<const0> ;
  assign AXI4_Lite_RDATA[19] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[18] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[17] = \<const0> ;
  assign AXI4_Lite_RDATA[16] = \<const0> ;
  assign AXI4_Lite_RDATA[15] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[14] = \<const0> ;
  assign AXI4_Lite_RDATA[13] = \<const0> ;
  assign AXI4_Lite_RDATA[12] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[11] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[10] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[9] = \<const0> ;
  assign AXI4_Lite_RDATA[8] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[7] = \<const0> ;
  assign AXI4_Lite_RDATA[6] = \<const0> ;
  assign AXI4_Lite_RDATA[5] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[4] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[3] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[2] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RDATA[1] = \<const0> ;
  assign AXI4_Lite_RDATA[0] = \^AXI4_Lite_RDATA [29];
  assign AXI4_Lite_RRESP[1] = \<const0> ;
  assign AXI4_Lite_RRESP[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip U0
       (.AXI4_Lite_ACLK(AXI4_Lite_ACLK),
        .AXI4_Lite_ARADDR(AXI4_Lite_ARADDR[15:2]),
        .AXI4_Lite_ARESETN(AXI4_Lite_ARESETN),
        .AXI4_Lite_ARREADY(AXI4_Lite_ARREADY),
        .AXI4_Lite_ARVALID(AXI4_Lite_ARVALID),
        .AXI4_Lite_AWADDR(AXI4_Lite_AWADDR[15:2]),
        .AXI4_Lite_AWREADY(AXI4_Lite_AWREADY),
        .AXI4_Lite_AWVALID(AXI4_Lite_AWVALID),
        .AXI4_Lite_BREADY(AXI4_Lite_BREADY),
        .AXI4_Lite_BVALID(AXI4_Lite_BVALID),
        .AXI4_Lite_RDATA(\^AXI4_Lite_RDATA ),
        .AXI4_Lite_RREADY(AXI4_Lite_RREADY),
        .AXI4_Lite_RVALID(AXI4_Lite_RVALID),
        .AXI4_Lite_WDATA(AXI4_Lite_WDATA),
        .AXI4_Lite_WREADY(AXI4_Lite_WREADY),
        .AXI4_Lite_WSTRB(AXI4_Lite_WSTRB),
        .AXI4_Lite_WVALID(AXI4_Lite_WVALID),
        .AXI4_Stream_Master_TDATA(AXI4_Stream_Master_TDATA),
        .AXI4_Stream_Master_TLAST(AXI4_Stream_Master_TLAST),
        .AXI4_Stream_Master_TREADY(AXI4_Stream_Master_TREADY),
        .AXI4_Stream_Slave_TDATA(AXI4_Stream_Slave_TDATA),
        .AXI4_Stream_Slave_TREADY(AXI4_Stream_Slave_TREADY),
        .AXI4_Stream_Slave_TVALID(AXI4_Stream_Slave_TVALID),
        .IPCORE_CLK(IPCORE_CLK),
        .IPCORE_RESETN(IPCORE_RESETN),
        .out_valid_reg(AXI4_Stream_Master_TVALID));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
