-- Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2018.3 (win64) Build 2405991 Thu Dec  6 23:38:27 MST 2018
-- Date        : Sat May  4 07:26:36 2019
-- Host        : DESKTOP-MC6SQ8S running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ System_MultiplyB_ip_0_0_sim_netlist.vhdl
-- Design      : System_MultiplyB_ip_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic is
  port (
    D : out STD_LOGIC_VECTOR ( 30 downto 0 );
    data_int : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \data_int_reg[30]_0\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \Out_tmp_reg[30]\ : in STD_LOGIC_VECTOR ( 30 downto 0 );
    w_d1 : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    \data_int_reg[1]_0\ : in STD_LOGIC;
    \data_int_reg[1]_1\ : in STD_LOGIC;
    \data_int_reg[1]_2\ : in STD_LOGIC;
    IPCORE_CLK : in STD_LOGIC;
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ADDRD : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic is
  signal \^data_int\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal p_1_out : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal ram_reg_0_3_30_31_n_0 : STD_LOGIC;
  signal wr_en : STD_LOGIC;
  signal NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Out_tmp[0]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \Out_tmp[10]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \Out_tmp[11]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \Out_tmp[12]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \Out_tmp[13]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \Out_tmp[14]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \Out_tmp[15]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \Out_tmp[16]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \Out_tmp[17]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \Out_tmp[18]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \Out_tmp[19]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \Out_tmp[1]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \Out_tmp[20]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \Out_tmp[21]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \Out_tmp[22]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \Out_tmp[23]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \Out_tmp[24]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \Out_tmp[25]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \Out_tmp[26]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \Out_tmp[27]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \Out_tmp[28]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \Out_tmp[29]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \Out_tmp[2]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \Out_tmp[30]_i_2\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \Out_tmp[3]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \Out_tmp[4]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \Out_tmp[5]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \Out_tmp[6]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \Out_tmp[7]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \Out_tmp[8]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \Out_tmp[9]_i_1\ : label is "soft_lutpair58";
  attribute SOFT_HLUTNM of \cache_data[0]_i_1\ : label is "soft_lutpair40";
  attribute SOFT_HLUTNM of \cache_data[10]_i_1\ : label is "soft_lutpair60";
  attribute SOFT_HLUTNM of \cache_data[11]_i_1\ : label is "soft_lutpair62";
  attribute SOFT_HLUTNM of \cache_data[12]_i_1\ : label is "soft_lutpair64";
  attribute SOFT_HLUTNM of \cache_data[13]_i_1\ : label is "soft_lutpair66";
  attribute SOFT_HLUTNM of \cache_data[14]_i_1\ : label is "soft_lutpair47";
  attribute SOFT_HLUTNM of \cache_data[15]_i_1\ : label is "soft_lutpair41";
  attribute SOFT_HLUTNM of \cache_data[16]_i_1\ : label is "soft_lutpair45";
  attribute SOFT_HLUTNM of \cache_data[17]_i_1\ : label is "soft_lutpair59";
  attribute SOFT_HLUTNM of \cache_data[18]_i_1\ : label is "soft_lutpair63";
  attribute SOFT_HLUTNM of \cache_data[19]_i_1\ : label is "soft_lutpair65";
  attribute SOFT_HLUTNM of \cache_data[1]_i_1\ : label is "soft_lutpair42";
  attribute SOFT_HLUTNM of \cache_data[20]_i_1\ : label is "soft_lutpair67";
  attribute SOFT_HLUTNM of \cache_data[21]_i_1\ : label is "soft_lutpair70";
  attribute SOFT_HLUTNM of \cache_data[22]_i_1\ : label is "soft_lutpair69";
  attribute SOFT_HLUTNM of \cache_data[23]_i_1\ : label is "soft_lutpair68";
  attribute SOFT_HLUTNM of \cache_data[24]_i_1\ : label is "soft_lutpair61";
  attribute SOFT_HLUTNM of \cache_data[25]_i_1\ : label is "soft_lutpair49";
  attribute SOFT_HLUTNM of \cache_data[26]_i_1\ : label is "soft_lutpair51";
  attribute SOFT_HLUTNM of \cache_data[27]_i_1\ : label is "soft_lutpair53";
  attribute SOFT_HLUTNM of \cache_data[28]_i_1\ : label is "soft_lutpair55";
  attribute SOFT_HLUTNM of \cache_data[29]_i_1\ : label is "soft_lutpair57";
  attribute SOFT_HLUTNM of \cache_data[2]_i_1\ : label is "soft_lutpair44";
  attribute SOFT_HLUTNM of \cache_data[30]_i_2\ : label is "soft_lutpair43";
  attribute SOFT_HLUTNM of \cache_data[3]_i_1\ : label is "soft_lutpair46";
  attribute SOFT_HLUTNM of \cache_data[4]_i_1\ : label is "soft_lutpair48";
  attribute SOFT_HLUTNM of \cache_data[5]_i_1\ : label is "soft_lutpair50";
  attribute SOFT_HLUTNM of \cache_data[6]_i_1\ : label is "soft_lutpair52";
  attribute SOFT_HLUTNM of \cache_data[7]_i_1\ : label is "soft_lutpair54";
  attribute SOFT_HLUTNM of \cache_data[8]_i_1\ : label is "soft_lutpair56";
  attribute SOFT_HLUTNM of \cache_data[9]_i_1\ : label is "soft_lutpair58";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_0_5 : label is "";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_3_0_5 : label is 3;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_3_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_12_17 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_12_17 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_12_17 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_12_17 : label is 12;
  attribute ram_slice_end of ram_reg_0_3_12_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_18_23 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_18_23 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_18_23 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_18_23 : label is 18;
  attribute ram_slice_end of ram_reg_0_3_18_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_24_29 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_24_29 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_24_29 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_24_29 : label is 24;
  attribute ram_slice_end of ram_reg_0_3_24_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_30_31 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_30_31 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_30_31 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_30_31 : label is 30;
  attribute ram_slice_end of ram_reg_0_3_30_31 : label is 31;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_6_11 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_6_11 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_6_11 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_6_11 : label is 6;
  attribute ram_slice_end of ram_reg_0_3_6_11 : label is 11;
begin
  data_int(30 downto 0) <= \^data_int\(30 downto 0);
\Out_tmp[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(0),
      I1 => \^data_int\(0),
      I2 => \Out_tmp_reg[30]\(0),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(0)
    );
\Out_tmp[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(10),
      I1 => \^data_int\(10),
      I2 => \Out_tmp_reg[30]\(10),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(10)
    );
\Out_tmp[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(11),
      I1 => \^data_int\(11),
      I2 => \Out_tmp_reg[30]\(11),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(11)
    );
\Out_tmp[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(12),
      I1 => \^data_int\(12),
      I2 => \Out_tmp_reg[30]\(12),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(12)
    );
\Out_tmp[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(13),
      I1 => \^data_int\(13),
      I2 => \Out_tmp_reg[30]\(13),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(13)
    );
\Out_tmp[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(14),
      I1 => \^data_int\(14),
      I2 => \Out_tmp_reg[30]\(14),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(14)
    );
\Out_tmp[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(15),
      I1 => \^data_int\(15),
      I2 => \Out_tmp_reg[30]\(15),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(15)
    );
\Out_tmp[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(16),
      I1 => \^data_int\(16),
      I2 => \Out_tmp_reg[30]\(16),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(16)
    );
\Out_tmp[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(17),
      I1 => \^data_int\(17),
      I2 => \Out_tmp_reg[30]\(17),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(17)
    );
\Out_tmp[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(18),
      I1 => \^data_int\(18),
      I2 => \Out_tmp_reg[30]\(18),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(18)
    );
\Out_tmp[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(19),
      I1 => \^data_int\(19),
      I2 => \Out_tmp_reg[30]\(19),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(19)
    );
\Out_tmp[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(1),
      I1 => \^data_int\(1),
      I2 => \Out_tmp_reg[30]\(1),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(1)
    );
\Out_tmp[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(20),
      I1 => \^data_int\(20),
      I2 => \Out_tmp_reg[30]\(20),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(20)
    );
\Out_tmp[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(21),
      I1 => \^data_int\(21),
      I2 => \Out_tmp_reg[30]\(21),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(21)
    );
\Out_tmp[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(22),
      I1 => \^data_int\(22),
      I2 => \Out_tmp_reg[30]\(22),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(22)
    );
\Out_tmp[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(23),
      I1 => \^data_int\(23),
      I2 => \Out_tmp_reg[30]\(23),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(23)
    );
\Out_tmp[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(24),
      I1 => \^data_int\(24),
      I2 => \Out_tmp_reg[30]\(24),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(24)
    );
\Out_tmp[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(25),
      I1 => \^data_int\(25),
      I2 => \Out_tmp_reg[30]\(25),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(25)
    );
\Out_tmp[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(26),
      I1 => \^data_int\(26),
      I2 => \Out_tmp_reg[30]\(26),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(26)
    );
\Out_tmp[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(27),
      I1 => \^data_int\(27),
      I2 => \Out_tmp_reg[30]\(27),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(27)
    );
\Out_tmp[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(28),
      I1 => \^data_int\(28),
      I2 => \Out_tmp_reg[30]\(28),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(28)
    );
\Out_tmp[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(29),
      I1 => \^data_int\(29),
      I2 => \Out_tmp_reg[30]\(29),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(29)
    );
\Out_tmp[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(2),
      I1 => \^data_int\(2),
      I2 => \Out_tmp_reg[30]\(2),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(2)
    );
\Out_tmp[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(30),
      I1 => \^data_int\(30),
      I2 => \Out_tmp_reg[30]\(30),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(30)
    );
\Out_tmp[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(3),
      I1 => \^data_int\(3),
      I2 => \Out_tmp_reg[30]\(3),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(3)
    );
\Out_tmp[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(4),
      I1 => \^data_int\(4),
      I2 => \Out_tmp_reg[30]\(4),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(4)
    );
\Out_tmp[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(5),
      I1 => \^data_int\(5),
      I2 => \Out_tmp_reg[30]\(5),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(5)
    );
\Out_tmp[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(6),
      I1 => \^data_int\(6),
      I2 => \Out_tmp_reg[30]\(6),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(6)
    );
\Out_tmp[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(7),
      I1 => \^data_int\(7),
      I2 => \Out_tmp_reg[30]\(7),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(7)
    );
\Out_tmp[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(8),
      I1 => \^data_int\(8),
      I2 => \Out_tmp_reg[30]\(8),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(8)
    );
\Out_tmp[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(9),
      I1 => \^data_int\(9),
      I2 => \Out_tmp_reg[30]\(9),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(9)
    );
\cache_data[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(0),
      I1 => \Out_tmp_reg[30]\(0),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(0)
    );
\cache_data[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(10),
      I1 => \Out_tmp_reg[30]\(10),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(10)
    );
\cache_data[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(11),
      I1 => \Out_tmp_reg[30]\(11),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(11)
    );
\cache_data[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(12),
      I1 => \Out_tmp_reg[30]\(12),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(12)
    );
\cache_data[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(13),
      I1 => \Out_tmp_reg[30]\(13),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(13)
    );
\cache_data[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(14),
      I1 => \Out_tmp_reg[30]\(14),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(14)
    );
\cache_data[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(15),
      I1 => \Out_tmp_reg[30]\(15),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(15)
    );
\cache_data[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(16),
      I1 => \Out_tmp_reg[30]\(16),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(16)
    );
\cache_data[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(17),
      I1 => \Out_tmp_reg[30]\(17),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(17)
    );
\cache_data[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(18),
      I1 => \Out_tmp_reg[30]\(18),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(18)
    );
\cache_data[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(19),
      I1 => \Out_tmp_reg[30]\(19),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(19)
    );
\cache_data[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(1),
      I1 => \Out_tmp_reg[30]\(1),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(1)
    );
\cache_data[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(20),
      I1 => \Out_tmp_reg[30]\(20),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(20)
    );
\cache_data[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(21),
      I1 => \Out_tmp_reg[30]\(21),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(21)
    );
\cache_data[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(22),
      I1 => \Out_tmp_reg[30]\(22),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(22)
    );
\cache_data[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(23),
      I1 => \Out_tmp_reg[30]\(23),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(23)
    );
\cache_data[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(24),
      I1 => \Out_tmp_reg[30]\(24),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(24)
    );
\cache_data[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(25),
      I1 => \Out_tmp_reg[30]\(25),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(25)
    );
\cache_data[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(26),
      I1 => \Out_tmp_reg[30]\(26),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(26)
    );
\cache_data[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(27),
      I1 => \Out_tmp_reg[30]\(27),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(27)
    );
\cache_data[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(28),
      I1 => \Out_tmp_reg[30]\(28),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(28)
    );
\cache_data[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(29),
      I1 => \Out_tmp_reg[30]\(29),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(29)
    );
\cache_data[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(2),
      I1 => \Out_tmp_reg[30]\(2),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(2)
    );
\cache_data[30]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(30),
      I1 => \Out_tmp_reg[30]\(30),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(30)
    );
\cache_data[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(3),
      I1 => \Out_tmp_reg[30]\(3),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(3)
    );
\cache_data[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(4),
      I1 => \Out_tmp_reg[30]\(4),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(4)
    );
\cache_data[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(5),
      I1 => \Out_tmp_reg[30]\(5),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(5)
    );
\cache_data[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(6),
      I1 => \Out_tmp_reg[30]\(6),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(6)
    );
\cache_data[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(7),
      I1 => \Out_tmp_reg[30]\(7),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(7)
    );
\cache_data[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(8),
      I1 => \Out_tmp_reg[30]\(8),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(8)
    );
\cache_data[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(9),
      I1 => \Out_tmp_reg[30]\(9),
      I2 => w_d1,
      O => \data_int_reg[30]_0\(9)
    );
\data_int_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(0),
      Q => \^data_int\(0),
      R => '0'
    );
\data_int_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(10),
      Q => \^data_int\(10),
      R => '0'
    );
\data_int_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(11),
      Q => \^data_int\(11),
      R => '0'
    );
\data_int_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(12),
      Q => \^data_int\(12),
      R => '0'
    );
\data_int_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(13),
      Q => \^data_int\(13),
      R => '0'
    );
\data_int_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(14),
      Q => \^data_int\(14),
      R => '0'
    );
\data_int_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(15),
      Q => \^data_int\(15),
      R => '0'
    );
\data_int_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(16),
      Q => \^data_int\(16),
      R => '0'
    );
\data_int_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(17),
      Q => \^data_int\(17),
      R => '0'
    );
\data_int_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(18),
      Q => \^data_int\(18),
      R => '0'
    );
\data_int_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(19),
      Q => \^data_int\(19),
      R => '0'
    );
\data_int_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(1),
      Q => \^data_int\(1),
      R => '0'
    );
\data_int_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(20),
      Q => \^data_int\(20),
      R => '0'
    );
\data_int_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(21),
      Q => \^data_int\(21),
      R => '0'
    );
\data_int_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(22),
      Q => \^data_int\(22),
      R => '0'
    );
\data_int_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(23),
      Q => \^data_int\(23),
      R => '0'
    );
\data_int_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(24),
      Q => \^data_int\(24),
      R => '0'
    );
\data_int_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(25),
      Q => \^data_int\(25),
      R => '0'
    );
\data_int_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(26),
      Q => \^data_int\(26),
      R => '0'
    );
\data_int_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(27),
      Q => \^data_int\(27),
      R => '0'
    );
\data_int_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(28),
      Q => \^data_int\(28),
      R => '0'
    );
\data_int_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(29),
      Q => \^data_int\(29),
      R => '0'
    );
\data_int_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(2),
      Q => \^data_int\(2),
      R => '0'
    );
\data_int_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(30),
      Q => \^data_int\(30),
      R => '0'
    );
\data_int_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(3),
      Q => \^data_int\(3),
      R => '0'
    );
\data_int_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(4),
      Q => \^data_int\(4),
      R => '0'
    );
\data_int_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(5),
      Q => \^data_int\(5),
      R => '0'
    );
\data_int_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(6),
      Q => \^data_int\(6),
      R => '0'
    );
\data_int_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(7),
      Q => \^data_int\(7),
      R => '0'
    );
\data_int_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(8),
      Q => \^data_int\(8),
      R => '0'
    );
\data_int_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => p_1_out(9),
      Q => \^data_int\(9),
      R => '0'
    );
ram_reg_0_3_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(1 downto 0),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(3 downto 2),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(5 downto 4),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => p_1_out(1 downto 0),
      DOB(1 downto 0) => p_1_out(3 downto 2),
      DOC(1 downto 0) => p_1_out(5 downto 4),
      DOD(1 downto 0) => NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => wr_en
    );
ram_reg_0_3_0_5_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AAA2"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \data_int_reg[1]_0\,
      I2 => \data_int_reg[1]_1\,
      I3 => \data_int_reg[1]_2\,
      O => wr_en
    );
ram_reg_0_3_12_17: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(13 downto 12),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(15 downto 14),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(17 downto 16),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => p_1_out(13 downto 12),
      DOB(1 downto 0) => p_1_out(15 downto 14),
      DOC(1 downto 0) => p_1_out(17 downto 16),
      DOD(1 downto 0) => NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => wr_en
    );
ram_reg_0_3_18_23: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(19 downto 18),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(21 downto 20),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(23 downto 22),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => p_1_out(19 downto 18),
      DOB(1 downto 0) => p_1_out(21 downto 20),
      DOC(1 downto 0) => p_1_out(23 downto 22),
      DOD(1 downto 0) => NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => wr_en
    );
ram_reg_0_3_24_29: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(25 downto 24),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(27 downto 26),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(29 downto 28),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => p_1_out(25 downto 24),
      DOB(1 downto 0) => p_1_out(27 downto 26),
      DOC(1 downto 0) => p_1_out(29 downto 28),
      DOD(1 downto 0) => NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => wr_en
    );
ram_reg_0_3_30_31: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(31 downto 30),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1) => ram_reg_0_3_30_31_n_0,
      DOA(0) => p_1_out(30),
      DOB(1 downto 0) => NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => wr_en
    );
ram_reg_0_3_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => AXI4_Stream_Slave_TDATA(7 downto 6),
      DIB(1 downto 0) => AXI4_Stream_Slave_TDATA(9 downto 8),
      DIC(1 downto 0) => AXI4_Stream_Slave_TDATA(11 downto 10),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => p_1_out(7 downto 6),
      DOB(1 downto 0) => p_1_out(9 downto 8),
      DOC(1 downto 0) => p_1_out(11 downto 10),
      DOD(1 downto 0) => NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => wr_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0 is
  port (
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data_int : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \data_int_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \Out_tmp_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    w_d1 : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    Num : in STD_LOGIC_VECTOR ( 2 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    wr_din : in STD_LOGIC_VECTOR ( 30 downto 0 );
    ADDRA : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ADDRD : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0 : entity is "MultiplyB_ip_SimpleDualPortRAM_generic";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0 is
  signal \^data_int\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \p_1_out__0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^wr_en\ : STD_LOGIC;
  signal NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \Out_tmp[0]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \Out_tmp[10]_i_1__0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \Out_tmp[11]_i_1__0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \Out_tmp[12]_i_1__0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \Out_tmp[13]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \Out_tmp[14]_i_1__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \Out_tmp[15]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \Out_tmp[16]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \Out_tmp[17]_i_1__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \Out_tmp[18]_i_1__0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \Out_tmp[19]_i_1__0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \Out_tmp[1]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \Out_tmp[20]_i_1__0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \Out_tmp[21]_i_1__0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \Out_tmp[22]_i_1__0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \Out_tmp[23]_i_1__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \Out_tmp[24]_i_1__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \Out_tmp[25]_i_1__0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \Out_tmp[26]_i_1__0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \Out_tmp[27]_i_1__0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \Out_tmp[28]_i_1__0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \Out_tmp[29]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \Out_tmp[2]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \Out_tmp[30]_i_1__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \Out_tmp[31]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \Out_tmp[3]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \Out_tmp[4]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \Out_tmp[5]_i_1__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \Out_tmp[6]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \Out_tmp[7]_i_1__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \Out_tmp[8]_i_1__0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \Out_tmp[9]_i_1__0\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \cache_data[0]_i_1__0\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \cache_data[10]_i_1__0\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \cache_data[11]_i_1__0\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \cache_data[12]_i_1__0\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \cache_data[13]_i_1__0\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \cache_data[14]_i_1__0\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \cache_data[15]_i_1__0\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \cache_data[16]_i_1__0\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \cache_data[17]_i_1__0\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \cache_data[18]_i_1__0\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \cache_data[19]_i_1__0\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \cache_data[1]_i_1__0\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \cache_data[20]_i_1__0\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \cache_data[21]_i_1__0\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \cache_data[22]_i_1__0\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \cache_data[23]_i_1__0\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \cache_data[24]_i_1__0\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \cache_data[25]_i_1__0\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \cache_data[26]_i_1__0\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \cache_data[27]_i_1__0\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \cache_data[28]_i_1__0\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \cache_data[29]_i_1__0\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \cache_data[2]_i_1__0\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \cache_data[30]_i_1__0\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \cache_data[31]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \cache_data[3]_i_1__0\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \cache_data[4]_i_1__0\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \cache_data[5]_i_1__0\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \cache_data[6]_i_1__0\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \cache_data[7]_i_1__0\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \cache_data[8]_i_1__0\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \cache_data[9]_i_1__0\ : label is "soft_lutpair23";
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_0_5 : label is "";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_3_0_5 : label is 3;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_3_0_5 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_3_0_5 : label is 5;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_12_17 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_12_17 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_12_17 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_12_17 : label is 12;
  attribute ram_slice_end of ram_reg_0_3_12_17 : label is 17;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_18_23 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_18_23 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_18_23 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_18_23 : label is 18;
  attribute ram_slice_end of ram_reg_0_3_18_23 : label is 23;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_24_29 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_24_29 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_24_29 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_24_29 : label is 24;
  attribute ram_slice_end of ram_reg_0_3_24_29 : label is 29;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_30_31 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_30_31 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_30_31 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_30_31 : label is 30;
  attribute ram_slice_end of ram_reg_0_3_30_31 : label is 31;
  attribute METHODOLOGY_DRC_VIOS of ram_reg_0_3_6_11 : label is "";
  attribute ram_addr_begin of ram_reg_0_3_6_11 : label is 0;
  attribute ram_addr_end of ram_reg_0_3_6_11 : label is 3;
  attribute ram_slice_begin of ram_reg_0_3_6_11 : label is 6;
  attribute ram_slice_end of ram_reg_0_3_6_11 : label is 11;
begin
  data_int(31 downto 0) <= \^data_int\(31 downto 0);
  wr_en <= \^wr_en\;
\Out_tmp[0]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(0),
      I1 => \^data_int\(0),
      I2 => \Out_tmp_reg[31]\(0),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(0)
    );
\Out_tmp[10]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(10),
      I1 => \^data_int\(10),
      I2 => \Out_tmp_reg[31]\(10),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(10)
    );
\Out_tmp[11]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(11),
      I1 => \^data_int\(11),
      I2 => \Out_tmp_reg[31]\(11),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(11)
    );
\Out_tmp[12]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(12),
      I1 => \^data_int\(12),
      I2 => \Out_tmp_reg[31]\(12),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(12)
    );
\Out_tmp[13]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(13),
      I1 => \^data_int\(13),
      I2 => \Out_tmp_reg[31]\(13),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(13)
    );
\Out_tmp[14]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(14),
      I1 => \^data_int\(14),
      I2 => \Out_tmp_reg[31]\(14),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(14)
    );
\Out_tmp[15]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(15),
      I1 => \^data_int\(15),
      I2 => \Out_tmp_reg[31]\(15),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(15)
    );
\Out_tmp[16]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(16),
      I1 => \^data_int\(16),
      I2 => \Out_tmp_reg[31]\(16),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(16)
    );
\Out_tmp[17]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(17),
      I1 => \^data_int\(17),
      I2 => \Out_tmp_reg[31]\(17),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(17)
    );
\Out_tmp[18]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(18),
      I1 => \^data_int\(18),
      I2 => \Out_tmp_reg[31]\(18),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(18)
    );
\Out_tmp[19]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(19),
      I1 => \^data_int\(19),
      I2 => \Out_tmp_reg[31]\(19),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(19)
    );
\Out_tmp[1]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(1),
      I1 => \^data_int\(1),
      I2 => \Out_tmp_reg[31]\(1),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(1)
    );
\Out_tmp[20]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(20),
      I1 => \^data_int\(20),
      I2 => \Out_tmp_reg[31]\(20),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(20)
    );
\Out_tmp[21]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(21),
      I1 => \^data_int\(21),
      I2 => \Out_tmp_reg[31]\(21),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(21)
    );
\Out_tmp[22]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(22),
      I1 => \^data_int\(22),
      I2 => \Out_tmp_reg[31]\(22),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(22)
    );
\Out_tmp[23]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(23),
      I1 => \^data_int\(23),
      I2 => \Out_tmp_reg[31]\(23),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(23)
    );
\Out_tmp[24]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(24),
      I1 => \^data_int\(24),
      I2 => \Out_tmp_reg[31]\(24),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(24)
    );
\Out_tmp[25]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(25),
      I1 => \^data_int\(25),
      I2 => \Out_tmp_reg[31]\(25),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(25)
    );
\Out_tmp[26]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(26),
      I1 => \^data_int\(26),
      I2 => \Out_tmp_reg[31]\(26),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(26)
    );
\Out_tmp[27]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(27),
      I1 => \^data_int\(27),
      I2 => \Out_tmp_reg[31]\(27),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(27)
    );
\Out_tmp[28]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(28),
      I1 => \^data_int\(28),
      I2 => \Out_tmp_reg[31]\(28),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(28)
    );
\Out_tmp[29]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(29),
      I1 => \^data_int\(29),
      I2 => \Out_tmp_reg[31]\(29),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(29)
    );
\Out_tmp[2]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(2),
      I1 => \^data_int\(2),
      I2 => \Out_tmp_reg[31]\(2),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(2)
    );
\Out_tmp[30]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(30),
      I1 => \^data_int\(30),
      I2 => \Out_tmp_reg[31]\(30),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(30)
    );
\Out_tmp[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(31),
      I1 => \^data_int\(31),
      I2 => \Out_tmp_reg[31]\(31),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(31)
    );
\Out_tmp[3]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(3),
      I1 => \^data_int\(3),
      I2 => \Out_tmp_reg[31]\(3),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(3)
    );
\Out_tmp[4]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(4),
      I1 => \^data_int\(4),
      I2 => \Out_tmp_reg[31]\(4),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(4)
    );
\Out_tmp[5]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(5),
      I1 => \^data_int\(5),
      I2 => \Out_tmp_reg[31]\(5),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(5)
    );
\Out_tmp[6]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(6),
      I1 => \^data_int\(6),
      I2 => \Out_tmp_reg[31]\(6),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(6)
    );
\Out_tmp[7]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(7),
      I1 => \^data_int\(7),
      I2 => \Out_tmp_reg[31]\(7),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(7)
    );
\Out_tmp[8]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(8),
      I1 => \^data_int\(8),
      I2 => \Out_tmp_reg[31]\(8),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(8)
    );
\Out_tmp[9]_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAACCF0"
    )
        port map (
      I0 => Q(9),
      I1 => \^data_int\(9),
      I2 => \Out_tmp_reg[31]\(9),
      I3 => w_d1,
      I4 => cache_valid,
      O => D(9)
    );
\cache_data[0]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(0),
      I1 => \Out_tmp_reg[31]\(0),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(0)
    );
\cache_data[10]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(10),
      I1 => \Out_tmp_reg[31]\(10),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(10)
    );
\cache_data[11]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(11),
      I1 => \Out_tmp_reg[31]\(11),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(11)
    );
\cache_data[12]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(12),
      I1 => \Out_tmp_reg[31]\(12),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(12)
    );
\cache_data[13]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(13),
      I1 => \Out_tmp_reg[31]\(13),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(13)
    );
\cache_data[14]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(14),
      I1 => \Out_tmp_reg[31]\(14),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(14)
    );
\cache_data[15]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(15),
      I1 => \Out_tmp_reg[31]\(15),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(15)
    );
\cache_data[16]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(16),
      I1 => \Out_tmp_reg[31]\(16),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(16)
    );
\cache_data[17]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(17),
      I1 => \Out_tmp_reg[31]\(17),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(17)
    );
\cache_data[18]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(18),
      I1 => \Out_tmp_reg[31]\(18),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(18)
    );
\cache_data[19]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(19),
      I1 => \Out_tmp_reg[31]\(19),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(19)
    );
\cache_data[1]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(1),
      I1 => \Out_tmp_reg[31]\(1),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(1)
    );
\cache_data[20]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(20),
      I1 => \Out_tmp_reg[31]\(20),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(20)
    );
\cache_data[21]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(21),
      I1 => \Out_tmp_reg[31]\(21),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(21)
    );
\cache_data[22]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(22),
      I1 => \Out_tmp_reg[31]\(22),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(22)
    );
\cache_data[23]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(23),
      I1 => \Out_tmp_reg[31]\(23),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(23)
    );
\cache_data[24]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(24),
      I1 => \Out_tmp_reg[31]\(24),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(24)
    );
\cache_data[25]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(25),
      I1 => \Out_tmp_reg[31]\(25),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(25)
    );
\cache_data[26]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(26),
      I1 => \Out_tmp_reg[31]\(26),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(26)
    );
\cache_data[27]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(27),
      I1 => \Out_tmp_reg[31]\(27),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(27)
    );
\cache_data[28]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(28),
      I1 => \Out_tmp_reg[31]\(28),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(28)
    );
\cache_data[29]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(29),
      I1 => \Out_tmp_reg[31]\(29),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(29)
    );
\cache_data[2]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(2),
      I1 => \Out_tmp_reg[31]\(2),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(2)
    );
\cache_data[30]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(30),
      I1 => \Out_tmp_reg[31]\(30),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(30)
    );
\cache_data[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(31),
      I1 => \Out_tmp_reg[31]\(31),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(31)
    );
\cache_data[3]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(3),
      I1 => \Out_tmp_reg[31]\(3),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(3)
    );
\cache_data[4]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(4),
      I1 => \Out_tmp_reg[31]\(4),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(4)
    );
\cache_data[5]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(5),
      I1 => \Out_tmp_reg[31]\(5),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(5)
    );
\cache_data[6]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(6),
      I1 => \Out_tmp_reg[31]\(6),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(6)
    );
\cache_data[7]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(7),
      I1 => \Out_tmp_reg[31]\(7),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(7)
    );
\cache_data[8]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(8),
      I1 => \Out_tmp_reg[31]\(8),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(8)
    );
\cache_data[9]_i_1__0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => \^data_int\(9),
      I1 => \Out_tmp_reg[31]\(9),
      I2 => w_d1,
      O => \data_int_reg[31]_0\(9)
    );
\data_int_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(0),
      Q => \^data_int\(0),
      R => '0'
    );
\data_int_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(10),
      Q => \^data_int\(10),
      R => '0'
    );
\data_int_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(11),
      Q => \^data_int\(11),
      R => '0'
    );
\data_int_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(12),
      Q => \^data_int\(12),
      R => '0'
    );
\data_int_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(13),
      Q => \^data_int\(13),
      R => '0'
    );
\data_int_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(14),
      Q => \^data_int\(14),
      R => '0'
    );
\data_int_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(15),
      Q => \^data_int\(15),
      R => '0'
    );
\data_int_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(16),
      Q => \^data_int\(16),
      R => '0'
    );
\data_int_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(17),
      Q => \^data_int\(17),
      R => '0'
    );
\data_int_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(18),
      Q => \^data_int\(18),
      R => '0'
    );
\data_int_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(19),
      Q => \^data_int\(19),
      R => '0'
    );
\data_int_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(1),
      Q => \^data_int\(1),
      R => '0'
    );
\data_int_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(20),
      Q => \^data_int\(20),
      R => '0'
    );
\data_int_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(21),
      Q => \^data_int\(21),
      R => '0'
    );
\data_int_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(22),
      Q => \^data_int\(22),
      R => '0'
    );
\data_int_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(23),
      Q => \^data_int\(23),
      R => '0'
    );
\data_int_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(24),
      Q => \^data_int\(24),
      R => '0'
    );
\data_int_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(25),
      Q => \^data_int\(25),
      R => '0'
    );
\data_int_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(26),
      Q => \^data_int\(26),
      R => '0'
    );
\data_int_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(27),
      Q => \^data_int\(27),
      R => '0'
    );
\data_int_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(28),
      Q => \^data_int\(28),
      R => '0'
    );
\data_int_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(29),
      Q => \^data_int\(29),
      R => '0'
    );
\data_int_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(2),
      Q => \^data_int\(2),
      R => '0'
    );
\data_int_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(30),
      Q => \^data_int\(30),
      R => '0'
    );
\data_int_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(31),
      Q => \^data_int\(31),
      R => '0'
    );
\data_int_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(3),
      Q => \^data_int\(3),
      R => '0'
    );
\data_int_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(4),
      Q => \^data_int\(4),
      R => '0'
    );
\data_int_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(5),
      Q => \^data_int\(5),
      R => '0'
    );
\data_int_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(6),
      Q => \^data_int\(6),
      R => '0'
    );
\data_int_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(7),
      Q => \^data_int\(7),
      R => '0'
    );
\data_int_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(8),
      Q => \^data_int\(8),
      R => '0'
    );
\data_int_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__0\(9),
      Q => \^data_int\(9),
      R => '0'
    );
ram_reg_0_3_0_5: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1) => wr_din(0),
      DIA(0) => '0',
      DIB(1 downto 0) => wr_din(2 downto 1),
      DIC(1 downto 0) => wr_din(4 downto 3),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \p_1_out__0\(1 downto 0),
      DOB(1 downto 0) => \p_1_out__0\(3 downto 2),
      DOC(1 downto 0) => \p_1_out__0\(5 downto 4),
      DOD(1 downto 0) => NLW_ram_reg_0_3_0_5_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
\ram_reg_0_3_0_5_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888088"
    )
        port map (
      I0 => internal_ready_delayed,
      I1 => out_valid,
      I2 => Num(0),
      I3 => Num(2),
      I4 => Num(1),
      O => \^wr_en\
    );
ram_reg_0_3_12_17: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => wr_din(12 downto 11),
      DIB(1 downto 0) => wr_din(14 downto 13),
      DIC(1 downto 0) => wr_din(16 downto 15),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \p_1_out__0\(13 downto 12),
      DOB(1 downto 0) => \p_1_out__0\(15 downto 14),
      DOC(1 downto 0) => \p_1_out__0\(17 downto 16),
      DOD(1 downto 0) => NLW_ram_reg_0_3_12_17_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
ram_reg_0_3_18_23: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => wr_din(18 downto 17),
      DIB(1 downto 0) => wr_din(20 downto 19),
      DIC(1 downto 0) => wr_din(22 downto 21),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \p_1_out__0\(19 downto 18),
      DOB(1 downto 0) => \p_1_out__0\(21 downto 20),
      DOC(1 downto 0) => \p_1_out__0\(23 downto 22),
      DOD(1 downto 0) => NLW_ram_reg_0_3_18_23_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
ram_reg_0_3_24_29: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => wr_din(24 downto 23),
      DIB(1 downto 0) => wr_din(26 downto 25),
      DIC(1 downto 0) => wr_din(28 downto 27),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \p_1_out__0\(25 downto 24),
      DOB(1 downto 0) => \p_1_out__0\(27 downto 26),
      DOC(1 downto 0) => \p_1_out__0\(29 downto 28),
      DOD(1 downto 0) => NLW_ram_reg_0_3_24_29_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
ram_reg_0_3_30_31: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => wr_din(30 downto 29),
      DIB(1 downto 0) => B"00",
      DIC(1 downto 0) => B"00",
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \p_1_out__0\(31 downto 30),
      DOB(1 downto 0) => NLW_ram_reg_0_3_30_31_DOB_UNCONNECTED(1 downto 0),
      DOC(1 downto 0) => NLW_ram_reg_0_3_30_31_DOC_UNCONNECTED(1 downto 0),
      DOD(1 downto 0) => NLW_ram_reg_0_3_30_31_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
ram_reg_0_3_6_11: unisim.vcomponents.RAM32M
    generic map(
      INIT_A => X"0000000000000000",
      INIT_B => X"0000000000000000",
      INIT_C => X"0000000000000000",
      INIT_D => X"0000000000000000"
    )
        port map (
      ADDRA(4 downto 2) => B"000",
      ADDRA(1 downto 0) => ADDRA(1 downto 0),
      ADDRB(4 downto 2) => B"000",
      ADDRB(1 downto 0) => ADDRA(1 downto 0),
      ADDRC(4 downto 2) => B"000",
      ADDRC(1 downto 0) => ADDRA(1 downto 0),
      ADDRD(4 downto 2) => B"000",
      ADDRD(1 downto 0) => ADDRD(1 downto 0),
      DIA(1 downto 0) => wr_din(6 downto 5),
      DIB(1 downto 0) => wr_din(8 downto 7),
      DIC(1 downto 0) => wr_din(10 downto 9),
      DID(1 downto 0) => B"00",
      DOA(1 downto 0) => \p_1_out__0\(7 downto 6),
      DOB(1 downto 0) => \p_1_out__0\(9 downto 8),
      DOC(1 downto 0) => \p_1_out__0\(11 downto 10),
      DOD(1 downto 0) => NLW_ram_reg_0_3_6_11_DOD_UNCONNECTED(1 downto 0),
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_singlebit is
  port (
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[21]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[30]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    wr_en : out STD_LOGIC;
    data_int_reg_0 : out STD_LOGIC;
    cache_data_reg : out STD_LOGIC;
    fifo_data_out : out STD_LOGIC;
    tlast_counter_out_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    tlast_size_value : in STD_LOGIC_VECTOR ( 30 downto 0 );
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    data_int_reg_1 : in STD_LOGIC;
    data_int_reg_2 : in STD_LOGIC;
    data_int_reg_3 : in STD_LOGIC;
    w_d1 : in STD_LOGIC;
    w_d2 : in STD_LOGIC;
    cache_wr_en : in STD_LOGIC;
    Out_rsvd_reg : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    out_wr_en : in STD_LOGIC;
    AXI4_Stream_Master_TLAST : in STD_LOGIC;
    IPCORE_CLK : in STD_LOGIC;
    In_rsvd : in STD_LOGIC;
    wr_addr : in STD_LOGIC_VECTOR ( 1 downto 0 );
    rd_addr : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_singlebit;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_singlebit is
  signal \^fifo_data_out\ : STD_LOGIC;
  signal \p_1_out__1\ : STD_LOGIC;
  signal w_waddr_1 : STD_LOGIC;
  signal \^wr_en\ : STD_LOGIC;
  signal NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of cache_data_i_1 : label is "soft_lutpair0";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of ram_reg_0_3_0_0 : label is "RAM16X1D";
  attribute ram_addr_begin : integer;
  attribute ram_addr_begin of ram_reg_0_3_0_0 : label is 0;
  attribute ram_addr_end : integer;
  attribute ram_addr_end of ram_reg_0_3_0_0 : label is 3;
  attribute ram_slice_begin : integer;
  attribute ram_slice_begin of ram_reg_0_3_0_0 : label is 0;
  attribute ram_slice_end : integer;
  attribute ram_slice_end of ram_reg_0_3_0_0 : label is 0;
  attribute SOFT_HLUTNM of w_d2_i_1 : label is "soft_lutpair0";
begin
  fifo_data_out <= \^fifo_data_out\;
  wr_en <= \^wr_en\;
Out_rsvd_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"ACFFAC00"
    )
        port map (
      I0 => Out_rsvd_reg,
      I1 => \^fifo_data_out\,
      I2 => cache_valid,
      I3 => out_wr_en,
      I4 => AXI4_Stream_Master_TLAST,
      O => cache_data_reg
    );
\auto_tlast0_carry__0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(21),
      I1 => tlast_size_value(20),
      I2 => tlast_size_value(22),
      I3 => tlast_counter_out_reg(23),
      I4 => tlast_size_value(21),
      I5 => tlast_counter_out_reg(22),
      O => \tlast_counter_out_reg[21]\(3)
    );
\auto_tlast0_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(18),
      I1 => tlast_size_value(17),
      I2 => tlast_size_value(19),
      I3 => tlast_counter_out_reg(20),
      I4 => tlast_size_value(18),
      I5 => tlast_counter_out_reg(19),
      O => \tlast_counter_out_reg[21]\(2)
    );
\auto_tlast0_carry__0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(15),
      I1 => tlast_size_value(14),
      I2 => tlast_size_value(16),
      I3 => tlast_counter_out_reg(17),
      I4 => tlast_size_value(15),
      I5 => tlast_counter_out_reg(16),
      O => \tlast_counter_out_reg[21]\(1)
    );
\auto_tlast0_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(12),
      I1 => tlast_size_value(11),
      I2 => tlast_size_value(13),
      I3 => tlast_counter_out_reg(14),
      I4 => tlast_size_value(12),
      I5 => tlast_counter_out_reg(13),
      O => \tlast_counter_out_reg[21]\(0)
    );
\auto_tlast0_carry__1_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => tlast_counter_out_reg(30),
      I1 => tlast_size_value(29),
      I2 => tlast_counter_out_reg(31),
      I3 => tlast_size_value(30),
      O => \tlast_counter_out_reg[30]\(2)
    );
\auto_tlast0_carry__1_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(27),
      I1 => tlast_size_value(26),
      I2 => tlast_size_value(28),
      I3 => tlast_counter_out_reg(29),
      I4 => tlast_size_value(27),
      I5 => tlast_counter_out_reg(28),
      O => \tlast_counter_out_reg[30]\(1)
    );
\auto_tlast0_carry__1_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(24),
      I1 => tlast_size_value(23),
      I2 => tlast_size_value(25),
      I3 => tlast_counter_out_reg(26),
      I4 => tlast_size_value(24),
      I5 => tlast_counter_out_reg(25),
      O => \tlast_counter_out_reg[30]\(0)
    );
auto_tlast0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(9),
      I1 => tlast_size_value(8),
      I2 => tlast_size_value(10),
      I3 => tlast_counter_out_reg(11),
      I4 => tlast_size_value(9),
      I5 => tlast_counter_out_reg(10),
      O => S(3)
    );
auto_tlast0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(6),
      I1 => tlast_size_value(5),
      I2 => tlast_size_value(7),
      I3 => tlast_counter_out_reg(8),
      I4 => tlast_size_value(6),
      I5 => tlast_counter_out_reg(7),
      O => S(2)
    );
auto_tlast0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => tlast_counter_out_reg(3),
      I1 => tlast_size_value(2),
      I2 => tlast_size_value(4),
      I3 => tlast_counter_out_reg(5),
      I4 => tlast_size_value(3),
      I5 => tlast_counter_out_reg(4),
      O => S(1)
    );
auto_tlast0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => tlast_counter_out_reg(0),
      I1 => Q(0),
      I2 => tlast_size_value(1),
      I3 => tlast_counter_out_reg(2),
      I4 => tlast_size_value(0),
      I5 => tlast_counter_out_reg(1),
      O => S(0)
    );
cache_data_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"B8FFB800"
    )
        port map (
      I0 => w_waddr_1,
      I1 => w_d1,
      I2 => w_d2,
      I3 => cache_wr_en,
      I4 => Out_rsvd_reg,
      O => data_int_reg_0
    );
data_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => IPCORE_CLK,
      CE => '1',
      D => \p_1_out__1\,
      Q => w_waddr_1,
      R => '0'
    );
ram_reg_0_3_0_0: unisim.vcomponents.RAM32X1D
    generic map(
      INIT => X"00000000"
    )
        port map (
      A0 => wr_addr(0),
      A1 => wr_addr(1),
      A2 => '0',
      A3 => '0',
      A4 => '0',
      D => In_rsvd,
      DPO => \p_1_out__1\,
      DPRA0 => rd_addr(0),
      DPRA1 => rd_addr(1),
      DPRA2 => '0',
      DPRA3 => '0',
      DPRA4 => '0',
      SPO => NLW_ram_reg_0_3_0_0_SPO_UNCONNECTED,
      WCLK => IPCORE_CLK,
      WE => \^wr_en\
    );
ram_reg_0_3_0_0_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"88888808"
    )
        port map (
      I0 => internal_ready_delayed,
      I1 => out_valid,
      I2 => data_int_reg_1,
      I3 => data_int_reg_2,
      I4 => data_int_reg_3,
      O => \^wr_en\
    );
w_d2_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => w_waddr_1,
      I1 => w_d1,
      I2 => w_d2,
      O => \^fifo_data_out\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_addr_decoder is
  port (
    read_reg_ip_timestamp : out STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[28]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[24]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[20]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[16]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[12]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[8]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \AXI4_Lite_ARADDR[2]\ : out STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    \read_reg_ip_timestamp_reg[30]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \AXI4_Lite_RDATA_tmp_reg[30]\ : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_ARVALID : in STD_LOGIC;
    \AXI4_Lite_RDATA_tmp_reg[30]_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]_1\ : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AR : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_addr_decoder;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_addr_decoder is
  signal \^q\ : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \^read_reg_ip_timestamp\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal write_reg_packet_size_axi4_stream_master : STD_LOGIC_VECTOR ( 31 to 31 );
begin
  Q(30 downto 0) <= \^q\(30 downto 0);
  read_reg_ip_timestamp(0) <= \^read_reg_ip_timestamp\(0);
\AXI4_Lite_RDATA_tmp[30]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[30]\,
      I1 => AXI4_Lite_ARADDR(0),
      I2 => \^read_reg_ip_timestamp\(0),
      I3 => AXI4_Lite_ARVALID,
      I4 => AXI4_Lite_ARADDR(1),
      I5 => \AXI4_Lite_RDATA_tmp_reg[30]_0\,
      O => \AXI4_Lite_ARADDR[2]\
    );
\read_reg_ip_timestamp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => \read_reg_ip_timestamp_reg[30]_0\(0),
      D => '1',
      Q => \^read_reg_ip_timestamp\(0)
    );
\tlast_size_value_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(8),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(3)
    );
\tlast_size_value_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(7),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(2)
    );
\tlast_size_value_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(6),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(1)
    );
\tlast_size_value_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(5),
      O => \write_reg_packet_size_axi4_stream_master_reg[8]_0\(0)
    );
\tlast_size_value_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(12),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(3)
    );
\tlast_size_value_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(11),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(2)
    );
\tlast_size_value_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(10),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(1)
    );
\tlast_size_value_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(9),
      O => \write_reg_packet_size_axi4_stream_master_reg[12]_0\(0)
    );
\tlast_size_value_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(16),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(3)
    );
\tlast_size_value_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(15),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(2)
    );
\tlast_size_value_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(14),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(1)
    );
\tlast_size_value_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(13),
      O => \write_reg_packet_size_axi4_stream_master_reg[16]_0\(0)
    );
\tlast_size_value_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(20),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(3)
    );
\tlast_size_value_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(19),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(2)
    );
\tlast_size_value_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(18),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(1)
    );
\tlast_size_value_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(17),
      O => \write_reg_packet_size_axi4_stream_master_reg[20]_0\(0)
    );
\tlast_size_value_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(24),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(3)
    );
\tlast_size_value_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(23),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(2)
    );
\tlast_size_value_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(22),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(1)
    );
\tlast_size_value_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(21),
      O => \write_reg_packet_size_axi4_stream_master_reg[24]_0\(0)
    );
\tlast_size_value_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(28),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(3)
    );
\tlast_size_value_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(27),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(2)
    );
\tlast_size_value_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(26),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(1)
    );
\tlast_size_value_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(25),
      O => \write_reg_packet_size_axi4_stream_master_reg[28]_0\(0)
    );
\tlast_size_value_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => write_reg_packet_size_axi4_stream_master(31),
      O => \write_reg_packet_size_axi4_stream_master_reg[31]_0\(2)
    );
\tlast_size_value_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(30),
      O => \write_reg_packet_size_axi4_stream_master_reg[31]_0\(1)
    );
\tlast_size_value_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(29),
      O => \write_reg_packet_size_axi4_stream_master_reg[31]_0\(0)
    );
tlast_size_value_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(4),
      O => S(3)
    );
tlast_size_value_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(3),
      O => S(2)
    );
tlast_size_value_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(2),
      O => S(1)
    );
tlast_size_value_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(1),
      O => S(0)
    );
\write_reg_packet_size_axi4_stream_master_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0),
      Q => \^q\(0)
    );
\write_reg_packet_size_axi4_stream_master_reg[10]\: unisim.vcomponents.FDPE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10),
      PRE => AR(1),
      Q => \^q\(10)
    );
\write_reg_packet_size_axi4_stream_master_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11),
      Q => \^q\(11)
    );
\write_reg_packet_size_axi4_stream_master_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12),
      Q => \^q\(12)
    );
\write_reg_packet_size_axi4_stream_master_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13),
      Q => \^q\(13)
    );
\write_reg_packet_size_axi4_stream_master_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(14),
      Q => \^q\(14)
    );
\write_reg_packet_size_axi4_stream_master_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(15),
      Q => \^q\(15)
    );
\write_reg_packet_size_axi4_stream_master_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(16),
      Q => \^q\(16)
    );
\write_reg_packet_size_axi4_stream_master_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(17),
      Q => \^q\(17)
    );
\write_reg_packet_size_axi4_stream_master_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(18),
      Q => \^q\(18)
    );
\write_reg_packet_size_axi4_stream_master_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(19),
      Q => \^q\(19)
    );
\write_reg_packet_size_axi4_stream_master_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1),
      Q => \^q\(1)
    );
\write_reg_packet_size_axi4_stream_master_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(20),
      Q => \^q\(20)
    );
\write_reg_packet_size_axi4_stream_master_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(21),
      Q => \^q\(21)
    );
\write_reg_packet_size_axi4_stream_master_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(22),
      Q => \^q\(22)
    );
\write_reg_packet_size_axi4_stream_master_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(23),
      Q => \^q\(23)
    );
\write_reg_packet_size_axi4_stream_master_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(24),
      Q => \^q\(24)
    );
\write_reg_packet_size_axi4_stream_master_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(25),
      Q => \^q\(25)
    );
\write_reg_packet_size_axi4_stream_master_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(26),
      Q => \^q\(26)
    );
\write_reg_packet_size_axi4_stream_master_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(27),
      Q => \^q\(27)
    );
\write_reg_packet_size_axi4_stream_master_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(28),
      Q => \^q\(28)
    );
\write_reg_packet_size_axi4_stream_master_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(29),
      Q => \^q\(29)
    );
\write_reg_packet_size_axi4_stream_master_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2),
      Q => \^q\(2)
    );
\write_reg_packet_size_axi4_stream_master_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(30),
      Q => \^q\(30)
    );
\write_reg_packet_size_axi4_stream_master_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(31),
      Q => write_reg_packet_size_axi4_stream_master(31)
    );
\write_reg_packet_size_axi4_stream_master_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3),
      Q => \^q\(3)
    );
\write_reg_packet_size_axi4_stream_master_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(0),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4),
      Q => \^q\(4)
    );
\write_reg_packet_size_axi4_stream_master_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5),
      Q => \^q\(5)
    );
\write_reg_packet_size_axi4_stream_master_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6),
      Q => \^q\(6)
    );
\write_reg_packet_size_axi4_stream_master_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7),
      Q => \^q\(7)
    );
\write_reg_packet_size_axi4_stream_master_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8),
      Q => \^q\(8)
    );
\write_reg_packet_size_axi4_stream_master_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => E(0),
      CLR => AR(1),
      D => \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9),
      Q => \^q\(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite_module is
  port (
    FSM_sequential_axi_lite_rstate_reg_0 : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARESETN_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AR : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_ARESETN_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_AWREADY : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_ARADDR_6_sp_1 : out STD_LOGIC;
    AXI4_Lite_ARADDR_12_sp_1 : out STD_LOGIC;
    AXI4_Lite_ARREADY : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \wdata_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    \AXI4_Lite_RDATA_tmp_reg[30]_0\ : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    read_reg_ip_timestamp : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite_module;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite_module is
  signal AXI4_Lite_ARADDR_12_sn_1 : STD_LOGIC;
  signal AXI4_Lite_ARADDR_6_sn_1 : STD_LOGIC;
  signal \^axi4_lite_awready\ : STD_LOGIC;
  signal \^axi4_lite_rdata\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \AXI4_Lite_RDATA_tmp[30]_i_1_n_0\ : STD_LOGIC;
  signal \AXI4_Lite_RDATA_tmp[30]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\ : STD_LOGIC;
  signal \^fsm_sequential_axi_lite_rstate_reg_0\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal aw_transfer : STD_LOGIC;
  signal axi_lite_rstate_next : STD_LOGIC;
  signal axi_lite_wstate_next : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal reset : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal soft_reset : STD_LOGIC;
  signal soft_reset_i_2_n_0 : STD_LOGIC;
  signal soft_reset_i_3_n_0 : STD_LOGIC;
  signal strobe_sw : STD_LOGIC;
  signal top_wr_enb : STD_LOGIC;
  signal w_transfer : STD_LOGIC;
  signal w_transfer_and_wstrb : STD_LOGIC;
  signal \^wdata_reg[31]_0\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \write_reg_packet_size_axi4_stream_master[31]_i_3_n_0\ : STD_LOGIC;
  signal \write_reg_packet_size_axi4_stream_master[31]_i_5_n_0\ : STD_LOGIC;
  signal \write_reg_packet_size_axi4_stream_master[31]_i_6_n_0\ : STD_LOGIC;
  signal \write_reg_packet_size_axi4_stream_master[31]_i_7_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI4_Lite_ARREADY_INST_0 : label is "soft_lutpair75";
  attribute SOFT_HLUTNM of AXI4_Lite_AWREADY_INST_0 : label is "soft_lutpair76";
  attribute SOFT_HLUTNM of \FSM_onehot_axi_lite_wstate[1]_i_1\ : label is "soft_lutpair76";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[0]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[1]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_axi_lite_wstate_reg[2]\ : label is "iSTATE:010,iSTATE0:100,iSTATE1:001";
  attribute SOFT_HLUTNM of FSM_sequential_axi_lite_rstate_i_1 : label is "soft_lutpair75";
  attribute FSM_ENCODED_STATES of FSM_sequential_axi_lite_rstate_reg : label is "iSTATE:0,iSTATE0:1";
begin
  AXI4_Lite_ARADDR_12_sp_1 <= AXI4_Lite_ARADDR_12_sn_1;
  AXI4_Lite_ARADDR_6_sp_1 <= AXI4_Lite_ARADDR_6_sn_1;
  AXI4_Lite_AWREADY <= \^axi4_lite_awready\;
  AXI4_Lite_RDATA(0) <= \^axi4_lite_rdata\(0);
  FSM_sequential_axi_lite_rstate_reg_0 <= \^fsm_sequential_axi_lite_rstate_reg_0\;
  Q(1 downto 0) <= \^q\(1 downto 0);
  \wdata_reg[31]_0\(31 downto 0) <= \^wdata_reg[31]_0\(31 downto 0);
AXI4_Lite_ARREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"04"
    )
        port map (
      I0 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I1 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I2 => AXI4_Lite_AWVALID,
      O => AXI4_Lite_ARREADY
    );
AXI4_Lite_AWREADY_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I1 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => \^axi4_lite_awready\
    );
\AXI4_Lite_RDATA_tmp[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEFFFFFF0E000000"
    )
        port map (
      I0 => \AXI4_Lite_RDATA_tmp_reg[30]_0\,
      I1 => \AXI4_Lite_RDATA_tmp[30]_i_3_n_0\,
      I2 => AXI4_Lite_AWVALID,
      I3 => AXI4_Lite_ARVALID,
      I4 => \^axi4_lite_awready\,
      I5 => \^axi4_lite_rdata\(0),
      O => \AXI4_Lite_RDATA_tmp[30]_i_1_n_0\
    );
\AXI4_Lite_RDATA_tmp[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => \write_reg_packet_size_axi4_stream_master[31]_i_7_n_0\,
      I1 => sel0(0),
      I2 => read_reg_ip_timestamp(0),
      I3 => AXI4_Lite_ARVALID,
      I4 => sel0(1),
      I5 => \write_reg_packet_size_axi4_stream_master[31]_i_5_n_0\,
      O => \AXI4_Lite_RDATA_tmp[30]_i_3_n_0\
    );
\AXI4_Lite_RDATA_tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => \AXI4_Lite_RDATA_tmp[30]_i_1_n_0\,
      Q => \^axi4_lite_rdata\(0)
    );
\FSM_onehot_axi_lite_wstate[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFC0D5C0"
    )
        port map (
      I0 => AXI4_Lite_AWVALID,
      I1 => AXI4_Lite_BREADY,
      I2 => \^q\(1),
      I3 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I4 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => axi_lite_wstate_next(0)
    );
\FSM_onehot_axi_lite_wstate[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4444F444"
    )
        port map (
      I0 => AXI4_Lite_WVALID,
      I1 => \^q\(0),
      I2 => AXI4_Lite_AWVALID,
      I3 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I4 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      O => axi_lite_wstate_next(1)
    );
\FSM_onehot_axi_lite_wstate[1]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      O => reset
    );
\FSM_onehot_axi_lite_wstate[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8F88"
    )
        port map (
      I0 => \^q\(0),
      I1 => AXI4_Lite_WVALID,
      I2 => AXI4_Lite_BREADY,
      I3 => \^q\(1),
      O => axi_lite_wstate_next(2)
    );
\FSM_onehot_axi_lite_wstate_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      D => axi_lite_wstate_next(0),
      PRE => reset,
      Q => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\
    );
\FSM_onehot_axi_lite_wstate_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_wstate_next(1),
      Q => \^q\(0)
    );
\FSM_onehot_axi_lite_wstate_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_wstate_next(2),
      Q => \^q\(1)
    );
FSM_sequential_axi_lite_rstate_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"50505C50"
    )
        port map (
      I0 => AXI4_Lite_RREADY,
      I1 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I2 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I3 => AXI4_Lite_ARVALID,
      I4 => AXI4_Lite_AWVALID,
      O => axi_lite_rstate_next
    );
FSM_sequential_axi_lite_rstate_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => axi_lite_rstate_next,
      Q => \^fsm_sequential_axi_lite_rstate_reg_0\
    );
Out_rsvd_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AR(0)
    );
\Out_tmp[31]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AXI4_Lite_ARESETN_0(0)
    );
\fifo_sample_count[2]_i_2__1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AR(1)
    );
out_valid_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => AXI4_Lite_ARESETN,
      I1 => soft_reset,
      I2 => IPCORE_RESETN,
      O => AXI4_Lite_ARESETN_1(0)
    );
soft_reset_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000200000000"
    )
        port map (
      I0 => soft_reset_i_2_n_0,
      I1 => sel0(1),
      I2 => sel0(0),
      I3 => sel0(3),
      I4 => sel0(2),
      I5 => soft_reset_i_3_n_0,
      O => strobe_sw
    );
soft_reset_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => sel0(12),
      I1 => sel0(13),
      I2 => sel0(10),
      I3 => sel0(11),
      I4 => top_wr_enb,
      I5 => \^wdata_reg[31]_0\(0),
      O => soft_reset_i_2_n_0
    );
soft_reset_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => sel0(6),
      I1 => sel0(7),
      I2 => sel0(4),
      I3 => sel0(5),
      I4 => sel0(9),
      I5 => sel0(8),
      O => soft_reset_i_3_n_0
    );
soft_reset_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => strobe_sw,
      Q => soft_reset
    );
\waddr[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"40"
    )
        port map (
      I0 => \^fsm_sequential_axi_lite_rstate_reg_0\,
      I1 => \FSM_onehot_axi_lite_wstate_reg_n_0_[0]\,
      I2 => AXI4_Lite_AWVALID,
      O => aw_transfer
    );
\waddr_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(8),
      Q => sel0(8)
    );
\waddr_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(9),
      Q => sel0(9)
    );
\waddr_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(10),
      Q => sel0(10)
    );
\waddr_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(11),
      Q => sel0(11)
    );
\waddr_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(12),
      Q => sel0(12)
    );
\waddr_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(13),
      Q => sel0(13)
    );
\waddr_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(0),
      Q => sel0(0)
    );
\waddr_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(1),
      Q => sel0(1)
    );
\waddr_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(2),
      Q => sel0(2)
    );
\waddr_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(3),
      Q => sel0(3)
    );
\waddr_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(4),
      Q => sel0(4)
    );
\waddr_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(5),
      Q => sel0(5)
    );
\waddr_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(6),
      Q => sel0(6)
    );
\waddr_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => aw_transfer,
      CLR => reset,
      D => AXI4_Lite_AWADDR(7),
      Q => sel0(7)
    );
\wdata[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => AXI4_Lite_WVALID,
      I1 => \^q\(0),
      O => w_transfer
    );
\wdata_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(0),
      Q => \^wdata_reg[31]_0\(0)
    );
\wdata_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(10),
      Q => \^wdata_reg[31]_0\(10)
    );
\wdata_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(11),
      Q => \^wdata_reg[31]_0\(11)
    );
\wdata_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(12),
      Q => \^wdata_reg[31]_0\(12)
    );
\wdata_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(13),
      Q => \^wdata_reg[31]_0\(13)
    );
\wdata_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(14),
      Q => \^wdata_reg[31]_0\(14)
    );
\wdata_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(15),
      Q => \^wdata_reg[31]_0\(15)
    );
\wdata_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(16),
      Q => \^wdata_reg[31]_0\(16)
    );
\wdata_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(17),
      Q => \^wdata_reg[31]_0\(17)
    );
\wdata_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(18),
      Q => \^wdata_reg[31]_0\(18)
    );
\wdata_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(19),
      Q => \^wdata_reg[31]_0\(19)
    );
\wdata_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(1),
      Q => \^wdata_reg[31]_0\(1)
    );
\wdata_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(20),
      Q => \^wdata_reg[31]_0\(20)
    );
\wdata_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(21),
      Q => \^wdata_reg[31]_0\(21)
    );
\wdata_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(22),
      Q => \^wdata_reg[31]_0\(22)
    );
\wdata_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(23),
      Q => \^wdata_reg[31]_0\(23)
    );
\wdata_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(24),
      Q => \^wdata_reg[31]_0\(24)
    );
\wdata_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(25),
      Q => \^wdata_reg[31]_0\(25)
    );
\wdata_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(26),
      Q => \^wdata_reg[31]_0\(26)
    );
\wdata_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(27),
      Q => \^wdata_reg[31]_0\(27)
    );
\wdata_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(28),
      Q => \^wdata_reg[31]_0\(28)
    );
\wdata_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(29),
      Q => \^wdata_reg[31]_0\(29)
    );
\wdata_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(2),
      Q => \^wdata_reg[31]_0\(2)
    );
\wdata_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(30),
      Q => \^wdata_reg[31]_0\(30)
    );
\wdata_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(31),
      Q => \^wdata_reg[31]_0\(31)
    );
\wdata_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(3),
      Q => \^wdata_reg[31]_0\(3)
    );
\wdata_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(4),
      Q => \^wdata_reg[31]_0\(4)
    );
\wdata_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(5),
      Q => \^wdata_reg[31]_0\(5)
    );
\wdata_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(6),
      Q => \^wdata_reg[31]_0\(6)
    );
\wdata_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(7),
      Q => \^wdata_reg[31]_0\(7)
    );
\wdata_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(8),
      Q => \^wdata_reg[31]_0\(8)
    );
\wdata_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => w_transfer,
      CLR => reset,
      D => AXI4_Lite_WDATA(9),
      Q => \^wdata_reg[31]_0\(9)
    );
wr_enb_1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => AXI4_Lite_WSTRB(2),
      I1 => AXI4_Lite_WSTRB(3),
      I2 => AXI4_Lite_WSTRB(0),
      I3 => AXI4_Lite_WSTRB(1),
      I4 => \^q\(0),
      I5 => AXI4_Lite_WVALID,
      O => w_transfer_and_wstrb
    );
wr_enb_1_reg: unisim.vcomponents.FDCE
     port map (
      C => AXI4_Lite_ACLK,
      CE => '1',
      CLR => reset,
      D => w_transfer_and_wstrb,
      Q => top_wr_enb
    );
\write_reg_packet_size_axi4_stream_master[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF80808080808080"
    )
        port map (
      I0 => AXI4_Lite_ARADDR_6_sn_1,
      I1 => \write_reg_packet_size_axi4_stream_master[31]_i_3_n_0\,
      I2 => AXI4_Lite_ARADDR_12_sn_1,
      I3 => \write_reg_packet_size_axi4_stream_master[31]_i_5_n_0\,
      I4 => \write_reg_packet_size_axi4_stream_master[31]_i_6_n_0\,
      I5 => \write_reg_packet_size_axi4_stream_master[31]_i_7_n_0\,
      O => E(0)
    );
\write_reg_packet_size_axi4_stream_master[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(4),
      I1 => AXI4_Lite_ARADDR(5),
      I2 => AXI4_Lite_ARADDR(2),
      I3 => AXI4_Lite_ARADDR(3),
      I4 => AXI4_Lite_ARADDR(7),
      I5 => AXI4_Lite_ARADDR(6),
      O => AXI4_Lite_ARADDR_6_sn_1
    );
\write_reg_packet_size_axi4_stream_master[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(0),
      I1 => AXI4_Lite_ARADDR(1),
      I2 => top_wr_enb,
      I3 => AXI4_Lite_ARVALID,
      O => \write_reg_packet_size_axi4_stream_master[31]_i_3_n_0\
    );
\write_reg_packet_size_axi4_stream_master[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => AXI4_Lite_ARADDR(10),
      I1 => AXI4_Lite_ARADDR(11),
      I2 => AXI4_Lite_ARADDR(8),
      I3 => AXI4_Lite_ARADDR(9),
      I4 => AXI4_Lite_ARADDR(13),
      I5 => AXI4_Lite_ARADDR(12),
      O => AXI4_Lite_ARADDR_12_sn_1
    );
\write_reg_packet_size_axi4_stream_master[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => sel0(4),
      I1 => sel0(5),
      I2 => sel0(2),
      I3 => sel0(3),
      I4 => sel0(7),
      I5 => sel0(6),
      O => \write_reg_packet_size_axi4_stream_master[31]_i_5_n_0\
    );
\write_reg_packet_size_axi4_stream_master[31]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0400"
    )
        port map (
      I0 => sel0(0),
      I1 => sel0(1),
      I2 => AXI4_Lite_ARVALID,
      I3 => top_wr_enb,
      O => \write_reg_packet_size_axi4_stream_master[31]_i_6_n_0\
    );
\write_reg_packet_size_axi4_stream_master[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => sel0(10),
      I1 => sel0(11),
      I2 => sel0(8),
      I3 => sel0(9),
      I4 => sel0(13),
      I5 => sel0(12),
      O => \write_reg_packet_size_axi4_stream_master[31]_i_7_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_src_MultiplyBy10 is
  port (
    wr_din : out STD_LOGIC_VECTOR ( 29 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_src_MultiplyBy10;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_src_MultiplyBy10 is
  signal ram_reg_0_3_0_5_i_2_n_0 : STD_LOGIC;
  signal ram_reg_0_3_0_5_i_2_n_1 : STD_LOGIC;
  signal ram_reg_0_3_0_5_i_2_n_2 : STD_LOGIC;
  signal ram_reg_0_3_0_5_i_2_n_3 : STD_LOGIC;
  signal ram_reg_0_3_0_5_i_3_n_0 : STD_LOGIC;
  signal ram_reg_0_3_0_5_i_4_n_0 : STD_LOGIC;
  signal ram_reg_0_3_0_5_i_5_n_0 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_1_n_0 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_1_n_1 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_1_n_2 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_1_n_3 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_2_n_0 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_3_n_0 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_4_n_0 : STD_LOGIC;
  signal ram_reg_0_3_12_17_i_5_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_10_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_1_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_1_n_1 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_1_n_2 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_1_n_3 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_2_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_2_n_1 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_2_n_2 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_2_n_3 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_3_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_4_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_5_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_6_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_7_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_8_n_0 : STD_LOGIC;
  signal ram_reg_0_3_18_23_i_9_n_0 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_1_n_0 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_1_n_1 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_1_n_2 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_1_n_3 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_2_n_0 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_3_n_0 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_4_n_0 : STD_LOGIC;
  signal ram_reg_0_3_24_29_i_5_n_0 : STD_LOGIC;
  signal ram_reg_0_3_30_31_i_1_n_3 : STD_LOGIC;
  signal ram_reg_0_3_30_31_i_2_n_0 : STD_LOGIC;
  signal ram_reg_0_3_30_31_i_3_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_10_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_1_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_1_n_1 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_1_n_2 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_1_n_3 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_2_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_2_n_1 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_2_n_2 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_2_n_3 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_3_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_4_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_5_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_6_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_7_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_8_n_0 : STD_LOGIC;
  signal ram_reg_0_3_6_11_i_9_n_0 : STD_LOGIC;
  signal NLW_ram_reg_0_3_30_31_i_1_CO_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal NLW_ram_reg_0_3_30_31_i_1_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 2 );
begin
ram_reg_0_3_0_5_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => ram_reg_0_3_0_5_i_2_n_0,
      CO(2) => ram_reg_0_3_0_5_i_2_n_1,
      CO(1) => ram_reg_0_3_0_5_i_2_n_2,
      CO(0) => ram_reg_0_3_0_5_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 1) => Q(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => wr_din(3 downto 0),
      S(3) => ram_reg_0_3_0_5_i_3_n_0,
      S(2) => ram_reg_0_3_0_5_i_4_n_0,
      S(1) => ram_reg_0_3_0_5_i_5_n_0,
      S(0) => Q(1)
    );
ram_reg_0_3_0_5_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(4),
      I1 => Q(2),
      O => ram_reg_0_3_0_5_i_3_n_0
    );
ram_reg_0_3_0_5_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      O => ram_reg_0_3_0_5_i_4_n_0
    );
ram_reg_0_3_0_5_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(2),
      I1 => Q(0),
      O => ram_reg_0_3_0_5_i_5_n_0
    );
ram_reg_0_3_12_17_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_6_11_i_2_n_0,
      CO(3) => ram_reg_0_3_12_17_i_1_n_0,
      CO(2) => ram_reg_0_3_12_17_i_1_n_1,
      CO(1) => ram_reg_0_3_12_17_i_1_n_2,
      CO(0) => ram_reg_0_3_12_17_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(16 downto 13),
      O(3 downto 0) => wr_din(15 downto 12),
      S(3) => ram_reg_0_3_12_17_i_2_n_0,
      S(2) => ram_reg_0_3_12_17_i_3_n_0,
      S(1) => ram_reg_0_3_12_17_i_4_n_0,
      S(0) => ram_reg_0_3_12_17_i_5_n_0
    );
ram_reg_0_3_12_17_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(16),
      I1 => Q(14),
      O => ram_reg_0_3_12_17_i_2_n_0
    );
ram_reg_0_3_12_17_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(15),
      I1 => Q(13),
      O => ram_reg_0_3_12_17_i_3_n_0
    );
ram_reg_0_3_12_17_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(14),
      I1 => Q(12),
      O => ram_reg_0_3_12_17_i_4_n_0
    );
ram_reg_0_3_12_17_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(13),
      I1 => Q(11),
      O => ram_reg_0_3_12_17_i_5_n_0
    );
ram_reg_0_3_18_23_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_12_17_i_1_n_0,
      CO(3) => ram_reg_0_3_18_23_i_1_n_0,
      CO(2) => ram_reg_0_3_18_23_i_1_n_1,
      CO(1) => ram_reg_0_3_18_23_i_1_n_2,
      CO(0) => ram_reg_0_3_18_23_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(20 downto 17),
      O(3 downto 0) => wr_din(19 downto 16),
      S(3) => ram_reg_0_3_18_23_i_3_n_0,
      S(2) => ram_reg_0_3_18_23_i_4_n_0,
      S(1) => ram_reg_0_3_18_23_i_5_n_0,
      S(0) => ram_reg_0_3_18_23_i_6_n_0
    );
ram_reg_0_3_18_23_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(21),
      I1 => Q(19),
      O => ram_reg_0_3_18_23_i_10_n_0
    );
ram_reg_0_3_18_23_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_18_23_i_1_n_0,
      CO(3) => ram_reg_0_3_18_23_i_2_n_0,
      CO(2) => ram_reg_0_3_18_23_i_2_n_1,
      CO(1) => ram_reg_0_3_18_23_i_2_n_2,
      CO(0) => ram_reg_0_3_18_23_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(24 downto 21),
      O(3 downto 0) => wr_din(23 downto 20),
      S(3) => ram_reg_0_3_18_23_i_7_n_0,
      S(2) => ram_reg_0_3_18_23_i_8_n_0,
      S(1) => ram_reg_0_3_18_23_i_9_n_0,
      S(0) => ram_reg_0_3_18_23_i_10_n_0
    );
ram_reg_0_3_18_23_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(20),
      I1 => Q(18),
      O => ram_reg_0_3_18_23_i_3_n_0
    );
ram_reg_0_3_18_23_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(19),
      I1 => Q(17),
      O => ram_reg_0_3_18_23_i_4_n_0
    );
ram_reg_0_3_18_23_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(18),
      I1 => Q(16),
      O => ram_reg_0_3_18_23_i_5_n_0
    );
ram_reg_0_3_18_23_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(17),
      I1 => Q(15),
      O => ram_reg_0_3_18_23_i_6_n_0
    );
ram_reg_0_3_18_23_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(24),
      I1 => Q(22),
      O => ram_reg_0_3_18_23_i_7_n_0
    );
ram_reg_0_3_18_23_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(23),
      I1 => Q(21),
      O => ram_reg_0_3_18_23_i_8_n_0
    );
ram_reg_0_3_18_23_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(22),
      I1 => Q(20),
      O => ram_reg_0_3_18_23_i_9_n_0
    );
ram_reg_0_3_24_29_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_18_23_i_2_n_0,
      CO(3) => ram_reg_0_3_24_29_i_1_n_0,
      CO(2) => ram_reg_0_3_24_29_i_1_n_1,
      CO(1) => ram_reg_0_3_24_29_i_1_n_2,
      CO(0) => ram_reg_0_3_24_29_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(28 downto 25),
      O(3 downto 0) => wr_din(27 downto 24),
      S(3) => ram_reg_0_3_24_29_i_2_n_0,
      S(2) => ram_reg_0_3_24_29_i_3_n_0,
      S(1) => ram_reg_0_3_24_29_i_4_n_0,
      S(0) => ram_reg_0_3_24_29_i_5_n_0
    );
ram_reg_0_3_24_29_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(28),
      I1 => Q(26),
      O => ram_reg_0_3_24_29_i_2_n_0
    );
ram_reg_0_3_24_29_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(27),
      I1 => Q(25),
      O => ram_reg_0_3_24_29_i_3_n_0
    );
ram_reg_0_3_24_29_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(26),
      I1 => Q(24),
      O => ram_reg_0_3_24_29_i_4_n_0
    );
ram_reg_0_3_24_29_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(25),
      I1 => Q(23),
      O => ram_reg_0_3_24_29_i_5_n_0
    );
ram_reg_0_3_30_31_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_24_29_i_1_n_0,
      CO(3 downto 1) => NLW_ram_reg_0_3_30_31_i_1_CO_UNCONNECTED(3 downto 1),
      CO(0) => ram_reg_0_3_30_31_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => Q(29),
      O(3 downto 2) => NLW_ram_reg_0_3_30_31_i_1_O_UNCONNECTED(3 downto 2),
      O(1 downto 0) => wr_din(29 downto 28),
      S(3 downto 2) => B"00",
      S(1) => ram_reg_0_3_30_31_i_2_n_0,
      S(0) => ram_reg_0_3_30_31_i_3_n_0
    );
ram_reg_0_3_30_31_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(30),
      I1 => Q(28),
      O => ram_reg_0_3_30_31_i_2_n_0
    );
ram_reg_0_3_30_31_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(29),
      I1 => Q(27),
      O => ram_reg_0_3_30_31_i_3_n_0
    );
ram_reg_0_3_6_11_i_1: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_0_5_i_2_n_0,
      CO(3) => ram_reg_0_3_6_11_i_1_n_0,
      CO(2) => ram_reg_0_3_6_11_i_1_n_1,
      CO(1) => ram_reg_0_3_6_11_i_1_n_2,
      CO(0) => ram_reg_0_3_6_11_i_1_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(8 downto 5),
      O(3 downto 0) => wr_din(7 downto 4),
      S(3) => ram_reg_0_3_6_11_i_3_n_0,
      S(2) => ram_reg_0_3_6_11_i_4_n_0,
      S(1) => ram_reg_0_3_6_11_i_5_n_0,
      S(0) => ram_reg_0_3_6_11_i_6_n_0
    );
ram_reg_0_3_6_11_i_10: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(9),
      I1 => Q(7),
      O => ram_reg_0_3_6_11_i_10_n_0
    );
ram_reg_0_3_6_11_i_2: unisim.vcomponents.CARRY4
     port map (
      CI => ram_reg_0_3_6_11_i_1_n_0,
      CO(3) => ram_reg_0_3_6_11_i_2_n_0,
      CO(2) => ram_reg_0_3_6_11_i_2_n_1,
      CO(1) => ram_reg_0_3_6_11_i_2_n_2,
      CO(0) => ram_reg_0_3_6_11_i_2_n_3,
      CYINIT => '0',
      DI(3 downto 0) => Q(12 downto 9),
      O(3 downto 0) => wr_din(11 downto 8),
      S(3) => ram_reg_0_3_6_11_i_7_n_0,
      S(2) => ram_reg_0_3_6_11_i_8_n_0,
      S(1) => ram_reg_0_3_6_11_i_9_n_0,
      S(0) => ram_reg_0_3_6_11_i_10_n_0
    );
ram_reg_0_3_6_11_i_3: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(8),
      I1 => Q(6),
      O => ram_reg_0_3_6_11_i_3_n_0
    );
ram_reg_0_3_6_11_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(7),
      I1 => Q(5),
      O => ram_reg_0_3_6_11_i_4_n_0
    );
ram_reg_0_3_6_11_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(6),
      I1 => Q(4),
      O => ram_reg_0_3_6_11_i_5_n_0
    );
ram_reg_0_3_6_11_i_6: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(5),
      I1 => Q(3),
      O => ram_reg_0_3_6_11_i_6_n_0
    );
ram_reg_0_3_6_11_i_7: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(12),
      I1 => Q(10),
      O => ram_reg_0_3_6_11_i_7_n_0
    );
ram_reg_0_3_6_11_i_8: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(11),
      I1 => Q(9),
      O => ram_reg_0_3_6_11_i_8_n_0
    );
ram_reg_0_3_6_11_i_9: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(10),
      I1 => Q(8),
      O => ram_reg_0_3_6_11_i_9_n_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite is
  port (
    FSM_sequential_axi_lite_rstate_reg : out STD_LOGIC;
    AXI4_Lite_ARESETN_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 0 to 0 );
    \write_reg_packet_size_axi4_stream_master_reg[31]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[28]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[24]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[20]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[16]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[12]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \write_reg_packet_size_axi4_stream_master_reg[8]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    AR : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_ARESETN_1 : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Lite_AWREADY : out STD_LOGIC;
    \FSM_onehot_axi_lite_wstate_reg[2]\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite is
  signal \^ar\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^axi4_lite_aresetn_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal read_reg_ip_timestamp : STD_LOGIC_VECTOR ( 30 to 30 );
  signal reg_enb_packet_size_axi4_stream_master : STD_LOGIC;
  signal top_data_write : STD_LOGIC_VECTOR ( 0 to 0 );
  signal u_MultiplyB_ip_addr_decoder_inst_n_63 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_13 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_14 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_15 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_16 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_17 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_18 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_19 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_20 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_21 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_22 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_23 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_24 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_25 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_26 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_27 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_28 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_29 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_30 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_31 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_32 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_33 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_34 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_35 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_36 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_37 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_38 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_39 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_40 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_41 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_42 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_43 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_8 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_module_inst_n_9 : STD_LOGIC;
begin
  AR(1 downto 0) <= \^ar\(1 downto 0);
  AXI4_Lite_ARESETN_0(0) <= \^axi4_lite_aresetn_0\(0);
u_MultiplyB_ip_addr_decoder_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_addr_decoder
     port map (
      AR(1 downto 0) => \^ar\(1 downto 0),
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(1 downto 0) => AXI4_Lite_ARADDR(1 downto 0),
      \AXI4_Lite_ARADDR[2]\ => u_MultiplyB_ip_addr_decoder_inst_n_63,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      \AXI4_Lite_RDATA_tmp_reg[30]\ => u_MultiplyB_ip_axi_lite_module_inst_n_9,
      \AXI4_Lite_RDATA_tmp_reg[30]_0\ => u_MultiplyB_ip_axi_lite_module_inst_n_8,
      E(0) => reg_enb_packet_size_axi4_stream_master,
      Q(30 downto 0) => Q(30 downto 0),
      S(3 downto 0) => S(3 downto 0),
      read_reg_ip_timestamp(0) => read_reg_ip_timestamp(30),
      \read_reg_ip_timestamp_reg[30]_0\(0) => \^axi4_lite_aresetn_0\(0),
      \write_reg_packet_size_axi4_stream_master_reg[12]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[12]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[16]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[16]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[20]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[20]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[24]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[24]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[28]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[28]\(3 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[31]_0\(2 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[31]\(2 downto 0),
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(31) => u_MultiplyB_ip_axi_lite_module_inst_n_13,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(30) => u_MultiplyB_ip_axi_lite_module_inst_n_14,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(29) => u_MultiplyB_ip_axi_lite_module_inst_n_15,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(28) => u_MultiplyB_ip_axi_lite_module_inst_n_16,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(27) => u_MultiplyB_ip_axi_lite_module_inst_n_17,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(26) => u_MultiplyB_ip_axi_lite_module_inst_n_18,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(25) => u_MultiplyB_ip_axi_lite_module_inst_n_19,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(24) => u_MultiplyB_ip_axi_lite_module_inst_n_20,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(23) => u_MultiplyB_ip_axi_lite_module_inst_n_21,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(22) => u_MultiplyB_ip_axi_lite_module_inst_n_22,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(21) => u_MultiplyB_ip_axi_lite_module_inst_n_23,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(20) => u_MultiplyB_ip_axi_lite_module_inst_n_24,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(19) => u_MultiplyB_ip_axi_lite_module_inst_n_25,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(18) => u_MultiplyB_ip_axi_lite_module_inst_n_26,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(17) => u_MultiplyB_ip_axi_lite_module_inst_n_27,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(16) => u_MultiplyB_ip_axi_lite_module_inst_n_28,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(15) => u_MultiplyB_ip_axi_lite_module_inst_n_29,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(14) => u_MultiplyB_ip_axi_lite_module_inst_n_30,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(13) => u_MultiplyB_ip_axi_lite_module_inst_n_31,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(12) => u_MultiplyB_ip_axi_lite_module_inst_n_32,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(11) => u_MultiplyB_ip_axi_lite_module_inst_n_33,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(10) => u_MultiplyB_ip_axi_lite_module_inst_n_34,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(9) => u_MultiplyB_ip_axi_lite_module_inst_n_35,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(8) => u_MultiplyB_ip_axi_lite_module_inst_n_36,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(7) => u_MultiplyB_ip_axi_lite_module_inst_n_37,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(6) => u_MultiplyB_ip_axi_lite_module_inst_n_38,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(5) => u_MultiplyB_ip_axi_lite_module_inst_n_39,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(4) => u_MultiplyB_ip_axi_lite_module_inst_n_40,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(3) => u_MultiplyB_ip_axi_lite_module_inst_n_41,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(2) => u_MultiplyB_ip_axi_lite_module_inst_n_42,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(1) => u_MultiplyB_ip_axi_lite_module_inst_n_43,
      \write_reg_packet_size_axi4_stream_master_reg[31]_1\(0) => top_data_write(0),
      \write_reg_packet_size_axi4_stream_master_reg[8]_0\(3 downto 0) => \write_reg_packet_size_axi4_stream_master_reg[8]\(3 downto 0)
    );
u_MultiplyB_ip_axi_lite_module_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite_module
     port map (
      AR(1 downto 0) => \^ar\(1 downto 0),
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(13 downto 0),
      AXI4_Lite_ARADDR_12_sp_1 => u_MultiplyB_ip_axi_lite_module_inst_n_9,
      AXI4_Lite_ARADDR_6_sp_1 => u_MultiplyB_ip_axi_lite_module_inst_n_8,
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARESETN_0(0) => \^axi4_lite_aresetn_0\(0),
      AXI4_Lite_ARESETN_1(0) => AXI4_Lite_ARESETN_1(0),
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(13 downto 0),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_RDATA(0) => AXI4_Lite_RDATA(0),
      \AXI4_Lite_RDATA_tmp_reg[30]_0\ => u_MultiplyB_ip_addr_decoder_inst_n_63,
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_WDATA(31 downto 0) => AXI4_Lite_WDATA(31 downto 0),
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      E(0) => reg_enb_packet_size_axi4_stream_master,
      FSM_sequential_axi_lite_rstate_reg_0 => FSM_sequential_axi_lite_rstate_reg,
      IPCORE_RESETN => IPCORE_RESETN,
      Q(1 downto 0) => \FSM_onehot_axi_lite_wstate_reg[2]\(1 downto 0),
      read_reg_ip_timestamp(0) => read_reg_ip_timestamp(30),
      \wdata_reg[31]_0\(31) => u_MultiplyB_ip_axi_lite_module_inst_n_13,
      \wdata_reg[31]_0\(30) => u_MultiplyB_ip_axi_lite_module_inst_n_14,
      \wdata_reg[31]_0\(29) => u_MultiplyB_ip_axi_lite_module_inst_n_15,
      \wdata_reg[31]_0\(28) => u_MultiplyB_ip_axi_lite_module_inst_n_16,
      \wdata_reg[31]_0\(27) => u_MultiplyB_ip_axi_lite_module_inst_n_17,
      \wdata_reg[31]_0\(26) => u_MultiplyB_ip_axi_lite_module_inst_n_18,
      \wdata_reg[31]_0\(25) => u_MultiplyB_ip_axi_lite_module_inst_n_19,
      \wdata_reg[31]_0\(24) => u_MultiplyB_ip_axi_lite_module_inst_n_20,
      \wdata_reg[31]_0\(23) => u_MultiplyB_ip_axi_lite_module_inst_n_21,
      \wdata_reg[31]_0\(22) => u_MultiplyB_ip_axi_lite_module_inst_n_22,
      \wdata_reg[31]_0\(21) => u_MultiplyB_ip_axi_lite_module_inst_n_23,
      \wdata_reg[31]_0\(20) => u_MultiplyB_ip_axi_lite_module_inst_n_24,
      \wdata_reg[31]_0\(19) => u_MultiplyB_ip_axi_lite_module_inst_n_25,
      \wdata_reg[31]_0\(18) => u_MultiplyB_ip_axi_lite_module_inst_n_26,
      \wdata_reg[31]_0\(17) => u_MultiplyB_ip_axi_lite_module_inst_n_27,
      \wdata_reg[31]_0\(16) => u_MultiplyB_ip_axi_lite_module_inst_n_28,
      \wdata_reg[31]_0\(15) => u_MultiplyB_ip_axi_lite_module_inst_n_29,
      \wdata_reg[31]_0\(14) => u_MultiplyB_ip_axi_lite_module_inst_n_30,
      \wdata_reg[31]_0\(13) => u_MultiplyB_ip_axi_lite_module_inst_n_31,
      \wdata_reg[31]_0\(12) => u_MultiplyB_ip_axi_lite_module_inst_n_32,
      \wdata_reg[31]_0\(11) => u_MultiplyB_ip_axi_lite_module_inst_n_33,
      \wdata_reg[31]_0\(10) => u_MultiplyB_ip_axi_lite_module_inst_n_34,
      \wdata_reg[31]_0\(9) => u_MultiplyB_ip_axi_lite_module_inst_n_35,
      \wdata_reg[31]_0\(8) => u_MultiplyB_ip_axi_lite_module_inst_n_36,
      \wdata_reg[31]_0\(7) => u_MultiplyB_ip_axi_lite_module_inst_n_37,
      \wdata_reg[31]_0\(6) => u_MultiplyB_ip_axi_lite_module_inst_n_38,
      \wdata_reg[31]_0\(5) => u_MultiplyB_ip_axi_lite_module_inst_n_39,
      \wdata_reg[31]_0\(4) => u_MultiplyB_ip_axi_lite_module_inst_n_40,
      \wdata_reg[31]_0\(3) => u_MultiplyB_ip_axi_lite_module_inst_n_41,
      \wdata_reg[31]_0\(2) => u_MultiplyB_ip_axi_lite_module_inst_n_42,
      \wdata_reg[31]_0\(1) => u_MultiplyB_ip_axi_lite_module_inst_n_43,
      \wdata_reg[31]_0\(0) => top_data_write(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_dut is
  port (
    wr_din : out STD_LOGIC_VECTOR ( 29 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_dut;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_dut is
begin
u_MultiplyB_ip_src_MultiplyBy10: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_src_MultiplyBy10
     port map (
      Q(30 downto 0) => Q(30 downto 0),
      wr_din(29 downto 0) => wr_din(29 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT_classic is
  port (
    Q_next_1 : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[21]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[30]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    data_int_reg : out STD_LOGIC;
    cache_data_reg : out STD_LOGIC;
    IPCORE_CLK : in STD_LOGIC;
    \fifo_sample_count_reg[2]_0\ : in STD_LOGIC;
    cache_valid : in STD_LOGIC;
    out_valid_0 : in STD_LOGIC;
    fifo_valid : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    tlast_counter_out_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    tlast_size_value : in STD_LOGIC_VECTOR ( 30 downto 0 );
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    cache_wr_en : in STD_LOGIC;
    Out_rsvd_reg : in STD_LOGIC;
    out_wr_en : in STD_LOGIC;
    AXI4_Stream_Master_TLAST : in STD_LOGIC;
    In_rsvd : in STD_LOGIC;
    \fifo_back_indx_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT_classic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT_classic is
  signal \fifo_back_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal fifo_data_out : STD_LOGIC;
  signal \fifo_front_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_sample_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_2_n_0\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[2]\ : STD_LOGIC;
  signal w_d1 : STD_LOGIC;
  signal \w_d1_i_1__1_n_0\ : STD_LOGIC;
  signal w_d2 : STD_LOGIC;
  signal wr_en : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \fifo_back_indx[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \fifo_back_indx[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \fifo_front_indx[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \fifo_front_indx[1]_i_1\ : label is "soft_lutpair2";
begin
\fifo_back_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => wr_en,
      I1 => \fifo_back_indx_reg_n_0_[0]\,
      O => \fifo_back_indx[0]_i_1_n_0\
    );
\fifo_back_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_back_indx_reg_n_0_[0]\,
      I1 => wr_en,
      I2 => \fifo_back_indx_reg_n_0_[1]\,
      O => \fifo_back_indx[1]_i_1_n_0\
    );
\fifo_back_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_back_indx[0]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[0]\
    );
\fifo_back_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_back_indx[1]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[1]\
    );
\fifo_front_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \w_d1_i_1__1_n_0\,
      I1 => \fifo_front_indx_reg_n_0_[0]\,
      O => \fifo_front_indx[0]_i_1_n_0\
    );
\fifo_front_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_front_indx_reg_n_0_[0]\,
      I1 => \w_d1_i_1__1_n_0\,
      I2 => \fifo_front_indx_reg_n_0_[1]\,
      O => \fifo_front_indx[1]_i_1_n_0\
    );
\fifo_front_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_front_indx[0]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[0]\
    );
\fifo_front_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_front_indx[1]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[1]\
    );
\fifo_sample_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55555567A8A8A8"
    )
        port map (
      I0 => \fifo_sample_count[2]_i_2_n_0\,
      I1 => \fifo_sample_count_reg_n_0_[1]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => out_valid,
      I4 => internal_ready_delayed,
      I5 => \fifo_sample_count_reg_n_0_[0]\,
      O => \fifo_sample_count[0]_i_1_n_0\
    );
\fifo_sample_count[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF8800F077FF8800"
    )
        port map (
      I0 => internal_ready_delayed,
      I1 => out_valid,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => \fifo_sample_count_reg_n_0_[0]\,
      I4 => \fifo_sample_count_reg_n_0_[1]\,
      I5 => \fifo_sample_count[2]_i_2_n_0\,
      O => \fifo_sample_count[1]_i_1_n_0\
    );
\fifo_sample_count[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F0F0F00078F0F0F0"
    )
        port map (
      I0 => internal_ready_delayed,
      I1 => out_valid,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      I3 => \fifo_sample_count_reg_n_0_[0]\,
      I4 => \fifo_sample_count_reg_n_0_[1]\,
      I5 => \fifo_sample_count[2]_i_2_n_0\,
      O => \fifo_sample_count[2]_i_1_n_0\
    );
\fifo_sample_count[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFEFEFEFEFEFE"
    )
        port map (
      I0 => \fifo_sample_count_reg_n_0_[2]\,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[1]\,
      I3 => fifo_valid,
      I4 => out_valid_0,
      I5 => cache_valid,
      O => \fifo_sample_count[2]_i_2_n_0\
    );
\fifo_sample_count_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \fifo_sample_count[0]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[0]\
    );
\fifo_sample_count_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \fifo_sample_count[1]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[1]\
    );
\fifo_sample_count_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \fifo_sample_count[2]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[2]\
    );
\fifo_valid_i_1__1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEAAAAA"
    )
        port map (
      I0 => \fifo_sample_count[2]_i_2_n_0\,
      I1 => fifo_valid,
      I2 => cache_valid,
      I3 => AXI4_Stream_Master_TREADY,
      I4 => out_valid_0,
      O => Q_next_1
    );
u_MultiplyB_ip_fifo_TLAST_OUT_classic_ram_singlebit: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_singlebit
     port map (
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      IPCORE_CLK => IPCORE_CLK,
      In_rsvd => In_rsvd,
      Out_rsvd_reg => Out_rsvd_reg,
      Q(0) => Q(0),
      S(3 downto 0) => S(3 downto 0),
      cache_data_reg => cache_data_reg,
      cache_valid => cache_valid,
      cache_wr_en => cache_wr_en,
      data_int_reg_0 => data_int_reg,
      data_int_reg_1 => \fifo_sample_count_reg_n_0_[2]\,
      data_int_reg_2 => \fifo_sample_count_reg_n_0_[0]\,
      data_int_reg_3 => \fifo_sample_count_reg_n_0_[1]\,
      fifo_data_out => fifo_data_out,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => out_valid,
      out_wr_en => out_wr_en,
      rd_addr(1) => \fifo_front_indx_reg_n_0_[1]\,
      rd_addr(0) => \fifo_front_indx_reg_n_0_[0]\,
      tlast_counter_out_reg(31 downto 0) => tlast_counter_out_reg(31 downto 0),
      \tlast_counter_out_reg[21]\(3 downto 0) => \tlast_counter_out_reg[21]\(3 downto 0),
      \tlast_counter_out_reg[30]\(2 downto 0) => \tlast_counter_out_reg[30]\(2 downto 0),
      tlast_size_value(30 downto 0) => tlast_size_value(30 downto 0),
      w_d1 => w_d1,
      w_d2 => w_d2,
      wr_addr(1) => \fifo_back_indx_reg_n_0_[1]\,
      wr_addr(0) => \fifo_back_indx_reg_n_0_[0]\,
      wr_en => wr_en
    );
\w_d1_i_1__1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => out_valid_0,
      I2 => fifo_valid,
      I3 => \fifo_sample_count_reg_n_0_[2]\,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      I5 => \fifo_sample_count_reg_n_0_[1]\,
      O => \w_d1_i_1__1_n_0\
    );
w_d1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \w_d1_i_1__1_n_0\,
      Q => w_d1
    );
w_d2_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => fifo_data_out,
      Q => w_d2
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT_classic is
  port (
    Q_next_1 : out STD_LOGIC;
    internal_ready : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \data_int_reg[31]\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    cache_valid : in STD_LOGIC;
    w_d1_reg_0 : in STD_LOGIC;
    fifo_valid : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 31 downto 0 );
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    \fifo_sample_count_reg[2]_0\ : in STD_LOGIC;
    wr_din : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \fifo_back_indx_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT_classic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT_classic is
  signal Num : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \fifo_back_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_back_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_front_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_front_indx_reg_n_0_[1]\ : STD_LOGIC;
  signal fifo_read_enable : STD_LOGIC;
  signal \fifo_sample_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_2__0_n_0\ : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_32 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_33 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_34 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_35 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_36 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_37 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_38 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_39 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_40 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_41 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_42 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_43 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_44 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_45 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_46 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_47 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_48 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_49 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_50 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_51 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_52 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_53 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_54 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_55 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_56 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_57 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_58 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_59 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_60 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_61 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_62 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_63 : STD_LOGIC;
  signal w_d1 : STD_LOGIC;
  signal \w_d2_reg_n_0_[0]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[10]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[11]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[12]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[13]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[14]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[15]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[16]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[17]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[18]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[19]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[1]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[20]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[21]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[22]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[23]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[24]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[25]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[26]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[27]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[28]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[29]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[2]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[30]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[31]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[3]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[4]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[5]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[6]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[7]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[8]\ : STD_LOGIC;
  signal \w_d2_reg_n_0_[9]\ : STD_LOGIC;
  signal w_mux1 : STD_LOGIC;
  signal wr_en : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \fifo_back_indx[0]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \fifo_back_indx[1]_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \fifo_front_indx[0]_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \fifo_front_indx[1]_i_1\ : label is "soft_lutpair38";
begin
\fifo_back_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => wr_en,
      I1 => \fifo_back_indx_reg_n_0_[0]\,
      O => \fifo_back_indx[0]_i_1_n_0\
    );
\fifo_back_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_back_indx_reg_n_0_[0]\,
      I1 => wr_en,
      I2 => \fifo_back_indx_reg_n_0_[1]\,
      O => \fifo_back_indx[1]_i_1_n_0\
    );
\fifo_back_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_back_indx[0]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[0]\
    );
\fifo_back_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_back_indx[1]_i_1_n_0\,
      Q => \fifo_back_indx_reg_n_0_[1]\
    );
\fifo_front_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => fifo_read_enable,
      I1 => \fifo_front_indx_reg_n_0_[0]\,
      O => \fifo_front_indx[0]_i_1_n_0\
    );
\fifo_front_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \fifo_front_indx_reg_n_0_[0]\,
      I1 => fifo_read_enable,
      I2 => \fifo_front_indx_reg_n_0_[1]\,
      O => \fifo_front_indx[1]_i_1_n_0\
    );
\fifo_front_indx[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => w_d1_reg_0,
      I2 => fifo_valid,
      I3 => Num(1),
      I4 => Num(0),
      I5 => Num(2),
      O => fifo_read_enable
    );
\fifo_front_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_front_indx[0]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[0]\
    );
\fifo_front_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_back_indx_reg[0]_0\(0),
      D => \fifo_front_indx[1]_i_1_n_0\,
      Q => \fifo_front_indx_reg_n_0_[1]\
    );
fifo_rd_ack_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"15"
    )
        port map (
      I0 => Num(2),
      I1 => Num(0),
      I2 => Num(1),
      O => internal_ready
    );
\fifo_sample_count[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA55555567A8A8A8"
    )
        port map (
      I0 => \fifo_sample_count[2]_i_2__0_n_0\,
      I1 => Num(1),
      I2 => Num(2),
      I3 => out_valid,
      I4 => internal_ready_delayed,
      I5 => Num(0),
      O => \fifo_sample_count[0]_i_1_n_0\
    );
\fifo_sample_count[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F80F007F7F8080"
    )
        port map (
      I0 => internal_ready_delayed,
      I1 => out_valid,
      I2 => Num(0),
      I3 => Num(2),
      I4 => Num(1),
      I5 => \fifo_sample_count[2]_i_2__0_n_0\,
      O => \fifo_sample_count[1]_i_1_n_0\
    );
\fifo_sample_count[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00F0007F80FF00"
    )
        port map (
      I0 => internal_ready_delayed,
      I1 => out_valid,
      I2 => Num(0),
      I3 => Num(2),
      I4 => Num(1),
      I5 => \fifo_sample_count[2]_i_2__0_n_0\,
      O => \fifo_sample_count[2]_i_1_n_0\
    );
\fifo_sample_count[2]_i_2__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFEFEFEFEFEFE"
    )
        port map (
      I0 => Num(1),
      I1 => Num(0),
      I2 => Num(2),
      I3 => fifo_valid,
      I4 => w_d1_reg_0,
      I5 => cache_valid,
      O => \fifo_sample_count[2]_i_2__0_n_0\
    );
\fifo_sample_count_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \fifo_sample_count[0]_i_1_n_0\,
      Q => Num(0)
    );
\fifo_sample_count_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \fifo_sample_count[1]_i_1_n_0\,
      Q => Num(1)
    );
\fifo_sample_count_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]_0\,
      D => \fifo_sample_count[2]_i_1_n_0\,
      Q => Num(2)
    );
\fifo_valid_i_1__0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEAAAAA"
    )
        port map (
      I0 => \fifo_sample_count[2]_i_2__0_n_0\,
      I1 => fifo_valid,
      I2 => cache_valid,
      I3 => AXI4_Stream_Master_TREADY,
      I4 => w_d1_reg_0,
      O => Q_next_1
    );
u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic_0
     port map (
      ADDRA(1) => \fifo_front_indx_reg_n_0_[1]\,
      ADDRA(0) => \fifo_front_indx_reg_n_0_[0]\,
      ADDRD(1) => \fifo_back_indx_reg_n_0_[1]\,
      ADDRD(0) => \fifo_back_indx_reg_n_0_[0]\,
      D(31 downto 0) => D(31 downto 0),
      IPCORE_CLK => IPCORE_CLK,
      Num(2 downto 0) => Num(2 downto 0),
      \Out_tmp_reg[31]\(31) => \w_d2_reg_n_0_[31]\,
      \Out_tmp_reg[31]\(30) => \w_d2_reg_n_0_[30]\,
      \Out_tmp_reg[31]\(29) => \w_d2_reg_n_0_[29]\,
      \Out_tmp_reg[31]\(28) => \w_d2_reg_n_0_[28]\,
      \Out_tmp_reg[31]\(27) => \w_d2_reg_n_0_[27]\,
      \Out_tmp_reg[31]\(26) => \w_d2_reg_n_0_[26]\,
      \Out_tmp_reg[31]\(25) => \w_d2_reg_n_0_[25]\,
      \Out_tmp_reg[31]\(24) => \w_d2_reg_n_0_[24]\,
      \Out_tmp_reg[31]\(23) => \w_d2_reg_n_0_[23]\,
      \Out_tmp_reg[31]\(22) => \w_d2_reg_n_0_[22]\,
      \Out_tmp_reg[31]\(21) => \w_d2_reg_n_0_[21]\,
      \Out_tmp_reg[31]\(20) => \w_d2_reg_n_0_[20]\,
      \Out_tmp_reg[31]\(19) => \w_d2_reg_n_0_[19]\,
      \Out_tmp_reg[31]\(18) => \w_d2_reg_n_0_[18]\,
      \Out_tmp_reg[31]\(17) => \w_d2_reg_n_0_[17]\,
      \Out_tmp_reg[31]\(16) => \w_d2_reg_n_0_[16]\,
      \Out_tmp_reg[31]\(15) => \w_d2_reg_n_0_[15]\,
      \Out_tmp_reg[31]\(14) => \w_d2_reg_n_0_[14]\,
      \Out_tmp_reg[31]\(13) => \w_d2_reg_n_0_[13]\,
      \Out_tmp_reg[31]\(12) => \w_d2_reg_n_0_[12]\,
      \Out_tmp_reg[31]\(11) => \w_d2_reg_n_0_[11]\,
      \Out_tmp_reg[31]\(10) => \w_d2_reg_n_0_[10]\,
      \Out_tmp_reg[31]\(9) => \w_d2_reg_n_0_[9]\,
      \Out_tmp_reg[31]\(8) => \w_d2_reg_n_0_[8]\,
      \Out_tmp_reg[31]\(7) => \w_d2_reg_n_0_[7]\,
      \Out_tmp_reg[31]\(6) => \w_d2_reg_n_0_[6]\,
      \Out_tmp_reg[31]\(5) => \w_d2_reg_n_0_[5]\,
      \Out_tmp_reg[31]\(4) => \w_d2_reg_n_0_[4]\,
      \Out_tmp_reg[31]\(3) => \w_d2_reg_n_0_[3]\,
      \Out_tmp_reg[31]\(2) => \w_d2_reg_n_0_[2]\,
      \Out_tmp_reg[31]\(1) => \w_d2_reg_n_0_[1]\,
      \Out_tmp_reg[31]\(0) => \w_d2_reg_n_0_[0]\,
      Q(31 downto 0) => Q(31 downto 0),
      cache_valid => cache_valid,
      data_int(31) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_32,
      data_int(30) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_33,
      data_int(29) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_34,
      data_int(28) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_35,
      data_int(27) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_36,
      data_int(26) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_37,
      data_int(25) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_38,
      data_int(24) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_39,
      data_int(23) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_40,
      data_int(22) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_41,
      data_int(21) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_42,
      data_int(20) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_43,
      data_int(19) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_44,
      data_int(18) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_45,
      data_int(17) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_46,
      data_int(16) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_47,
      data_int(15) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_48,
      data_int(14) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_49,
      data_int(13) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_50,
      data_int(12) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_51,
      data_int(11) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_52,
      data_int(10) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_53,
      data_int(9) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_54,
      data_int(8) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_55,
      data_int(7) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_56,
      data_int(6) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_57,
      data_int(5) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_58,
      data_int(4) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_59,
      data_int(3) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_60,
      data_int(2) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_61,
      data_int(1) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_62,
      data_int(0) => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_63,
      \data_int_reg[31]_0\(31 downto 0) => \data_int_reg[31]\(31 downto 0),
      internal_ready_delayed => internal_ready_delayed,
      out_valid => out_valid,
      w_d1 => w_d1,
      wr_din(30 downto 0) => wr_din(30 downto 0),
      wr_en => wr_en
    );
w_d1_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => w_d1_reg_0,
      I2 => fifo_valid,
      I3 => Num(2),
      I4 => Num(0),
      I5 => Num(1),
      O => w_mux1
    );
w_d1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(0),
      D => w_mux1,
      Q => w_d1
    );
\w_d2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_63,
      Q => \w_d2_reg_n_0_[0]\
    );
\w_d2_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_53,
      Q => \w_d2_reg_n_0_[10]\
    );
\w_d2_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_52,
      Q => \w_d2_reg_n_0_[11]\
    );
\w_d2_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_51,
      Q => \w_d2_reg_n_0_[12]\
    );
\w_d2_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_50,
      Q => \w_d2_reg_n_0_[13]\
    );
\w_d2_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_49,
      Q => \w_d2_reg_n_0_[14]\
    );
\w_d2_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_48,
      Q => \w_d2_reg_n_0_[15]\
    );
\w_d2_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_47,
      Q => \w_d2_reg_n_0_[16]\
    );
\w_d2_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_46,
      Q => \w_d2_reg_n_0_[17]\
    );
\w_d2_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_45,
      Q => \w_d2_reg_n_0_[18]\
    );
\w_d2_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_44,
      Q => \w_d2_reg_n_0_[19]\
    );
\w_d2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_62,
      Q => \w_d2_reg_n_0_[1]\
    );
\w_d2_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_43,
      Q => \w_d2_reg_n_0_[20]\
    );
\w_d2_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_42,
      Q => \w_d2_reg_n_0_[21]\
    );
\w_d2_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_41,
      Q => \w_d2_reg_n_0_[22]\
    );
\w_d2_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_40,
      Q => \w_d2_reg_n_0_[23]\
    );
\w_d2_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_39,
      Q => \w_d2_reg_n_0_[24]\
    );
\w_d2_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_38,
      Q => \w_d2_reg_n_0_[25]\
    );
\w_d2_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_37,
      Q => \w_d2_reg_n_0_[26]\
    );
\w_d2_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_36,
      Q => \w_d2_reg_n_0_[27]\
    );
\w_d2_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_35,
      Q => \w_d2_reg_n_0_[28]\
    );
\w_d2_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_34,
      Q => \w_d2_reg_n_0_[29]\
    );
\w_d2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_61,
      Q => \w_d2_reg_n_0_[2]\
    );
\w_d2_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_33,
      Q => \w_d2_reg_n_0_[30]\
    );
\w_d2_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_32,
      Q => \w_d2_reg_n_0_[31]\
    );
\w_d2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_60,
      Q => \w_d2_reg_n_0_[3]\
    );
\w_d2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_59,
      Q => \w_d2_reg_n_0_[4]\
    );
\w_d2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_58,
      Q => \w_d2_reg_n_0_[5]\
    );
\w_d2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_57,
      Q => \w_d2_reg_n_0_[6]\
    );
\w_d2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_56,
      Q => \w_d2_reg_n_0_[7]\
    );
\w_d2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_55,
      Q => \w_d2_reg_n_0_[8]\
    );
\w_d2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_ram_generic_n_54,
      Q => \w_d2_reg_n_0_[9]\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_classic is
  port (
    Q_next_1 : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 30 downto 0 );
    \data_int_reg[30]\ : out STD_LOGIC_VECTOR ( 30 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 1 downto 0 );
    cache_valid : in STD_LOGIC;
    fifo_valid_reg : in STD_LOGIC;
    fifo_valid : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    fifo_valid_reg_0 : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_classic;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_classic is
  signal data_int : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \fifo_back_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_back_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_front_indx[1]_i_1_n_0\ : STD_LOGIC;
  signal fifo_read_enable : STD_LOGIC;
  signal \fifo_sample_count[0]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[1]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count[2]_i_1_n_0\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[0]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[1]\ : STD_LOGIC;
  signal \fifo_sample_count_reg_n_0_[2]\ : STD_LOGIC;
  signal fifo_valid_i_2_n_0 : STD_LOGIC;
  signal rd_addr : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal w_d1 : STD_LOGIC;
  signal w_d2 : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal wr_addr : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of AXI4_Stream_Slave_TREADY_INST_0 : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \fifo_back_indx[0]_i_1\ : label is "soft_lutpair71";
  attribute SOFT_HLUTNM of \fifo_front_indx[0]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \fifo_front_indx[1]_i_1\ : label is "soft_lutpair73";
  attribute SOFT_HLUTNM of \fifo_sample_count[0]_i_1\ : label is "soft_lutpair72";
  attribute SOFT_HLUTNM of \fifo_sample_count[1]_i_1\ : label is "soft_lutpair72";
begin
AXI4_Stream_Slave_TREADY_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EF"
    )
        port map (
      I0 => \fifo_sample_count_reg_n_0_[1]\,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[2]\,
      O => AXI4_Stream_Slave_TREADY
    );
\fifo_back_indx[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"555DAAA2"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[2]\,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => wr_addr(0),
      O => \fifo_back_indx[0]_i_1_n_0\
    );
\fifo_back_indx[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"555DFFFFAAA20000"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[2]\,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => wr_addr(0),
      I5 => wr_addr(1),
      O => \fifo_back_indx[1]_i_1_n_0\
    );
\fifo_back_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_back_indx[0]_i_1_n_0\,
      Q => wr_addr(0)
    );
\fifo_back_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_back_indx[1]_i_1_n_0\,
      Q => wr_addr(1)
    );
\fifo_front_indx[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => fifo_read_enable,
      I1 => rd_addr(0),
      O => \fifo_front_indx[0]_i_1_n_0\
    );
\fifo_front_indx[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => rd_addr(0),
      I1 => fifo_read_enable,
      I2 => rd_addr(1),
      O => \fifo_front_indx[1]_i_1_n_0\
    );
\fifo_front_indx_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_front_indx[0]_i_1_n_0\,
      Q => rd_addr(0)
    );
\fifo_front_indx_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_front_indx[1]_i_1_n_0\,
      Q => rd_addr(1)
    );
\fifo_sample_count[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A5AD5A52"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[2]\,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => fifo_read_enable,
      O => \fifo_sample_count[0]_i_1_n_0\
    );
\fifo_sample_count[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FA0D5FA0"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[2]\,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => fifo_read_enable,
      O => \fifo_sample_count[1]_i_1_n_0\
    );
\fifo_sample_count[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCC16CCC"
    )
        port map (
      I0 => AXI4_Stream_Slave_TVALID,
      I1 => \fifo_sample_count_reg_n_0_[2]\,
      I2 => \fifo_sample_count_reg_n_0_[0]\,
      I3 => \fifo_sample_count_reg_n_0_[1]\,
      I4 => fifo_read_enable,
      O => \fifo_sample_count[2]_i_1_n_0\
    );
\fifo_sample_count_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_sample_count[0]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[0]\
    );
\fifo_sample_count_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_sample_count[1]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[1]\
    );
\fifo_sample_count_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => \fifo_sample_count[2]_i_1_n_0\,
      Q => \fifo_sample_count_reg_n_0_[2]\
    );
fifo_valid_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAEAAAAA"
    )
        port map (
      I0 => fifo_valid_i_2_n_0,
      I1 => fifo_valid,
      I2 => cache_valid,
      I3 => fifo_valid_reg_0,
      I4 => fifo_valid_reg,
      O => Q_next_1
    );
fifo_valid_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00FEFEFEFEFEFEFE"
    )
        port map (
      I0 => \fifo_sample_count_reg_n_0_[2]\,
      I1 => \fifo_sample_count_reg_n_0_[0]\,
      I2 => \fifo_sample_count_reg_n_0_[1]\,
      I3 => fifo_valid,
      I4 => fifo_valid_reg,
      I5 => cache_valid,
      O => fifo_valid_i_2_n_0
    );
u_MultiplyB_ip_fifo_data_classic_ram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_SimpleDualPortRAM_generic
     port map (
      ADDRA(1 downto 0) => rd_addr(1 downto 0),
      ADDRD(1 downto 0) => wr_addr(1 downto 0),
      AXI4_Stream_Slave_TDATA(31 downto 0) => AXI4_Stream_Slave_TDATA(31 downto 0),
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      D(30 downto 0) => D(30 downto 0),
      IPCORE_CLK => IPCORE_CLK,
      \Out_tmp_reg[30]\(30 downto 0) => w_d2(30 downto 0),
      Q(30 downto 0) => Q(30 downto 0),
      cache_valid => cache_valid,
      data_int(30 downto 0) => data_int(30 downto 0),
      \data_int_reg[1]_0\ => \fifo_sample_count_reg_n_0_[2]\,
      \data_int_reg[1]_1\ => \fifo_sample_count_reg_n_0_[0]\,
      \data_int_reg[1]_2\ => \fifo_sample_count_reg_n_0_[1]\,
      \data_int_reg[30]_0\(30 downto 0) => \data_int_reg[30]\(30 downto 0),
      w_d1 => w_d1
    );
\w_d1_i_1__0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7F7F7F7F7F7F7F00"
    )
        port map (
      I0 => cache_valid,
      I1 => fifo_valid_reg,
      I2 => fifo_valid,
      I3 => \fifo_sample_count_reg_n_0_[2]\,
      I4 => \fifo_sample_count_reg_n_0_[0]\,
      I5 => \fifo_sample_count_reg_n_0_[1]\,
      O => fifo_read_enable
    );
w_d1_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => fifo_read_enable,
      Q => w_d1
    );
\w_d2_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(0),
      Q => w_d2(0)
    );
\w_d2_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(10),
      Q => w_d2(10)
    );
\w_d2_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(11),
      Q => w_d2(11)
    );
\w_d2_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(12),
      Q => w_d2(12)
    );
\w_d2_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(13),
      Q => w_d2(13)
    );
\w_d2_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(14),
      Q => w_d2(14)
    );
\w_d2_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(15),
      Q => w_d2(15)
    );
\w_d2_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(16),
      Q => w_d2(16)
    );
\w_d2_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(17),
      Q => w_d2(17)
    );
\w_d2_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(18),
      Q => w_d2(18)
    );
\w_d2_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(19),
      Q => w_d2(19)
    );
\w_d2_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(1),
      Q => w_d2(1)
    );
\w_d2_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(20),
      Q => w_d2(20)
    );
\w_d2_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(21),
      Q => w_d2(21)
    );
\w_d2_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(22),
      Q => w_d2(22)
    );
\w_d2_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(23),
      Q => w_d2(23)
    );
\w_d2_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(24),
      Q => w_d2(24)
    );
\w_d2_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(25),
      Q => w_d2(25)
    );
\w_d2_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(1),
      D => data_int(26),
      Q => w_d2(26)
    );
\w_d2_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(1),
      D => data_int(27),
      Q => w_d2(27)
    );
\w_d2_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(1),
      D => data_int(28),
      Q => w_d2(28)
    );
\w_d2_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(1),
      D => data_int(29),
      Q => w_d2(29)
    );
\w_d2_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(2),
      Q => w_d2(2)
    );
\w_d2_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(1),
      D => data_int(30),
      Q => w_d2(30)
    );
\w_d2_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(3),
      Q => w_d2(3)
    );
\w_d2_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(4),
      Q => w_d2(4)
    );
\w_d2_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(5),
      Q => w_d2(5)
    );
\w_d2_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(6),
      Q => w_d2(6)
    );
\w_d2_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(7),
      Q => w_d2(7)
    );
\w_d2_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(8),
      Q => w_d2(8)
    );
\w_d2_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => w_d1,
      CLR => AR(0),
      D => data_int(9),
      Q => w_d2(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT is
  port (
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    S : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[21]\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \tlast_counter_out_reg[30]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    \fifo_sample_count_reg[2]\ : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    tlast_counter_out_reg : in STD_LOGIC_VECTOR ( 31 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    tlast_size_value : in STD_LOGIC_VECTOR ( 30 downto 0 );
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    In_rsvd : in STD_LOGIC;
    \fifo_back_indx_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT is
  signal \^axi4_stream_master_tlast\ : STD_LOGIC;
  signal Q_next : STD_LOGIC;
  signal Q_next_1 : STD_LOGIC;
  signal cache_data_reg_n_0 : STD_LOGIC;
  signal cache_valid : STD_LOGIC;
  signal cache_wr_en : STD_LOGIC;
  signal fifo_valid : STD_LOGIC;
  signal out_valid_0 : STD_LOGIC;
  signal \out_valid_i_1__1_n_0\ : STD_LOGIC;
  signal out_wr_en : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_12 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_13 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of Out_rsvd_i_3 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of cache_data_i_2 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \cache_valid_i_1__1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \out_valid_i_1__1\ : label is "soft_lutpair3";
begin
  AXI4_Stream_Master_TLAST <= \^axi4_stream_master_tlast\;
Out_rsvd_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => out_valid_0,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => out_wr_en
    );
Out_rsvd_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]\,
      D => u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_13,
      Q => \^axi4_stream_master_tlast\
    );
cache_data_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A600"
    )
        port map (
      I0 => cache_valid,
      I1 => out_valid_0,
      I2 => AXI4_Stream_Master_TREADY,
      I3 => fifo_valid,
      O => cache_wr_en
    );
cache_data_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]\,
      D => u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_12,
      Q => cache_data_reg_n_0
    );
\cache_valid_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F220"
    )
        port map (
      I0 => out_valid_0,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => Q_next
    );
cache_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]\,
      D => Q_next,
      Q => cache_valid
    );
fifo_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]\,
      D => Q_next_1,
      Q => fifo_valid
    );
\out_valid_i_1__1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EEFE"
    )
        port map (
      I0 => fifo_valid,
      I1 => cache_valid,
      I2 => out_valid_0,
      I3 => AXI4_Stream_Master_TREADY,
      O => \out_valid_i_1__1_n_0\
    );
out_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => \fifo_sample_count_reg[2]\,
      D => \out_valid_i_1__1_n_0\,
      Q => out_valid_0
    );
u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT_classic
     port map (
      AXI4_Stream_Master_TLAST => \^axi4_stream_master_tlast\,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      IPCORE_CLK => IPCORE_CLK,
      In_rsvd => In_rsvd,
      Out_rsvd_reg => cache_data_reg_n_0,
      Q(0) => Q(0),
      Q_next_1 => Q_next_1,
      S(3 downto 0) => S(3 downto 0),
      cache_data_reg => u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_13,
      cache_valid => cache_valid,
      cache_wr_en => cache_wr_en,
      data_int_reg => u_MultiplyB_ip_fifo_TLAST_OUT_classic_inst_n_12,
      \fifo_back_indx_reg[0]_0\(0) => \fifo_back_indx_reg[0]\(0),
      \fifo_sample_count_reg[2]_0\ => \fifo_sample_count_reg[2]\,
      fifo_valid => fifo_valid,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => out_valid,
      out_valid_0 => out_valid_0,
      out_wr_en => out_wr_en,
      tlast_counter_out_reg(31 downto 0) => tlast_counter_out_reg(31 downto 0),
      \tlast_counter_out_reg[21]\(3 downto 0) => \tlast_counter_out_reg[21]\(3 downto 0),
      \tlast_counter_out_reg[30]\(2 downto 0) => \tlast_counter_out_reg[30]\(2 downto 0),
      tlast_size_value(30 downto 0) => tlast_size_value(30 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data is
  port (
    out_valid_reg_0 : out STD_LOGIC;
    In_rsvd : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    out_valid_reg_1 : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    fifo_valid_reg_0 : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data is
  signal Q_next : STD_LOGIC;
  signal Q_next_1 : STD_LOGIC;
  signal Q_next_2 : STD_LOGIC;
  signal cache_data : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal cache_valid : STD_LOGIC;
  signal cache_wr_en : STD_LOGIC;
  signal data_out_next : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal fifo_data_out_unsigned : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal fifo_valid : STD_LOGIC;
  signal \^out_valid_reg_0\ : STD_LOGIC;
  signal out_wr_en : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of cache_valid_i_1 : label is "soft_lutpair74";
  attribute SOFT_HLUTNM of out_valid_i_1 : label is "soft_lutpair74";
begin
  out_valid_reg_0 <= \^out_valid_reg_0\;
\Out_tmp[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => out_wr_en
    );
\Out_tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(0),
      Q => Q(0)
    );
\Out_tmp_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(10),
      Q => Q(10)
    );
\Out_tmp_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(11),
      Q => Q(11)
    );
\Out_tmp_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(12),
      Q => Q(12)
    );
\Out_tmp_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(13),
      Q => Q(13)
    );
\Out_tmp_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(14),
      Q => Q(14)
    );
\Out_tmp_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(15),
      Q => Q(15)
    );
\Out_tmp_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(16),
      Q => Q(16)
    );
\Out_tmp_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(17),
      Q => Q(17)
    );
\Out_tmp_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(18),
      Q => Q(18)
    );
\Out_tmp_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(19),
      Q => Q(19)
    );
\Out_tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(1),
      Q => Q(1)
    );
\Out_tmp_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(20),
      Q => Q(20)
    );
\Out_tmp_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(21),
      Q => Q(21)
    );
\Out_tmp_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(22),
      Q => Q(22)
    );
\Out_tmp_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(23),
      Q => Q(23)
    );
\Out_tmp_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(24),
      Q => Q(24)
    );
\Out_tmp_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(25),
      Q => Q(25)
    );
\Out_tmp_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(26),
      Q => Q(26)
    );
\Out_tmp_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(27),
      Q => Q(27)
    );
\Out_tmp_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(28),
      Q => Q(28)
    );
\Out_tmp_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(29),
      Q => Q(29)
    );
\Out_tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(2),
      Q => Q(2)
    );
\Out_tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(30),
      Q => Q(30)
    );
\Out_tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(3),
      Q => Q(3)
    );
\Out_tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(4),
      Q => Q(4)
    );
\Out_tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(5),
      Q => Q(5)
    );
\Out_tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(6),
      Q => Q(6)
    );
\Out_tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(7),
      Q => Q(7)
    );
\Out_tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(8),
      Q => Q(8)
    );
\Out_tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => AR(1),
      D => data_out_next(9),
      Q => Q(9)
    );
\cache_data[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A600"
    )
        port map (
      I0 => cache_valid,
      I1 => \^out_valid_reg_0\,
      I2 => fifo_valid_reg_0,
      I3 => fifo_valid,
      O => cache_wr_en
    );
\cache_data_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(0),
      Q => cache_data(0)
    );
\cache_data_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(10),
      Q => cache_data(10)
    );
\cache_data_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(11),
      Q => cache_data(11)
    );
\cache_data_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(12),
      Q => cache_data(12)
    );
\cache_data_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(13),
      Q => cache_data(13)
    );
\cache_data_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(14),
      Q => cache_data(14)
    );
\cache_data_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(15),
      Q => cache_data(15)
    );
\cache_data_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(16),
      Q => cache_data(16)
    );
\cache_data_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(17),
      Q => cache_data(17)
    );
\cache_data_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(18),
      Q => cache_data(18)
    );
\cache_data_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(19),
      Q => cache_data(19)
    );
\cache_data_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(1),
      Q => cache_data(1)
    );
\cache_data_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(20),
      Q => cache_data(20)
    );
\cache_data_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(21),
      Q => cache_data(21)
    );
\cache_data_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(22),
      Q => cache_data(22)
    );
\cache_data_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(23),
      Q => cache_data(23)
    );
\cache_data_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(24),
      Q => cache_data(24)
    );
\cache_data_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(25),
      Q => cache_data(25)
    );
\cache_data_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(26),
      Q => cache_data(26)
    );
\cache_data_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(1),
      D => fifo_data_out_unsigned(27),
      Q => cache_data(27)
    );
\cache_data_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(1),
      D => fifo_data_out_unsigned(28),
      Q => cache_data(28)
    );
\cache_data_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(1),
      D => fifo_data_out_unsigned(29),
      Q => cache_data(29)
    );
\cache_data_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(2),
      Q => cache_data(2)
    );
\cache_data_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(1),
      D => fifo_data_out_unsigned(30),
      Q => cache_data(30)
    );
\cache_data_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(3),
      Q => cache_data(3)
    );
\cache_data_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(4),
      Q => cache_data(4)
    );
\cache_data_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(5),
      Q => cache_data(5)
    );
\cache_data_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(6),
      Q => cache_data(6)
    );
\cache_data_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(7),
      Q => cache_data(7)
    );
\cache_data_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(8),
      Q => cache_data(8)
    );
\cache_data_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => fifo_data_out_unsigned(9),
      Q => cache_data(9)
    );
cache_valid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F220"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => Q_next
    );
cache_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => Q_next,
      Q => cache_valid
    );
fifo_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => Q_next_1,
      Q => fifo_valid
    );
out_valid_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => fifo_valid,
      I1 => cache_valid,
      I2 => fifo_valid_reg_0,
      I3 => \^out_valid_reg_0\,
      O => Q_next_2
    );
out_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(1),
      D => Q_next_2,
      Q => \^out_valid_reg_0\
    );
ram_reg_0_3_0_0_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      I2 => CO(0),
      O => In_rsvd
    );
\tlast_counter_out[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => fifo_valid_reg_0,
      O => out_valid_reg_1
    );
u_MultiplyB_ip_fifo_data_classic_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_classic
     port map (
      AR(1 downto 0) => AR(1 downto 0),
      AXI4_Stream_Slave_TDATA(31 downto 0) => AXI4_Stream_Slave_TDATA(31 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      D(30 downto 0) => data_out_next(30 downto 0),
      IPCORE_CLK => IPCORE_CLK,
      Q(30 downto 0) => cache_data(30 downto 0),
      Q_next_1 => Q_next_1,
      cache_valid => cache_valid,
      \data_int_reg[30]\(30 downto 0) => fifo_data_out_unsigned(30 downto 0),
      fifo_valid => fifo_valid,
      fifo_valid_reg => \^out_valid_reg_0\,
      fifo_valid_reg_0 => fifo_valid_reg_0
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT is
  port (
    out_valid_reg_0 : out STD_LOGIC;
    internal_ready : out STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    \fifo_sample_count_reg[2]\ : in STD_LOGIC;
    wr_din : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \Out_tmp_reg[0]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \fifo_back_indx_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT is
  signal Q_next : STD_LOGIC;
  signal Q_next_1 : STD_LOGIC;
  signal Q_next_2 : STD_LOGIC;
  signal \cache_data_reg_n_0_[0]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[10]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[11]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[12]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[13]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[14]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[15]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[16]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[17]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[18]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[19]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[1]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[20]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[21]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[22]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[23]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[24]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[25]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[26]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[27]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[28]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[29]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[2]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[30]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[31]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[3]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[4]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[5]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[6]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[7]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[8]\ : STD_LOGIC;
  signal \cache_data_reg_n_0_[9]\ : STD_LOGIC;
  signal cache_valid : STD_LOGIC;
  signal cache_wr_en : STD_LOGIC;
  signal fifo_valid : STD_LOGIC;
  signal \^out_valid_reg_0\ : STD_LOGIC;
  signal out_wr_en : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_10 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_11 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_12 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_13 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_14 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_15 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_16 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_17 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_18 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_19 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_2 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_20 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_21 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_22 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_23 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_24 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_25 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_26 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_27 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_28 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_29 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_3 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_30 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_31 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_32 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_33 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_34 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_35 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_36 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_37 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_38 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_39 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_4 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_40 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_41 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_42 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_43 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_44 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_45 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_46 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_47 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_48 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_49 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_5 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_50 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_51 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_52 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_53 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_54 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_55 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_56 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_57 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_58 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_59 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_6 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_60 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_61 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_62 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_63 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_64 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_65 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_7 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_8 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_9 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cache_valid_i_1__0\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \out_valid_i_1__0\ : label is "soft_lutpair39";
begin
  out_valid_reg_0 <= \^out_valid_reg_0\;
\Out_tmp[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DDD0"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => out_wr_en
    );
\Out_tmp_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_33,
      Q => AXI4_Stream_Master_TDATA(0)
    );
\Out_tmp_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_23,
      Q => AXI4_Stream_Master_TDATA(10)
    );
\Out_tmp_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_22,
      Q => AXI4_Stream_Master_TDATA(11)
    );
\Out_tmp_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_21,
      Q => AXI4_Stream_Master_TDATA(12)
    );
\Out_tmp_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_20,
      Q => AXI4_Stream_Master_TDATA(13)
    );
\Out_tmp_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_19,
      Q => AXI4_Stream_Master_TDATA(14)
    );
\Out_tmp_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_18,
      Q => AXI4_Stream_Master_TDATA(15)
    );
\Out_tmp_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_17,
      Q => AXI4_Stream_Master_TDATA(16)
    );
\Out_tmp_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_16,
      Q => AXI4_Stream_Master_TDATA(17)
    );
\Out_tmp_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_15,
      Q => AXI4_Stream_Master_TDATA(18)
    );
\Out_tmp_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_14,
      Q => AXI4_Stream_Master_TDATA(19)
    );
\Out_tmp_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_32,
      Q => AXI4_Stream_Master_TDATA(1)
    );
\Out_tmp_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_13,
      Q => AXI4_Stream_Master_TDATA(20)
    );
\Out_tmp_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_12,
      Q => AXI4_Stream_Master_TDATA(21)
    );
\Out_tmp_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_11,
      Q => AXI4_Stream_Master_TDATA(22)
    );
\Out_tmp_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_10,
      Q => AXI4_Stream_Master_TDATA(23)
    );
\Out_tmp_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_9,
      Q => AXI4_Stream_Master_TDATA(24)
    );
\Out_tmp_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_8,
      Q => AXI4_Stream_Master_TDATA(25)
    );
\Out_tmp_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_7,
      Q => AXI4_Stream_Master_TDATA(26)
    );
\Out_tmp_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_6,
      Q => AXI4_Stream_Master_TDATA(27)
    );
\Out_tmp_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_5,
      Q => AXI4_Stream_Master_TDATA(28)
    );
\Out_tmp_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_4,
      Q => AXI4_Stream_Master_TDATA(29)
    );
\Out_tmp_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_31,
      Q => AXI4_Stream_Master_TDATA(2)
    );
\Out_tmp_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_3,
      Q => AXI4_Stream_Master_TDATA(30)
    );
\Out_tmp_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_2,
      Q => AXI4_Stream_Master_TDATA(31)
    );
\Out_tmp_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_30,
      Q => AXI4_Stream_Master_TDATA(3)
    );
\Out_tmp_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_29,
      Q => AXI4_Stream_Master_TDATA(4)
    );
\Out_tmp_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_28,
      Q => AXI4_Stream_Master_TDATA(5)
    );
\Out_tmp_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_27,
      Q => AXI4_Stream_Master_TDATA(6)
    );
\Out_tmp_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_26,
      Q => AXI4_Stream_Master_TDATA(7)
    );
\Out_tmp_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_25,
      Q => AXI4_Stream_Master_TDATA(8)
    );
\Out_tmp_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => out_wr_en,
      CLR => \Out_tmp_reg[0]_0\(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_24,
      Q => AXI4_Stream_Master_TDATA(9)
    );
\cache_data[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"A600"
    )
        port map (
      I0 => cache_valid,
      I1 => \^out_valid_reg_0\,
      I2 => AXI4_Stream_Master_TREADY,
      I3 => fifo_valid,
      O => cache_wr_en
    );
\cache_data_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_65,
      Q => \cache_data_reg_n_0_[0]\
    );
\cache_data_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_55,
      Q => \cache_data_reg_n_0_[10]\
    );
\cache_data_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_54,
      Q => \cache_data_reg_n_0_[11]\
    );
\cache_data_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_53,
      Q => \cache_data_reg_n_0_[12]\
    );
\cache_data_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_52,
      Q => \cache_data_reg_n_0_[13]\
    );
\cache_data_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_51,
      Q => \cache_data_reg_n_0_[14]\
    );
\cache_data_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_50,
      Q => \cache_data_reg_n_0_[15]\
    );
\cache_data_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_49,
      Q => \cache_data_reg_n_0_[16]\
    );
\cache_data_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_48,
      Q => \cache_data_reg_n_0_[17]\
    );
\cache_data_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_47,
      Q => \cache_data_reg_n_0_[18]\
    );
\cache_data_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_46,
      Q => \cache_data_reg_n_0_[19]\
    );
\cache_data_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_64,
      Q => \cache_data_reg_n_0_[1]\
    );
\cache_data_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_45,
      Q => \cache_data_reg_n_0_[20]\
    );
\cache_data_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_44,
      Q => \cache_data_reg_n_0_[21]\
    );
\cache_data_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_43,
      Q => \cache_data_reg_n_0_[22]\
    );
\cache_data_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_42,
      Q => \cache_data_reg_n_0_[23]\
    );
\cache_data_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_41,
      Q => \cache_data_reg_n_0_[24]\
    );
\cache_data_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_40,
      Q => \cache_data_reg_n_0_[25]\
    );
\cache_data_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_39,
      Q => \cache_data_reg_n_0_[26]\
    );
\cache_data_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_38,
      Q => \cache_data_reg_n_0_[27]\
    );
\cache_data_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_37,
      Q => \cache_data_reg_n_0_[28]\
    );
\cache_data_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_36,
      Q => \cache_data_reg_n_0_[29]\
    );
\cache_data_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_63,
      Q => \cache_data_reg_n_0_[2]\
    );
\cache_data_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_35,
      Q => \cache_data_reg_n_0_[30]\
    );
\cache_data_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_34,
      Q => \cache_data_reg_n_0_[31]\
    );
\cache_data_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_62,
      Q => \cache_data_reg_n_0_[3]\
    );
\cache_data_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_61,
      Q => \cache_data_reg_n_0_[4]\
    );
\cache_data_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_60,
      Q => \cache_data_reg_n_0_[5]\
    );
\cache_data_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_59,
      Q => \cache_data_reg_n_0_[6]\
    );
\cache_data_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_58,
      Q => \cache_data_reg_n_0_[7]\
    );
\cache_data_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_57,
      Q => \cache_data_reg_n_0_[8]\
    );
\cache_data_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => cache_wr_en,
      CLR => AR(0),
      D => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_56,
      Q => \cache_data_reg_n_0_[9]\
    );
\cache_valid_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F220"
    )
        port map (
      I0 => \^out_valid_reg_0\,
      I1 => AXI4_Stream_Master_TREADY,
      I2 => cache_valid,
      I3 => fifo_valid,
      O => Q_next
    );
cache_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(0),
      D => Q_next,
      Q => cache_valid
    );
fifo_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(0),
      D => Q_next_1,
      Q => fifo_valid
    );
\out_valid_i_1__0\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"EFEE"
    )
        port map (
      I0 => fifo_valid,
      I1 => cache_valid,
      I2 => AXI4_Stream_Master_TREADY,
      I3 => \^out_valid_reg_0\,
      O => Q_next_2
    );
out_valid_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(0),
      D => Q_next_2,
      Q => \^out_valid_reg_0\
    );
u_MultiplyB_ip_fifo_data_OUT_classic_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT_classic
     port map (
      AR(0) => AR(0),
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      D(31) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_2,
      D(30) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_3,
      D(29) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_4,
      D(28) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_5,
      D(27) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_6,
      D(26) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_7,
      D(25) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_8,
      D(24) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_9,
      D(23) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_10,
      D(22) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_11,
      D(21) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_12,
      D(20) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_13,
      D(19) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_14,
      D(18) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_15,
      D(17) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_16,
      D(16) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_17,
      D(15) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_18,
      D(14) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_19,
      D(13) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_20,
      D(12) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_21,
      D(11) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_22,
      D(10) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_23,
      D(9) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_24,
      D(8) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_25,
      D(7) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_26,
      D(6) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_27,
      D(5) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_28,
      D(4) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_29,
      D(3) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_30,
      D(2) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_31,
      D(1) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_32,
      D(0) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_33,
      IPCORE_CLK => IPCORE_CLK,
      Q(31) => \cache_data_reg_n_0_[31]\,
      Q(30) => \cache_data_reg_n_0_[30]\,
      Q(29) => \cache_data_reg_n_0_[29]\,
      Q(28) => \cache_data_reg_n_0_[28]\,
      Q(27) => \cache_data_reg_n_0_[27]\,
      Q(26) => \cache_data_reg_n_0_[26]\,
      Q(25) => \cache_data_reg_n_0_[25]\,
      Q(24) => \cache_data_reg_n_0_[24]\,
      Q(23) => \cache_data_reg_n_0_[23]\,
      Q(22) => \cache_data_reg_n_0_[22]\,
      Q(21) => \cache_data_reg_n_0_[21]\,
      Q(20) => \cache_data_reg_n_0_[20]\,
      Q(19) => \cache_data_reg_n_0_[19]\,
      Q(18) => \cache_data_reg_n_0_[18]\,
      Q(17) => \cache_data_reg_n_0_[17]\,
      Q(16) => \cache_data_reg_n_0_[16]\,
      Q(15) => \cache_data_reg_n_0_[15]\,
      Q(14) => \cache_data_reg_n_0_[14]\,
      Q(13) => \cache_data_reg_n_0_[13]\,
      Q(12) => \cache_data_reg_n_0_[12]\,
      Q(11) => \cache_data_reg_n_0_[11]\,
      Q(10) => \cache_data_reg_n_0_[10]\,
      Q(9) => \cache_data_reg_n_0_[9]\,
      Q(8) => \cache_data_reg_n_0_[8]\,
      Q(7) => \cache_data_reg_n_0_[7]\,
      Q(6) => \cache_data_reg_n_0_[6]\,
      Q(5) => \cache_data_reg_n_0_[5]\,
      Q(4) => \cache_data_reg_n_0_[4]\,
      Q(3) => \cache_data_reg_n_0_[3]\,
      Q(2) => \cache_data_reg_n_0_[2]\,
      Q(1) => \cache_data_reg_n_0_[1]\,
      Q(0) => \cache_data_reg_n_0_[0]\,
      Q_next_1 => Q_next_1,
      cache_valid => cache_valid,
      \data_int_reg[31]\(31) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_34,
      \data_int_reg[31]\(30) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_35,
      \data_int_reg[31]\(29) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_36,
      \data_int_reg[31]\(28) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_37,
      \data_int_reg[31]\(27) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_38,
      \data_int_reg[31]\(26) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_39,
      \data_int_reg[31]\(25) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_40,
      \data_int_reg[31]\(24) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_41,
      \data_int_reg[31]\(23) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_42,
      \data_int_reg[31]\(22) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_43,
      \data_int_reg[31]\(21) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_44,
      \data_int_reg[31]\(20) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_45,
      \data_int_reg[31]\(19) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_46,
      \data_int_reg[31]\(18) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_47,
      \data_int_reg[31]\(17) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_48,
      \data_int_reg[31]\(16) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_49,
      \data_int_reg[31]\(15) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_50,
      \data_int_reg[31]\(14) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_51,
      \data_int_reg[31]\(13) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_52,
      \data_int_reg[31]\(12) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_53,
      \data_int_reg[31]\(11) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_54,
      \data_int_reg[31]\(10) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_55,
      \data_int_reg[31]\(9) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_56,
      \data_int_reg[31]\(8) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_57,
      \data_int_reg[31]\(7) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_58,
      \data_int_reg[31]\(6) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_59,
      \data_int_reg[31]\(5) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_60,
      \data_int_reg[31]\(4) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_61,
      \data_int_reg[31]\(3) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_62,
      \data_int_reg[31]\(2) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_63,
      \data_int_reg[31]\(1) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_64,
      \data_int_reg[31]\(0) => u_MultiplyB_ip_fifo_data_OUT_classic_inst_n_65,
      \fifo_back_indx_reg[0]_0\(0) => \fifo_back_indx_reg[0]\(0),
      \fifo_sample_count_reg[2]_0\ => \fifo_sample_count_reg[2]\,
      fifo_valid => fifo_valid,
      internal_ready => internal_ready,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => out_valid,
      w_d1_reg_0 => \^out_valid_reg_0\,
      wr_din(30 downto 0) => wr_din(30 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_master is
  port (
    out_valid_reg : out STD_LOGIC;
    CO : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    internal_ready : out STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 0 to 0 );
    \fifo_sample_count_reg[2]\ : in STD_LOGIC;
    \tlast_counter_out_reg[31]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 30 downto 0 );
    S : in STD_LOGIC_VECTOR ( 3 downto 0 );
    auto_tlast0_carry_i_3 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    auto_tlast0_carry_i_1 : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \auto_tlast0_carry__0_i_4\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \auto_tlast0_carry__0_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \auto_tlast0_carry__0_i_1\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \auto_tlast0_carry__1_i_3\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \auto_tlast0_carry__1_i_2\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    internal_ready_delayed : in STD_LOGIC;
    out_valid : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    wr_din : in STD_LOGIC_VECTOR ( 30 downto 0 );
    \Out_tmp_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    In_rsvd : in STD_LOGIC;
    \fifo_back_indx_reg[0]\ : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_master;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_master is
  signal \^co\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \auto_tlast0_carry__0_n_0\ : STD_LOGIC;
  signal \auto_tlast0_carry__0_n_1\ : STD_LOGIC;
  signal \auto_tlast0_carry__0_n_2\ : STD_LOGIC;
  signal \auto_tlast0_carry__0_n_3\ : STD_LOGIC;
  signal \auto_tlast0_carry__1_n_2\ : STD_LOGIC;
  signal \auto_tlast0_carry__1_n_3\ : STD_LOGIC;
  signal auto_tlast0_carry_n_0 : STD_LOGIC;
  signal auto_tlast0_carry_n_1 : STD_LOGIC;
  signal auto_tlast0_carry_n_2 : STD_LOGIC;
  signal auto_tlast0_carry_n_3 : STD_LOGIC;
  signal \tlast_counter_out[0]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_6_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[0]_i_7_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[12]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[16]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[20]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[24]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[28]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[4]_i_5_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_3_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_4_n_0\ : STD_LOGIC;
  signal \tlast_counter_out[8]_i_5_n_0\ : STD_LOGIC;
  signal tlast_counter_out_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \tlast_counter_out_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \tlast_counter_out_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal tlast_size_value : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \tlast_size_value_carry__0_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__0_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__0_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__0_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__1_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__2_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__3_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__4_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_0\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_1\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__5_n_3\ : STD_LOGIC;
  signal \tlast_size_value_carry__6_n_2\ : STD_LOGIC;
  signal \tlast_size_value_carry__6_n_3\ : STD_LOGIC;
  signal tlast_size_value_carry_n_0 : STD_LOGIC;
  signal tlast_size_value_carry_n_1 : STD_LOGIC;
  signal tlast_size_value_carry_n_2 : STD_LOGIC;
  signal tlast_size_value_carry_n_3 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_1 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_10 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_11 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_2 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_3 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_4 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_5 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_6 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_7 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_8 : STD_LOGIC;
  signal u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_9 : STD_LOGIC;
  signal NLW_auto_tlast0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_auto_tlast0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_auto_tlast0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_auto_tlast0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_tlast_size_value_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_tlast_size_value_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  CO(0) <= \^co\(0);
auto_tlast0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => auto_tlast0_carry_n_0,
      CO(2) => auto_tlast0_carry_n_1,
      CO(1) => auto_tlast0_carry_n_2,
      CO(0) => auto_tlast0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_auto_tlast0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_1,
      S(2) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_2,
      S(1) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_3,
      S(0) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_4
    );
\auto_tlast0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => auto_tlast0_carry_n_0,
      CO(3) => \auto_tlast0_carry__0_n_0\,
      CO(2) => \auto_tlast0_carry__0_n_1\,
      CO(1) => \auto_tlast0_carry__0_n_2\,
      CO(0) => \auto_tlast0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_auto_tlast0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_5,
      S(2) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_6,
      S(1) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_7,
      S(0) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_8
    );
\auto_tlast0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \auto_tlast0_carry__0_n_0\,
      CO(3) => \NLW_auto_tlast0_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \^co\(0),
      CO(1) => \auto_tlast0_carry__1_n_2\,
      CO(0) => \auto_tlast0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_auto_tlast0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_9,
      S(1) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_10,
      S(0) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_11
    );
\tlast_counter_out[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(0),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_3_n_0\
    );
\tlast_counter_out[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(3),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_4_n_0\
    );
\tlast_counter_out[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(2),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_5_n_0\
    );
\tlast_counter_out[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(1),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_6_n_0\
    );
\tlast_counter_out[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1555"
    )
        port map (
      I0 => tlast_counter_out_reg(0),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[0]_i_7_n_0\
    );
\tlast_counter_out[12]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(15),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_2_n_0\
    );
\tlast_counter_out[12]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(14),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_3_n_0\
    );
\tlast_counter_out[12]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(13),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_4_n_0\
    );
\tlast_counter_out[12]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(12),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[12]_i_5_n_0\
    );
\tlast_counter_out[16]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(19),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_2_n_0\
    );
\tlast_counter_out[16]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(18),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_3_n_0\
    );
\tlast_counter_out[16]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(17),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_4_n_0\
    );
\tlast_counter_out[16]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(16),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[16]_i_5_n_0\
    );
\tlast_counter_out[20]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(23),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_2_n_0\
    );
\tlast_counter_out[20]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(22),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_3_n_0\
    );
\tlast_counter_out[20]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(21),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_4_n_0\
    );
\tlast_counter_out[20]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(20),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[20]_i_5_n_0\
    );
\tlast_counter_out[24]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(27),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_2_n_0\
    );
\tlast_counter_out[24]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(26),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_3_n_0\
    );
\tlast_counter_out[24]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(25),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_4_n_0\
    );
\tlast_counter_out[24]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(24),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[24]_i_5_n_0\
    );
\tlast_counter_out[28]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(31),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_2_n_0\
    );
\tlast_counter_out[28]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(30),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_3_n_0\
    );
\tlast_counter_out[28]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(29),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_4_n_0\
    );
\tlast_counter_out[28]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(28),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[28]_i_5_n_0\
    );
\tlast_counter_out[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(7),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_2_n_0\
    );
\tlast_counter_out[4]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(6),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_3_n_0\
    );
\tlast_counter_out[4]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(5),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_4_n_0\
    );
\tlast_counter_out[4]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(4),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[4]_i_5_n_0\
    );
\tlast_counter_out[8]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(11),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_2_n_0\
    );
\tlast_counter_out[8]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(10),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_3_n_0\
    );
\tlast_counter_out[8]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(9),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_4_n_0\
    );
\tlast_counter_out[8]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2AAA"
    )
        port map (
      I0 => tlast_counter_out_reg(8),
      I1 => \^co\(0),
      I2 => internal_ready_delayed,
      I3 => out_valid,
      O => \tlast_counter_out[8]_i_5_n_0\
    );
\tlast_counter_out_reg[0]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[0]_i_2_n_7\,
      Q => tlast_counter_out_reg(0)
    );
\tlast_counter_out_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \tlast_counter_out_reg[0]_i_2_n_0\,
      CO(2) => \tlast_counter_out_reg[0]_i_2_n_1\,
      CO(1) => \tlast_counter_out_reg[0]_i_2_n_2\,
      CO(0) => \tlast_counter_out_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \tlast_counter_out[0]_i_3_n_0\,
      O(3) => \tlast_counter_out_reg[0]_i_2_n_4\,
      O(2) => \tlast_counter_out_reg[0]_i_2_n_5\,
      O(1) => \tlast_counter_out_reg[0]_i_2_n_6\,
      O(0) => \tlast_counter_out_reg[0]_i_2_n_7\,
      S(3) => \tlast_counter_out[0]_i_4_n_0\,
      S(2) => \tlast_counter_out[0]_i_5_n_0\,
      S(1) => \tlast_counter_out[0]_i_6_n_0\,
      S(0) => \tlast_counter_out[0]_i_7_n_0\
    );
\tlast_counter_out_reg[10]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[8]_i_1_n_5\,
      Q => tlast_counter_out_reg(10)
    );
\tlast_counter_out_reg[11]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[8]_i_1_n_4\,
      Q => tlast_counter_out_reg(11)
    );
\tlast_counter_out_reg[12]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[12]_i_1_n_7\,
      Q => tlast_counter_out_reg(12)
    );
\tlast_counter_out_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[8]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[12]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[12]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[12]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[12]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[12]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[12]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[12]_i_1_n_7\,
      S(3) => \tlast_counter_out[12]_i_2_n_0\,
      S(2) => \tlast_counter_out[12]_i_3_n_0\,
      S(1) => \tlast_counter_out[12]_i_4_n_0\,
      S(0) => \tlast_counter_out[12]_i_5_n_0\
    );
\tlast_counter_out_reg[13]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[12]_i_1_n_6\,
      Q => tlast_counter_out_reg(13)
    );
\tlast_counter_out_reg[14]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[12]_i_1_n_5\,
      Q => tlast_counter_out_reg(14)
    );
\tlast_counter_out_reg[15]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[12]_i_1_n_4\,
      Q => tlast_counter_out_reg(15)
    );
\tlast_counter_out_reg[16]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[16]_i_1_n_7\,
      Q => tlast_counter_out_reg(16)
    );
\tlast_counter_out_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[12]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[16]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[16]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[16]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[16]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[16]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[16]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[16]_i_1_n_7\,
      S(3) => \tlast_counter_out[16]_i_2_n_0\,
      S(2) => \tlast_counter_out[16]_i_3_n_0\,
      S(1) => \tlast_counter_out[16]_i_4_n_0\,
      S(0) => \tlast_counter_out[16]_i_5_n_0\
    );
\tlast_counter_out_reg[17]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[16]_i_1_n_6\,
      Q => tlast_counter_out_reg(17)
    );
\tlast_counter_out_reg[18]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[16]_i_1_n_5\,
      Q => tlast_counter_out_reg(18)
    );
\tlast_counter_out_reg[19]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[16]_i_1_n_4\,
      Q => tlast_counter_out_reg(19)
    );
\tlast_counter_out_reg[1]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[0]_i_2_n_6\,
      Q => tlast_counter_out_reg(1)
    );
\tlast_counter_out_reg[20]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[20]_i_1_n_7\,
      Q => tlast_counter_out_reg(20)
    );
\tlast_counter_out_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[16]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[20]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[20]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[20]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[20]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[20]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[20]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[20]_i_1_n_7\,
      S(3) => \tlast_counter_out[20]_i_2_n_0\,
      S(2) => \tlast_counter_out[20]_i_3_n_0\,
      S(1) => \tlast_counter_out[20]_i_4_n_0\,
      S(0) => \tlast_counter_out[20]_i_5_n_0\
    );
\tlast_counter_out_reg[21]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[20]_i_1_n_6\,
      Q => tlast_counter_out_reg(21)
    );
\tlast_counter_out_reg[22]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[20]_i_1_n_5\,
      Q => tlast_counter_out_reg(22)
    );
\tlast_counter_out_reg[23]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[20]_i_1_n_4\,
      Q => tlast_counter_out_reg(23)
    );
\tlast_counter_out_reg[24]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[24]_i_1_n_7\,
      Q => tlast_counter_out_reg(24)
    );
\tlast_counter_out_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[20]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[24]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[24]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[24]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[24]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[24]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[24]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[24]_i_1_n_7\,
      S(3) => \tlast_counter_out[24]_i_2_n_0\,
      S(2) => \tlast_counter_out[24]_i_3_n_0\,
      S(1) => \tlast_counter_out[24]_i_4_n_0\,
      S(0) => \tlast_counter_out[24]_i_5_n_0\
    );
\tlast_counter_out_reg[25]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[24]_i_1_n_6\,
      Q => tlast_counter_out_reg(25)
    );
\tlast_counter_out_reg[26]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[24]_i_1_n_5\,
      Q => tlast_counter_out_reg(26)
    );
\tlast_counter_out_reg[27]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[24]_i_1_n_4\,
      Q => tlast_counter_out_reg(27)
    );
\tlast_counter_out_reg[28]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[28]_i_1_n_7\,
      Q => tlast_counter_out_reg(28)
    );
\tlast_counter_out_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[24]_i_1_n_0\,
      CO(3) => \NLW_tlast_counter_out_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \tlast_counter_out_reg[28]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[28]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[28]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[28]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[28]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[28]_i_1_n_7\,
      S(3) => \tlast_counter_out[28]_i_2_n_0\,
      S(2) => \tlast_counter_out[28]_i_3_n_0\,
      S(1) => \tlast_counter_out[28]_i_4_n_0\,
      S(0) => \tlast_counter_out[28]_i_5_n_0\
    );
\tlast_counter_out_reg[29]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[28]_i_1_n_6\,
      Q => tlast_counter_out_reg(29)
    );
\tlast_counter_out_reg[2]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[0]_i_2_n_5\,
      Q => tlast_counter_out_reg(2)
    );
\tlast_counter_out_reg[30]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[28]_i_1_n_5\,
      Q => tlast_counter_out_reg(30)
    );
\tlast_counter_out_reg[31]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => AR(0),
      D => \tlast_counter_out_reg[28]_i_1_n_4\,
      Q => tlast_counter_out_reg(31)
    );
\tlast_counter_out_reg[3]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[0]_i_2_n_4\,
      Q => tlast_counter_out_reg(3)
    );
\tlast_counter_out_reg[4]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[4]_i_1_n_7\,
      Q => tlast_counter_out_reg(4)
    );
\tlast_counter_out_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[0]_i_2_n_0\,
      CO(3) => \tlast_counter_out_reg[4]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[4]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[4]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[4]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[4]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[4]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[4]_i_1_n_7\,
      S(3) => \tlast_counter_out[4]_i_2_n_0\,
      S(2) => \tlast_counter_out[4]_i_3_n_0\,
      S(1) => \tlast_counter_out[4]_i_4_n_0\,
      S(0) => \tlast_counter_out[4]_i_5_n_0\
    );
\tlast_counter_out_reg[5]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[4]_i_1_n_6\,
      Q => tlast_counter_out_reg(5)
    );
\tlast_counter_out_reg[6]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[4]_i_1_n_5\,
      Q => tlast_counter_out_reg(6)
    );
\tlast_counter_out_reg[7]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[4]_i_1_n_4\,
      Q => tlast_counter_out_reg(7)
    );
\tlast_counter_out_reg[8]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[8]_i_1_n_7\,
      Q => tlast_counter_out_reg(8)
    );
\tlast_counter_out_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_counter_out_reg[4]_i_1_n_0\,
      CO(3) => \tlast_counter_out_reg[8]_i_1_n_0\,
      CO(2) => \tlast_counter_out_reg[8]_i_1_n_1\,
      CO(1) => \tlast_counter_out_reg[8]_i_1_n_2\,
      CO(0) => \tlast_counter_out_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \tlast_counter_out_reg[8]_i_1_n_4\,
      O(2) => \tlast_counter_out_reg[8]_i_1_n_5\,
      O(1) => \tlast_counter_out_reg[8]_i_1_n_6\,
      O(0) => \tlast_counter_out_reg[8]_i_1_n_7\,
      S(3) => \tlast_counter_out[8]_i_2_n_0\,
      S(2) => \tlast_counter_out[8]_i_3_n_0\,
      S(1) => \tlast_counter_out[8]_i_4_n_0\,
      S(0) => \tlast_counter_out[8]_i_5_n_0\
    );
\tlast_counter_out_reg[9]\: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => \tlast_counter_out_reg[31]_0\,
      CLR => \fifo_sample_count_reg[2]\,
      D => \tlast_counter_out_reg[8]_i_1_n_6\,
      Q => tlast_counter_out_reg(9)
    );
tlast_size_value_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => tlast_size_value_carry_n_0,
      CO(2) => tlast_size_value_carry_n_1,
      CO(1) => tlast_size_value_carry_n_2,
      CO(0) => tlast_size_value_carry_n_3,
      CYINIT => Q(0),
      DI(3 downto 0) => Q(4 downto 1),
      O(3 downto 0) => tlast_size_value(4 downto 1),
      S(3 downto 0) => S(3 downto 0)
    );
\tlast_size_value_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => tlast_size_value_carry_n_0,
      CO(3) => \tlast_size_value_carry__0_n_0\,
      CO(2) => \tlast_size_value_carry__0_n_1\,
      CO(1) => \tlast_size_value_carry__0_n_2\,
      CO(0) => \tlast_size_value_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(8 downto 5),
      O(3 downto 0) => tlast_size_value(8 downto 5),
      S(3 downto 0) => auto_tlast0_carry_i_3(3 downto 0)
    );
\tlast_size_value_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__0_n_0\,
      CO(3) => \tlast_size_value_carry__1_n_0\,
      CO(2) => \tlast_size_value_carry__1_n_1\,
      CO(1) => \tlast_size_value_carry__1_n_2\,
      CO(0) => \tlast_size_value_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(12 downto 9),
      O(3 downto 0) => tlast_size_value(12 downto 9),
      S(3 downto 0) => auto_tlast0_carry_i_1(3 downto 0)
    );
\tlast_size_value_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__1_n_0\,
      CO(3) => \tlast_size_value_carry__2_n_0\,
      CO(2) => \tlast_size_value_carry__2_n_1\,
      CO(1) => \tlast_size_value_carry__2_n_2\,
      CO(0) => \tlast_size_value_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(16 downto 13),
      O(3 downto 0) => tlast_size_value(16 downto 13),
      S(3 downto 0) => \auto_tlast0_carry__0_i_4\(3 downto 0)
    );
\tlast_size_value_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__2_n_0\,
      CO(3) => \tlast_size_value_carry__3_n_0\,
      CO(2) => \tlast_size_value_carry__3_n_1\,
      CO(1) => \tlast_size_value_carry__3_n_2\,
      CO(0) => \tlast_size_value_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(20 downto 17),
      O(3 downto 0) => tlast_size_value(20 downto 17),
      S(3 downto 0) => \auto_tlast0_carry__0_i_3\(3 downto 0)
    );
\tlast_size_value_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__3_n_0\,
      CO(3) => \tlast_size_value_carry__4_n_0\,
      CO(2) => \tlast_size_value_carry__4_n_1\,
      CO(1) => \tlast_size_value_carry__4_n_2\,
      CO(0) => \tlast_size_value_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(24 downto 21),
      O(3 downto 0) => tlast_size_value(24 downto 21),
      S(3 downto 0) => \auto_tlast0_carry__0_i_1\(3 downto 0)
    );
\tlast_size_value_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__4_n_0\,
      CO(3) => \tlast_size_value_carry__5_n_0\,
      CO(2) => \tlast_size_value_carry__5_n_1\,
      CO(1) => \tlast_size_value_carry__5_n_2\,
      CO(0) => \tlast_size_value_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => Q(28 downto 25),
      O(3 downto 0) => tlast_size_value(28 downto 25),
      S(3 downto 0) => \auto_tlast0_carry__1_i_3\(3 downto 0)
    );
\tlast_size_value_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \tlast_size_value_carry__5_n_0\,
      CO(3 downto 2) => \NLW_tlast_size_value_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \tlast_size_value_carry__6_n_2\,
      CO(0) => \tlast_size_value_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => Q(30 downto 29),
      O(3) => \NLW_tlast_size_value_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => tlast_size_value(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => \auto_tlast0_carry__1_i_2\(2 downto 0)
    );
u_MultiplyB_ip_fifo_TLAST_OUT_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_TLAST_OUT
     port map (
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      IPCORE_CLK => IPCORE_CLK,
      In_rsvd => In_rsvd,
      Q(0) => Q(0),
      S(3) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_1,
      S(2) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_2,
      S(1) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_3,
      S(0) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_4,
      \fifo_back_indx_reg[0]\(0) => \fifo_back_indx_reg[0]\(0),
      \fifo_sample_count_reg[2]\ => \fifo_sample_count_reg[2]\,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => out_valid,
      tlast_counter_out_reg(31 downto 0) => tlast_counter_out_reg(31 downto 0),
      \tlast_counter_out_reg[21]\(3) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_5,
      \tlast_counter_out_reg[21]\(2) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_6,
      \tlast_counter_out_reg[21]\(1) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_7,
      \tlast_counter_out_reg[21]\(0) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_8,
      \tlast_counter_out_reg[30]\(2) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_9,
      \tlast_counter_out_reg[30]\(1) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_10,
      \tlast_counter_out_reg[30]\(0) => u_MultiplyB_ip_fifo_TLAST_OUT_inst_n_11,
      tlast_size_value(30 downto 0) => tlast_size_value(31 downto 1)
    );
u_MultiplyB_ip_fifo_data_OUT_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data_OUT
     port map (
      AR(0) => AR(0),
      AXI4_Stream_Master_TDATA(31 downto 0) => AXI4_Stream_Master_TDATA(31 downto 0),
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      IPCORE_CLK => IPCORE_CLK,
      \Out_tmp_reg[0]_0\(0) => \Out_tmp_reg[0]\(0),
      \fifo_back_indx_reg[0]\(0) => \fifo_back_indx_reg[0]\(0),
      \fifo_sample_count_reg[2]\ => \fifo_sample_count_reg[2]\,
      internal_ready => internal_ready,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => out_valid,
      out_valid_reg_0 => out_valid_reg,
      wr_din(30 downto 0) => wr_din(30 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_slave is
  port (
    out_valid : out STD_LOGIC;
    internal_ready_delayed : out STD_LOGIC;
    In_rsvd : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    out_valid_reg : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 30 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AR : in STD_LOGIC_VECTOR ( 1 downto 0 );
    internal_ready : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    CO : in STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_slave;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_slave is
  signal \^internal_ready_delayed\ : STD_LOGIC;
begin
  internal_ready_delayed <= \^internal_ready_delayed\;
fifo_rd_ack_reg: unisim.vcomponents.FDCE
     port map (
      C => IPCORE_CLK,
      CE => '1',
      CLR => AR(0),
      D => internal_ready,
      Q => \^internal_ready_delayed\
    );
u_MultiplyB_ip_fifo_data_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_fifo_data
     port map (
      AR(1 downto 0) => AR(1 downto 0),
      AXI4_Stream_Slave_TDATA(31 downto 0) => AXI4_Stream_Slave_TDATA(31 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      CO(0) => CO(0),
      IPCORE_CLK => IPCORE_CLK,
      In_rsvd => In_rsvd,
      Q(30 downto 0) => Q(30 downto 0),
      fifo_valid_reg_0 => \^internal_ready_delayed\,
      out_valid_reg_0 => out_valid,
      out_valid_reg_1 => out_valid_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip is
  port (
    AXI4_Lite_RVALID : out STD_LOGIC;
    AXI4_Lite_BVALID : out STD_LOGIC;
    AXI4_Lite_WREADY : out STD_LOGIC;
    out_valid_reg : out STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 0 to 0 );
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    IPCORE_CLK : in STD_LOGIC;
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 13 downto 0 );
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_ARESETN : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip is
  signal In_rsvd : STD_LOGIC;
  signal Out_tmp : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal internal_ready : STD_LOGIC;
  signal internal_ready_delayed : STD_LOGIC;
  signal relop_relop1 : STD_LOGIC;
  signal reset : STD_LOGIC;
  signal u_MultiplyB_ip_axi4_stream_slave_inst_n_4 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_3 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_37 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_38 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_39 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_4 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_40 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_41 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_42 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_43 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_44 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_45 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_46 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_47 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_48 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_49 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_5 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_50 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_51 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_52 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_53 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_54 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_55 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_56 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_57 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_58 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_59 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_60 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_61 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_62 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_63 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_64 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_65 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_66 : STD_LOGIC;
  signal u_MultiplyB_ip_axi_lite_inst_n_67 : STD_LOGIC;
  signal \u_MultiplyB_ip_fifo_data_inst/out_valid\ : STD_LOGIC;
  signal wr_din : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal write_reg_packet_size_axi4_stream_master : STD_LOGIC_VECTOR ( 30 downto 0 );
begin
u_MultiplyB_ip_axi4_stream_master_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_master
     port map (
      AR(0) => u_MultiplyB_ip_axi_lite_inst_n_67,
      AXI4_Stream_Master_TDATA(31 downto 0) => AXI4_Stream_Master_TDATA(31 downto 0),
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      CO(0) => relop_relop1,
      IPCORE_CLK => IPCORE_CLK,
      In_rsvd => In_rsvd,
      \Out_tmp_reg[0]\(0) => reset,
      Q(30 downto 0) => write_reg_packet_size_axi4_stream_master(30 downto 0),
      S(3) => u_MultiplyB_ip_axi_lite_inst_n_61,
      S(2) => u_MultiplyB_ip_axi_lite_inst_n_62,
      S(1) => u_MultiplyB_ip_axi_lite_inst_n_63,
      S(0) => u_MultiplyB_ip_axi_lite_inst_n_64,
      \auto_tlast0_carry__0_i_1\(3) => u_MultiplyB_ip_axi_lite_inst_n_41,
      \auto_tlast0_carry__0_i_1\(2) => u_MultiplyB_ip_axi_lite_inst_n_42,
      \auto_tlast0_carry__0_i_1\(1) => u_MultiplyB_ip_axi_lite_inst_n_43,
      \auto_tlast0_carry__0_i_1\(0) => u_MultiplyB_ip_axi_lite_inst_n_44,
      \auto_tlast0_carry__0_i_3\(3) => u_MultiplyB_ip_axi_lite_inst_n_45,
      \auto_tlast0_carry__0_i_3\(2) => u_MultiplyB_ip_axi_lite_inst_n_46,
      \auto_tlast0_carry__0_i_3\(1) => u_MultiplyB_ip_axi_lite_inst_n_47,
      \auto_tlast0_carry__0_i_3\(0) => u_MultiplyB_ip_axi_lite_inst_n_48,
      \auto_tlast0_carry__0_i_4\(3) => u_MultiplyB_ip_axi_lite_inst_n_49,
      \auto_tlast0_carry__0_i_4\(2) => u_MultiplyB_ip_axi_lite_inst_n_50,
      \auto_tlast0_carry__0_i_4\(1) => u_MultiplyB_ip_axi_lite_inst_n_51,
      \auto_tlast0_carry__0_i_4\(0) => u_MultiplyB_ip_axi_lite_inst_n_52,
      \auto_tlast0_carry__1_i_2\(2) => u_MultiplyB_ip_axi_lite_inst_n_3,
      \auto_tlast0_carry__1_i_2\(1) => u_MultiplyB_ip_axi_lite_inst_n_4,
      \auto_tlast0_carry__1_i_2\(0) => u_MultiplyB_ip_axi_lite_inst_n_5,
      \auto_tlast0_carry__1_i_3\(3) => u_MultiplyB_ip_axi_lite_inst_n_37,
      \auto_tlast0_carry__1_i_3\(2) => u_MultiplyB_ip_axi_lite_inst_n_38,
      \auto_tlast0_carry__1_i_3\(1) => u_MultiplyB_ip_axi_lite_inst_n_39,
      \auto_tlast0_carry__1_i_3\(0) => u_MultiplyB_ip_axi_lite_inst_n_40,
      auto_tlast0_carry_i_1(3) => u_MultiplyB_ip_axi_lite_inst_n_53,
      auto_tlast0_carry_i_1(2) => u_MultiplyB_ip_axi_lite_inst_n_54,
      auto_tlast0_carry_i_1(1) => u_MultiplyB_ip_axi_lite_inst_n_55,
      auto_tlast0_carry_i_1(0) => u_MultiplyB_ip_axi_lite_inst_n_56,
      auto_tlast0_carry_i_3(3) => u_MultiplyB_ip_axi_lite_inst_n_57,
      auto_tlast0_carry_i_3(2) => u_MultiplyB_ip_axi_lite_inst_n_58,
      auto_tlast0_carry_i_3(1) => u_MultiplyB_ip_axi_lite_inst_n_59,
      auto_tlast0_carry_i_3(0) => u_MultiplyB_ip_axi_lite_inst_n_60,
      \fifo_back_indx_reg[0]\(0) => u_MultiplyB_ip_axi_lite_inst_n_65,
      \fifo_sample_count_reg[2]\ => u_MultiplyB_ip_axi_lite_inst_n_66,
      internal_ready => internal_ready,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => \u_MultiplyB_ip_fifo_data_inst/out_valid\,
      out_valid_reg => out_valid_reg,
      \tlast_counter_out_reg[31]_0\ => u_MultiplyB_ip_axi4_stream_slave_inst_n_4,
      wr_din(30 downto 1) => wr_din(31 downto 2),
      wr_din(0) => Out_tmp(0)
    );
u_MultiplyB_ip_axi4_stream_slave_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi4_stream_slave
     port map (
      AR(1) => u_MultiplyB_ip_axi_lite_inst_n_65,
      AR(0) => u_MultiplyB_ip_axi_lite_inst_n_66,
      AXI4_Stream_Slave_TDATA(31 downto 0) => AXI4_Stream_Slave_TDATA(31 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      CO(0) => relop_relop1,
      IPCORE_CLK => IPCORE_CLK,
      In_rsvd => In_rsvd,
      Q(30 downto 0) => Out_tmp(30 downto 0),
      internal_ready => internal_ready,
      internal_ready_delayed => internal_ready_delayed,
      out_valid => \u_MultiplyB_ip_fifo_data_inst/out_valid\,
      out_valid_reg => u_MultiplyB_ip_axi4_stream_slave_inst_n_4
    );
u_MultiplyB_ip_axi_lite_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_axi_lite
     port map (
      AR(1) => u_MultiplyB_ip_axi_lite_inst_n_65,
      AR(0) => u_MultiplyB_ip_axi_lite_inst_n_66,
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(13 downto 0),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARESETN_0(0) => reset,
      AXI4_Lite_ARESETN_1(0) => u_MultiplyB_ip_axi_lite_inst_n_67,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(13 downto 0),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_RDATA(0) => AXI4_Lite_RDATA(0),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_WDATA(31 downto 0) => AXI4_Lite_WDATA(31 downto 0),
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      \FSM_onehot_axi_lite_wstate_reg[2]\(1) => AXI4_Lite_BVALID,
      \FSM_onehot_axi_lite_wstate_reg[2]\(0) => AXI4_Lite_WREADY,
      FSM_sequential_axi_lite_rstate_reg => AXI4_Lite_RVALID,
      IPCORE_RESETN => IPCORE_RESETN,
      Q(30 downto 0) => write_reg_packet_size_axi4_stream_master(30 downto 0),
      S(3) => u_MultiplyB_ip_axi_lite_inst_n_61,
      S(2) => u_MultiplyB_ip_axi_lite_inst_n_62,
      S(1) => u_MultiplyB_ip_axi_lite_inst_n_63,
      S(0) => u_MultiplyB_ip_axi_lite_inst_n_64,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(3) => u_MultiplyB_ip_axi_lite_inst_n_53,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(2) => u_MultiplyB_ip_axi_lite_inst_n_54,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(1) => u_MultiplyB_ip_axi_lite_inst_n_55,
      \write_reg_packet_size_axi4_stream_master_reg[12]\(0) => u_MultiplyB_ip_axi_lite_inst_n_56,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(3) => u_MultiplyB_ip_axi_lite_inst_n_49,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(2) => u_MultiplyB_ip_axi_lite_inst_n_50,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(1) => u_MultiplyB_ip_axi_lite_inst_n_51,
      \write_reg_packet_size_axi4_stream_master_reg[16]\(0) => u_MultiplyB_ip_axi_lite_inst_n_52,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(3) => u_MultiplyB_ip_axi_lite_inst_n_45,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(2) => u_MultiplyB_ip_axi_lite_inst_n_46,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(1) => u_MultiplyB_ip_axi_lite_inst_n_47,
      \write_reg_packet_size_axi4_stream_master_reg[20]\(0) => u_MultiplyB_ip_axi_lite_inst_n_48,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(3) => u_MultiplyB_ip_axi_lite_inst_n_41,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(2) => u_MultiplyB_ip_axi_lite_inst_n_42,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(1) => u_MultiplyB_ip_axi_lite_inst_n_43,
      \write_reg_packet_size_axi4_stream_master_reg[24]\(0) => u_MultiplyB_ip_axi_lite_inst_n_44,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(3) => u_MultiplyB_ip_axi_lite_inst_n_37,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(2) => u_MultiplyB_ip_axi_lite_inst_n_38,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(1) => u_MultiplyB_ip_axi_lite_inst_n_39,
      \write_reg_packet_size_axi4_stream_master_reg[28]\(0) => u_MultiplyB_ip_axi_lite_inst_n_40,
      \write_reg_packet_size_axi4_stream_master_reg[31]\(2) => u_MultiplyB_ip_axi_lite_inst_n_3,
      \write_reg_packet_size_axi4_stream_master_reg[31]\(1) => u_MultiplyB_ip_axi_lite_inst_n_4,
      \write_reg_packet_size_axi4_stream_master_reg[31]\(0) => u_MultiplyB_ip_axi_lite_inst_n_5,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(3) => u_MultiplyB_ip_axi_lite_inst_n_57,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(2) => u_MultiplyB_ip_axi_lite_inst_n_58,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(1) => u_MultiplyB_ip_axi_lite_inst_n_59,
      \write_reg_packet_size_axi4_stream_master_reg[8]\(0) => u_MultiplyB_ip_axi_lite_inst_n_60
    );
u_MultiplyB_ip_dut_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip_dut
     port map (
      Q(30 downto 0) => Out_tmp(30 downto 0),
      wr_din(29 downto 0) => wr_din(31 downto 2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    IPCORE_CLK : in STD_LOGIC;
    IPCORE_RESETN : in STD_LOGIC;
    AXI4_Stream_Master_TREADY : in STD_LOGIC;
    AXI4_Stream_Slave_TDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Stream_Slave_TVALID : in STD_LOGIC;
    AXI4_Lite_ACLK : in STD_LOGIC;
    AXI4_Lite_ARESETN : in STD_LOGIC;
    AXI4_Lite_AWADDR : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_AWVALID : in STD_LOGIC;
    AXI4_Lite_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    AXI4_Lite_WVALID : in STD_LOGIC;
    AXI4_Lite_BREADY : in STD_LOGIC;
    AXI4_Lite_ARADDR : in STD_LOGIC_VECTOR ( 15 downto 0 );
    AXI4_Lite_ARVALID : in STD_LOGIC;
    AXI4_Lite_RREADY : in STD_LOGIC;
    AXI4_Stream_Master_TDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Stream_Master_TVALID : out STD_LOGIC;
    AXI4_Stream_Master_TLAST : out STD_LOGIC;
    AXI4_Stream_Slave_TREADY : out STD_LOGIC;
    AXI4_Lite_AWREADY : out STD_LOGIC;
    AXI4_Lite_WREADY : out STD_LOGIC;
    AXI4_Lite_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_BVALID : out STD_LOGIC;
    AXI4_Lite_ARREADY : out STD_LOGIC;
    AXI4_Lite_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    AXI4_Lite_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    AXI4_Lite_RVALID : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "System_MultiplyB_ip_0_0,MultiplyB_ip,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "MultiplyB_ip,Vivado 2018.3";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \^axi4_lite_rdata\ : STD_LOGIC_VECTOR ( 29 to 29 );
  attribute x_interface_info : string;
  attribute x_interface_info of AXI4_Lite_ACLK : signal is "xilinx.com:signal:clock:1.0 AXI4_Lite_signal_clock CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of AXI4_Lite_ACLK : signal is "XIL_INTERFACENAME AXI4_Lite_signal_clock, ASSOCIATED_BUSIF AXI4_Lite, ASSOCIATED_RESET AXI4_Lite_ARESETN, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_ARESETN : signal is "xilinx.com:signal:reset:1.0 AXI4_Lite_signal_reset RST";
  attribute x_interface_parameter of AXI4_Lite_ARESETN : signal is "XIL_INTERFACENAME AXI4_Lite_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_ARREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARREADY";
  attribute x_interface_info of AXI4_Lite_ARVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARVALID";
  attribute x_interface_info of AXI4_Lite_AWREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWREADY";
  attribute x_interface_info of AXI4_Lite_AWVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWVALID";
  attribute x_interface_info of AXI4_Lite_BREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BREADY";
  attribute x_interface_info of AXI4_Lite_BVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BVALID";
  attribute x_interface_info of AXI4_Lite_RREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RREADY";
  attribute x_interface_info of AXI4_Lite_RVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RVALID";
  attribute x_interface_info of AXI4_Lite_WREADY : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WREADY";
  attribute x_interface_info of AXI4_Lite_WVALID : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WVALID";
  attribute x_interface_info of AXI4_Stream_Master_TLAST : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TLAST";
  attribute x_interface_info of AXI4_Stream_Master_TREADY : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TREADY";
  attribute x_interface_parameter of AXI4_Stream_Master_TREADY : signal is "XIL_INTERFACENAME AXI4_Stream_Master, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Stream_Master_TVALID : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TVALID";
  attribute x_interface_info of AXI4_Stream_Slave_TREADY : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TREADY";
  attribute x_interface_info of AXI4_Stream_Slave_TVALID : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TVALID";
  attribute x_interface_info of IPCORE_CLK : signal is "xilinx.com:signal:clock:1.0 IPCORE_CLK CLK";
  attribute x_interface_parameter of IPCORE_CLK : signal is "XIL_INTERFACENAME IPCORE_CLK, ASSOCIATED_RESET IPCORE_RESETN, ASSOCIATED_BUSIF AXI4_Stream_Master:AXI4_Stream_Slave, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0";
  attribute x_interface_info of IPCORE_RESETN : signal is "xilinx.com:signal:reset:1.0 IPCORE_RESETN RST";
  attribute x_interface_parameter of IPCORE_RESETN : signal is "XIL_INTERFACENAME IPCORE_RESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_ARADDR : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite ARADDR";
  attribute x_interface_info of AXI4_Lite_AWADDR : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite AWADDR";
  attribute x_interface_parameter of AXI4_Lite_AWADDR : signal is "XIL_INTERFACENAME AXI4_Lite, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 16, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute x_interface_info of AXI4_Lite_BRESP : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite BRESP";
  attribute x_interface_info of AXI4_Lite_RDATA : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RDATA";
  attribute x_interface_info of AXI4_Lite_RRESP : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite RRESP";
  attribute x_interface_info of AXI4_Lite_WDATA : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WDATA";
  attribute x_interface_info of AXI4_Lite_WSTRB : signal is "xilinx.com:interface:aximm:1.0 AXI4_Lite WSTRB";
  attribute x_interface_info of AXI4_Stream_Master_TDATA : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Master TDATA";
  attribute x_interface_info of AXI4_Stream_Slave_TDATA : signal is "xilinx.com:interface:axis:1.0 AXI4_Stream_Slave TDATA";
  attribute x_interface_parameter of AXI4_Stream_Slave_TDATA : signal is "XIL_INTERFACENAME AXI4_Stream_Slave, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 0, HAS_TLAST 0, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN System_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0";
begin
  AXI4_Lite_BRESP(1) <= \<const0>\;
  AXI4_Lite_BRESP(0) <= \<const0>\;
  AXI4_Lite_RDATA(31) <= \<const0>\;
  AXI4_Lite_RDATA(30) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(29) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(28) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(27) <= \<const0>\;
  AXI4_Lite_RDATA(26) <= \<const0>\;
  AXI4_Lite_RDATA(25) <= \<const0>\;
  AXI4_Lite_RDATA(24) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(23) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(22) <= \<const0>\;
  AXI4_Lite_RDATA(21) <= \<const0>\;
  AXI4_Lite_RDATA(20) <= \<const0>\;
  AXI4_Lite_RDATA(19) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(18) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(17) <= \<const0>\;
  AXI4_Lite_RDATA(16) <= \<const0>\;
  AXI4_Lite_RDATA(15) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(14) <= \<const0>\;
  AXI4_Lite_RDATA(13) <= \<const0>\;
  AXI4_Lite_RDATA(12) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(11) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(10) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(9) <= \<const0>\;
  AXI4_Lite_RDATA(8) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(7) <= \<const0>\;
  AXI4_Lite_RDATA(6) <= \<const0>\;
  AXI4_Lite_RDATA(5) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(4) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(3) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(2) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RDATA(1) <= \<const0>\;
  AXI4_Lite_RDATA(0) <= \^axi4_lite_rdata\(29);
  AXI4_Lite_RRESP(1) <= \<const0>\;
  AXI4_Lite_RRESP(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_MultiplyB_ip
     port map (
      AXI4_Lite_ACLK => AXI4_Lite_ACLK,
      AXI4_Lite_ARADDR(13 downto 0) => AXI4_Lite_ARADDR(15 downto 2),
      AXI4_Lite_ARESETN => AXI4_Lite_ARESETN,
      AXI4_Lite_ARREADY => AXI4_Lite_ARREADY,
      AXI4_Lite_ARVALID => AXI4_Lite_ARVALID,
      AXI4_Lite_AWADDR(13 downto 0) => AXI4_Lite_AWADDR(15 downto 2),
      AXI4_Lite_AWREADY => AXI4_Lite_AWREADY,
      AXI4_Lite_AWVALID => AXI4_Lite_AWVALID,
      AXI4_Lite_BREADY => AXI4_Lite_BREADY,
      AXI4_Lite_BVALID => AXI4_Lite_BVALID,
      AXI4_Lite_RDATA(0) => \^axi4_lite_rdata\(29),
      AXI4_Lite_RREADY => AXI4_Lite_RREADY,
      AXI4_Lite_RVALID => AXI4_Lite_RVALID,
      AXI4_Lite_WDATA(31 downto 0) => AXI4_Lite_WDATA(31 downto 0),
      AXI4_Lite_WREADY => AXI4_Lite_WREADY,
      AXI4_Lite_WSTRB(3 downto 0) => AXI4_Lite_WSTRB(3 downto 0),
      AXI4_Lite_WVALID => AXI4_Lite_WVALID,
      AXI4_Stream_Master_TDATA(31 downto 0) => AXI4_Stream_Master_TDATA(31 downto 0),
      AXI4_Stream_Master_TLAST => AXI4_Stream_Master_TLAST,
      AXI4_Stream_Master_TREADY => AXI4_Stream_Master_TREADY,
      AXI4_Stream_Slave_TDATA(31 downto 0) => AXI4_Stream_Slave_TDATA(31 downto 0),
      AXI4_Stream_Slave_TREADY => AXI4_Stream_Slave_TREADY,
      AXI4_Stream_Slave_TVALID => AXI4_Stream_Slave_TVALID,
      IPCORE_CLK => IPCORE_CLK,
      IPCORE_RESETN => IPCORE_RESETN,
      out_valid_reg => AXI4_Stream_Master_TVALID
    );
end STRUCTURE;
