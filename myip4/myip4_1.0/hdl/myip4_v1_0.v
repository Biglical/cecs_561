
`timescale 1 ns / 1 ps

	module myip4_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface SAXI
		parameter integer C_SAXI_DATA_WIDTH	= 32,
		parameter integer C_SAXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface SAXI
		input wire  saxi_aclk,
		input wire  saxi_aresetn,
		input wire [C_SAXI_ADDR_WIDTH-1 : 0] saxi_awaddr,
		input wire [2 : 0] saxi_awprot,
		input wire  saxi_awvalid,
		output wire  saxi_awready,
		input wire [C_SAXI_DATA_WIDTH-1 : 0] saxi_wdata,
		input wire [(C_SAXI_DATA_WIDTH/8)-1 : 0] saxi_wstrb,
		input wire  saxi_wvalid,
		output wire  saxi_wready,
		output wire [1 : 0] saxi_bresp,
		output wire  saxi_bvalid,
		input wire  saxi_bready,
		input wire [C_SAXI_ADDR_WIDTH-1 : 0] saxi_araddr,
		input wire [2 : 0] saxi_arprot,
		input wire  saxi_arvalid,
		output wire  saxi_arready,
		output wire [C_SAXI_DATA_WIDTH-1 : 0] saxi_rdata,
		output wire [1 : 0] saxi_rresp,
		output wire  saxi_rvalid,
		input wire  saxi_rready
	);
// Instantiation of Axi Bus Interface SAXI
	myip4_v1_0_SAXI # ( 
		.C_S_AXI_DATA_WIDTH(C_SAXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_SAXI_ADDR_WIDTH)
	) myip4_v1_0_SAXI_inst (
		.S_AXI_ACLK(saxi_aclk),
		.S_AXI_ARESETN(saxi_aresetn),
		.S_AXI_AWADDR(saxi_awaddr),
		.S_AXI_AWPROT(saxi_awprot),
		.S_AXI_AWVALID(saxi_awvalid),
		.S_AXI_AWREADY(saxi_awready),
		.S_AXI_WDATA(saxi_wdata),
		.S_AXI_WSTRB(saxi_wstrb),
		.S_AXI_WVALID(saxi_wvalid),
		.S_AXI_WREADY(saxi_wready),
		.S_AXI_BRESP(saxi_bresp),
		.S_AXI_BVALID(saxi_bvalid),
		.S_AXI_BREADY(saxi_bready),
		.S_AXI_ARADDR(saxi_araddr),
		.S_AXI_ARPROT(saxi_arprot),
		.S_AXI_ARVALID(saxi_arvalid),
		.S_AXI_ARREADY(saxi_arready),
		.S_AXI_RDATA(saxi_rdata),
		.S_AXI_RRESP(saxi_rresp),
		.S_AXI_RVALID(saxi_rvalid),
		.S_AXI_RREADY(saxi_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
